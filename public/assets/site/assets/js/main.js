function touchMoveOff(e){
    e.preventDefault()
}

function navMenuActive(){
    $("#open-menu").hide()
    $("#open-menu").removeClass("nav-mobile-menu-button")
    $("#close-menu").show()
    $(".mobile-nav-section").show()

    //window.scrollTo(0, 0);
    var navMobile = document.getElementById("mobile-nav-block");
    TweenLite.to(navMobile, 0.5, {right: 0, ease:Power2.easeInOut});
    $("html").css("overflow-y", "hidden")
    //$("#nav-mobile").css("position", "fixed")
    //$("#nav-mobile").css("z-index", "999")

    window.addEventListener('touchmove', touchMoveOff, { passive: false });
}

function navMenuDisactive(){
    $("#open-menu").show()
    $("#open-menu").addClass("nav-mobile-menu-button")
    $("#close-menu").hide()

    var navMobile = document.getElementById("mobile-nav-block");
    var right = window.innerWidth > 478 ? -375 : -window.innerWidth
    TweenLite.to(navMobile, 0.5, {right: right, ease:Power2.easeInOut, onComplete: function(){$(".mobile-nav-section").hide()} });
    $("html").css("overflow-y", "auto")
    //$("#nav-mobile").css("position", "unset")
    //$("#nav-mobile").css("z-index", "auto")

    window.removeEventListener('touchmove', touchMoveOff, { passive: false });
}


$(window).on("load", function(){

    $("#teams-tab").tab('show')

    $("#all-country").tab('show')

    $("#members-tab").tab('show')

    $('.select2-football-1').select2();

    $(".langs-block__selected").on("click", function(){
        if($(".langs-block__lang-list").is(":visible")) $(".langs-block__lang-list").hide()
        else $(".langs-block__lang-list").show()
    })

    $(document).delegate(".select2-section-football-1", "click", function(){
        $(".select2-results__options").find("[aria-selected='true']").css("background-color", "#F6F6F6")
        $(".select2-results__options").find("[aria-selected='true']").css("color", "#000")
    })

    $("#open-menu").on("click", navMenuActive)
    $("#close-menu").on("click", navMenuDisactive)

    if($("#menu").is(":visible")){
        $(".search-input").css("width", $("#menu").width())
        $(".search-input").css("margin-right", "65px")
    }
    else{
        $(".search-input").css("width", "calc(100% - "+($("#icons-block").width() + 20) +"px)")
    }

    //$(".search-input").css("width", $("#menu").is(":visible") ? $("#menu").width() : "calc(100% - "+$("#icons-block").width()+"px)")
    $("#open-search").on("click", function(){
        $("#menu").hide()
        $(".search-input").show()

        $("#open-search").hide()
        $("#close-search").show()
    })

    $("#close-search").on("click", function(){
        $("#menu").show()
        $(".search-input").hide()

        $("#open-search").show()
        $("#close-search").hide()
    })

})