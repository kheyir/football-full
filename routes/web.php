<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/foot/{path?}', 'angular_admin')
    ->where('path', '^(?!api).*$')
    ->name('angularAdmin');

    Route::get('/{lang?}', ['uses'=>'Site\WebController@home', 'as'=>'angularClient'])->where('lang', '(az|ru)');

    Route::get('/404', ['uses'=>'Site\WebController@page404']);

    Route::get('/{lang}/news', ['uses'=>'Site\WebController@news'])->where('lang', '(az|ru)');

    Route::get('/{lang}/news/search/{searchWord}', ['uses'=>'Site\WebController@newsSearch'])->where('lang', '(az|ru)');

    Route::get('/{lang}/news/country/{typeSlug}', ['uses'=>'Site\WebController@newsCountry'])->where('lang', '(az|ru)');

    Route::get('/{lang}/news/category/{typeSlug}', ['uses'=>'Site\WebController@newsCategory'])->where('lang', '(az|ru)');

    Route::get('/{lang}/news/tournament/{typeSlug}', ['uses'=>'Site\WebController@newsTournament'])->where('lang', '(az|ru)');

    Route::get('/{lang}/news-item/{slug}', ['uses'=>'Site\WebController@newsItem'])->where('lang', '(az|ru)');

    Route::get('/{lang}/tournament/{slug}', ['uses'=>'Site\WebController@tournament'])->where('lang', '(az|ru)');

    Route::get('/{lang}/tournaments', ['uses'=>'Site\WebController@tournaments'])->where('lang', '(az|ru)');

    Route::get('/{lang}/team/{slug}', ['uses'=>'Site\WebController@team'])->where('lang', '(az|ru)');

    Route::get('/{lang}/videos', ['uses'=>'Site\WebController@videos'])->where('lang', '(az|ru)');

    Route::get('/{lang}/review', ['uses'=>'Site\WebController@review'])->where('lang', '(az|ru)');

    Route::get('/{lang}/advertising', ['uses'=>'Site\WebController@advertising'])->where('lang', '(az|ru)');

    Route::get('/{lang}/about', ['uses'=>'Site\WebController@about'])->where('lang', '(az|ru)');

    Route::get('/{path?}', ['uses'=>'Site\WebController@page404'])->where('path', '^(?!api).*$');

    // Route::get('/{path?}', ['uses'=>'Site\WebController@index', 'as'=>'angularClient'])->where('path', '^(?!api).*$');

// Route::view('/{path?}', 'angular_client')
//     ->where('path', '^(?!api).*$')
//     ->name('angularClient');
