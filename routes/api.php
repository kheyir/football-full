<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Site'], function(){
    Route::get('{lang}/home/get-home-items', ['uses'=>'HomeController@getHomeItems', 'as'=>'itemsGetHome'])->where(['lang'=>'[a-z]{2}']);

    Route::get('{lang}/news/get-home', ['uses'=>'NewsController@getSlidersNews', 'as'=>'newsGetHome'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/news/get-latest', ['uses'=>'NewsController@getLatest', 'as'=>'newsGetLatest'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/news/get-tops', ['uses'=>'NewsController@getTops', 'as'=>'newsGetTops'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/news/get-last-right-slide', ['uses'=>'NewsController@getLastRightSlide', 'as'=>'newsGetLastRightSlide'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/news/get-by-featured-categories', ['uses'=>'NewsController@getNewsByAllFeaturedCategories', 'as'=>'newsGetByAllFeaturedCategories'])->where(['lang'=>'[a-z]{2}']);

    Route::get('{lang}/news/get-all/{page?}', ['uses'=>'NewsController@getAll', 'as'=>'newsGetAll'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);


    Route::get('{lang}/news/get-news-by-country/{slug}/{page?}', ['uses'=>'NewsController@getNewsByCountry', 'as'=>'newsGetByCountry'])->where(['lang'=>'[a-z]{2}', 'page'=>'[a-zA-Z0-9-]+']);
    Route::get('{lang}/news/get-news-by-category/{slug}/{page?}', ['uses'=>'NewsController@getNewsByCategory', 'as'=>'newsGetByCategory'])->where(['lang'=>'[a-z]{2}', 'page'=>'[a-zA-Z0-9-]+']);
    Route::get('{lang}/news/get-news-by-tournament/{slug}/{page?}', ['uses'=>'NewsController@getNewsByTournament', 'as'=>'newsGetByTournament'])->where(['lang'=>'[a-z]{2}', 'page'=>'[a-zA-Z0-9-]+']);
    Route::get('{lang}/news/get-news-by-season/{id}/{page?}', ['uses'=>'NewsController@getNewsBySeason', 'as'=>'newsGetBySeason'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);
    Route::get('{lang}/news/get-news-by-team/{id}/{page?}', ['uses'=>'NewsController@getNewsByTeam', 'as'=>'newsGetByTeam'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);


    Route::get('{lang}/news-item/{slug}', ['uses'=>'NewsController@getNewsItem', 'as'=>'newsGetNewsItem'])->where(['lang'=>'[a-z]{2}', 'id'=>'[a-zA-Z0-9-]+']);


    Route::get('{lang}/categories/get', ['uses'=>'CategoriesController@getCategories', 'as'=>'categoriesGet'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/categories/get-full-types', ['uses'=>'CategoriesController@getFullTypes', 'as'=>'typesGetFull'])->where(['lang'=>'[a-z]{2}']);


    Route::get('{lang}/countries/get-all-for-head', ['uses'=>'CountriesController@getAllForHead', 'as'=>'countriesGetAllForHead'])->where(['lang'=>'[a-z]{2}']);

    Route::get('{lang}/countries/get-all', ['uses'=>'CountriesController@getAll', 'as'=>'countriesGetAll'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/tournaments/get-all', ['uses'=>'TournamentsController@getAll', 'as'=>'tournamentsGetAll'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/categories/get-all', ['uses'=>'CategoriesController@getAll', 'as'=>'categoriesGetAll'])->where(['lang'=>'[a-z]{2}']);

    Route::get('{lang}/tournaments/get-tournaments-is-news-category', ['uses'=>'TournamentsController@getTournamentsIsNewsCategory', 'as'=>'tournamentsGetIsNewsCategory'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/tournaments/get-tournaments-is-videos-category', ['uses'=>'TournamentsController@getTournamentsIsVideosCategory', 'as'=>'tournamentsGetIsNewsCategory'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/tournament/get-tournament-by-slug/{slug}', ['uses'=>'TournamentsController@getTournamentBySlug'])->where(['lang'=>'[a-z]{2}', 'id'=>'[a-zA-Z0-9-]+']);
    Route::get('{lang}/tournaments/get-global-tournaments', ['uses'=>'TournamentsController@getGlobalTournaments', 'as'=>'tournamentsGetGlobal'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/tournaments/get-tournaments-priority-for-country', ['uses'=>'TournamentsController@getTournamentsPriorityForCountry', 'as'=>'tournamentsGetPriorityCountry'])->where(['lang'=>'[a-z]{2}']);
    Route::get('{lang}/tournaments/all', ['uses'=>'TournamentsController@getAll', 'as'=>'tournamentsGetAll'])->where(['lang'=>'[a-z]{2}']);


    Route::get('{lang}/seasons/all', ['uses'=>'SeasonsController@getAll', 'as'=>'seasonsGetAll'])->where(['lang'=>'[a-z]{2}']);


    Route::get('{lang}/stages/get-stages-by-season/{id}', ['uses'=>'StagesController@getStagesBySeason', 'as'=>'stagesGetBySeason'])->where(['lang'=>'[a-z]{2}', 'id'=>'[0-9]+']);


    Route::get('{lang}/elements/get-elements-by-season/{id}', ['uses'=>'ElementsController@getElementsBySeason', 'as'=>'elementsGetBySeason'])->where(['lang'=>'[a-z]{2}', 'id'=>'[0-9]+']);


    Route::get('{lang}/teams/get-teams-by-season/{id}', ['uses'=>'TeamsController@getTeamsBySeason', 'as'=>'teamsGetBySeason'])->where(['lang'=>'[a-z]{2}', 'id'=>'[0-9]+']);
    Route::get('{lang}/team/get-team-by-slug/{slug}', ['uses'=>'TeamsController@getTeamBySlug', 'as'=>'teamGetById'])->where(['lang'=>'[a-z]{2}', 'id'=>'[a-zA-Z0-9-]+']);
    Route::get('{lang}/teams/get-teams-that-have-videos/{page?}', ['uses'=>'TeamsController@getTeams'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);


    Route::get('{lang}/players/get-players-by-team/{id}', ['uses'=>'PlayersController@getPlayersByTeam', 'as'=>'playersGetByTeam'])->where(['lang'=>'[a-z]{2}', 'id'=>'[0-9]+']);
    Route::get('{lang}/players/get-players-that-have-videos/{page?}', ['uses'=>'PlayersController@getPlayersThatHaveVideo'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);


    Route::get('{lang}/groups/get-tournament-sidebar-groups-by-season/{seasonId}', ['uses'=>'GroupsController@getSidebarGroupsBySeason', 'as'=>'groupsGetTournamentSidebar'])->where(['lang'=>'[a-z]{2}', 'seasonId'=>'[0-9]+']);
    Route::get('{lang}/groups/get-tournament-sidebar-for-home', ['uses'=>'GroupsController@getSidebarTournamentsForHome', 'as'=>'groupsGetTournamentSidebarForHome'])->where(['lang'=>'[a-z]{2}']);

    Route::get('{lang}/matches/get-matches-by-season/{id}', ['uses'=>'MatchesController@getMatchesBySeason', 'as'=>'matchesGetBySeason'])->where(['lang'=>'[a-z]{2}', 'id'=>'[0-9]+']);
    Route::get('{lang}/matches/get-matches-by-season-and-stage/{seasonId}/{stageId}', ['uses'=>'MatchesController@getMatchesBySeasonAndStage', 'as'=>'matchesGetBySeason'])->where(['lang'=>'[a-z]{2}', 'seasonId'=>'[0-9]+', 'stageId'=>'[0-9]+']);
    Route::get('{lang}/matches/get-matches-by-team/{teamId}/{page}', ['uses'=>'MatchesController@getMatchesByTeam', 'as'=>'matchesGetByTeam'])->where(['lang'=>'[a-z]{2}', 'teamId'=>'[0-9]+', 'page'=>'[0-9]+']);
    Route::get('{lang}/matches/get-matches-by-team-with-videos/{teamId}/{page}', ['uses'=>'MatchesController@getMatchesByTeamWithVideos', 'as'=>'matchesGetByTeamWith'])->where(['lang'=>'[a-z]{2}', 'teamId'=>'[0-9]+', 'page'=>'[0-9]+']);
    Route::get('{lang}/matches/get-matches/{page?}', ['uses'=>'MatchesController@getMatches', 'as'=>'matchesGet'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);
    Route::get('{lang}/matches/get-matches-that-have-videos/{page?}', ['uses'=>'MatchesController@getMatchesThatHaveVideo'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);

    Route::get('{lang}/videos/get-top-by-tournament/{id}', ['uses'=>'VideosController@getTopVideosByTournament', 'as'=>'videosGetTopByTournament'])->where(['lang'=>'[a-z]{2}', 'id'=>'[0-9]+']);
    Route::get('{lang}/videos/get-videos-by-team-and-match/{teamId}/{matchId}', ['uses'=>'VideosController@getVideosByTeamAndMatch', 'as'=>'videosGetByTeamAndMatch'])->where(['lang'=>'[a-z]{2}', 'teamId'=>'[0-9]+', 'matchId'=>'[0-9]+']);
    Route::get('{lang}/videos/get-videos/{page?}', ['uses'=>'VideosController@getVideos'])->where(['lang'=>'[a-z]{2}', 'page'=>'[0-9]+']);


    Route::get('{lang}/page/{slug}', ['uses'=>'PagesController@getPage', 'as'=>'pageGetPageItem'])->where(['lang'=>'[a-z]{2}']);

});

Route::group(['namespace'=>'Admin', 'prefix' => 'foot'], function(){

    Route::post('/login', ['uses'=>'AuthController@login', 'as'=>'adminLogin']);

    Route::group(['middleware'=>['IsAuthAsAdmin']], function(){
        Route::get('/get-auth', ['uses'=>'AuthController@getAuth', 'as'=>'adminGetAuth']);
        Route::get('/auth-check', ['uses'=>'AuthController@authCheck', 'as'=>'adminAuthCheck']);
        Route::get('/logout', ['uses'=>'AuthController@logout', 'as'=>'adminLogout']);

        Route::get('/languages/list', ['uses'=>'LanguagesController@getAll', 'as'=>'adminLanguages']);
        Route::get('/dashboard', ['uses'=>'DashboardController@get', 'as'=>'adminDashboard']);


        Route::get('/groups/all', ['uses'=>'GroupsController@getGroups']);

        Route::get('/groups/list/{page?}', ['uses'=>'GroupsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/group/add', ['uses'=>'GroupsController@add']);
        Route::get('/group/{id}', ['uses'=>'GroupsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/group/edit/{id}', ['uses'=>'GroupsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/group/delete/{id?}', ['uses'=>'GroupsController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/coaches/all', ['uses'=>'CoachesController@getCoaches']);

        Route::get('/coaches/list/{page?}', ['uses'=>'CoachesController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/coach/add', ['uses'=>'CoachesController@add']);
        Route::get('/coach/{id}', ['uses'=>'CoachesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/coach/edit/{id}', ['uses'=>'CoachesController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/coach/delete/{id?}', ['uses'=>'CoachesController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/positions/all', ['uses'=>'PositionsController@getPositions']);

        Route::get('/positions/list/{page?}', ['uses'=>'PositionsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/position/add', ['uses'=>'PositionsController@add']);
        Route::get('/position/{id}', ['uses'=>'PositionsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/position/edit/{id}', ['uses'=>'PositionsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/position/delete/{id?}', ['uses'=>'PositionsController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/tournaments-categories/all', ['uses'=>'TournamentsCategoriesController@getTournamentsCategories']);

        Route::get('/tournaments-categories/list/{page?}', ['uses'=>'TournamentsCategoriesController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/tournaments-category/add', ['uses'=>'TournamentsCategoriesController@add']);
        Route::get('/tournaments-category/{id}', ['uses'=>'TournamentsCategoriesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/tournaments-category/edit/{id}', ['uses'=>'TournamentsCategoriesController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/tournaments-category/delete/{id?}', ['uses'=>'TournamentsCategoriesController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/countries/all', ['uses'=>'CountriesController@getCountries']);

        Route::get('/countries/list/{page?}', ['uses'=>'CountriesController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/country/add', ['uses'=>'CountriesController@add']);
        Route::get('/country/{id}', ['uses'=>'CountriesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/country/edit/{id}', ['uses'=>'CountriesController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/country/delete/{id?}', ['uses'=>'CountriesController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/cities/all', ['uses'=>'CitiesController@getCities']);

        Route::get('/cities/list/{page?}', ['uses'=>'CitiesController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/city/add', ['uses'=>'CitiesController@add']);
        Route::get('/city/{id}', ['uses'=>'CitiesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/city/edit/{id}', ['uses'=>'CitiesController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/city/delete/{id?}', ['uses'=>'CitiesController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/stadiums/all', ['uses'=>'StadiumsController@getStadiums']);

        Route::get('/stadiums/list/{page?}', ['uses'=>'StadiumsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/stadium/add', ['uses'=>'StadiumsController@add']);
        Route::get('/stadium/{id}', ['uses'=>'StadiumsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/stadium/edit/{id}', ['uses'=>'StadiumsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/stadium/delete/{id?}', ['uses'=>'StadiumsController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/categories/all', ['uses'=>'CategoriesController@getCategories']);

        Route::get('/categories/list/{page?}', ['uses'=>'CategoriesController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/category/add', ['uses'=>'CategoriesController@add']);
        Route::get('/category/{id}', ['uses'=>'CategoriesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/category/edit/{id}', ['uses'=>'CategoriesController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/category/delete/{id?}', ['uses'=>'CategoriesController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/teams/all', ['uses'=>'TeamsController@getTeams']);

        Route::get('/teams/list/{page?}', ['uses'=>'TeamsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/team/add', ['uses'=>'TeamsController@add']);
        Route::get('/team/{id}', ['uses'=>'TeamsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/team/edit/{id}', ['uses'=>'TeamsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/team/delete/{id?}', ['uses'=>'TeamsController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/stages/all', ['uses'=>'StagesController@getStages']);

        Route::get('/stages/list/{page?}', ['uses'=>'StagesController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/stage/add', ['uses'=>'StagesController@add']);
        Route::get('/stage/{id}', ['uses'=>'StagesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/stage/edit/{id}', ['uses'=>'StagesController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/stage/delete/{id?}', ['uses'=>'StagesController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/elements/all', ['uses'=>'ElementsController@getElements']);

        Route::get('/elements/list/{page?}', ['uses'=>'ElementsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/element/add', ['uses'=>'ElementsController@add']);
        Route::get('/element/{id}', ['uses'=>'ElementsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/element/edit/{id}', ['uses'=>'ElementsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/element/delete/{id?}', ['uses'=>'ElementsController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/tournaments/all', ['uses'=>'TournamentsController@getTournaments']);

        Route::get('/tournaments/list/{page?}', ['uses'=>'TournamentsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/tournament/add', ['uses'=>'TournamentsController@add']);
        Route::get('/tournament/{id}', ['uses'=>'TournamentsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/tournament/edit/{id}', ['uses'=>'TournamentsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/tournament/delete/{id?}', ['uses'=>'TournamentsController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/players/all', ['uses'=>'PlayersController@getPlayers']);

        Route::get('/players/list/{page?}', ['uses'=>'PlayersController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/player/add', ['uses'=>'PlayersController@add']);
        Route::get('/player/{id}', ['uses'=>'PlayersController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/player/edit/{id}', ['uses'=>'PlayersController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/player/delete/{id?}', ['uses'=>'PlayersController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/seasons/all', ['uses'=>'SeasonsController@getSeasons']);

        Route::get('/seasons/list/{page?}', ['uses'=>'SeasonsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/season/add', ['uses'=>'SeasonsController@add']);
        Route::get('/season/{id}', ['uses'=>'SeasonsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/season/edit/{id}', ['uses'=>'SeasonsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/season/delete/{id?}', ['uses'=>'SeasonsController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/matches/list/{page?}', ['uses'=>'MatchesController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/match/add', ['uses'=>'MatchesController@add']);
        Route::get('/match/{id}', ['uses'=>'MatchesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/match/edit/{id}', ['uses'=>'MatchesController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/match/delete/{id?}', ['uses'=>'MatchesController@delete'])->where(['id'=>'[0-9]+']);


        Route::get('/news/list/{page?}', ['uses'=>'NewsController@getAll'])->where(['page'=>'[0-9]+']);
        Route::post('/news-item/add', ['uses'=>'NewsController@add']);
        Route::get('/news-item/{id}', ['uses'=>'NewsController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/news-item/edit/{id}', ['uses'=>'NewsController@edit'])->where(['id'=>'[0-9]+']);
        Route::get('/news-item/delete/{id?}', ['uses'=>'NewsController@delete'])->where(['id'=>'[0-9]+']);

        Route::post('/news-item-image-upload', ['uses'=>'NewsController@imageUpload']);


        Route::get('/pages/list', ['uses'=>'PagesController@getAll']);
        Route::get('/page/{id}', ['uses'=>'PagesController@getById'])->where(['id'=>'[0-9]+']);
        Route::post('/page/edit/{id}', ['uses'=>'PagesController@edit'])->where(['id'=>'[0-9]+']);
    });
});