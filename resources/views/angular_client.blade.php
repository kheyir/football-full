<!doctype html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-MB09LPRMEG"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-MB09LPRMEG');
    </script>

    <meta charset="utf-8">
    <title>{{$title}}</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/site/assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/site/assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/site/assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/assets/site/assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="/assets/site/assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    @if($description) <meta name="description" content="{{$description}}"> @endif
    @if($ogUrl) <meta property="og:url" content="{{$ogUrl}}"> @endif
    @if($ogSiteName) <meta property="og:site_name" content="{{$ogSiteName}}"> @endif
    @if($ogTitle) <meta property="og:title" content="{{$ogTitle}}"> @endif
    @if($ogType) <meta property="og:type" content="{{$ogType}}"> @endif
    @if($ogImage) <meta property="og:image" content="{{$ogImage}}"> @endif
    @if($ogDescription) <meta property="og:description" content="{{$ogDescription}}"> @endif

    @if(isset($section)) <meta property="article:section" content="{{$section}}"> @endif
    @if(isset($publishedTime)) <meta property="article:published_time" content="{{$publishedTime}}"> @endif
    @if(isset($twitterCard)) <meta property="twitter:card" content="{{$twitterCard}}"> @endif
    @if(isset($twitterTitle)) <meta property="twitter:title" content="{{$twitterTitle}}"> @endif
    @if(isset($twitterDescription)) <meta property="twitter:description" content="{{$twitterDescription}}"> @endif

    <link rel="stylesheet" href="/assets/site/assets/css/site.css">


    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    {{-- <script type="text/javascript" src="https://connect.facebook.net/en_US/sdk.js"></script> --}}
</head>
<body>
<app-root></app-root>


{{-- <script src="runtime-es2015.edb2fcf2778e7bf1d426.js" type="module"></script>
<script src="polyfills-es2015.1926cdb7cdc6bc340ca5.js" type="module"></script>
<script src="main-es2015.ebaf3dfc07e87b926607.js" type="module"></script> --}}
<script src="/assets/site/runtime-es5.cdfb0ddb511f65fdc0a0.js" nomodule defer></script>
<script src="/assets/site/polyfills-es5.6d5623a3cbf779a24f5d.js" nomodule defer></script>
{{-- <script src="/assets/site/main-es5.ebaf3dfc07e87b926607.js" nomodule defer></script> --}}

<script src="/assets/site/assets/js/scripts.js"></script>
</body>
</html>
