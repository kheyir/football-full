<!doctype html>
<html lang="{{Lang::getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" type="image/png" href="{{asset("/assets/site/images/logo-fav.png")}}"/>

    <title>{{Lang::get("site.title")}}</title>

    <meta name="description" content="{{Lang::get("site.description")}}">
    <meta name="keywords" content="{{Lang::get("site.keywords")}}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset("/assets/site/css/reset.css")}}">
    <link rel="stylesheet" href="{{asset("/assets/site/css/bootstrap-grid.min.css")}}">
    <link rel="stylesheet" href="{{asset("/assets/site/css/style.css?v=1.6")}}">
    <link rel="stylesheet" href="{{asset("/assets/site/css/responsive.css?v=1.9")}}">
{{--    <link rel="stylesheet" href="{{asset("/assets/site/fonts/Proxima_Nova/stylesheet.css")}}">--}}
{{--    <link rel="stylesheet" href="{{asset("/assets/site/fonts/SanFrancisco/san_francisco.css")}}">--}}
    <link rel="stylesheet" href="{{asset("/assets/site/fonts/SF/style.css")}}">
    <link rel="stylesheet" href="{{asset('assets/site/owlcarousel/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    {{--<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>--}}

</head>
<body>

<script src="{{asset("/assets/site/js/jquery-3.3.1.min.js")}}"></script>
<script src="{{asset("/assets/site/js/jquery.mask.js")}}"></script>
<script src="{{asset("/assets/site/js/isotope.pkgd.min.js")}}"></script>
<script src="{{asset("/assets/site/js/main.js?v=2")}}"></script>

<div id="main" class="">
    @yield("header")
    <div class="content">
        @yield("content")
    </div>
    @yield("footer")
</div>

</body>
</html>