<!doctype html>
<html lang="az" style="font-size: 16px">
<head>
    <meta charset="utf-8">
    <title>Admin Panel - Football.az</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">

    {{-- <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" /> --}}

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/assets/admin/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/assets/css/colors.css" rel="stylesheet" type="text/css">


    <!-- Core JS files -->
    <!-- <script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script> -->
    <script type="text/javascript" src="/assets/admin/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/admin/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/admin/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- <script type="text/javascript" src="/assets/js/plugins/ui/nicescroll.min.js"></script> -->
    <script type="text/javascript" src="/assets/admin/assets/js/plugins/ui/drilldown.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/admin/assets/js/core/app.js"></script>

    <script type="text/javascript" src="/assets/admin/assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /theme JS files -->

    <script>
        if (global === undefined) {
            var global = window;
        }
    </script>
    <link rel="stylesheet" href="/assets/admin/styles.9f8d46d1ea669e21d14b.css">
</head>
<body>
<app-root></app-root>
<script src="/assets/admin/runtime-es2015.edb2fcf2778e7bf1d426.js" type="module"></script><script src="/assets/admin/runtime-es5.edb2fcf2778e7bf1d426.js" nomodule defer></script><script src="/assets/admin/polyfills-es5.1e5f9711c799ca1782ca.js" nomodule defer></script><script src="/assets/admin/polyfills-es2015.1926cdb7cdc6bc340ca5.js" type="module"></script><script src="/assets/admin/main-es2015.a515ed16efce9e8856b7.js" type="module"></script><script src="/assets/admin/main-es5.a515ed16efce9e8856b7.js" nomodule defer></script>

</body>
</html>
