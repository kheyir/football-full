<?php

 return [
    "metaDescription" => "Новости, матчи, статистики и т.д. мира футбола Азербайджана и всего мира",
    "news" => "Новости",
    "search" => "Поиск",
    "video" => "Видео",
    "review" => "Обзор",
    "advertising" => "Реклама",
    "about" => "О нас",
    "tournaments" => "Турниры"
 ];