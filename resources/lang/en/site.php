<?php

 return [

     "title" => "Position.az",
     "description" => "Job searching in Azerbaijan",
     "keywords" => "job, jobsearch, hr, recruitment, employment, vacancy, website in Azerbaijan, IRES, employment agency",

     "header_vacancy_count_info" => "Today added :vacanciesCount vacancies and :resumesCount resumes",

     "about_us" => "About us",
     "contact_us" => "Contacts",
     "cv_base" => "CV Database",
     "publish_cv" => "Post a CV",
     "publish_vacancy" => "Post a vacancy",
     "search_placeholder" => "Search",

     "all_rights_reserved" => "© All rights reserved",
     "made_with_love" => "Position.az",


     "vacancies_published" => "Vacancies posted on the site",
     "resumes_published" => "Resume posted",
     "companies_found" => "Companies found those whom they were looking for",
     "people_search" => "Man per day looking for job on our site",

     "do_something_great" => "Do something great",

     "phone_number" => "Phone",
     "add_phone_number" => "Additional phone",
     "social_networks" => "Social networks",

     "contacts_mini_description" => "Компания «Proline Chemicals LTD» специализируется на производстве и продаже моющих и чистящих средст в соответствии с передовыми стандартами качества.",


     "vacancies" => "Vacancies",
     "views_count" => "View count",
     "position" => "Position",
     "employer" => "Employer",
     "duration" => "Duration",
     "date_of_issue" => "Date of issue",

     "all_vacancies" => "All vacancies",
     "all_resumes" => "All resumes",

     "vacancy" => "Vacancy",
     "company" => "Company",
     "site" => "Site",
     "salary" => "Salary",
     "contacts" => "Contacts",
     "other_vacancies_in_this_category" => "Other vacancies in this category",
     "other_vacancies_by_this_company" => "Other vacancies of this employer",
     "see_more" => "See more",

     "search_results" => "Search result for «:searchRequest»‎",
     "by_your_request_nothing_has_been_found" => "No results found for your request",

     "file_with_vacancy_info" => "File with information about the vacancy",
     "select_or_drop_here" => "Select or drag docx(doc) file here",
     "allowed_only_doc_files" => "Only docx(doc) files are allowed",
     "file_is_bigger_than_5_mb" => "File is more than 5 MB",
     "download_template" => "Download template",
     "send_vacancy" => "Send vacancy",

     "vacancy_sent" => "Vacancy sent",
     "thanks_for_choice_us" => "<p>Thank you for choosing us!</p>",

     "for_vacancies" => "For posting vacancies and publication requests",
     "for_adsense" => "For advertisers, complaints and suggestions",
     "keep_for_updates" => "Keep for updates",

     "resumes" => "Resumes",
     "name_and_surname_of_candidate" => "Name and surname of the candidate",
     "all" => "All",
     "experience_word" => "Experience",
     "education_word" => "Education",
     "city_word" => "City",
     "resume" => "Resume",
     "placement_date" => "Placement date",
     "man" => "Male",
     "mini_man" => "M",
     "woman" => "Female",
     "mini_woman" => "F",
     "age" => "Age",
     "e.mail" => "E-mail",
     "skills" => "Skills",
     "additional_info_about_yourself" => "Additional info about yourself",
     "driving_license_categories" => "Driving license categories",
     "post_resume" => "Post a CV",
     "name" => "Name",
     "surname" => "Surname",
     "patronymic" => "Patronymic",
     "gender" => "Gender",
     "photo" => "Photo",
     "select_or_drop_here2" => "Select or drop here",
     "select_from_list" => "Select from the list",
     "more_about_education" => "More about education",
     "more_about_experience" => "More about experience",
     "category" => "Category",
     "sub_category" => "Subcategory",
     "position2" => "Position",
     "min_salary_azn" => "Minimum salary (AZN)",
     "if_exists" => "(if exists)",
     "for_typing_phone" => "Enter several numbers separated by commas",
     "publish_resume" => "Publish resume",
     "allowed_only_png_jpeg_files" => "Allowed only png, jpeg files",
     "to_print" => "Print",
     "hidden_number" => "Check to hide your number from list",
     "number_is_hidden" => "Hidden",
     "added_successfully" => "
     <p>Dear user,</p>
     <p>Your resume was received. After being checked by the moderator, the resume will be posted on our website.</p>
     ",
     "extended_successfully" => "
     <p>Dear user,</p>
     <p>Your resume has been expired.</p>
     ",
     "unsubscribed_successfully" => "
     <p>Dear user,</p>
     <p>Subscription has been stopped</p>
     ",
     "rules" => "Rules",
     "publishing_rules" => "Publishing rules",
     "other_resumes_in_this_category" => "Other resumes in this category",

     "education" => [
         "0" => "Science Degree",
         "1" => "Higher",
         "2" => "Incomplete Higher",
         "3" => "Secondary Technical",
         "4" => "Specialized Secondary",
         "5" => "Secondary",
         "6" => "-"
     ],

     "experience" => [
         "0" => "Less than 1 year",
         "1" => "from 1 to 3 years",
         "2" => "from 3 to 5 years",
         "3" => "More than 5 years"
     ],

     "city" => [
         "1" => "Aghdam",
         "2" => "Agdash",
         "3" => "Aghjabadi",
         "4" => "Agstafa",
         "5" => "Agsu",
         "6" => "Ali-Bayramli",
         "7" => "Baku",
         "8" => "Barda",
         "9" => "Beylagan",
         "10" => "Balakan",
         "11" => "Bilasuvar",
         "12" => "Gabala",
         "13" => "Gazakh",
         "14" => "Gadabay",
         "15" => "Goychay",
         "16" => "Goranboy",
         "17" => "Guba",
         "18" => "Gusar",
         "19" => "Ganja",
         "20" => "Dashkesan",
         "21" => "Jalilabad",
         "22" => "Yevlakh",
         "23" => "Zaqatala",
         "24" => "Imishli",
         "25" => "Ismayilli",
         "26" => "Garadagh",
         "27" => "Gakh",
         "28" => "Kurdamir",
         "29" => "Lerik",
         "30" => "Lankaran",
         "31" => "Masally",
         "32" => "Mingachevir",
         "33" => "Naftalan",
         "34" => "Nakhchivan",
         "35" => "Saatly",
         "36" => "Sabirabad",
         "37" => "Salyan",
         "38" => "Samukh",
         "39" => "Siyazan",
         "40" => "Sumgayit",
         "41" => "Tartar",
         "42" => "Tovuz",
         "43" => "Fizuli",
         "44" => "Khachmaz",
         "45" => "Khizi",
         "46" => "Khirdalan",
         "47" => "Shabran",
         "48" => "Shamakhi",
         "49" => "Shamkir",
         "50" => "Sharur",
         "51" => "Shaki",
         "52" => "Shirvan",
         "53" => "Yardimli",
     ],

 ];