<?php

 return [
    "metaDescription" => "Azərbaycan və dünyada futbol aləmindən xəbərlər, oyunlar, statistikalar və s.",
    "news" => "Xəbərlər",
    "search" => "Axtarış",
    "video" => "Video",
    "review" => "İcmal",
    "advertising" => "Reklam",
    "about" => "Haqqımızda",
    "tournaments" => "Turnirlər"
 ];