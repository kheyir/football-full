import {ValidatorFn, AbstractControl} from '@angular/forms'

export class CustomValidators {

    onlyFileType(fileType: string): ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} | null => {
            const forbidden = nameRe.test(control.value);
            return forbidden ? {'forbiddenName': {value: control.value}} : null;
        };
    }
}
