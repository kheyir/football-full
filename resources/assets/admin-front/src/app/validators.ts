// FORM GROUP VALIDATORS
export function dependencyArrayOfCheckbox(dependencyKey: string, dependentKey: string) {
    return (group: any) => {
        let dependency = group.controls[dependencyKey];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (dependency.value && dependent.value.length == 0) {
            errors.required = true;
            group.controls[dependentKey].setErrors({'required': true});
        }
    }
}

export function dependencyNumericOfCheckbox(dependencyKey: string, dependentKey: string) {
    return (group: any) => {
        let dependency = group.controls[dependencyKey];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (dependency.value && (dependent.value == null || dependent.value == '')) {
            errors.required = true;
            group.controls[dependentKey].setErrors(errors);
        }
        else{
            if(Object.keys(errors).length > 0){
                delete errors.required
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[dependentKey].setErrors(errors);
        }
    }
}

export function dependencyCheckboxOfCheckbox(dependencyKey: string, dependentKey: string) {
    return (group: any) => {
        let dependency = group.controls[dependencyKey];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (dependency.value && !dependent.value) {
            errors.required = true;
            group.controls[dependentKey].setErrors(errors);
        }
        else{
            if(Object.keys(errors).length > 0){
                delete errors.required
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[dependentKey].setErrors(errors);
        }
    }
}

export function dependencyFieldOfCheckbox(dependencyKey: string, dependentKey: string) {
    return (group: any) => {
        let dependency = group.controls[dependencyKey];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (dependency.value && (dependent.value == null || dependent.value == '')) {
            errors.required = true;
            group.controls[dependentKey].setErrors(errors);
        }
        else{
            if(Object.keys(errors).length > 0){
                delete errors.required
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[dependentKey].setErrors(errors);
        }
    }
}

export function dependencyFieldsOfHasGroups(dependencyKey: string, dependentKey: string) {
    return (group: any) => {
        let dependency = group.controls[dependencyKey];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (dependency.value && dependency.value.has_groups && (dependent.value == null || dependent.value == '')) {
            errors.required = true;
            group.controls[dependentKey].setErrors(errors);
        }
        else{
            if(Object.keys(errors).length > 0){
                delete errors.required
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[dependentKey].setErrors(errors);
        }
    }
}

export function dependencyArrayOfHasGroups(dependencyKey: string, dependentKey: string) {
    return (group: any) => {
        let dependency = group.controls[dependencyKey];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (dependency.value && dependency.value.has_groups && (dependent.value == null || dependent.value == '')) {
            errors.required = true;
            group.controls[dependentKey].setErrors(errors);
        }
        else{
            if(Object.keys(errors).length > 0){
                delete errors.required
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[dependentKey].setErrors(errors);
        }
    }
}

export function dependencyArrayOfCheckboxReverseAndHasGroups(dependency1Key: string, dependency2Key: string, dependentKey: string) {
    return (group: any) => {
        let dependency1 = group.controls[dependency1Key];
        let dependency2 = group.controls[dependency2Key];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (!dependency1.value && dependency2.value && dependency2.value.has_groups && (dependent.value == null || dependent.value == '')) {
            errors.required = true;
            group.controls[dependentKey].setErrors(errors);
        }
        else{
            if(Object.keys(errors).length > 0){
                delete errors.required
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[dependentKey].setErrors(errors);
        }
    }
}

export function requiredAllIfOneOfThreeNotEmpty(formControl1Key: string, formControl2Key: string, formControl3Key: string, formControl4Key: string) {
    return (group: any) => {

        let formControl1 = group.controls[formControl1Key];
        let formControl2 = group.controls[formControl2Key];
        let formControl3 = group.controls[formControl3Key];
        let formControl4 = group.controls[formControl4Key];

        let errors1: any = group.controls[formControl1Key].errors || {};
        let errors2: any = group.controls[formControl2Key].errors || {};
        let errors3: any = group.controls[formControl3Key].errors || {};
        let errors4: any = group.controls[formControl4Key].errors || {};

        if ((formControl1.value === null || formControl1.value === '') &&
            (formControl2.value === null || formControl2.value === '') &&
            (formControl4.value === null || formControl3.value === '') &&
            (formControl3.value === null || formControl4.value === '')) {
            if(Object.keys(errors1).length > 0){
                delete errors1.required
                if(Object.keys(errors1).length == 0) errors1 = null;
            }
            else{
                errors1 = null;
            }

            if(Object.keys(errors2).length > 0){
                delete errors2.required
                if(Object.keys(errors2).length == 0) errors2 = null;
            }
            else{
                errors2 = null;
            }

            if(Object.keys(errors3).length > 0){
                delete errors3.required
                if(Object.keys(errors3).length == 0) errors3 = null;
            }
            else{
                errors3 = null;
            }

            if(Object.keys(errors4).length > 0){
                delete errors4.required
                if(Object.keys(errors4).length == 0) errors4 = null;
            }
            else{
                errors4 = null;
            }

            group.controls[formControl1Key].setErrors(errors1);
            group.controls[formControl2Key].setErrors(errors2);
            group.controls[formControl3Key].setErrors(errors3);
            group.controls[formControl4Key].setErrors(errors4);
        }
        else{
            if(formControl1.value === null || formControl1.value === ''){
                errors1.required = true;
                group.controls[formControl1Key].setErrors(errors1);
            }

            if(formControl2.value === null || formControl2.value === ''){
                errors2.required = true;
                group.controls[formControl2Key].setErrors(errors2);
            }

            if(formControl3.value === null || formControl3.value === ''){
                errors3.required = true;
                group.controls[formControl3Key].setErrors(errors3);
            }

            if(formControl4.value === null || formControl4.value === ''){
                errors4.required = true;
                group.controls[formControl4Key].setErrors(errors4);
            }
        }
    }
}

export function requiredIfBothAreEmpty(languages, formControl1Key: string, formControl2Key: string, formControl3Key: string, formControl4Key: string) {
    return (group: any) => {

        let isEmpty = true;
        languages.map((language) => {
            let formControl1 = group.controls[`${formControl1Key}[${language.locale}]`];
            let formControl2 = group.controls[`${formControl2Key}[${language.locale}]`];
            let formControl3 = group.controls[`${formControl3Key}[${language.locale}]`];
            let formControl4 = group.controls[`${formControl4Key}[${language.locale}]`];

            if ((formControl1.value === null || formControl1.value === '') &&
                (formControl2.value === null || formControl2.value === '') &&
                (formControl3.value === null || formControl3.value === '') &&
                (formControl4.value === null || formControl4.value === '')) {

                isEmpty = isEmpty && true;
            }
            else {
                isEmpty = isEmpty && false;
            }
        })

        languages.map((language) => {

            let errors4: any = group.controls[`${formControl4Key}[${language.locale}]`].errors || {};

            if(isEmpty){
                errors4.oneOfAllMustBeFilled = true;
                group.controls[`${formControl4Key}[${language.locale}]`].setErrors(errors4);
            }
            else{
                if(Object.keys(errors4).length > 0){
                    delete errors4.oneOfAllMustBeFilled
                    if(Object.keys(errors4).length == 0) errors4 = null;
                }
                else{
                    errors4 = null;
                }

                group.controls[`${formControl4Key}[${language.locale}]`].setErrors(errors4);
            }
        })
    }
}

export function dependencyFieldOfCheckboxReverse(dependencyKey: string, dependentKey: string) {
    return (group: any) => {
        let dependency = group.controls[dependencyKey];
        let dependent = group.controls[dependentKey];

        let errors: any = group.controls[dependentKey].errors || {};

        if (!dependency.value && (dependent.value == null || dependent.value == '')) {
            errors.required = true;
            group.controls[dependentKey].setErrors(errors);
        }
        else{
            if(Object.keys(errors).length > 0){
                delete errors.required
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[dependentKey].setErrors(errors);
        }
    }
}

export function fileType(fileKey: string, neededFileTypes: any) {
    return (group: any) => {
        let file = group.controls[fileKey];

        let errors: any = group.controls[fileKey].errors || {};

        if(file && file.value && file.value.type && neededFileTypes.indexOf(file.value.type) == -1){
            errors.isNotNeededFileType = true
            group.controls[fileKey].setErrors(errors);
        }else{
            if(Object.keys(errors).length > 0){
                delete errors.isNotNeededFileType
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[fileKey].setErrors(errors);
        }
    }
}

export function fileSize(fileKey: string, neededFileSize: number) {
    return (group: any) => {
        let file = group.controls[fileKey];

        let errors: any = group.controls[fileKey].errors || {};

        if(file && file.value && file.value.type && file.value.size > neededFileSize){
            errors.isNotNeededFileSize = true
            group.controls[fileKey].setErrors(errors);
        }else{
            if(Object.keys(errors).length > 0){
                delete errors.isNotNeededFileSize
                if(Object.keys(errors).length == 0) errors = null;
            }
            else{
                errors = null;
            }

            group.controls[fileKey].setErrors(errors);
        }
    }
}