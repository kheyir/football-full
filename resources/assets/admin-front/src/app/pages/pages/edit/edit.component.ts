import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ActivatedRoute, Router} from "@angular/router";
import {PagesService} from "../../../services/pages.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  id;
  pageItem = null;

  mainGroup: FormGroup;

  tournaments = null;
  seasons = null;
  categories = null;
  countries = null;
  teams = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  public Editor = ClassicEditor;

  config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'link', '|', 'undo', 'redo', '|', 'MediaEmbed'],
    mediaEmbed: {
      previewsInData: true
    }
  }

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private pagesService: PagesService,
      private loaderService: LoaderService
  ) {
    this.Editor.defaultConfig = this.config;
  }

  ngOnInit() {

    this.route.paramMap
        .subscribe(params => {
          this.id = params.get('id')

          this.getPageItem();
        })
  }

  getPageItem(){

    let data = {
      id: this.id
    }

    this.loaderService.increaseLoaderCount()

    this.pageItem = null;

    this.pagesService.getPageItemById(data)
        .subscribe((response: any) => {

          this.createFilterForm();

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.pageItem = response.pageItem;
          }
          else{
            this.router.navigate(["/404"])
          }
        })
  }

  createFilterForm(){

    this.mainGroup = this.formBuilder.group({
      text: [null, Validators.required]
    });

    this.showForm = true;

    this.loaderService.decreaseLoaderCount()
  }

  onSubmit(){

    console.log(this.mainGroup)

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){

      const formData = new FormData();

      formData.append('text', this.mainGroup.get('text').value);

      let data = {
        id: this.id
      }

      this.loaderService.increaseLoaderCount()

      this.pagesService.edit(data, formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/pages/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

}
