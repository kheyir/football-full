import { Component, OnInit } from '@angular/core';
import {PagesService} from "../../../services/pages.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  pages = null;

  constructor(
      private loaderService: LoaderService,
      private pagesService: PagesService
  ) { }

  ngOnInit() {
    this.getPages()
  }

  getPages(){
    this.loaderService.increaseLoaderCount()

    this.pages = null;
    this.pagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.pages = response.pages;
          }
        })
  }

}
