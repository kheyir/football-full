import { Component, OnInit } from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {StagesService} from "../../../services/stages.service";
import {GroupsService} from "../../../services/groups.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {dependencyArrayOfCheckbox} from "../../../validators";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  groups = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private stagesService: StagesService,
      private groupsService: GroupsService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
    this.getGroups();
  }

  createFilterForm(){

    let mainGroupObj = {
      priority: [0, Validators.required],
      has_groups: [null],
      groups: [[]],
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj, {
      validators: dependencyArrayOfCheckbox('has_groups', 'groups')
    });

    this.showForm = true;
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getGroups(){
    this.groups = null;

    this.loaderService.increaseLoaderCount();

    this.groupsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.groups = response.groups;
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      let httpParams = new HttpParams();

      if(this.mainGroup.get('has_groups').value) httpParams = httpParams.append('has_groups', this.mainGroup.get('has_groups').value);
      httpParams = httpParams.append('priority', this.mainGroup.get('priority').value);
      if(this.mainGroup.get('has_groups').value) httpParams = httpParams.append('groups', this.mainGroup.get('groups').value);

      this.languages.forEach((item: any) => {
        httpParams = httpParams.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      this.loaderService.increaseLoaderCount()

      this.stagesService.add(httpParams)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/stages/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

}
