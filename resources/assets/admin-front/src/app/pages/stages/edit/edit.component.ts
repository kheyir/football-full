import { Component, OnInit } from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {StagesService} from "../../../services/stages.service";
import {GroupsService} from "../../../services/groups.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {dependencyArrayOfCheckbox} from "../../../validators";

@Component({
  selector: 'edit-add',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  id;
  stage = null;

  stageGroups

  groups = null;

  mainGroup: FormGroup;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private stagesService: StagesService,
      private groupsService: GroupsService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
    this.getGroups();

    this.route.paramMap
        .subscribe(params => {
          this.id = params.get('id')

          this.getStage();
        })
  }

  getStage(){

    let data = {
      id: this.id
    }

    this.loaderService.increaseLoaderCount()

    this.stage = null;

    this.stagesService.getStageById(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.stage = response.stage;

            this.stageGroups = this.stage.groups.map((item: any) => {
                return item.group_id;
            });
          }
          else{
            this.router.navigate(["/404"])
          }
        })
  }

  createFilterForm(){

    let mainGroupObj = {
        priority: [0, Validators.required],
        has_groups: [null],
        groups: [[]],
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj, {
        validator: dependencyArrayOfCheckbox('has_groups', 'groups')
    });

    this.showForm = true;

    this.loaderService.decreaseLoaderCount()
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getGroups(){
    this.groups = null;

    this.loaderService.increaseLoaderCount();

    this.groupsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.groups = response.groups;
            }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      let httpParams = new HttpParams();

      if(this.mainGroup.get('has_groups').value) httpParams = httpParams.append('has_groups', this.mainGroup.get('has_groups').value);
      httpParams = httpParams.append('priority', this.mainGroup.get('priority').value);
      if(this.mainGroup.get('has_groups').value) httpParams = httpParams.append('groups', this.mainGroup.get('groups').value);

      this.languages.forEach((item: any) => {
        httpParams = httpParams.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      let data = {
        id: this.id
      }

      this.loaderService.increaseLoaderCount()

      this.stagesService.edit(data, httpParams)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/stages/list'])
            }
            else{
                alert(response.message);
            }
          })
    }
  }

  onDeleting(event){

    let needableElement = this.stage.groups.find(function(item){
        return item.group_id == event.value.group_id;
    })

    if(needableElement && needableElement.seasons__stages__group_id){
        event.selected = true;
        this.mainGroup.controls['groups'].value.push(event.value.group_id)

        this.mainGroup.patchValue({
            groups: this.mainGroup.controls['groups'].value
        })
    }
  }

}
