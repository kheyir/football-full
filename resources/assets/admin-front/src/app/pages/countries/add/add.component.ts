import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {CountriesService} from "../../../services/countries.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {FileTypeValidator} from '../../../directives/file-type-validator.directive';
import {FileSizeValidator} from '../../../directives/file-size-validator.directive';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private countriesService: CountriesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
  }

  createFilterForm(){

    let mainGroupObj = {
      slug: [null, [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9-]+')]],
      in_head: [null],
      is_news_category: [null],
      flag: [null, [FileTypeValidator, FileSizeValidator]]
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      formData.append('slug', this.mainGroup.get('slug').value);
      if(this.mainGroup.get('in_head').value) formData.append('in_head', this.mainGroup.get('in_head').value);
      if(this.mainGroup.get('is_news_category').value) formData.append('is_news_category', this.mainGroup.get('is_news_category').value);
      if(this.mainGroup.get('flag').value) formData.append('flag', this.mainGroup.get('flag').value);

      this.languages.forEach((item: any) => {
        formData.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      this.loaderService.increaseLoaderCount()

      this.countriesService.add(formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/countries/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

  onFileChange(event){

    let formControlNameObj = {}

    if(event.target.files && event.target.files.length) {

      let file = event.target.files[0];

      formControlNameObj[event.target.dataset.formControlName] = file
    }
    else{
      formControlNameObj[event.target.dataset.formControlName] = null;
    }

    this.mainGroup.patchValue(formControlNameObj);

    this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

}
