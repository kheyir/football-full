import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CountriesService} from "../../../services/countries.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {FileTypeValidator} from '../../../directives/file-type-validator.directive';
import {FileSizeValidator} from '../../../directives/file-size-validator.directive';

@Component({
  selector: 'edit-add',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  id;
  country = null;

  mainGroup: FormGroup;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  get oldFlag(){
      return this.mainGroup.get('old_flag').value
  }

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private countriesService: CountriesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {

    this.route.paramMap
        .subscribe(params => {
          this.id = params.get('id')

          this.getLanguages();
        })
  }

  getCountry(){

    let data = {
      id: this.id
    }

    this.loaderService.increaseLoaderCount()

    this.country = null;

    this.countriesService.getCountryById(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.country = response.country

              if(!this.country.flag){
                  this.mainGroup.patchValue({
                      old_flag: 2
                  })
              }
              else{
                  this.mainGroup.patchValue({
                      old_flag: 1
                  })
              }
          }
          else{
            this.router.navigate(["/404"])
          }
        })
  }

  createFilterForm(){

    let mainGroupObj = {
      slug: [null, [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9-]+')]],
      in_head: [null],
      is_news_category: [null],
      flag: [null, [FileTypeValidator, FileSizeValidator]],
      old_flag: [1, [Validators.required]]
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;

    this.loaderService.decreaseLoaderCount()


    this.getCountry();
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    console.log(this.mainGroup)

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      formData.append('slug', this.mainGroup.get('slug').value);
      if(this.mainGroup.get('in_head').value) formData.append('in_head', this.mainGroup.get('in_head').value);
      if(this.mainGroup.get('is_news_category').value) formData.append('is_news_category', this.mainGroup.get('is_news_category').value);
      if(this.mainGroup.get('flag').value) formData.append('flag', this.mainGroup.get('flag').value);
      if(this.mainGroup.get('old_flag').value) formData.append('old_flag', this.mainGroup.get('old_flag').value);

      this.languages.forEach((item: any) => {
          formData.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      let data = {
        id: this.id
      }

      this.loaderService.increaseLoaderCount()

      this.countriesService.edit(data, formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/countries/list'])
            }
            else{
                alert(response.message);
            }
          })
    }
  }

  onFileChange(event){

      let formControlNameObj = {}

      if(event.target.files && event.target.files.length) {

          let file = event.target.files[0];

          formControlNameObj[event.target.dataset.formControlName] = file
      }
      else{
          formControlNameObj[event.target.dataset.formControlName] = null;
      }

      this.mainGroup.patchValue(formControlNameObj);

      this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

  deleteFile(event){
      let formControlNameObj = {}
      formControlNameObj[event.currentTarget.dataset.formControlName] = 2

      this.mainGroup.patchValue(formControlNameObj);
      this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

}
