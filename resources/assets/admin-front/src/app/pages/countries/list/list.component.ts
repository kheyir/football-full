import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CountriesService} from "../../../services/countries.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  countries = null;

  public pageNumber: number = 1;
  public selectedId: number = 0;

  inHeadStatuses = [
      {label: 'Hamısı', value: 0},
      {label: 'Bəli', value: 1},
      {label: 'Xeyr', value: 2},
  ];

  isNewsCategoryStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private countriesService: CountriesService,
  ) { }

  ngOnInit() {

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            name: paramsCopy.name ? paramsCopy.name : '',
            in_head: paramsCopy.in_head ? parseInt(paramsCopy.in_head) : 0,
            is_news_category: paramsCopy.is_news_category ? parseInt(paramsCopy.is_news_category) : 0,
          })

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      name: ['', Validators.minLength(3)],
      in_head: [0],
      is_news_category: [0]
    })
  }

  getCountries(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.countries = null;
    this.countriesService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.countries = response.countries;
          }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){
      const data = {
        pageNumber: this.pageNumber,

        name: this.filterGroup.get("name").value,
        in_head: this.filterGroup.get("in_head").value,
        is_news_category: this.filterGroup.get("is_news_category").value,
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getCountries(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.countriesService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

}
