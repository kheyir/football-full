import { Component, OnInit } from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ElementsService} from "../../../services/elements.service";
import {StagesService} from "../../../services/stages.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  stages = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private elementsService: ElementsService,
      private stagesService: StagesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
    this.getStages();
  }

  createFilterForm(){

    let mainGroupObj = {
      stage_id: [null, Validators.required],
      priority: [0, Validators.required],
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getStages(){
    this.stages = null;

    this.loaderService.increaseLoaderCount();

    this.stagesService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.stages = response.stages;
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      let httpParams = new HttpParams();

      httpParams = httpParams.append('stage_id', this.mainGroup.get('stage_id').value);
      httpParams = httpParams.append('priority', this.mainGroup.get('priority').value);

      this.languages.forEach((item: any) => {
        httpParams = httpParams.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      this.loaderService.increaseLoaderCount()

      this.elementsService.add(httpParams)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/elements/list'])
            }
            else{
                alert(response.message);
            }
          })
    }
  }

}
