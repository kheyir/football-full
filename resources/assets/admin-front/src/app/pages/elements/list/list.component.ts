import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ElementsService} from "../../../services/elements.service";
import {StagesService} from "../../../services/stages.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  elements = null;
  stages = null;

  public pageNumber: number = 1;
  public selectedId: number = 0;

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private elementsService: ElementsService,
      private stagesService: StagesService,
  ) { }

  ngOnInit() {

    this.getStages();
    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            name: paramsCopy.name ? paramsCopy.name : '',
            stage_id: paramsCopy.stage_id ? parseInt(paramsCopy.stage_id) : 0
          })

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      name: ['', Validators.minLength(3)],
      stage_id: [0],
    })
  }

  getElements(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.elements = null;
    this.elementsService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.elements = response.elements;
          }
        })
  }

  getStages(){
      this.stages = null;

      this.loaderService.increaseLoaderCount();

      this.stagesService.getAll()
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.stages = [{name: 'Hamısı', id: 0}, ...response.stages]
            }
          })
  }

  filter(){

    if(!this.filterGroup.invalid){
      const data = {
        pageNumber: this.pageNumber,

        name: this.filterGroup.get("name").value,
        stage_id: this.filterGroup.get("stage_id").value,
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getElements(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.elementsService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

}
