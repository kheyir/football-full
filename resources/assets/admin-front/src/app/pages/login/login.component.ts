import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {CookieService} from 'ngx-cookie-service';
import {LoginService} from '../../services/login.service'
import {LoaderService} from '../../services/loader.service'
import {environment} from '../../../environments/environment'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  public formGroup: FormGroup;
  public error: string = "";

  public submitSent = false;

  constructor(private formBuilder: FormBuilder,
              private loginService: LoginService,
              private cookie: CookieService,
              private loaderService: LoaderService) { }

  ngOnInit() {
    this.createForm()
  }

  createForm(){
    this.formGroup = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  formValidation(){
    if(this.formGroup.invalid) return false;
    else return true;
  }

  formSubmit(){
    this.error = "";
    this.submitSent = true;
    if(this.formValidation()){

      let data = {
        username: this.formGroup.get("username").value,
        password: this.formGroup.get("password").value
      }

      this.loaderService.increaseLoaderCount()
      this.loginService.signIn(data)
          .subscribe((response: any) => {
                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                  if(environment.secure){
                    this.cookie.set("token", response.token, 1440, '/', environment.BASE_DOMAIN, true);
                  }
                  else{
                    this.cookie.set("token", response.token, 1440, '/', window.location.hostname, false, "Strict");
                  }

                  window.location.href = '/foot'
                }
              },
              (errorResponse) => {
                this.loaderService.decreaseLoaderCount()
                if(errorResponse.status == 401){

                  if(environment.secure){
                    this.cookie.delete("token", '/', environment.BASE_DOMAIN)
                  }
                  else{
                    this.cookie.delete("token", '/', window.location.hostname)
                  }
                  this.error = errorResponse.error.message;
                }
              })
    }
  }

}
