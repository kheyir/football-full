import { Component, OnInit } from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {GroupsService} from "../../../services/groups.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private groupsService: GroupsService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
  }

  createFilterForm(){

    let mainGroupObj = {
        priority: [0, Validators.required]
    }

    this.languages.forEach((item: any) => {
        mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  onSubmit(){

      this.tryingAtOnceToSend = true;

      if(!this.mainGroup.invalid){
          let httpParams = new HttpParams();

          httpParams = httpParams.append('priority', this.mainGroup.get('priority').value);

          this.languages.forEach((item: any) => {
              httpParams = httpParams.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
          })

          this.loaderService.increaseLoaderCount()

          this.groupsService.add(httpParams)
              .subscribe((response: any) => {

                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.router.navigate(['/groups/list'])
                }
                else{
                    alert(response.message);
                }
              })
      }
  }

}
