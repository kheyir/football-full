import { Component, OnInit } from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {StadiumsService} from "../../../services/stadiums.service";
import {CitiesService} from "../../../services/cities.service";
import {CountriesService} from "../../../services/countries.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";

@Component({
  selector: 'edit-add',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  id;
  stadium = null;

  cities = null;
  countries = null;

  mainGroup: FormGroup;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private stadiumsService: StadiumsService,
      private citiesService: CitiesService,
      private countriesService: CountriesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();

    this.route.paramMap
        .subscribe(params => {
          this.id = params.get('id')

          this.getStadium();
        })
  }

  getStadium(){

    let data = {
      id: this.id
    }

    this.loaderService.increaseLoaderCount()

    this.stadium = null;

    this.stadiumsService.getStadiumById(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.stadium = response.stadium
          }
          else{
            this.router.navigate(["/404"])
          }
        })
  }

  createFilterForm(){

    let mainGroupObj = {
        city_id: [null, Validators.required],
        country_id: [null, Validators.required]
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;

    this.loaderService.decreaseLoaderCount()

    this.getCountries();
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getCities(data: any = {}){
    this.cities = null;

    this.loaderService.increaseLoaderCount();

    this.citiesService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.cities = response.cities;

                this.mainGroup.patchValue({
                    city_id: this.cities[0].id
                })
            }
        })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.countries = response.countries;

                if(this.countries.length > 0){

                    this.mainGroup.patchValue({
                        country_id: this.countries[0].id
                    })

                    let data = {
                        country_id: this.countries[0].id
                    }
                    this.getCities(data)
                }
            }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      let httpParams = new HttpParams();

      httpParams = httpParams.append('city_id', this.mainGroup.get('city_id').value);

      this.languages.forEach((item: any) => {
        httpParams = httpParams.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      let data = {
        id: this.id
      }

      this.loaderService.increaseLoaderCount()

      this.stadiumsService.edit(data, httpParams)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/stadiums/list'])
            }
            else{
                alert(response.message);
            }
          })
    }
  }

  onCountryChange(){
    let data = {
        country_id: this.mainGroup.get('country_id').value
    }

    this.getCities(data)
  }

}
