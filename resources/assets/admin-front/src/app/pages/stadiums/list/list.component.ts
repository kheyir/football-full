import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {StadiumsService} from "../../../services/stadiums.service";
import {CitiesService} from "../../../services/cities.service";
import {CountriesService} from "../../../services/countries.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  stadiums = null;
  cities = [{name: 'Hamısı', id: 0}];

  countries = null;

  public pageNumber: number = 1;
  public selectedId: number = 0;

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private stadiumsService: StadiumsService,
      private citiesService: CitiesService,
      private countriesService: CountriesService,
  ) { }

  ngOnInit() {

    this.getCountries();

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            name: paramsCopy.name ? paramsCopy.name : '',
            city_id: paramsCopy.city_id ? parseInt(paramsCopy.city_id) : 0,
            country_id: paramsCopy.country_id ? parseInt(paramsCopy.country_id) : 0
          })

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      name: ['', Validators.minLength(3)],
      city_id: [0],
      country_id: [0],
    })
  }

  getStadiums(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.stadiums = null;
    this.stadiumsService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.stadiums = response.stadiums;
          }
        })
  }

  getCities(data: any = {}){
      this.cities = null;

      this.loaderService.increaseLoaderCount();

      this.citiesService.getAll(data)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.cities = [{name: 'Hamısı', id: 0}, ...response.cities];

                this.filterGroup.patchValue({
                    city_id: this.cities[0].id
                })
            }
          })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.countries = [{name: 'Hamısı', id: 0}, ...response.countries];
            }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){
      const data = {
        pageNumber: this.pageNumber,

        name: this.filterGroup.get("name").value,
        city_id: this.filterGroup.get("city_id").value,
        country_id: this.filterGroup.get("country_id").value
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getStadiums(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.stadiumsService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

  onCountryChange(){
    let data = {
        country_id: this.filterGroup.get('country_id').value
    }

    this.getCities(data)
  }

}
