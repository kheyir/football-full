import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoriesService} from "../../../services/categories.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  categories = null;

  public pageNumber: number = 1;
  public selectedId: number = 0;

  isNewsCategoryStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  isFeaturedStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  isForVideoStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private categoriesService: CategoriesService,
  ) { }

  ngOnInit() {

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            name: paramsCopy.name ? paramsCopy.name : '',
              is_news_category: paramsCopy.is_news_category ? parseInt(paramsCopy.is_news_category) : 0,
              is_featured: paramsCopy.is_featured ? parseInt(paramsCopy.is_featured) : 0,
              is_for_video: paramsCopy.is_for_video ? parseInt(paramsCopy.is_for_video) : 0
          })

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      name: ['', Validators.minLength(3)],
      is_news_category: [0],
      is_featured: [0],
      is_for_video: [0]
    })
  }

  getCategories(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.categories = null;
    this.categoriesService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.categories = response.categories;
          }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){
      const data = {
        pageNumber: this.pageNumber,

        name: this.filterGroup.get("name").value,
        is_news_category: this.filterGroup.get("is_news_category").value,
        is_featured: this.filterGroup.get("is_featured").value,
        is_for_video: this.filterGroup.get("is_for_video").value,
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getCategories(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.categoriesService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

}
