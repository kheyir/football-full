import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoriesService} from "../../../services/categories.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";

@Component({
  selector: 'edit-add',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  id;
  category = null;

  mainGroup: FormGroup;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private categoriesService: CategoriesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();

    this.route.paramMap
        .subscribe(params => {
          this.id = params.get('id')

          this.getCategory();
        })
  }

  getCategory(){

    let data = {
      id: this.id
    }

    this.loaderService.increaseLoaderCount()

    this.category = null;

    this.categoriesService.getCategoryById(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.category = response.category
          }
          else{
            this.router.navigate(["/404"])
          }
        })
  }

  createFilterForm(){

    let mainGroupObj = {
      slug: [null, [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9-]+')]],
      is_news_category: [null],
      is_featured: [null],
      is_for_video: [null]
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;

    this.loaderService.decreaseLoaderCount()
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      formData.append('slug', this.mainGroup.get('slug').value);
      if(this.mainGroup.get('is_news_category').value) formData.append('is_news_category', this.mainGroup.get('is_news_category').value);
      if(this.mainGroup.get('is_featured').value) formData.append('is_featured', this.mainGroup.get('is_featured').value);
      if(this.mainGroup.get('is_for_video').value) formData.append('is_for_video', this.mainGroup.get('is_for_video').value);

      this.languages.forEach((item: any) => {
          formData.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      let data = {
        id: this.id
      }

      this.loaderService.increaseLoaderCount()

      this.categoriesService.edit(data, formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/categories/list'])
            }
            else{
                alert(response.message);
            }
          })
    }
  }

}
