import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MatchesService} from "../../../services/matches.service";
import {CountriesService} from "../../../services/countries.service";
import {StadiumsService} from "../../../services/stadiums.service";
import {TeamsService} from "../../../services/teams.service";
import {TournamentsService} from "../../../services/tournaments.service";
import {SeasonsService} from "../../../services/seasons.service";
import {StagesService} from "../../../services/stages.service";
import {ElementsService} from "../../../services/elements.service";
import {GroupsService} from "../../../services/groups.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  matches = null;
  countries = null;
  stadiums = [{name: 'Hamısı', id: 0}];
  homeTeams = null;
  guestTeams = null;
  tournaments = null;
  seasons = [{label: 'Hamısı', id: 0}];
  stages = null;
  elements = [{name: 'Hamısı', id: 0}];
  groups = [{name: 'Hamısı', id: 0}];

  matchStatuses = {
    "0": "Başlamayıb",
    "1": "Yekunlaşıb",
    "2": "Davam edir",
    "3": "Ləğv olunub",
    "4": "Təxirə salınıb"
  }

  statusStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Başlamayıb', value: 1},
    {label: 'Yekunlaşıb', value: 2},
    {label: 'Davam edir', value: 3},
    {label: 'Ləğv olunub', value: 4},
    {label: 'Təxirə salınıb', value: 5}
  ];

  isActiveStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  public pageNumber: number = 1;
  public selectedId: number = 0;

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private matchesService: MatchesService,
      private countriesService: CountriesService,
      private stadiumsService: StadiumsService,
      private teamsService: TeamsService,
      private tournamentsService: TournamentsService,
      private seasonsService: SeasonsService,
      private stagesService: StagesService,
      private elementsService: ElementsService,
      private groupsService: GroupsService,
  ) { }

  ngOnInit() {
    this.getCountries()
    this.getTeams()
    this.getTournaments()
    this.getStages()

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            country_id: paramsCopy.country_id ? parseInt(paramsCopy.country_id) : 0,
            stadium_id: paramsCopy.stadium_id ? parseInt(paramsCopy.stadium_id) : 0,
            status: paramsCopy.status ? parseInt(paramsCopy.status) : 0,
            min_start_date: paramsCopy.min_start_date && (new Date(paramsCopy.min_start_date)).getTime() ? (new Date(paramsCopy.min_start_date)) : null,
            max_start_date: paramsCopy.max_start_date && (new Date(paramsCopy.max_start_date)).getTime() ? (new Date(paramsCopy.max_start_date)) : null,
            is_active: paramsCopy.is_active ? parseInt(paramsCopy.is_active) : 0,
            home_team_id: paramsCopy.home_team_id ? parseInt(paramsCopy.home_team_id) : 0,
            guest_team_id: paramsCopy.guest_team_id ? parseInt(paramsCopy.guest_team_id) : 0,
            tournament_id: paramsCopy.tournament_id ? parseInt(paramsCopy.tournament_id) : 0,
            seasons_id: paramsCopy.seasons_id ? parseInt(paramsCopy.seasons_id) : 0,
            stage_id: paramsCopy.stage_id ? parseInt(paramsCopy.stage_id) : 0,
            element_id: paramsCopy.element_id ? parseInt(paramsCopy.element_id) : 0,
            group_id: paramsCopy.group_id ? parseInt(paramsCopy.group_id) : 0
          })

          this.onCountryChange()
          this.onTournamentChange()
          this.onStageChange()

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      country_id: [0],
      stadium_id: [0],
      status: [0],
      min_start_date: [null],
      max_start_date: [null],
      is_active: [0],
      home_team_id: [0],
      guest_team_id: [0],
      tournament_id: [0],
      seasons_id: [0],
      stage_id: [0],
      element_id: [0],
      group_id: [0]
    })
  }

  getMatches(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.matches = null;
    this.matchesService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.matches = response.matches;
          }
        })
  }

  getCountries(){
        this.countries = null;

        this.loaderService.increaseLoaderCount();

        this.countriesService.getAll()
            .subscribe((response: any) => {

                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.countries = [{name: 'Hamısı', id: 0}, ...response.countries]
                }
            })
  }

  getStadiums(data: any = {}){
        this.stadiums = null;

        this.loaderService.increaseLoaderCount();

        this.stadiumsService.getAll(data)
            .subscribe((response: any) => {

                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.stadiums = [{name: 'Hamısı', id: 0}, ...response.stadiums]
                }
            })
    }

  getTeams(){
    this.homeTeams = null;
    this.guestTeams = null;

    this.loaderService.increaseLoaderCount();

    this.teamsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.homeTeams = [{name: 'Hamısı', id: 0}, ...response.teams]
                this.guestTeams = [{name: 'Hamısı', id: 0}, ...response.teams]
            }
        })
  }

  getTournaments(){
    this.tournaments = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.tournaments = [{name: 'Hamısı', id: 0}, ...response.tournaments]
            }
        })
  }

  getSeasons(data: any = {}){
    this.seasons = null;

    this.loaderService.increaseLoaderCount();

    this.seasonsService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.seasons = response.seasons.map(item => {
                    item.label = item.start_year + (item.end_year ? ' - ' + item.end_year : '')
                    return item
                })
                this.seasons = [{label: 'Hamısı', id: 0}, ...this.seasons]
            }
        })
  }

  getStages(){
    this.stages = null;

    this.loaderService.increaseLoaderCount();

    this.stagesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.stages = [{name: 'Hamısı', id: 0}, ...response.stages]
            }
        })
  }

  getElements(data: any = {}){
    this.elements = null;

    this.loaderService.increaseLoaderCount();

    this.elementsService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.elements = [{name: 'Hamısı', id: 0}, ...response.elements]
            }
        })
  }

  getGroups(data: any = {}){
    this.groups = null;

    this.loaderService.increaseLoaderCount();

    this.groupsService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.groups = [{name: 'Hamısı', id: 0}, ...response.groups]
            }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){

      let minDate = this.filterGroup.get("min_start_date").value
      let maxDate = this.filterGroup.get("max_start_date").value

      console.log(this.filterGroup.get("min_start_date").value)
      console.log((new Date(minDate)).getTime())

      const data = {
        pageNumber: this.pageNumber,

        country_id: this.filterGroup.get("country_id").value,
        stadium_id: this.filterGroup.get("stadium_id").value,
        status: this.filterGroup.get("status").value,
        min_start_date: (new Date(minDate)).getTime() ? minDate.getFullYear() + '-' + (minDate.getMonth()+1) + '-' + minDate.getDate() + ' ' + minDate.getHours() + ':' + minDate.getMinutes() + ':' + minDate.getSeconds() : '',
        max_start_date: (new Date(maxDate)).getTime() ? maxDate.getFullYear() + '-' + (maxDate.getMonth()+1) + '-' + maxDate.getDate() + ' ' + maxDate.getHours() + ':' + maxDate.getMinutes() + ':' + maxDate.getSeconds() : '',
        is_active: this.filterGroup.get("is_active").value,
        home_team_id: this.filterGroup.get("home_team_id").value,
        guest_team_id: this.filterGroup.get("guest_team_id").value,
        tournament_id: this.filterGroup.get("tournament_id").value,
        seasons_id: this.filterGroup.get("seasons_id").value,
        stage_id: this.filterGroup.get("stage_id").value,
        element_id: this.filterGroup.get("element_id").value,
        group_id: this.filterGroup.get("group_id").value
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getMatches(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.matchesService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

  onCountryChange(){
      if(this.filterGroup.get('country_id').value){
          let data = {
              country_id: this.filterGroup.get('country_id').value
          }

          this.getStadiums(data);
      }
      else{
          this.stadiums = [{name: 'Hamısı', id: 0}];
      }
  }

  onTournamentChange(){
    if(this.filterGroup.get('tournament_id').value){
        let data = {
            tournament_id: this.filterGroup.get('tournament_id').value
        }

        this.getSeasons(data);
    }
    else{
        this.seasons = [{label: 'Hamısı', id: 0}]

        this.filterGroup.patchValue({
            season_id: 0
        })
    }
  }

  onStageChange(){
      if(this.filterGroup.get('stage_id').value){
          let data = {
              stage_id: this.filterGroup.get('stage_id').value
          }

          this.getElements(data);
          this.getGroups(data);
      }
      else{
        this.elements = [{name: 'Hamısı', id: 0}];
        this.groups = [{name: 'Hamısı', id: 0}];
      }
  }

}
