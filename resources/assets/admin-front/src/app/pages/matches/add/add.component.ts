import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from "@angular/forms";
import {Router} from "@angular/router";
import {MatchesService} from "../../../services/matches.service";
import {CountriesService} from "../../../services/countries.service";
import {StadiumsService} from "../../../services/stadiums.service";
import {TeamsService} from "../../../services/teams.service";
import {TournamentsService} from "../../../services/tournaments.service";
import {SeasonsService} from "../../../services/seasons.service";
import {StagesService} from "../../../services/stages.service";
import {ElementsService} from "../../../services/elements.service";
import {GroupsService} from "../../../services/groups.service";
import {PlayersService} from "../../../services/players.service";
import {CategoriesService} from "../../../services/categories.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {FileTypeValidator} from '../../../directives/file-type-validator.directive';
import {FileSizeValidator} from '../../../directives/file-size-validator.directive';
import {dependencyArrayOfCheckboxReverseAndHasGroups, dependencyFieldOfCheckboxReverse, fileType, fileSize} from '../../../validators'

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  countries = null;
  stadiums = null;
  tournaments = null;
  seasons = null;
  stages = null;
  elements = null;
  groups = null;
  teams = null;
  forSelectTeams = null;
  forVideosTeams = null;
  categories = null;
  players = null;

  statusStatuses = [
    {label: 'Başlamayıb', value: 0},
    {label: 'Yekunlaşıb', value: 1},
    {label: 'Davam edir', value: 2},
    {label: 'Ləğv olunub', value: 3},
    {label: 'Təxirə salınıb', value: 4}
  ];

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  get videos(){
    return this.mainGroup.get('videos') || null;
  }

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private matchesService: MatchesService,
      private countriesService: CountriesService,
      private stadiumsService: StadiumsService,
      private teamsService: TeamsService,
      private tournamentsService: TournamentsService,
      private seasonsService: SeasonsService,
      private stagesService: StagesService,
      private elementsService: ElementsService,
      private groupsService: GroupsService,
      private playersService: PlayersService,
      private categoriesService: CategoriesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
  }

  createFilterForm(){

    let mainGroupObj = {
      country_id: [null, Validators.required],
      stadium_id: [null, Validators.required],
      friendly_match: [null],
      tournament_id: [null],
      season_id: [null],
      stage: [null],
      element_id: [null],
      group_id: [null],
      start_date: [null, Validators.required],
      home_team_id: [null, Validators.required],
      guest_team_id: [null, Validators.required],
      home_team_goals_count: [null, [Validators.required, Validators.min(0), Validators.max(200)]],
      guest_team_goals_count: [null, [Validators.required, Validators.min(0), Validators.max(200)]],
      status: [null, Validators.required],
      is_active: [1],
      preview_image: [null, [FileTypeValidator, FileSizeValidator]],

      videos: this.formBuilder.array([])
    }

    this.mainGroup = this.formBuilder.group(mainGroupObj, {
      validators: [
        dependencyFieldOfCheckboxReverse('friendly_match', 'tournament_id'),
        dependencyFieldOfCheckboxReverse('friendly_match', 'season_id'),
        dependencyFieldOfCheckboxReverse('friendly_match', 'stage'),
        dependencyFieldOfCheckboxReverse('friendly_match', 'element_id'),
        dependencyArrayOfCheckboxReverseAndHasGroups('friendly_match', 'stage', 'group_id'),
      ]
    });

    this.showForm = true;

    this.getCountries()
    this.getTournaments()
    this.getVideoCategories()
    this.getPlayers()
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.countries = response.countries
          }
        })
  }

  getStadiums(data: any = {}){
    this.stadiums = null;

    this.loaderService.increaseLoaderCount();

    this.stadiumsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.stadiums = response.stadiums
          }
        })
  }

  getTeams(data: any = {}){
    this.teams = null;

    this.mainGroup.patchValue({
      home_team_id: null,
      guest_team_id: null
    })

    this.loaderService.increaseLoaderCount();

    this.teamsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.teams = response.teams
            this.onTeamsChange()
          }
        })
  }

  getTournaments(){
    this.tournaments = null;

    this.mainGroup.patchValue({
      tournament_id: null
    })

    this.loaderService.increaseLoaderCount();

    this.tournamentsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.tournaments = response.tournaments
          }
        })
  }

  getSeasons(data: any = {}){
    this.seasons = null;

    this.mainGroup.patchValue({
      season_id: null
    })

    this.loaderService.increaseLoaderCount();

    this.seasonsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.seasons = response.seasons.map(item => {
              item.label = item.start_year + (item.end_year ? ' - ' + item.end_year : '')
              return item
            })
          }
        })
  }

  getStages(data: any = {}){
    this.stages = null;
    this.elements = null;
    this.groups = null;
    this.mainGroup.patchValue({
      stage: null,
      element_id: null,
      group_id: null
    })

    this.loaderService.increaseLoaderCount();

    this.stagesService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.stages = response.stages
          }
        })
  }

  getElements(data: any = {}){
    this.elements = null;

    this.mainGroup.patchValue({
      element_id: null
    })

    this.loaderService.increaseLoaderCount();

    this.elementsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.elements = response.elements
          }
        })
  }

  getGroups(data: any = {}){
    this.groups = null;

    this.mainGroup.patchValue({
      group_id: null
    })

    this.loaderService.increaseLoaderCount();

    this.groupsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.groups = response.groups
          }
        })
  }

  getPlayers(data: any = {}){

    this.loaderService.increaseLoaderCount();

    this.playersService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.players = response.players
          }
        })
  }

  getVideoCategories(){
    this.categories = null;

    let data = {
      is_for_video: 1
    }

    this.loaderService.increaseLoaderCount();

    this.categoriesService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.categories = response.categories
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      const startTime = this.mainGroup.get('start_date').value;

      formData.append('stadium_id', this.mainGroup.get('stadium_id').value);
      if(this.mainGroup.get('friendly_match').value) formData.append('friendly_match', this.mainGroup.get('friendly_match').value);
      if(!this.mainGroup.get('friendly_match').value) formData.append('season_id', this.mainGroup.get('season_id').value);
      if(!this.mainGroup.get('friendly_match').value) formData.append('stage_id', this.mainGroup.get('stage').value.id);
      if(!this.mainGroup.get('friendly_match').value) formData.append('element_id', this.mainGroup.get('element_id').value);
      if(!this.mainGroup.get('friendly_match').value && this.mainGroup.get('stage').value.has_groups) formData.append('has_groups', this.mainGroup.get('stage').value.has_groups);
      if(!this.mainGroup.get('friendly_match').value && this.mainGroup.get('stage').value.has_groups) formData.append('group_id', this.mainGroup.get('group_id').value);
      formData.append('start_time', startTime.getFullYear() + '-' + (startTime.getMonth()+1) + '-' + startTime.getDate() + ' ' + startTime.getHours() + ':' + startTime.getMinutes() + ':' + startTime.getSeconds());
      formData.append('home_team_id', this.mainGroup.get('home_team_id').value);
      formData.append('guest_team_id', this.mainGroup.get('guest_team_id').value);
      formData.append('home_team_goals_count', this.mainGroup.get('home_team_goals_count').value);
      formData.append('guest_team_goals_count', this.mainGroup.get('guest_team_goals_count').value);
      formData.append('status', this.mainGroup.get('status').value);
      if(this.mainGroup.get('is_active').value) formData.append('is_active', this.mainGroup.get('is_active').value);
      if(this.mainGroup.get('preview_image').value) formData.append('preview_image', this.mainGroup.get('preview_image').value);

      if(this.mainGroup.get('videos').value.length > 0){
        this.mainGroup.get('videos').value.forEach((item: any) => {
          formData.append('file[]', item.file);
          formData.append('categories[]', item.categories);
          formData.append('video_preview_image[]', item.preview_image);
          formData.append('team_id[]', item.team_id);
          formData.append('player_id[]', item.player_id);
          formData.append('minute[]', item.minute);
          if(item.is_top_goal) formData.append('is_top_goal[]', '1');
          else formData.append('is_top_goal[]', '0');
        })
      }

      this.loaderService.increaseLoaderCount()

      this.matchesService.add(formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/matches/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

  onFileChange(event){

    let formControlNameObj = {}

    if(event.target.files && event.target.files.length) {

      let file = event.target.files[0];

      formControlNameObj[event.target.dataset.formControlName] = file
    }
    else{
      formControlNameObj[event.target.dataset.formControlName] = null;
    }

    this.mainGroup.patchValue(formControlNameObj);

    this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

  initItems(formControlName){
    switch (formControlName){
      case 'videos':
          return this.formBuilder.group({
            file: [null, Validators.required],
            categories: [null, Validators.required],
            preview_image: [null, [Validators.required, FileTypeValidator, FileSizeValidator]],
            team_id: [null, Validators.required],
            player_id: [null, Validators.required],
            minute: [null, [Validators.required, Validators.min(0), Validators.max(200)]],
            is_top_goal: [null]
          }, {
            validators: [
              fileType('file', ["video/mp4", "video/webm"]),
              fileSize('file', 150000000)
            ]
          })
      default:
        return this.formBuilder.group({
          file: [null, Validators.required],
          categories: [null, Validators.required],
          preview_image: [null, [Validators.required, FileTypeValidator, FileSizeValidator]],
          team_id: [null, Validators.required],
          player_id: [null, Validators.required],
          minute: [null, Validators.required],
          is_top_goal: [null]
        }, {
          validators: [
            fileType('file', ["video/mp4", "video/webm"]),
            fileSize('file', 150000000)
          ]
        })
    }
  }

  addToTable(event){

    const controls = <FormArray>this.mainGroup.get(event.currentTarget.dataset.formControlName) ;

    controls.push(this.initItems(event.currentTarget.dataset.formControlName));
  }

  deleteFromTable(event, index){
    const controls = <FormArray>this.mainGroup.get(event.currentTarget.dataset.formControlName);

    controls.removeAt(index)
  }

  onTeamsChange(){
    this.forSelectTeams = this.teams.filter((teamItem) => {
      return teamItem.id != this.mainGroup.get("home_team_id").value && teamItem.id != this.mainGroup.get("guest_team_id").value
    });

    this.forVideosTeams = this.teams.filter((teamItem) => {
      return teamItem.id == this.mainGroup.get("home_team_id").value || teamItem.id == this.mainGroup.get("guest_team_id").value
    });

    this.mainGroup['controls']['videos']['controls'].map((videoItem: any) => {
      if(videoItem.get("team_id").value != this.mainGroup.get("home_team_id").value && videoItem.get("team_id").value != this.mainGroup.get("guest_team_id").value){
        videoItem.patchValue({
          team_id: null
        })
      }
    })
  }

  onCountryChange(){
    if(this.mainGroup.get('country_id').value){
      let data = {
        country_id: this.mainGroup.get('country_id').value
      }

      this.getStadiums(data);
    }
    else{
      this.stadiums = [{name: 'Hamısı', id: 0}];
    }
  }

  onTournamentChange(){

    this.seasons = null;
    this.stages = null;
    this.elements = null;
    this.groups = null;
    this.teams = [];

    this.onTeamsChange()

    this.mainGroup.patchValue({
      season_id: null,
      stage: null,
      element_id: null,
      group_id: null,
      home_team_id: null,
      guest_team_id: null
    })

    if(this.mainGroup.get('tournament_id').value){
      let data = {
        tournament_id: this.mainGroup.get('tournament_id').value
      }

      this.getSeasons(data);
    }
  }

  onSeasonChange(){

    this.stages = null;
    this.elements = null;
    this.groups = null;
    this.teams = [];

    this.onTeamsChange()

    this.mainGroup.patchValue({
      stage: null,
      element_id: null,
      group_id: null,
      home_team_id: null,
      guest_team_id: null
    })

    if(this.mainGroup.get('season_id').value){
      let data = {
        season_id: this.mainGroup.get('season_id').value
      }

      this.getStages(data);
      this.getTeams(data);
    }
  }

  onFriendlyMatchChange(){

    this.mainGroup.patchValue({
      home_team_id: null,
      guest_team_id: null
    })

    this.teams = [];

    this.onTeamsChange()

    if(this.mainGroup.get("friendly_match")){
      this.getTeams()
    }
  }

  onStageChange(){

    this.elements = null;
    this.groups = null;

    let patchObj: any = {
      element_id: null,
      group_id: null
    }

    if(this.mainGroup.get('stage').value.has_groups){
      this.teams = []

      this.onTeamsChange()

      patchObj.home_team_id = null;
      patchObj.guest_team_id = null;
    }
    else{

      let data = {
        season_id: this.mainGroup.get('season_id').value
      }

      this.getTeams(data);
    }

    this.mainGroup.patchValue(patchObj)

    if(this.mainGroup.get('stage').value && this.mainGroup.get('stage').value.id){
      let data = {
        stage_id: this.mainGroup.get('stage').value.id
      }

      this.getElements(data);
      this.getGroups(data);
    }
  }

  onGroupChange(){

    this.teams = [];
    this.onTeamsChange()

    this.mainGroup.patchValue({
      home_team_id: null,
      guest_team_id: null
    })

    if(this.mainGroup.get('season_id').value && this.mainGroup.get('group_id').value){
      let data = {
        season_id: this.mainGroup.get('season_id').value,
        group_id: this.mainGroup.get('group_id').value
      }

      this.getTeams(data);
    }
  }

  onVideoFileChange(event, index){

    let formControlNameObj = {}

    if(event.currentTarget.files && event.currentTarget.files.length) {

      let file = event.currentTarget.files[0];

      formControlNameObj[event.currentTarget.dataset.formControlName] = file
    }
    else{
      formControlNameObj[event.currentTarget.dataset.formControlName] = null;
    }

    this.mainGroup['controls']['videos']['controls'][index].patchValue(formControlNameObj);

    this.mainGroup['controls']['videos']['controls'][index]['controls'][event.currentTarget.dataset.formControlName].markAsDirty()
  }

}
