import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TournamentsService} from "../../../services/tournaments.service";
import {CountriesService} from "../../../services/countries.service";
import {TournamentsCategoriesService} from "../../../services/tournaments-categories.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {FileTypeValidator} from '../../../directives/file-type-validator.directive';
import {FileSizeValidator} from '../../../directives/file-size-validator.directive';
import {dependencyNumericOfCheckbox} from '../../../validators';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  countries = null;
  tournamentsCategories = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private tournamentsService: TournamentsService,
      private countriesService: CountriesService,
      private tournamentsCategoriesService: TournamentsCategoriesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
  }

  createFilterForm(){

    let mainGroupObj = {
      slug: [null, [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9-]+')]],
      tournaments_category_id: [null, Validators.required],
      country_id: [0, Validators.required],
      is_news_category: [null],
      is_videos_category: [null],
      is_league: [null],
      priority_for_country: [null],
      league_victory_points: [3],
      league_draw_points: [1],
      logo_mini: [null, [Validators.required, FileTypeValidator, FileSizeValidator]],
      logo_big: [null, [Validators.required, FileTypeValidator, FileSizeValidator]]
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj, {
      validators: [
        dependencyNumericOfCheckbox('is_league', 'league_victory_points'),
        dependencyNumericOfCheckbox('is_league', 'league_draw_points')
      ]
    });

    this.showForm = true;


    this.getCountries();
    this.getTournamentsCategories();
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.countries = [{name: 'Yoxdur', id: 0}, ...response.countries];
          }
        })
  }

  getTournamentsCategories(){
    this.tournamentsCategories = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsCategoriesService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.tournamentsCategories = response.tournamentsCategories;
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      formData.append('slug', this.mainGroup.get('slug').value);
      formData.append('tournaments_category_id', this.mainGroup.get('tournaments_category_id').value);
      formData.append('country_id', this.mainGroup.get('country_id').value);
      if(this.mainGroup.get('is_news_category').value) formData.append('is_news_category', this.mainGroup.get('is_news_category').value);
      if(this.mainGroup.get('is_videos_category').value) formData.append('is_videos_category', this.mainGroup.get('is_videos_category').value);
      if(this.mainGroup.get('is_league').value) formData.append('is_league', this.mainGroup.get('is_league').value);
      if(this.mainGroup.get('is_league').value && this.mainGroup.get('priority_for_country').value) formData.append('priority_for_country', this.mainGroup.get('priority_for_country').value);
      if(this.mainGroup.get('is_league').value) formData.append('league_victory_points', this.mainGroup.get('league_victory_points').value);
      if(this.mainGroup.get('is_league').value) formData.append('league_draw_points', this.mainGroup.get('league_draw_points').value);
      formData.append('logo_mini', this.mainGroup.get('logo_mini').value);
      formData.append('logo_big', this.mainGroup.get('logo_big').value);

      this.languages.forEach((item: any) => {
        formData.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      this.loaderService.increaseLoaderCount()

      this.tournamentsService.add(formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/tournaments/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

  onFileChange(event){

    let formControlNameObj = {}

    if(event.target.files && event.target.files.length) {

      let file = event.target.files[0];

      formControlNameObj[event.target.dataset.formControlName] = file
    }
    else{
      formControlNameObj[event.target.dataset.formControlName] = null;
    }

    this.mainGroup.patchValue(formControlNameObj);

    this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

}
