import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {TournamentsService} from "../../../services/tournaments.service";
import {CountriesService} from "../../../services/countries.service";
import {TournamentsCategoriesService} from "../../../services/tournaments-categories.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  tournaments = null;
  countries = null;
  tournamentsCategories = null;

  public pageNumber: number = 1;
  public selectedId: number = 0;

  isNewsCategoryStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  isVideoCategoryStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  priorityForCountryStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  isLeagueStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private tournamentsService: TournamentsService,
      private countriesService: CountriesService,
      private tournamentsCategoriesService: TournamentsCategoriesService,
  ) { }

  ngOnInit() {
    this.getCountries()
    this.getTournamentsCategories()

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            name: paramsCopy.name ? paramsCopy.name : '',
            tournaments_category_id: paramsCopy.tournaments_category_id ? parseInt(paramsCopy.tournaments_category_id) : 0,
            country_id: paramsCopy.country_id ? parseInt(paramsCopy.country_id) : -1,
            is_news_category: paramsCopy.is_news_category ? parseInt(paramsCopy.is_news_category) : 0,
            is_videos_category: paramsCopy.is_videos_category ? parseInt(paramsCopy.is_videos_category) : 0,
            priority_for_country: paramsCopy.priority_for_country ? parseInt(paramsCopy.priority_for_country) : 0,
            is_league: paramsCopy.is_league ? parseInt(paramsCopy.is_league) : 0,
          })

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      name: ['', Validators.minLength(3)],
      tournaments_category_id: [0],
      country_id: [-1],
      is_news_category: [0],
      is_videos_category: [0],
      priority_for_country: [0],
      is_league: [0],
    })
  }

  getTournaments(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.tournaments = null;
    this.tournamentsService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.tournaments = response.tournaments;
          }
        })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.countries = [{name: 'Hamısı', id: -1}, {name: 'Yoxdur', id: 0}, ...response.countries]
            }
        })
  }

  getTournamentsCategories(){
    this.tournamentsCategories = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsCategoriesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.tournamentsCategories = [{name: 'Hamısı', id: 0}, ...response.tournamentsCategories]
            }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){
      const data = {
        pageNumber: this.pageNumber,

        name: this.filterGroup.get("name").value,
        tournaments_category_id: this.filterGroup.get("tournaments_category_id").value,
        country_id: this.filterGroup.get("country_id").value,
        is_news_category: this.filterGroup.get("is_news_category").value,
        is_videos_category: this.filterGroup.get("is_videos_category").value,
        priority_for_country: this.filterGroup.get("priority_for_country").value,
        is_league: this.filterGroup.get("is_league").value,
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getTournaments(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.tournamentsService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

}
