import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {TeamsService} from "../../../services/teams.service";
import {CountriesService} from "../../../services/countries.service";
import {StadiumsService} from "../../../services/stadiums.service";
import {CoachesService} from "../../../services/coaches.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {FileTypeValidator} from '../../../directives/file-type-validator.directive';
import {FileSizeValidator} from '../../../directives/file-size-validator.directive';

@Component({
  selector: 'edit-add',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  id;
  team = null;

  mainGroup: FormGroup;

  countries = null;
  stadiums = null;
  coaches = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private teamsService: TeamsService,
      private countriesService: CountriesService,
      private stadiumsService: StadiumsService,
      private coachesService: CoachesService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();

    this.route.paramMap
        .subscribe(params => {
          this.id = params.get('id')

          this.getTeam();
        })
  }

  getTeam(){

    let data = {
      id: this.id
    }

    this.loaderService.increaseLoaderCount()

    this.team = null;

    this.teamsService.getTeamById(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.team = response.team;

            let data = {
              country_id: this.team.country_id
            }

            this.getStadiums(data)
          }
          else{
            this.router.navigate(["/404"])
          }
        })
  }

  createFilterForm(){

    let mainGroupObj = {
        slug: [null, [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9-]+')]],
        country_id: [null, Validators.required],
        stadium_id: [0],
        coach_id: [null, Validators.required],
        logo: [null, [FileTypeValidator, FileSizeValidator]],
        year: [0, Validators.min(1300)],
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;

    this.loaderService.decreaseLoaderCount()


    this.getCountries();
    this.getCoach();
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.countries = response.countries;
            }
        })
  }

  getStadiums(data: any = {}){
    this.stadiums = null;

    this.loaderService.increaseLoaderCount();

    this.stadiumsService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.stadiums = [{name: 'Yoxdur', id: 0}, ...response.stadiums];
            }
        })
  }

  getCoach(){
    this.coaches = null;

    this.loaderService.increaseLoaderCount();

    this.coachesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.coaches = response.coaches;
            }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      formData.append('slug', this.mainGroup.get('slug').value);
      formData.append('country_id', this.mainGroup.get('country_id').value);
      if(this.mainGroup.get('stadium_id').value) formData.append('stadium_id', this.mainGroup.get('stadium_id').value);
      formData.append('coach_id', this.mainGroup.get('coach_id').value);
      if(this.mainGroup.get('logo').value)formData.append('logo', this.mainGroup.get('logo').value);
      formData.append('year', this.mainGroup.get('year').value);

      this.languages.forEach((item: any) => {
          formData.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      let data = {
        id: this.id
      }

      this.loaderService.increaseLoaderCount()

      this.teamsService.edit(data, formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/teams/list'])
            }
            else{
                alert(response.message);
            }
          })
    }
  }

  onFileChange(event){

      let formControlNameObj = {}

      if(event.target.files && event.target.files.length) {

          let file = event.target.files[0];

          formControlNameObj[event.target.dataset.formControlName] = file
      }
      else{
          formControlNameObj[event.target.dataset.formControlName] = null;
      }

      this.mainGroup.patchValue(formControlNameObj);

      this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

  onCountryChange(){

    if(this.mainGroup.get('country_id').value){

        let data = {
            country_id: this.mainGroup.get('country_id').value
        }

        this.getStadiums(data)
    }
  }

}
