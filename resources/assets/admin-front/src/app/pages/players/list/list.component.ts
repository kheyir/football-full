import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {PlayersService} from "../../../services/players.service";
import {CountriesService} from "../../../services/countries.service";
import {PositionsService} from "../../../services/positions.service";
import {TeamsService} from "../../../services/teams.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  players = null;
  countries = null;
  positions = null;
  teams = null;

  public pageNumber: number = 1;
  public selectedId: number = 0;

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private playersService: PlayersService,
      private countriesService: CountriesService,
      private positionsService: PositionsService,
      private teamsService: TeamsService,
  ) { }

  ngOnInit() {
    this.getCountries()
    this.getPositions()
    this.getTeams()

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            name: paramsCopy.name ? paramsCopy.name : '',
            country_id: paramsCopy.country_id ? parseInt(paramsCopy.country_id) : 0,
            position_id: paramsCopy.position_id ? parseInt(paramsCopy.position_id) : 0,
            team_id: paramsCopy.team_id ? parseInt(paramsCopy.team_id) : 0,
          })

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      name: ['', Validators.minLength(3)],
      country_id: [0],
      position_id: [0],
      team_id: [0],
    })
  }

  getPlayers(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.players = null;
    this.playersService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.players = response.players;
          }
        })
  }

  getCountries(){
        this.countries = null;

        this.loaderService.increaseLoaderCount();

        this.countriesService.getAll()
            .subscribe((response: any) => {

                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.countries = [{name: 'Hamısı', id: 0}, ...response.countries]
                }
            })
  }

  getPositions(){
        this.positions = null;

        this.loaderService.increaseLoaderCount();

        this.positionsService.getAll()
            .subscribe((response: any) => {

                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.positions = [{name: 'Hamısı', id: 0}, ...response.positions]
                }
            })
    }

  getTeams(){
    this.teams = null;

    this.loaderService.increaseLoaderCount();

    this.teamsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.teams = [{name: 'Hamısı', id: 0}, ...response.teams]
            }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){
      const data = {
        pageNumber: this.pageNumber,

        name: this.filterGroup.get("name").value,
        country_id: this.filterGroup.get("country_id").value,
        position_id: this.filterGroup.get("position_id").value,
        team_id: this.filterGroup.get("team_id").value,
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getPlayers(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.playersService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

}
