import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from "@angular/forms";
import {Router} from "@angular/router";
import {PlayersService} from "../../../services/players.service";
import {CountriesService} from "../../../services/countries.service";
import {PositionsService} from "../../../services/positions.service";
import {TeamsService} from "../../../services/teams.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {FileTypeValidator} from '../../../directives/file-type-validator.directive';
import {FileSizeValidator} from '../../../directives/file-size-validator.directive';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  countries = null;
  positions = null;
  teams = null;
  notSelectedTeams = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  get teamsAndNumbers(){
    return this.mainGroup.get('teamsAndNumbers') || null;
  }

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private playersService: PlayersService,
      private countriesService: CountriesService,
      private positionsService: PositionsService,
      private teamsService: TeamsService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
  }

  createFilterForm(){

    let mainGroupObj = {
      country_id: [null, Validators.required],
      position_id: [null, Validators.required],
      photo: [null, [FileTypeValidator, FileSizeValidator]],
      date_of_birthday: [null, Validators.required],
      height: [null],
      weight: [null],
      teamsAndNumbers: this.formBuilder.array([])
    }

    this.languages.forEach((item: any) => {
      mainGroupObj['name[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    })

    this.mainGroup = this.formBuilder.group(mainGroupObj);

    this.showForm = true;


    this.getCountries();
    this.getPositions();
    this.getTeams();
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.countries = response.countries;
          }
        })
  }

  getPositions(){
    this.positions = null;

    this.loaderService.increaseLoaderCount();

    this.positionsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.positions = response.positions;
          }
        })
  }

  getTeams(){
    this.teams = null;

    this.loaderService.increaseLoaderCount();

    this.teamsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.teams = response.teams;
            this.notSelectedTeams = response.teams;
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      const dateOfBirthday = this.mainGroup.get('date_of_birthday').value;

      formData.append('country_id', this.mainGroup.get('country_id').value);
      formData.append('position_id', this.mainGroup.get('position_id').value);
      if(this.mainGroup.get('photo').value) formData.append('photo', this.mainGroup.get('photo').value);
      formData.append('date_of_birthday', dateOfBirthday.year + '-' + dateOfBirthday.month + '-' + dateOfBirthday.day);
      if(this.mainGroup.get('height').value) formData.append('height', this.mainGroup.get('height').value);
      if(this.mainGroup.get('weight').value) formData.append('weight', this.mainGroup.get('weight').value);

      if(this.mainGroup.get('teamsAndNumbers').value.length > 0){
        this.mainGroup.get('teamsAndNumbers').value.forEach((item: any) => {
          formData.append('team_id[]', item.team_id);
          formData.append('number[]', item.number);
        })
      }

      this.languages.forEach((item: any) => {
        formData.append(`name[${item.locale}]`, this.mainGroup.get('name[' + item.locale + ']').value);
      })

      this.loaderService.increaseLoaderCount()

      this.playersService.add(formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/players/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

  onFileChange(event){

    let formControlNameObj = {}

    if(event.target.files && event.target.files.length) {

      let file = event.target.files[0];

      formControlNameObj[event.target.dataset.formControlName] = file
    }
    else{
      formControlNameObj[event.target.dataset.formControlName] = null;
    }

    this.mainGroup.patchValue(formControlNameObj);

    this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

  initItems(formControlName){
    switch (formControlName){
      case 'teamsAndNumbers':
          return this.formBuilder.group({
            team_id: [null, Validators.required],
            number: [null, Validators.required]
          })
      default:
        return this.formBuilder.group({
          team_id: [null, Validators.required],
          number: [null, Validators.required]
        })
    }
  }

  addToTable(event){

    const controls = <FormArray>this.mainGroup.get(event.currentTarget.dataset.formControlName) ;

    controls.push(this.initItems(event.currentTarget.dataset.formControlName));
  }

  deleteFromTable(event, index){
    const controls = <FormArray>this.mainGroup.get(event.currentTarget.dataset.formControlName);

    controls.removeAt(index)
  }

  onTeamsChange(){
    let arrayOfTeamIds = new Array<number>();
    this.mainGroup.get('teamsAndNumbers').value.map((teamAndNumber) => {
      arrayOfTeamIds.push(teamAndNumber.team_id)
    })

    this.notSelectedTeams = this.teams.filter(function(notSelectedTeamItem) {
      return arrayOfTeamIds.indexOf(notSelectedTeamItem.id) == -1
    });
  }

}
