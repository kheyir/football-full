import { Component, OnInit } from '@angular/core';
import {LoaderService} from "../../services/loader.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  constructor(
      private loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.loaderService.decreaseLoaderCount()
  }

}
