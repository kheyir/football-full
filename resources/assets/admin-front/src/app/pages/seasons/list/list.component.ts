import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {SeasonsService} from "../../../services/seasons.service";
import {CountriesService} from "../../../services/countries.service";
import {TournamentsService} from "../../../services/tournaments.service";
import {TournamentsCategoriesService} from "../../../services/tournaments-categories.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  seasons = null;
  tournaments = null;
  tournamentsCategories = null;
  countries = null;

  public pageNumber: number = 1;
  public selectedId: number = 0;

  isLeagueStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private seasonsService: SeasonsService,
      private countriesService: CountriesService,
      private tournamentsService: TournamentsService,
      private tournamentsCategoriesService: TournamentsCategoriesService,
  ) { }

  ngOnInit() {
    this.getTournaments()
    this.getTournamentsCategories()
    this.getCountries()

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            name: paramsCopy.name ? paramsCopy.name : '',
            tournament_id: paramsCopy.tournament_id ? parseInt(paramsCopy.tournament_id) : 0,
            tournaments_category_id: paramsCopy.tournaments_category_id ? parseInt(paramsCopy.tournaments_category_id) : 0,
            country_id: paramsCopy.country_id ? parseInt(paramsCopy.country_id) : -1,
            is_league: paramsCopy.is_league ? parseInt(paramsCopy.is_league) : 0,
            start_year: paramsCopy.start_year ? parseInt(paramsCopy.start_year) : '',
            end_year: paramsCopy.end_year ? parseInt(paramsCopy.end_year) : '',
          })

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      name: ['', Validators.minLength(3)],
      tournament_id: [0],
      tournaments_category_id: [0],
      country_id: [-1],
      is_league: [0],
      start_year: [null],
      end_year: [null],
    })
  }

  getSeasons(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.seasons = null;
    this.seasonsService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.seasons = response.seasons;
          }
        })
  }

  getCountries(){
        this.countries = null;

        this.loaderService.increaseLoaderCount();

        this.countriesService.getAll()
            .subscribe((response: any) => {

                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.countries = [{name: 'Hamısı', id: -1}, {name: 'Yoxdur', id: 0}, ...response.countries]
                }
            })
  }

  getTournaments(){
    this.tournaments = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.tournaments = [{name: 'Hamısı', id: 0}, ...response.tournaments]
            }
        })
  }

  getTournamentsCategories(){
    this.tournamentsCategories = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsCategoriesService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.tournamentsCategories = [{name: 'Hamısı', id: 0}, ...response.tournamentsCategories]
            }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){
      const data = {
        pageNumber: this.pageNumber,

        name: this.filterGroup.get("name").value,
        tournament_id: this.filterGroup.get("tournament_id").value,
        tournaments_category_id: this.filterGroup.get("tournaments_category_id").value,
        country_id: this.filterGroup.get("country_id").value,
        is_league: this.filterGroup.get("is_league").value,
        start_year: this.filterGroup.get("start_year").value,
        end_year: this.filterGroup.get("end_year").value,
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getSeasons(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.seasonsService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

}
