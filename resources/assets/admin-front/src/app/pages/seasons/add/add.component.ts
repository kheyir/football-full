import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from "@angular/forms";
import {Router} from "@angular/router";
import {SeasonsService} from "../../../services/seasons.service";
import {TournamentsService} from "../../../services/tournaments.service";
import {TeamsService} from "../../../services/teams.service";
import {StagesService} from "../../../services/stages.service";
import {ElementsService} from "../../../services/elements.service";
import {GroupsService} from "../../../services/groups.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {dependencyFieldOfCheckbox, dependencyFieldsOfHasGroups, dependencyArrayOfHasGroups} from "../../../validators";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  tournaments = null;
  teams = null;
  groupsTeams = [];

  stages = null;
  notSelectedStages = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  get fullStages(){
    return this.mainGroup.get('full_stages') || null;
  }

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private seasonsService: SeasonsService,
      private tournamentsService: TournamentsService,
      private teamsService: TeamsService,
      private stagesService: StagesService,
      private elementsService: ElementsService,
      private groupsService: GroupsService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.getLanguages();
  }

  createFilterForm(){

    let mainGroupObj = {
      tournament_id: [null, Validators.required],
      start_year: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
      has_end_year: [null],
      end_year: [null, [Validators.minLength(4), Validators.maxLength(4)]],
      is_in_home: [null],
      teams_ids: [null, Validators.required],
      full_stages: this.formBuilder.array([])
    }

    this.mainGroup = this.formBuilder.group(mainGroupObj, {
      validators: [
        dependencyFieldOfCheckbox('has_end_year', 'end_year')
      ]
    });

    this.showForm = true;


    this.getTournaments();
    this.getTeams();
    this.getStages();
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getTournaments(){
    this.tournaments = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.tournaments = response.tournaments;
          }
        })
  }

  getTeams(){
    this.teams = null;

    this.loaderService.increaseLoaderCount();

    this.teamsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.teams = response.teams;
          }
        })
  }

  getStages(){
    this.stages = null;

    this.loaderService.increaseLoaderCount();

    this.stagesService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.stages = response.stages;
            this.notSelectedStages = response.stages;
          }
        })
  }

  getElements(data, index){
    this.mainGroup.controls['full_stages']['controls'][index].patchValue({
      'elements': []
    });

    this.loaderService.increaseLoaderCount();

    this.elementsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.mainGroup.controls['full_stages']['controls'][index].patchValue({
              'elements': response.elements
            });
          }
        })
  }

  getGroups(data, index){
    this.mainGroup.controls['full_stages']['controls'][index].patchValue({
      'groups': [],
      'notSelectedGroups': []
    });

    this.loaderService.increaseLoaderCount();

    this.groupsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.mainGroup.controls['full_stages']['controls'][index].patchValue({
              'groups': response.groups,
              'notSelectedGroups': response.groups
            });
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    console.log(this.mainGroup)

    if(!this.mainGroup.invalid){
      const formData = new FormData();

      formData.append('tournament_id', this.mainGroup.get('tournament_id').value);
      formData.append('start_year', this.mainGroup.get('start_year').value);
      if(this.mainGroup.get('has_end_year').value && this.mainGroup.get('end_year').value) formData.append('end_year', this.mainGroup.get('end_year').value);
      if(this.mainGroup.get('is_in_home').value) formData.append('is_in_home', this.mainGroup.get('is_in_home').value);
      formData.append('teams_ids', this.mainGroup.get('teams_ids').value);

      this.mainGroup['controls']['full_stages']['controls'].map((fullStage) => {
        let stageId = fullStage['controls']['stage'].value.id;

        formData.append(`stage_id[${stageId}]`, stageId);
        formData.append(`elements_ids[${stageId}]`, fullStage['controls']['elements_ids'].value);

        formData.append(`has_groups[${stageId}]`, fullStage['controls']['stage'].value.has_groups);

        if(fullStage['controls']['stage'].value.has_groups){
          formData.append(`draw_points[${stageId}]`, fullStage['controls']['group_draw_points'].value);
          formData.append(`victory_points[${stageId}]`, fullStage['controls']['group_victory_points'].value);

          fullStage['controls']['full_groups']['controls'].map(fullGroup => {
            formData.append(`stages__group_id[${stageId}][]`, fullGroup['controls']['group'].value.stages__group_id);
            formData.append(`group teams_ids[${stageId}][]`, fullGroup['controls']['teams_ids'].value);
          })

        }
      })

      this.loaderService.increaseLoaderCount()

      this.seasonsService.add(formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/seasons/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

  onTeamsChange(){
    this.groupsTeams = this.teams.filter((team) => {
      let teamsIds = this.mainGroup.get('teams_ids').value;

      return teamsIds.indexOf(team.id) > -1;
    })

    this.mainGroup['controls']['full_stages']['controls'].map((fullStageItem, fullStageIndex) => {
      fullStageItem.patchValue({
        groupsTeams: this.groupsTeams,
        notSelectedGroupsTeams: this.groupsTeams,
      })

      this.toFilterGroupsTeams(fullStageIndex)
    })
  }

  initItems(formControlName){
    switch (formControlName){
      case 'full_stages':
          return this.formBuilder.group({
            stage: [null, Validators.required],
            elements: [[], Validators.required],
            elements_ids: [null, Validators.required],

            groups: [[]],
            notSelectedGroups: [[]],

            groupsTeams: [this.groupsTeams],
            notSelectedGroupsTeams: [this.groupsTeams],

            full_groups: this.formBuilder.array([]),
            group_draw_points: [null],
            group_victory_points: [null]
          }, {
            validators: [
              dependencyFieldsOfHasGroups('stage', 'group_draw_points'),
              dependencyFieldsOfHasGroups('stage', 'group_victory_points'),
              dependencyArrayOfHasGroups('stage', 'full_groups'),

              dependencyArrayOfHasGroups('stage', 'groups'),

              dependencyArrayOfHasGroups('stage', 'groupsTeams')
            ]
          })

      case 'full_groups':
        return this.formBuilder.group({
          group: [null, Validators.required],
          teams_ids: [null, Validators.required],
        })

      default:
        return this.formBuilder.group({
        })
    }
  }

  onStageChange(index){
    this.mainGroup.controls['full_stages']['controls'][index].patchValue({
      'elements_ids': null,

      'group_draw_points': null,
      'group_victory_points': null
    });

    this.mainGroup.controls['full_stages']['controls'][index]['controls']['full_groups']['controls'].map((fullGroupItem, fullGroupIndex) => {
      this.mainGroup.controls['full_stages']['controls'][index]['controls']['full_groups'].removeAt(fullGroupIndex)
    })


    this.toFilterGroupsTeams(index)


    let stage = this.mainGroup.controls['full_stages']['controls'][index]['controls']['stage'].value;

    if(stage){

      let stageId = stage.id

      let data = {
        stage_id: stageId
      }

      this.getElements(data, index)

      console.log(this.mainGroup)

      if(this.mainGroup.controls['full_stages']['controls'][index]['controls']['stage'].value.has_groups){
        let data = {
          stage_id: stageId
        }

        this.getGroups(data, index)

        this.mainGroup.controls['full_stages']['controls'][index]['controls']['full_groups'].push(this.initItems('full_groups'));
      }
    }
    else{
      this.mainGroup.controls['full_stages']['controls'][index].patchValue({
        'elements': [],
        'groups': [],
        'notSelectedGroups': []
      });
    }

    this.toFilterStages()
  }

  toFilterStages(){
    let arrayOfStagesIds = new Array<number>();

    this.mainGroup.get('full_stages').value.map((fullStage) => {
      if(fullStage.stage) arrayOfStagesIds.push(fullStage.stage.id)
    })

    this.notSelectedStages = this.stages.filter(function(stage) {
      return arrayOfStagesIds.indexOf(stage.id) == -1
    });
  }

  toFilterGroups(index){
    let arrayOfGroupsIds = new Array<number>();

    this.mainGroup['controls']['full_stages']['controls'][index]['controls']['full_groups'].value.map((fullGroup) => {
      if(fullGroup.group) arrayOfGroupsIds.push(fullGroup.group.id)
    })

    this.mainGroup.get('full_stages')['controls'][index].patchValue({
      'notSelectedGroups': this.mainGroup['controls']['full_stages']['controls'][index]['controls']['groups'].value.filter(function(group) {
        return arrayOfGroupsIds.indexOf(group.id) == -1
      })
    })

  }

  toFilterGroupsTeams(index){
    let arrayOfGroupsIds = new Array<number>();

    this.mainGroup['controls']['full_stages']['controls'][index]['controls']['full_groups'].value.map((fullGroup) => {
      if(fullGroup.teams_ids && fullGroup.teams_ids.length > 0) arrayOfGroupsIds = [...arrayOfGroupsIds, ...fullGroup['teams_ids']]
    })

    this.mainGroup.get('full_stages')['controls'][index].patchValue({
      'notSelectedGroupsTeams': this.mainGroup['controls']['full_stages']['controls'][index]['controls']['groupsTeams'].value.filter(function(groupsTeam) {
        return arrayOfGroupsIds.indexOf(groupsTeam.id) == -1
      })
    })

  }

  addToTable(event){
    const controls = <FormArray>this.mainGroup.get(event.currentTarget.dataset.formControlName) ;

    controls.push(this.initItems(event.currentTarget.dataset.formControlName));

    this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

  deleteFromTable(event, index){
    const controls = <FormArray>this.mainGroup.get(event.currentTarget.dataset.formControlName);

    controls.removeAt(index)

    this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

  addToGroupsTable(event, index){

    const controls = <FormArray>this.mainGroup['controls']['full_stages']['controls'][index]['controls'][event.currentTarget.dataset.formControlName]

    controls.push(this.initItems(event.currentTarget.dataset.formControlName));

    this.mainGroup['controls']['full_stages']['controls'][index]['controls'][event.currentTarget.dataset.formControlName].markAsDirty()
  }

  deleteFromGroupsTable(event, index, jndex){
    const controls = <FormArray>this.mainGroup['controls']['full_stages']['controls'][index]['controls'][event.currentTarget.dataset.formControlName];

    controls.removeAt(jndex)

    this.mainGroup['controls']['full_stages']['controls'][index]['controls'][event.currentTarget.dataset.formControlName].markAsDirty()
  }

}
