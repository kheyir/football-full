import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {HttpClient} from '@angular/common/http';
import {Router} from "@angular/router";
import {NewsService} from "../../../services/news.service";
import {TournamentsService} from "../../../services/tournaments.service";
import {SeasonsService} from "../../../services/seasons.service";
import {CategoriesService} from "../../../services/categories.service";
import {CountriesService} from "../../../services/countries.service";
import {TeamsService} from "../../../services/teams.service";
import {LoaderService} from "../../../services/loader.service";
import {LanguagesService} from "../../../services/languages.service";
import {FileTypeValidator} from '../../../directives/file-type-validator.directive';
import {FileSizeValidator} from '../../../directives/file-size-validator.directive';
import {requiredAllIfOneOfThreeNotEmpty, requiredIfBothAreEmpty} from '../../../validators'

import Quill from 'quill';
import { ImageHandler } from 'ngx-quill-upload';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {

  mainGroup: FormGroup;

  tournaments = null;
  seasons = null;
  categories = null;
  countries = null;
  teams = null;

  languages = null;
  showForm = false;
  tryingAtOnceToSend = false;

  public Editor = ClassicEditor;

  modules: any = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons

      [{'header': 2}],                                  // custom button values

      [{'header': [2, 3, 4, false]}],

      [{'color': []}, {'background': []}],          // dropdown with defaults from theme
      
      [{'align': []}],

      ['clean'],                                       // remove formatting button

      ['link', 'image', 'video',]                   // link and image, video

    ],

    imageHandler: {
      upload: (file) => {
        console.log(file)
        return new Promise((resolve, reject) => {
          const uploadData = new FormData();
          uploadData.append('file', file, file.name);

          return this.http.post('/api/foot/news-item-image-upload', uploadData).toPromise()
            .then((response: any) => {
                resolve(response.url);
            })
            .catch(error => {
              reject('Upload failed'); 
              console.error('Error:', error);
            });
      })
      }
    }
  };

  config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'link', '|', 'undo', 'redo', '|', 'imageUpload', 'MediaEmbed'],
    mediaEmbed: {
      previewsInData: true
    }
  }

  constructor(
      private router: Router,
      private formBuilder: FormBuilder,
      private newsService: NewsService,
      private tournamentsService: TournamentsService,
      private seasonsService: SeasonsService,
      private categoriesService: CategoriesService,
      private countriesService: CountriesService,
      private teamsService: TeamsService,
      private loaderService: LoaderService,
      private languagesService: LanguagesService,
      private http: HttpClient
  ) {
    this.Editor.defaultConfig = this.config;
  }

  ngOnInit() {
    this.getLanguages();
  }

  createFilterForm(){

    let mainGroupObj = {
      locale: [null, Validators.required],
      title: [null, [Validators.required, Validators.maxLength(200)]],
      short_text: [null, [Validators.required, Validators.maxLength(200)]],
      large_text: [null, [Validators.required]],
      views_count: [null, [Validators.required]],
      image: [null, [Validators.required, FileTypeValidator, FileSizeValidator]],
      creating_date: [null, [Validators.required]],
      tournament_id: [null],
      season_id: [null],
      categories_ids: [null],
      countries_ids: [null],
      teams_ids: [null],
      priority: [0, [Validators.required]],
      is_in_slider: [null],
      is_in_right: [null],
      is_active: [1],
      to_publish: [1]
    }

    let validators: any[] = [];
    // this.languages.forEach((item: any) => {
    //   mainGroupObj['title[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    //   mainGroupObj['short_text[' + item.locale + ']'] = ['', [Validators.required, Validators.maxLength(200)]]
    //   mainGroupObj['large_text[' + item.locale + ']'] = ['', [Validators.required]]
    //   mainGroupObj['views_count[' + item.locale + ']'] = [0, [Validators.required]]

      // validators.push(requiredAllIfOneOfThreeNotEmpty(`title[${item.locale}]`, `short_text[${item.locale}]`, `large_text[${item.locale}]`, `views_count[${item.locale}]`))
    // })

    // validators.push(requiredIfBothAreEmpty(this.languages, 'title', 'short_text', 'large_text', 'views_count'))

    this.mainGroup = this.formBuilder.group(mainGroupObj, {
      validators: validators
    });

    this.showForm = true;

    this.getNewsCategories();
    this.getCountries();
    this.getTeams();
    this.getTournaments();
  }

  getLanguages(){

    this.loaderService.increaseLoaderCount()

    this.languages = null;
    this.languagesService.getList()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.languages = response.languages;

            this.createFilterForm();
          }
        })
  }

  getNewsCategories(){
    this.categories = null;

    this.loaderService.increaseLoaderCount();

    let data: any = {
      is_news_category: 1
    }

    this.categoriesService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.categories = response.categories
          }
        })
  }

  getCountries(){
    this.countries = null;

    this.loaderService.increaseLoaderCount();

    this.countriesService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.countries = response.countries;
          }
        })
  }

  getTeams(){
    this.teams = null;

    this.loaderService.increaseLoaderCount();

    this.teamsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.teams = response.teams
          }
        })
  }

  getTournaments(){
    this.tournaments = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsService.getAll()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.tournaments = response.tournaments
          }
        })
  }

  getSeasons(data: any = {}){
    this.seasons = null;

    this.loaderService.increaseLoaderCount();

    this.seasonsService.getAll(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          if(response.code == 0){
            this.seasons = response.seasons.map(item => {
              item.name = item.start_year + (item.end_year ? ' - ' + item.end_year : '')
              return item
            })
          }
        })
  }

  onSubmit(){

    this.tryingAtOnceToSend = true;

    console.log(this.mainGroup)

    if(!this.mainGroup.invalid){

      let creatingDate = this.mainGroup.get('creating_date').value;

      const formData = new FormData();

      formData.append('locale', this.mainGroup.get('locale').value);

      formData.append('title', this.mainGroup.get('title').value);
      formData.append('short_text', this.mainGroup.get('short_text').value);
      formData.append('large_text', this.mainGroup.get('large_text').value);
      formData.append('views_count', this.mainGroup.get('views_count').value);

      formData.append('image', this.mainGroup.get('image').value);
      formData.append('creating_date', creatingDate.getFullYear() + '-' + (creatingDate.getMonth()+1) + '-' + creatingDate.getDate() + ' ' + creatingDate.getHours() + ':' + creatingDate.getMinutes() + ':' + creatingDate.getSeconds());
      if(this.mainGroup.get('tournament_id').value) formData.append('tournament_id', this.mainGroup.get('tournament_id').value);
      if(this.mainGroup.get('season_id').value) formData.append('season_id', this.mainGroup.get('season_id').value);
      if(this.mainGroup.get('categories_ids').value) formData.append('categories_ids', this.mainGroup.get('categories_ids').value);
      if(this.mainGroup.get('countries_ids').value) formData.append('countries_ids', this.mainGroup.get('countries_ids').value);
      if(this.mainGroup.get('teams_ids').value) formData.append('teams_ids', this.mainGroup.get('teams_ids').value);
      formData.append('priority', this.mainGroup.get('priority').value);
      if(this.mainGroup.get('is_in_slider').value) formData.append('is_in_slider', this.mainGroup.get('is_in_slider').value);
      if(this.mainGroup.get('is_in_right').value) formData.append('is_in_right', this.mainGroup.get('is_in_right').value);
      if(this.mainGroup.get('is_active').value) formData.append('is_active', this.mainGroup.get('is_active').value);
      if(this.mainGroup.get('to_publish').value) formData.append('to_publish', this.mainGroup.get('to_publish').value);

      // this.languages.forEach((item: any) => {
      //   formData.append(`title[${item.locale}]`, this.mainGroup.get('title[' + item.locale + ']').value);
      //   formData.append(`short_text[${item.locale}]`, this.mainGroup.get('short_text[' + item.locale + ']').value);
      //   formData.append(`large_text[${item.locale}]`, this.mainGroup.get('large_text[' + item.locale + ']').value);
      //   formData.append(`views_count[${item.locale}]`, this.mainGroup.get('views_count[' + item.locale + ']').value);
      // })

      this.loaderService.increaseLoaderCount()

      this.newsService.add(formData)
          .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
              this.router.navigate(['/news/list'])
            }
            else{
              alert(response.message);
            }
          })
    }
  }

  onFileChange(event){

    let formControlNameObj = {}

    if(event.target.files && event.target.files.length) {

      let file = event.target.files[0];

      formControlNameObj[event.target.dataset.formControlName] = file
    }
    else{
      formControlNameObj[event.target.dataset.formControlName] = null;
    }

    this.mainGroup.patchValue(formControlNameObj);

    this.mainGroup.get(event.currentTarget.dataset.formControlName).markAsDirty()
  }

  onTournamentChange(){
    if(this.mainGroup.get('tournament_id').value){
      let data = {
        tournament_id: this.mainGroup.get('tournament_id').value
      }

      this.getSeasons(data);
    }
    else{
      this.seasons = []

      this.mainGroup.patchValue({
        season_id: null
      })
    }
  }

}
