import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbDate} from "@ng-bootstrap/ng-bootstrap";
import {NewsService} from "../../../services/news.service";
import {CategoriesService} from "../../../services/categories.service";
import {CountriesService} from "../../../services/countries.service";
import {TeamsService} from "../../../services/teams.service";
import {TournamentsService} from "../../../services/tournaments.service";
import {SeasonsService} from "../../../services/seasons.service";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public filterGroup: FormGroup;
  public filterIsSentFirstTime = false;

  news = null;
  categories = null;
  countries = null;
  teams = null;
  tournaments = null;
  seasons: any = [{name: 'Hamısı', id: 0}];

  isInSliderStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  isInRightStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  isActiveStatuses = [
    {label: 'Hamısı', value: 0},
    {label: 'Bəli', value: 1},
    {label: 'Xeyr', value: 2},
  ];

  public pageNumber: number = 1;
  public selectedId: number = 0;

  @ViewChild('modalCloseButton', {static: false}) modalCloseButton: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private loaderService: LoaderService,
      private newsService: NewsService,
      private categoriesService: CategoriesService,
      private countriesService: CountriesService,
      private teamsService: TeamsService,
      private tournamentsService: TournamentsService,
      private seasonsService: SeasonsService,
  ) { }

  ngOnInit() {
    this.getNewsCategories()
    this.getCountries()
    this.getTeams()
    this.getTournaments()

    this.createFilterForm()
    this.queryParamsToProps()
  }

  queryParamsToProps(){
    this.route.queryParams
        .subscribe(params => {

          let paramsCopy = JSON.parse(JSON.stringify(params))

          this.pageNumber = paramsCopy.pageNumber ? paramsCopy.pageNumber : 1

          delete paramsCopy.pageNumber

          this.filterGroup.setValue({
            title: paramsCopy.title ? paramsCopy.title : '',
            category_id: paramsCopy.category_id ? parseInt(paramsCopy.category_id) : 0,
            country_id: paramsCopy.country_id ? parseInt(paramsCopy.country_id) : 0,
            team_id: paramsCopy.team_id ? parseInt(paramsCopy.team_id) : 0,
            tournament_id: paramsCopy.tournament_id ? parseInt(paramsCopy.tournament_id) : 0,
            season_id: paramsCopy.season_id ? parseInt(paramsCopy.season_id) : 0,
            min_creating_date: paramsCopy.min_creating_date && (new Date(paramsCopy.min_creating_date)).getTime() ? (new Date(paramsCopy.min_creating_date)) : null,
            max_creating_date: paramsCopy.max_creating_date && (new Date(paramsCopy.max_creating_date)).getTime() ? (new Date(paramsCopy.max_creating_date)) : null,
            is_in_slider: paramsCopy.is_in_slider ? parseInt(paramsCopy.is_in_slider) : 0,
            is_in_right: paramsCopy.is_in_right ? parseInt(paramsCopy.is_in_right) : 0,
            is_active: paramsCopy.is_active ? parseInt(paramsCopy.is_active) : 0,
          })

          this.onTournamentChange()

          if(!this.filterIsSentFirstTime) this.filter()
        });
  }

  createFilterForm(){
    this.filterGroup = this.formBuilder.group({
      title: ['', Validators.minLength(3)],
      category_id: [0],
      country_id: [0],
      team_id: [0],
      tournament_id: [0],
      season_id: [0],
      min_creating_date: [null],
      max_creating_date: [null],
      is_in_slider: [0],
      is_in_right: [0],
      is_active: [0]
    })
  }

  getNews(data : any = {}){

    this.loaderService.increaseLoaderCount()

    this.news = null;
    this.newsService.getList(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){
            this.news = response.news;
          }
        })
  }

  getNewsCategories(){
    this.categories = null;

    this.loaderService.increaseLoaderCount();

    let data: any = {
        is_news_category: 1
    }

    this.categoriesService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.categories = [{name: 'Hamısı', id: 0}, ...response.categories]
            }
        })
  }

  getCountries(){
        this.countries = null;

        this.loaderService.increaseLoaderCount();

        this.countriesService.getAll()
            .subscribe((response: any) => {

                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.countries = [{name: 'Hamısı', id: 0}, ...response.countries]
                }
            })
  }

  getTeams(){
    this.teams = null;

    this.loaderService.increaseLoaderCount();

    this.teamsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.teams = [{name: 'Hamısı', id: 0}, ...response.teams]
            }
        })
  }

  getTournaments(){
    this.tournaments = null;

    this.loaderService.increaseLoaderCount();

    this.tournamentsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.tournaments = [{name: 'Hamısı', id: 0}, ...response.tournaments]
            }
        })
  }

  getSeasons(data: any = {}){
    this.seasons = null;

    this.loaderService.increaseLoaderCount();

    this.seasonsService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.seasons = response.seasons.map(item => {
                    item.name = item.start_year + (item.end_year ? ' - ' + item.end_year : '')
                    return item
                })
                this.seasons = [{name: 'Hamısı', id: 0}, ...this.seasons]
            }
        })
  }

  filter(){

    if(!this.filterGroup.invalid){

      let minCreatingDate = this.filterGroup.get("min_creating_date").value
      let maxCreatingDate = this.filterGroup.get("max_creating_date").value

      const data = {
        pageNumber: this.pageNumber,

        title: this.filterGroup.get("title").value,
        category_id: this.filterGroup.get("category_id").value,
        country_id: this.filterGroup.get("country_id").value,
        team_id: this.filterGroup.get("team_id").value,
        tournament_id: this.filterGroup.get("tournament_id").value,
        season_id: this.filterGroup.get("season_id").value,
        min_creating_date: (new Date(minCreatingDate)).getTime() ? minCreatingDate.getFullYear() + '-' + (minCreatingDate.getMonth()+1) + '-' + minCreatingDate.getDate() + ' ' + minCreatingDate.getHours() + ':' + minCreatingDate.getMinutes() + ':' + minCreatingDate.getSeconds() : '',
        max_creating_date: (new Date(maxCreatingDate)).getTime() ? maxCreatingDate.getFullYear() + '-' + (maxCreatingDate.getMonth()+1) + '-' + maxCreatingDate.getDate() + ' ' + maxCreatingDate.getHours() + ':' + maxCreatingDate.getMinutes() + ':' + maxCreatingDate.getSeconds() : '',
        is_in_slider: this.filterGroup.get("is_in_slider").value,
        is_in_right: this.filterGroup.get("is_in_right").value,
        is_active: this.filterGroup.get("is_active").value,
      }

      if(this.filterIsSentFirstTime) this.router.navigate([], {queryParams: data});

      this.getNews(data)

      this.filterIsSentFirstTime = true;
    }
  }

  selectId(id){
    this.selectedId = id
  }

  delete(){
    this.modalCloseButton.nativeElement.click();

    this.loaderService.increaseLoaderCount()

    let data = {
      id: this.selectedId
    }

    this.newsService.delete(data)
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()

          alert(response.message);

          if(response.code == 0){
            this.filter()
          }
        })
  }

  pageChanged(event){
    this.pageNumber = event;

    this.filter()
  }

  onTournamentChange(){
    if(this.filterGroup.get('tournament_id').value){
        let data = {
            tournament_id: this.filterGroup.get('tournament_id').value
        }

        this.getSeasons(data);
    }
    else{
        this.seasons = [{name: 'Hamısı', id: 0}]

        this.filterGroup.patchValue({
            season_id: 0
        })
    }
  }

}
