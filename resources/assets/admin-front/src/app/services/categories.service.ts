import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("is_for_video")) httpParams = httpParams.append("is_for_video", data.is_for_video);
    if(data.hasOwnProperty("is_news_category")) httpParams = httpParams.append("is_news_category", data.is_news_category);

    return this.http.get(environment.URL + '/categories/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);
    if(data.hasOwnProperty("is_featured")) httpParams = httpParams.append("is_featured", data.is_featured);
    if(data.hasOwnProperty("is_news_category")) httpParams = httpParams.append("is_news_category", data.is_news_category);
    if(data.hasOwnProperty("is_for_video")) httpParams = httpParams.append("is_for_video", data.is_for_video);

    return this.http.get(environment.URL + `/categories/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/category/add', formData)
  }

  getCategoryById(data: any = {}){
    return this.http.get(environment.URL + `/category/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/category/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/category/delete/${data.id}`)
  }
}
