import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ElementsService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("stage_id")) httpParams = httpParams.append("stage_id", data.stage_id);
    if(data.hasOwnProperty("season_id")) httpParams = httpParams.append("season_id", data.season_id);

    return this.http.get(environment.URL + '/elements/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);
    if(data.hasOwnProperty("stage_id")) httpParams = httpParams.append("stage_id", data.stage_id);

    return this.http.get(environment.URL + `/elements/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/element/add', {}, {params: httpParams})
  }

  getElementById(data: any = {}){
    return this.http.get(environment.URL + `/element/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/element/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/element/delete/${data.id}`)
  }
}
