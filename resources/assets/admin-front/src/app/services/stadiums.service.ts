import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StadiumsService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("country_id")) httpParams = httpParams.append("country_id", data.country_id);

    return this.http.get(environment.URL + '/stadiums/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);
    if(data.hasOwnProperty("city_id")) httpParams = httpParams.append("city_id", data.city_id);

    return this.http.get(environment.URL + `/stadiums/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/stadium/add', {}, {params: httpParams})
  }

  getStadiumById(data: any = {}){
    return this.http.get(environment.URL + `/stadium/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/stadium/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/stadium/delete/${data.id}`)
  }
}
