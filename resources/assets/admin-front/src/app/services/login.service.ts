import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  signIn(data){
    return this.http.post(environment.URL + "/login", data);
  }

  check(){
    return this.http.get(environment.URL + "/auth-check");
  }

  getAuth(){
    return this.http.get(environment.URL + "/get-auth");
  }

  logout(){
    return this.http.get(environment.URL + "/logout");
  }
}
