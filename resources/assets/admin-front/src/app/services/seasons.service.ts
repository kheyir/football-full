import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SeasonsService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.tournament_id) httpParams = httpParams.append("tournament_id", data.tournament_id);

    return this.http.get(environment.URL + '/seasons/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.name) httpParams = httpParams.append("name", data.name);
    if(data.tournament_id) httpParams = httpParams.append("tournament_id", data.tournament_id);
    if(data.tournaments_category_id) httpParams = httpParams.append("tournaments_category_id", data.tournaments_category_id);
    if(data.country_id > -1) httpParams = httpParams.append("country_id", data.country_id);
    if(data.is_league) httpParams = httpParams.append("is_league", data.is_league);
    if(data.start_year) httpParams = httpParams.append("start_year", data.start_year);
    if(data.end_year) httpParams = httpParams.append("end_year", data.end_year);

    return this.http.get(environment.URL + `/seasons/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/season/add', formData)
  }

  getSeasonById(data: any = {}){
    return this.http.get(environment.URL + `/season/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/season/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/season/delete/${data.id}`)
  }
}
