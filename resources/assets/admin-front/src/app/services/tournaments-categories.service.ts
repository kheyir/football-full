import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TournamentsCategoriesService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(environment.URL + '/tournaments-categories/all')
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);

    return this.http.get(environment.URL + `/tournaments-categories/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/tournaments-category/add', {}, {params: httpParams})
  }

  getTournamentsCategoryById(data: any = {}){
    return this.http.get(environment.URL + `/tournaments-category/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/tournaments-category/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/tournaments-category/delete/${data.id}`)
  }
}
