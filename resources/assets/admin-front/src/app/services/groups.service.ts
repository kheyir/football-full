import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("stage_id")) httpParams = httpParams.append("stage_id", data.stage_id);

    return this.http.get(environment.URL + '/groups/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);

    return this.http.get(environment.URL + `/groups/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/group/add', {}, {params: httpParams})
  }

  getGroupById(data: any = {}){
    return this.http.get(environment.URL + `/group/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/group/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/group/delete/${data.id}`)
  }
}
