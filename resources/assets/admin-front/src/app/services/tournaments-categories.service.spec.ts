import { TestBed } from '@angular/core/testing';

import { TournamentsCategoriesService } from './tournaments-categories.service';

describe('TournamentsCategoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TournamentsCategoriesService = TestBed.get(TournamentsCategoriesService);
    expect(service).toBeTruthy();
  });
});
