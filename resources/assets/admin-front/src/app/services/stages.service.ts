import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StagesService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("season_id")) httpParams = httpParams.append("season_id", data.season_id);

    return this.http.get(environment.URL + '/stages/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);

    return this.http.get(environment.URL + `/stages/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/stage/add', {}, {params: httpParams})
  }

  getStageById(data: any = {}){
    return this.http.get(environment.URL + `/stage/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/stage/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/stage/delete/${data.id}`)
  }
}
