import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CoachesService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(environment.URL + '/coaches/all')
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);

    return this.http.get(environment.URL + `/coaches/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/coach/add', {}, {params: httpParams})
  }

  getCoachById(data: any = {}){
    return this.http.get(environment.URL + `/coach/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/coach/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/coach/delete/${data.id}`)
  }
}
