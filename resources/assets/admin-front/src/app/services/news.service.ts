import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("season_id")) httpParams = httpParams.append("season_id", data.season_id);
    if(data.hasOwnProperty("season_id") && data.hasOwnProperty("group_id")) httpParams = httpParams.append("group_id", data.group_id);

    return this.http.get(environment.URL + '/news/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("title")) httpParams = httpParams.append("title", data.title);
    if(data.hasOwnProperty("short_text")) httpParams = httpParams.append("short_text", data.short_text);
    if(data.hasOwnProperty("category_id")) httpParams = httpParams.append("category_id", data.category_id);
    if(data.hasOwnProperty("country_id")) httpParams = httpParams.append("country_id", data.country_id);
    if(data.hasOwnProperty("team_id")) httpParams = httpParams.append("team_id", data.team_id);
    if(data.hasOwnProperty("tournament_id")) httpParams = httpParams.append("tournament_id", data.tournament_id);
    if(data.hasOwnProperty("season_id")) httpParams = httpParams.append("season_id", data.season_id);
    if(data.hasOwnProperty("min_creating_date")) httpParams = httpParams.append("min_creating_date", data.min_creating_date);
    if(data.hasOwnProperty("max_creating_date")) httpParams = httpParams.append("max_creating_date", data.max_creating_date);
    if(data.hasOwnProperty("is_in_slider")) httpParams = httpParams.append("is_in_slider", data.is_in_slider);
    if(data.hasOwnProperty("is_in_right")) httpParams = httpParams.append("is_in_right", data.is_in_right);
    if(data.hasOwnProperty("is_active")) httpParams = httpParams.append("is_active", data.is_active);

    return this.http.get(environment.URL + `/news/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/news-item/add', formData)
  }

  getNewsItemById(data: any = {}){
    return this.http.get(environment.URL + `/news-item/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/news-item/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/news-item/delete/${data.id}`)
  }
}
