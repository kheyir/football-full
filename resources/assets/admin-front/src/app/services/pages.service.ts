import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor(private http: HttpClient) { }

  getList(){

    return this.http.get(environment.URL + `/pages/list`)
  }

  getPageItemById(data: any = {}){
    return this.http.get(environment.URL + `/page/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/page/edit/${data.id}`, formData)
  }
}
