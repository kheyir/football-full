import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("country_id")) httpParams = httpParams.append("country_id", data.country_id);

    return this.http.get(environment.URL + '/cities/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);
    if(data.hasOwnProperty("country_id")) httpParams = httpParams.append("country_id", data.country_id);

    return this.http.get(environment.URL + `/cities/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/city/add', {}, {params: httpParams})
  }

  getCityById(data: any = {}){
    return this.http.get(environment.URL + `/city/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/city/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/city/delete/${data.id}`)
  }
}
