import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.team_id) httpParams = httpParams.append("team_id", data.team_id);

    return this.http.get(environment.URL + '/players/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.name) httpParams = httpParams.append("name", data.name);
    if(data.country_id) httpParams = httpParams.append("country_id", data.country_id);
    if(data.position_id) httpParams = httpParams.append("position_id", data.position_id);
    if(data.team_id) httpParams = httpParams.append("team_id", data.team_id);

    return this.http.get(environment.URL + `/players/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/player/add', formData)
  }

  getPlayerById(data: any = {}){
    return this.http.get(environment.URL + `/player/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/player/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/player/delete/${data.id}`)
  }
}
