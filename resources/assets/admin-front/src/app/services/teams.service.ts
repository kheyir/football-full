import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  constructor(private http: HttpClient) { }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("season_id")) httpParams = httpParams.append("season_id", data.season_id);
    if(data.hasOwnProperty("season_id") && data.hasOwnProperty("group_id")) httpParams = httpParams.append("group_id", data.group_id);

    return this.http.get(environment.URL + '/teams/all', {params: httpParams})
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);
    if(data.hasOwnProperty("country_id")) httpParams = httpParams.append("country_id", data.country_id);

    return this.http.get(environment.URL + `/teams/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/team/add', formData)
  }

  getTeamById(data: any = {}){
    return this.http.get(environment.URL + `/team/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/team/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/team/delete/${data.id}`)
  }
}
