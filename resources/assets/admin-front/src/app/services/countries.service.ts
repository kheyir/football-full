import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(environment.URL + '/countries/all')
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);
    if(data.hasOwnProperty("in_head")) httpParams = httpParams.append("in_head", data.in_head);
    if(data.hasOwnProperty("is_news_category")) httpParams = httpParams.append("is_news_category", data.is_news_category);

    return this.http.get(environment.URL + `/countries/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/country/add', formData)
  }

  getCountryById(data: any = {}){
    return this.http.get(environment.URL + `/country/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/country/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/country/delete/${data.id}`)
  }
}
