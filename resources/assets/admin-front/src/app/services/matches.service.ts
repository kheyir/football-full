import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MatchesService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(environment.URL + '/matches/all')
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.country_id) httpParams = httpParams.append("country_id", data.country_id);
    if(data.stadium_id) httpParams = httpParams.append("stadium_id", data.stadium_id);
    if(data.status) httpParams = httpParams.append("status", data.status);
    if(data.min_start_date) httpParams = httpParams.append("min_start_date", data.min_start_date);
    if(data.max_start_date) httpParams = httpParams.append("max_start_date", data.max_start_date);
    if(data.is_active) httpParams = httpParams.append("is_active", data.is_active);
    if(data.home_team_id) httpParams = httpParams.append("home_team_id", data.home_team_id);
    if(data.guest_team_id) httpParams = httpParams.append("guest_team_id", data.guest_team_id);
    if(data.tournament_id) httpParams = httpParams.append("tournament_id", data.tournament_id);
    if(data.seasons_id) httpParams = httpParams.append("seasons_id", data.seasons_id);
    if(data.stage_id) httpParams = httpParams.append("stage_id", data.stage_id);
    if(data.element_id) httpParams = httpParams.append("element_id", data.element_id);
    if(data.group_id) httpParams = httpParams.append("group_id", data.group_id);

    return this.http.get(environment.URL + `/matches/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/match/add', formData)
  }

  getMatchById(data: any = {}){
    return this.http.get(environment.URL + `/match/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/match/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/match/delete/${data.id}`)
  }
}
