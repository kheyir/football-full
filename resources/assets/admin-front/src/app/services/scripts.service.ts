import { Injectable } from '@angular/core';
import $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class ScriptsService {

  constructor() { }

  init(){
    $(document).delegate("[data-action='collapse']", "click", function(){
      let $panel = $(this).closest(".panel")

      console.log($(this))

      if($panel.hasClass("panel-collapsed")){
        $panel.removeClass("panel-collapsed")
        $panel.find(".panel-body").hide()
        $(this).addClass("rotate-180")
      }
      else{
        $panel.addClass("panel-collapsed")
        $panel.find(".panel-body").show()
        $(this).removeClass("rotate-180")
      }
    })
  }
}
