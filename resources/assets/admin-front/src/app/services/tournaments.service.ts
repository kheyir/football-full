import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TournamentsService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(environment.URL + '/tournaments/all')
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);
    if(data.hasOwnProperty("tournaments_category_id")) httpParams = httpParams.append("tournaments_category_id", data.tournaments_category_id);
    if(data.country_id > -1) httpParams = httpParams.append("country_id", data.country_id);
    if(data.hasOwnProperty("is_news_category")) httpParams = httpParams.append("is_news_category", data.is_news_category);
    if(data.hasOwnProperty("is_videos_category")) httpParams = httpParams.append("is_videos_category", data.is_videos_category);
    if(data.hasOwnProperty("priority_for_country")) httpParams = httpParams.append("priority_for_country", data.priority_for_country);
    if(data.hasOwnProperty("is_league")) httpParams = httpParams.append("is_league", data.is_league);

    return this.http.get(environment.URL + `/tournaments/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(formData){
    return this.http.post(environment.URL + '/tournament/add', formData)
  }

  getTournamentById(data: any = {}){
    return this.http.get(environment.URL + `/tournament/${data.id}`)
  }

  edit(data, formData){
    return this.http.post(environment.URL + `/tournament/edit/${data.id}`, formData)
  }

  delete(data){
    return this.http.get(environment.URL + `/tournament/delete/${data.id}`)
  }
}
