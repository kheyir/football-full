import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PositionsService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(environment.URL + '/positions/all')
  }

  getList(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("name")) httpParams = httpParams.append("name", data.name);

    return this.http.get(environment.URL + `/positions/list/${data.pageNumber || 1}`, {params: httpParams})
  }

  add(httpParams){
    return this.http.post(environment.URL + '/position/add', {}, {params: httpParams})
  }

  getPositionById(data: any = {}){
    return this.http.get(environment.URL + `/position/${data.id}`)
  }

  edit(data, httpParams){
    return this.http.post(environment.URL + `/position/edit/${data.id}`, {}, {params: httpParams})
  }

  delete(data){
    return this.http.get(environment.URL + `/position/delete/${data.id}`)
  }
}
