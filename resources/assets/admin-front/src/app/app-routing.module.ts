import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

import { ListComponent as GroupsListComponent } from './pages/groups/list/list.component';
import { AddComponent as GroupsAddComponent } from './pages/groups/add/add.component';
import { EditComponent as GroupsEditComponent } from './pages/groups/edit/edit.component';

import { ListComponent as CoachesListComponent } from './pages/coaches/list/list.component';
import { AddComponent as CoachesAddComponent } from './pages/coaches/add/add.component';
import { EditComponent as CoachesEditComponent } from './pages/coaches/edit/edit.component';

import { ListComponent as PositionsListComponent } from './pages/positions/list/list.component';
import { AddComponent as PositionsAddComponent } from './pages/positions/add/add.component';
import { EditComponent as PositionsEditComponent } from './pages/positions/edit/edit.component';

import { ListComponent as TournamentsCategoriesListComponent } from './pages/tournaments-categories/list/list.component';
import { AddComponent as TournamentsCategoriesAddComponent } from './pages/tournaments-categories/add/add.component';
import { EditComponent as TournamentsCategoriesEditComponent } from './pages/tournaments-categories/edit/edit.component';

import { ListComponent as CountriesListComponent } from './pages/countries/list/list.component';
import { AddComponent as CountriesAddComponent } from './pages/countries/add/add.component';
import { EditComponent as CountriesEditComponent } from './pages/countries/edit/edit.component';

import { ListComponent as CitiesListComponent } from './pages/cities/list/list.component';
import { AddComponent as CitiesAddComponent } from './pages/cities/add/add.component';
import { EditComponent as CitiesEditComponent } from './pages/cities/edit/edit.component';

import { ListComponent as StadiumsListComponent } from './pages/stadiums/list/list.component';
import { AddComponent as StadiumsAddComponent } from './pages/stadiums/add/add.component';
import { EditComponent as StadiumsEditComponent } from './pages/stadiums/edit/edit.component';

import { ListComponent as CategoriesListComponent } from './pages/categories/list/list.component';
import { AddComponent as CategoriesAddComponent } from './pages/categories/add/add.component';
import { EditComponent as CategoriesEditComponent } from './pages/categories/edit/edit.component';

import { ListComponent as TeamsListComponent } from './pages/teams/list/list.component';
import { AddComponent as TeamsAddComponent } from './pages/teams/add/add.component';
import { EditComponent as TeamsEditComponent } from './pages/teams/edit/edit.component';

import { ListComponent as StagesListComponent } from './pages/stages/list/list.component';
import { AddComponent as StagesAddComponent } from './pages/stages/add/add.component';
import { EditComponent as StagesEditComponent } from './pages/stages/edit/edit.component';

import { ListComponent as ElementsListComponent } from './pages/elements/list/list.component';
import { AddComponent as ElementsAddComponent } from './pages/elements/add/add.component';
import { EditComponent as ElementsEditComponent } from './pages/elements/edit/edit.component';

import { ListComponent as TournamentsListComponent } from './pages/tournaments/list/list.component';
import { AddComponent as TournamentsAddComponent } from './pages/tournaments/add/add.component';
import { EditComponent as TournamentsEditComponent } from './pages/tournaments/edit/edit.component';

import { ListComponent as PlayersListComponent } from './pages/players/list/list.component';
import { AddComponent as PlayersAddComponent } from './pages/players/add/add.component';
import { EditComponent as PlayersEditComponent } from './pages/players/edit/edit.component';

import { ListComponent as SeasonsListComponent } from './pages/seasons/list/list.component';
import { AddComponent as SeasonsAddComponent } from './pages/seasons/add/add.component';
import { EditComponent as SeasonsEditComponent } from './pages/seasons/edit/edit.component';

import { ListComponent as MatchesListComponent } from './pages/matches/list/list.component';
import { AddComponent as MatchesAddComponent } from './pages/matches/add/add.component';
import { EditComponent as MatchesEditComponent } from './pages/matches/edit/edit.component';

import { ListComponent as NewsListComponent } from './pages/news/list/list.component';
import { AddComponent as NewsAddComponent } from './pages/news/add/add.component';
import { EditComponent as NewsEditComponent } from './pages/news/edit/edit.component';

import { ListComponent as PageListComponent } from './pages/pages/list/list.component';
import { EditComponent as PageEditComponent } from './pages/pages/edit/edit.component';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },

  { path: 'groups/list', component: GroupsListComponent },
  { path: 'group/add', component: GroupsAddComponent },
  { path: 'group/edit/:id', component: GroupsEditComponent },

  { path: 'coaches/list', component: CoachesListComponent },
  { path: 'coach/add', component: CoachesAddComponent },
  { path: 'coach/edit/:id', component: CoachesEditComponent },

  { path: 'positions/list', component: PositionsListComponent },
  { path: 'position/add', component: PositionsAddComponent },
  { path: 'position/edit/:id', component: PositionsEditComponent },

  { path: 'tournaments-categories/list', component: TournamentsCategoriesListComponent },
  { path: 'tournaments-category/add', component: TournamentsCategoriesAddComponent },
  { path: 'tournaments-category/edit/:id', component: TournamentsCategoriesEditComponent },

  { path: 'countries/list', component: CountriesListComponent },
  { path: 'country/add', component: CountriesAddComponent },
  { path: 'country/edit/:id', component: CountriesEditComponent },

  { path: 'cities/list', component: CitiesListComponent },
  { path: 'city/add', component: CitiesAddComponent },
  { path: 'city/edit/:id', component: CitiesEditComponent },

  { path: 'stadiums/list', component: StadiumsListComponent },
  { path: 'stadium/add', component: StadiumsAddComponent },
  { path: 'stadium/edit/:id', component: StadiumsEditComponent },

  { path: 'categories/list', component: CategoriesListComponent },
  { path: 'category/add', component: CategoriesAddComponent },
  { path: 'category/edit/:id', component: CategoriesEditComponent },

  { path: 'teams/list', component: TeamsListComponent },
  { path: 'team/add', component: TeamsAddComponent },
  { path: 'team/edit/:id', component: TeamsEditComponent },

  { path: 'stages/list', component: StagesListComponent },
  { path: 'stage/add', component: StagesAddComponent },
  { path: 'stage/edit/:id', component: StagesEditComponent },

  { path: 'elements/list', component: ElementsListComponent },
  { path: 'element/add', component: ElementsAddComponent },
  { path: 'element/edit/:id', component: ElementsEditComponent },

  { path: 'tournaments/list', component: TournamentsListComponent },
  { path: 'tournament/add', component: TournamentsAddComponent },
  { path: 'tournament/edit/:id', component: TournamentsEditComponent },

  { path: 'players/list', component: PlayersListComponent },
  { path: 'player/add', component: PlayersAddComponent },
  { path: 'player/edit/:id', component: PlayersEditComponent },

  { path: 'seasons/list', component: SeasonsListComponent },
  { path: 'season/add', component: SeasonsAddComponent },
  { path: 'season/edit/:id', component: SeasonsEditComponent },

  { path: 'matches/list', component: MatchesListComponent },
  { path: 'match/add', component: MatchesAddComponent },
  { path: 'match/edit/:id', component: MatchesEditComponent },

  { path: 'news/list', component: NewsListComponent },
  { path: 'news-item/add', component: NewsAddComponent },
  { path: 'news-item/edit/:id', component: NewsEditComponent },

  { path: 'pages/list', component: PageListComponent },
  { path: 'page/edit/:id', component: PageEditComponent },

  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
