import {Directive} from "@angular/core";
import {NG_VALIDATORS, Validator, FormControl} from "@angular/forms";

@Directive({
  selector: "[appFileType]",
  providers: [
    { provide: NG_VALIDATORS, useExisting: FileTypeValidator, multi: true },
  ]
})
export class FileTypeValidator implements Validator {
  static validate(c: FormControl): {[key: string]: any} {
    return !c.value || c.value && c.value.type.startsWith('image') ? null : { "isNotImage" : true};
  }

  validate(c: FormControl): {[key: string]: any} {
    return FileTypeValidator.validate(c);
  }
}