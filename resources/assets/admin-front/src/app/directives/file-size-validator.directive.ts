import {Directive} from "@angular/core";
import {NG_VALIDATORS, Validator, FormControl} from "@angular/forms";

@Directive({
  selector: "[appFileSize]",
  providers: [
    { provide: NG_VALIDATORS, useExisting: FileSizeValidator, multi: true },
  ]
})
export class FileSizeValidator implements Validator {
  static validate(c: FormControl): {[key: string]: any} {
    return !c.value || c.value && c.value.size <= 5000000 ? null : { "sizeIsBig" : true};
  }

  validate(c: FormControl): {[key: string]: any} {
    return FileSizeValidator.validate(c);
  }
}