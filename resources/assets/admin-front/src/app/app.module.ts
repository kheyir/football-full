import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { NgSelectModule } from '@ng-select/ng-select';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgbModule, NgbTimepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {CookieService} from 'ngx-cookie-service';
import { LightboxModule } from 'ngx-lightbox';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { AngularEditorModule } from '@kolkov/angular-editor';

import { QuillModule } from 'ngx-quill'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { FooterComponent } from './container/footer/footer.component';
import { HeaderComponent } from './container/header/header.component';
import { LoaderComponent } from './container/loader/loader.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

import { LoaderService } from './services/loader.service';

import { ListComponent as GroupsListComponent } from './pages/groups/list/list.component';
import { AddComponent as GroupsAddComponent } from './pages/groups/add/add.component';
import { EditComponent as GroupsEditComponent } from './pages/groups/edit/edit.component';

import { ListComponent as CoachesListComponent } from './pages/coaches/list/list.component';
import { AddComponent as CoachesAddComponent } from './pages/coaches/add/add.component';
import { EditComponent as CoachesEditComponent } from './pages/coaches/edit/edit.component';

import { ListComponent as PositionsListComponent } from './pages/positions/list/list.component';
import { AddComponent as PositionsAddComponent } from './pages/positions/add/add.component';
import { EditComponent as PositionsEditComponent } from './pages/positions/edit/edit.component';

import { ListComponent as TournamentsCategoriesListComponent } from './pages/tournaments-categories/list/list.component';
import { AddComponent as TournamentsCategoriesAddComponent } from './pages/tournaments-categories/add/add.component';
import { EditComponent as TournamentsCategoriesEditComponent } from './pages/tournaments-categories/edit/edit.component';

import { ListComponent as CountriesListComponent } from './pages/countries/list/list.component';
import { AddComponent as CountriesAddComponent } from './pages/countries/add/add.component';
import { EditComponent as CountriesEditComponent } from './pages/countries/edit/edit.component';

import { ListComponent as CitiesListComponent } from './pages/cities/list/list.component';
import { AddComponent as CitiesAddComponent } from './pages/cities/add/add.component';
import { EditComponent as CitiesEditComponent } from './pages/cities/edit/edit.component';

import { ListComponent as StadiumsListComponent } from './pages/stadiums/list/list.component';
import { AddComponent as StadiumsAddComponent } from './pages/stadiums/add/add.component';
import { EditComponent as StadiumsEditComponent } from './pages/stadiums/edit/edit.component';

import { ListComponent as CategoriesListComponent } from './pages/categories/list/list.component';
import { AddComponent as CategoriesAddComponent } from './pages/categories/add/add.component';
import { EditComponent as CategoriesEditComponent } from './pages/categories/edit/edit.component';

import { ListComponent as TeamsListComponent } from './pages/teams/list/list.component';
import { AddComponent as TeamsAddComponent } from './pages/teams/add/add.component';
import { EditComponent as TeamsEditComponent } from './pages/teams/edit/edit.component';

import { ListComponent as StagesListComponent } from './pages/stages/list/list.component';
import { AddComponent as StagesAddComponent } from './pages/stages/add/add.component';
import { EditComponent as StagesEditComponent } from './pages/stages/edit/edit.component';

import { ListComponent as ElementsListComponent } from './pages/elements/list/list.component';
import { AddComponent as ElementsAddComponent } from './pages/elements/add/add.component';
import { EditComponent as ElementsEditComponent } from './pages/elements/edit/edit.component';

import { ListComponent as TournamentsListComponent } from './pages/tournaments/list/list.component';
import { AddComponent as TournamentsAddComponent } from './pages/tournaments/add/add.component';
import { EditComponent as TournamentsEditComponent } from './pages/tournaments/edit/edit.component';

import { ListComponent as PlayersListComponent } from './pages/players/list/list.component';
import { AddComponent as PlayersAddComponent } from './pages/players/add/add.component';
import { EditComponent as PlayersEditComponent } from './pages/players/edit/edit.component';

import { ListComponent as SeasonsListComponent } from './pages/seasons/list/list.component';
import { AddComponent as SeasonsAddComponent } from './pages/seasons/add/add.component';
import { EditComponent as SeasonsEditComponent } from './pages/seasons/edit/edit.component';

import { ListComponent as MatchesListComponent } from './pages/matches/list/list.component';
import { AddComponent as MatchesAddComponent } from './pages/matches/add/add.component';
import { EditComponent as MatchesEditComponent } from './pages/matches/edit/edit.component';

import { ListComponent as NewsListComponent } from './pages/news/list/list.component';
import { AddComponent as NewsAddComponent } from './pages/news/add/add.component';
import { EditComponent as NewsEditComponent } from './pages/news/edit/edit.component';

import { FileTypeValidator } from './directives/file-type-validator.directive';
import { FileSizeValidator } from './directives/file-size-validator.directive';
import { VideoModalComponent } from './container/video-modal/video-modal.component';
import { ListComponent } from './pages/pages/list/list.component';
import { EditComponent } from './pages/pages/edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    FooterComponent,
    HeaderComponent,
    LoaderComponent,
    DashboardComponent,

    GroupsListComponent,
    GroupsAddComponent,
    GroupsEditComponent,

    CoachesListComponent,
    CoachesAddComponent,
    CoachesEditComponent,

    PositionsListComponent,
    PositionsAddComponent,
    PositionsEditComponent,

    TournamentsCategoriesListComponent,
    TournamentsCategoriesAddComponent,
    TournamentsCategoriesEditComponent,

    CountriesListComponent,
    CountriesAddComponent,
    CountriesEditComponent,

    CitiesListComponent,
    CitiesAddComponent,
    CitiesEditComponent,

    StadiumsListComponent,
    StadiumsAddComponent,
    StadiumsEditComponent,

    CategoriesListComponent,
    CategoriesAddComponent,
    CategoriesEditComponent,

    TeamsListComponent,
    TeamsAddComponent,
    TeamsEditComponent,

    StagesListComponent,
    StagesAddComponent,
    StagesEditComponent,

    ElementsListComponent,
    ElementsAddComponent,
    ElementsEditComponent,

    TournamentsListComponent,
    TournamentsAddComponent,
    TournamentsEditComponent,

    PlayersListComponent,
    PlayersAddComponent,
    PlayersEditComponent,

    SeasonsListComponent,
    SeasonsAddComponent,
    SeasonsEditComponent,

    MatchesListComponent,
    MatchesAddComponent,
    MatchesEditComponent,

    NewsListComponent,
    NewsAddComponent,
    NewsEditComponent,

    FileTypeValidator,
    FileSizeValidator,
    VideoModalComponent,
    ListComponent,
    EditComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularEditorModule,
    NgxPaginationModule,
    NgbModule,
    NgbTimepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    LightboxModule,
    CKEditorModule,
    QuillModule.forRoot()

  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/foot'},
    LoaderService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
