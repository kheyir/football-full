import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  videoModalIsActive$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

  constructor() { }

  setVideoModalActivity(newVideoActivity: boolean){
    this.videoModalIsActive$.next(newVideoActivity)
  }
}
