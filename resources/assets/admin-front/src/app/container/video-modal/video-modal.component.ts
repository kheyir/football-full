import { Component, OnInit, Input } from '@angular/core';
import {SettingsService} from "../../store/settings.service";

@Component({
  selector: 'app-video-modal',
  templateUrl: './video-modal.component.html',
  styleUrls: ['./video-modal.component.sass']
})
export class VideoModalComponent implements OnInit {

  @Input() videoSrc: string;
  isActive = this.settingsService.videoModalIsActive$.value

  constructor(
      private settingsService: SettingsService
  ) {
    this.settingsService.videoModalIsActive$
        .subscribe(value => {
          this.isActive = value
        })
  }

  ngOnInit() {
  }

  closeVideModal(){
    this.settingsService.setVideoModalActivity(false)
  }

}
