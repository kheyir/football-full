import { Component, OnInit, OnDestroy } from '@angular/core';
import {LoaderService} from '../../services/loader.service'
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.sass']
})
export class LoaderComponent implements OnInit, OnDestroy {

  public loadingStatus$: any;

  public loaderStatus: boolean;

  constructor(private loaderService: LoaderService) {

    this.loadingStatus$ = this.loaderService.loadersCount$
        .subscribe(value => {
          this.loaderStatus = value > 0 ? true : false;
        })
  }

  ngOnInit() {
  }

  ngOnDestroy(): void{
    this.loadingStatus$.unsubscribe();
  }

}
