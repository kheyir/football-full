import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
// import { HeaderService } from './header.service';
import {environment} from '../../../environments/environment'
import {LoginService} from "../../services/login.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  username;

  constructor(
      private cookie: CookieService,
      private loginService: LoginService,
  ) {
  }

  ngOnInit() {
    this.getAuth()
  }

  getAuth(){

    this.username = null
    this.loginService.getAuth()
        .subscribe((response: any) => {
          if(response.code == 0){
            this.username = response.username
          }
        })
  }

  signOut(){

    this.loginService.logout()
        .subscribe((response: any) => {
          if(response.code == 0){

            if(environment.secure){
              this.cookie.delete("token", '/', environment.BASE_DOMAIN)
            }
            else{
              this.cookie.delete("token", '/', window.location.hostname)
            }

            window.location.href = "/foot"
          }
        })
  }

}
