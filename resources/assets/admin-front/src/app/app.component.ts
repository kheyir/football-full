import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {ScriptsService} from './services/scripts.service'
import {LoginService} from "./services/login.service";
import {ActivatedRoute, Router} from "@angular/router";
import {LoaderService} from "./services/loader.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  public loggedIn: number = -1;

  constructor(
      private router: Router,
      private scriptsService: ScriptsService,
      private loginService: LoginService,
      private loaderService: LoaderService
  ){}

  ngOnInit(){

    this.scriptsService.init();

    this.authCheck();
  }

  authCheck(){

    this.loaderService.increaseLoaderCount()
    this.loginService.check()
        .subscribe((response: any) => {

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
          if(response.code == 0){

            this.loggedIn = 1;
          }
          else{
              this.router.navigate(["/"])
              this.loggedIn = 0;
          }
        },
        (errorResponse) => {
          this.loggedIn = 0;

          this.loaderService.decreaseLoaderCount()
          this.loaderService.decreaseLoaderCount()
        })
  }

}
