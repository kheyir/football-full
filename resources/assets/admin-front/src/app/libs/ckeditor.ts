/**
 * @license Copyright (c) 2014-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';

class MainEditor extends ClassicEditor {
	builtinPlugins = [
		// SimpleUploadAdapter
	]

	defaultConfig = {
		toolbar: [ 'heading', '|', 'bold', 'italic', 'link', '|', 'undo', 'redo', '|', 'imageUpload', 'MediaEmbed'],
		mediaEmbed: {
			previewsInData: true
		},
		simpleUpload: {
			uploadUrl: 'http://example.com'
		}
	}
}

export default MainEditor;
