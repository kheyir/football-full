import { Injectable } from '@angular/core';
import { gsap } from 'gsap'
import {TweenLite, Power2, CSSPlugin} from "gsap/all";
import $ from 'jquery'
import { ActivatedRoute, Router } from '@angular/router';
import { SettingsService } from './store/settings.service';

gsap.registerPlugin(CSSPlugin)

@Injectable({
  providedIn: 'root'
})
export class ScriptsService {

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private settingsService: SettingsService
  ) { }

  init = () => {
    
    this.initialization();
  }

  ligasButtons = () => {
    $(".owl-stage").each(function(index, item){
        var owlItemWidth = 0;
        $(item).find(".owl-item").each((owlItemIndex, owlItemItem) =>{
            owlItemWidth += $(owlItemItem).outerWidth(true)
        })
        if(owlItemWidth < $(item).parent().width()){
            $(item).closest(".slider-wrapper").find(".slider-button-block").hide()
        }
        else{
            $(item).closest(".slider-wrapper").find(".slider-button-block").show()
        }
    })
  }

  initialization = () => {

    $(document).delegate(".nav-tabs__link", "click", (e) => {

        $(e.target).closest(".nav-tabs").find(".nav-tabs__link").each(function(index, item){

            $(item).closest(".nav-tabs__item").removeClass("active")

            $($(item).data("tab")).css("display", "none")
        })

        $(e.target).closest(".nav-tabs__item").addClass("active")

        let linkTag = $(e.target)

        if(!linkTag.hasClass(".nav-tabs__link")) linkTag = linkTag.closest(".nav-tabs__link")

        $(linkTag.data("tab")).css("display", "block")
    })

    // $("#teams-tab").tab('show')

    // $("#all-country").tab('show')

    // $("#members-tab").tab('show')

    // $('.select2-football-1').select2();

    $(".langs-block__selected").on("click", function(){
        if($(".langs-block__lang-list").is(":visible")) $(".langs-block__lang-list").hide()
        else $(".langs-block__lang-list").show()
    })

    $(document).delegate(".select2-section-football-1", "click", function(){
        $(".select2-results__options").find("[aria-selected='true']").css("background-color", "#F6F6F6")
        $(".select2-results__options").find("[aria-selected='true']").css("color", "#000")
    })


    function navMenuActive(){
        $("#open-menu").hide()
        $("#open-menu").removeClass("nav-mobile-menu-button")
        $("#close-menu").show()
        $(".mobile-nav-section").show()
    
        //window.scrollTo(0, 0);
        var navMobile = document.getElementById("mobile-nav-block");
        TweenLite.to(navMobile, 0.5, {right: "0px"});
        $("html").css("overflow-y", "hidden")
        //$("#nav-mobile").css("position", "fixed")
        //$("#nav-mobile").css("z-index", "999")

    }
    
    function navMenuDisactive(){
        $("#open-menu").show()
        $("#open-menu").addClass("nav-mobile-menu-button")
        $("#close-menu").hide()
    
        var navMobile = document.getElementById("mobile-nav-block");
        var right = window.innerWidth > 478 ? -375 : -window.innerWidth
        TweenLite.to(navMobile, 0.5, {right: `${right}px`, onComplete: function(){$(".mobile-nav-section").hide()} });
        $("html").css("overflow-y", "auto")
        //$("#nav-mobile").css("position", "unset")
        //$("#nav-mobile").css("z-index", "auto")
        console.log("aue")
    }

    $("#open-menu").on("click", navMenuActive)
    $("#close-menu").on("click", navMenuDisactive)
    $(document).delegate(".mobile-nav__list__item__link, .mob-langs-list__link", "click", function(){
        $("#close-menu").trigger("click")
    })

    // if($("#menu").is(":visible")){
    //     $(".search-input").css("width", $("#menu").width())
    //     $(".search-input").css("margin-right", "65px")
    // }
    // else{
    //     $(".search-input").css("width", "calc(100% - "+($("#icons-block").width() + 20) +"px)")
    // }

    //$(".search-input").css("width", $("#menu").is(":visible") ? $("#menu").width() : "calc(100% - "+$("#icons-block").width()+"px)")
    $("#open-search").on("click", function(){
        $("#menu").hide()
        $(".search-input--header").show()

        $("#open-search").hide()
        $("#close-search").show()
    })

    $("#close-search").on("click", function(){
        $("#menu").show()
        $(".search-input--header").hide()

        $("#open-search").show()
        $("#close-search").hide()
    })

    $("body").delegate(".filter-open-button", "click", function(e){
        console.log("click", e)
        if($(e.target).hasClass("active")){
            $(e.target).next().hide()
            $(e.target).removeClass("active")
        } 
        else{
            $(e.target).next().show()
            $(e.target).addClass("active")
        }
    })

    $("body").delegate(".table-game-video tr", "click", function(){
        $(this).parent().find("tr").css("display", "none")
        $(this).css("display", "table-row")
        $(this).closest(".table-game-video-block").find("#main-goal-videos").show()
        $(this).closest(".table-game-video-block").find(".table-game-video-button").css("display", "flex")
        $(this).closest(".table-game-video-block").find(".table-game-video__text").hide()
    })

    $(document).delegate(".table-game-video-button", "click", function(){
        // console.log($(this).closest(".table-game-video-block"))
        $(".table-game-video-block").find(".table-game-video").first().find("tr").css("display", "table-row")
        $(".table-game-video-block").find("#main-goal-videos").hide()
        // $("display", "none")
        $(".table-game-video-block").find(".table-game-video__text").show()
    });


    this.contentHeightChange();

    $(window).resize(() => {
      this.ligasButtons();
      this.contentHeightChange();
    });

  }

  checkLangParam(lang){

    if(["az", "ru", "en"].indexOf(lang) > -1 || lang == null){

        if(lang == null) lang = "az"

        this.settingsService.setLang(lang)
    }
    else{
        this.router.navigate(["/404"])
    }
  }

  contentHeightChange = () => {
    let headerSectionHeight = $("#header-section").outerHeight(true)
    let navSection = $("#nav-section").outerHeight(true)

    let footer = $(".footer").outerHeight(true)

    $(".content").css("min-height", `calc(100vh - ${headerSectionHeight + navSection + footer}px)`)
  }
}
