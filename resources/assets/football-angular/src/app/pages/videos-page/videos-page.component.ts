import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { TranslateService } from '@ngx-translate/core';
import $ from 'jquery';
import {ActivatedRoute} from "@angular/router";
import { CategoriesService } from '../../services/categories.service';
import { TournamentsService } from '../../services/tournaments.service';
import { SeasonsService } from '../../services/seasons.service';
import { StagesService } from '../../services/stages.service';
import { MatchesService } from '../../services/matches.service';
import { PlayersService } from '../../services/players.service';
import { TeamsService } from '../../services/teams.service';
import { VideosService } from '../../services/videos.service';
import { ScriptsService } from '../../scripts.service';
import { LoaderService } from '../../services/loader.service';
import { SettingsService } from '../../store/settings.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-videos-page',
  templateUrl: './videos-page.component.html',
  styleUrls: ['./videos-page.component.sass']
})
export class VideosPageComponent implements OnInit {

  allNews = 'Hamısı'

  matchGroup: FormGroup;
  playerGroup: FormGroup;
  teamGroup: FormGroup;

  tabType = 'category';
  categoryId = null;

  categories = null;

  matches = null;
  players = null;
  teams = null;

  matchesPage = 1;
  playersPage = 1;
  teamsPage = 1;

  matchId = null;
  playerId = null;
  teamId = null;

  videos = null;
  videosPage = 1;

  selectedVideoSrc = null;


  optionsVideos: OwlOptions = {
    items: 5,
    loop: false,
    nav: false,
    dots: false,
    autoWidth: true,
    touchDrag: false,
    margin: 16,
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 2
        },
        768: {
            items: 3
        }
    }
}

  tournaments = [{name: this.allNews, id: 0}]

  seasons = [{label: this.allNews, id: 0}]

  stages = [{stage_name: this.allNews, stage_id: 0}]

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private categoriesService: CategoriesService,
      private tournamentsService: TournamentsService,
      private seasonsService: SeasonsService,
      private stagesService: StagesService,
      private matchesService: MatchesService,
      private playersService: PlayersService,
      private teamsService: TeamsService,
      private videosService: VideosService,
      private scriptsService: ScriptsService,
      private loaderService: LoaderService,
      private settingsService: SettingsService,
      private translateService: TranslateService,
      private _seoService: SEOService
  ) {
  }

  ngOnInit() {

    this.createAllForms()

    this.route.paramMap
      .subscribe(params => {

          this.translateService.get('all')
              .subscribe((text: string) => {

                  this.allNews = text;

                  this.tournaments[0].name = text;
                  this.tournaments = [...this.tournaments]

                  this.seasons[0].label = text;
                  this.seasons = [...this.seasons]

                  this.stages[0].stage_name = text;
                  this.stages = [...this.stages]

              })

          this.getVideoCategories()
          this.getTournaments()
      })

    $(".nav-tabs").each(function(index, item){
        $(item).find(".nav-tabs__item").first().addClass("active")
    })

    this.loaderService.decreaseLoaderCount()

    this._seoService.seoVideo()
  }

  createAllForms(){
      this.matchGroup = this.formBuilder.group({
          tournament_id: [0],
          season_id: [0],
          stage_id: [0]
      })

      this.playerGroup = this.formBuilder.group({
          player_name: [null, [Validators.minLength(2), Validators.maxLength(200)]],
      })

      this.teamGroup = this.formBuilder.group({
          team_name: [null, [Validators.minLength(2), Validators.maxLength(200)]],
      })
  }

  getVideoCategories(){
    let data = {
        is_for_video: 1
    }

    this.categoriesService.getCategories(data)
        .subscribe((response: any) => {
            if(response.code == 0){
                this.categories = response.categories;

                if(this.categories.length > 0){
                    this.categoryId = this.categories[0].id

                    this.getMatches();
                }
                else{
                    this.tabType = "players"
                }

                setTimeout(() => {
                    this.scriptsService.ligasButtons()
                }, 1000);
            }
        })
  }



  getTournaments(){
    this.tournaments = [{name: this.allNews, id: 0}];

    this.matchGroup.patchValue({
        tournament_id: 0
    })

    this.loaderService.increaseLoaderCount();

    this.tournamentsService.getAll()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.tournaments = [this.tournaments[0], ...response.tournaments]

                this.matchGroup.patchValue({
                    tournament_id: 0
                })
            }
        })
  }

  getSeasons(data: any = {}){
    this.seasons = [{label: this.allNews, id: 0}];

    this.matchGroup.patchValue({
        season_id: 0
    })

    this.loaderService.increaseLoaderCount();

    this.seasonsService.getAll(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.seasons = [this.seasons[0], ...response.seasons.map(item => {
                    item.label = item.start_year + (item.end_year ? ' - ' + item.end_year : '')
                    return item
                })];

                this.matchGroup.patchValue({
                    season_id: 0
                })
            }
        })
  }

  getStages(data: any = {}){
    this.stages = [{stage_name: this.allNews, stage_id: 0}];

    this.matchGroup.patchValue({
        stage_id: 0,
    })

    this.loaderService.increaseLoaderCount();

    this.stagesService.getStagesBySeason(data)
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.stages = [this.stages[0], ...response.stages]

                this.matchGroup.patchValue({
                    stage_id: 0
                })
            }
        })
  }

  getMatches(){
    this.matches = null;

    let data = {
        page: this.matchesPage,
        category_id: this.categoryId,
        tournament_id: this.matchGroup.get('tournament_id').value,
        season_id: this.matchGroup.get('season_id').value,
        stage_id: this.matchGroup.get('stage_id').value,
        matchesPage: this.matchesPage
    }

    this.matchesService.getMatchesThatHaveVideo(data)
        .subscribe((response: any) => {

            if(response.code == 0){
                this.matches = response.matches;
            }
        })
  }

  getPlayers(){
    this.players = null;

    let data = {
        page: this.playersPage,
        player_id: this.playerId,
        player_name: this.playerGroup.get('player_name').value,
    }

    this.playersService.getPlayers(data)
        .subscribe((response: any) => {

            if(response.code == 0){
                this.players = response.players;
            }
        })
  }

  getTeams(){
    this.teams = null;

    let data = {
        page: this.teamsPage,
        team_name: this.teamGroup.get('team_name').value,
    }

    this.teamsService.getTeams(data)
        .subscribe((response: any) => {

            if(response.code == 0){
                this.teams = response.teams;
            }
        })
  }

  getVideos(){
    this.videos = null;

    let data = {
        page: this.videosPage,
        match_id: this.matchId,
        player_id: this.playerId,
        team_id: this.teamId,
    }

    this.videosService.getVideos(data)
        .subscribe((response: any) => {
            if(response.code == 0){
                this.videos = response.videos;
            }
        })
  }

  onTournamentChange(){
    let data = {
        tournament_id: this.matchGroup.get("tournament_id").value
    }

    this.getSeasons(data)
  }

  onSeasonsChange(){
    let data = {
        id: this.matchGroup.get("season_id").value
    }

    this.getStages(data)
  }

  changeTab(type, categoryId){
      this.tabType = type;
      this.categoryId = categoryId;

      switch(this.tabType){
          case 'category':
              this.getMatches();
              break;
          case 'players':
              this.getPlayers();
              break;
          case 'teams':
              this.getTeams();
              break;
      }
  }

  prepareVideos(id){

      if(id != null){
          switch(this.tabType){
              case 'category':
                  this.matchId = id
                  break;
              case 'players':
                  this.playerId = id
                  break;
              case 'teams':
                  this.teamId = id
                  break;
          }

          this.getVideos();
      }else{
          this.matchId = null;
          this.playerId = null;
          this.teamId = null;
      }
  }

  showVideoModal(videoSrc){
    this.selectedVideoSrc = videoSrc;

    this.settingsService.setVideoModalActivity(true);
  }

  matchPageChanged(page){
    this.matchesPage = page;

    this.getMatches();
  }

  playerPageChanged(page){
    this.playersPage = page;

    this.getPlayers();
  }

  teamPageChanged(page){
    this.teamsPage = page;

    this.getTeams();
  }

  videoPageChanged(page){
    this.videosPage = page;

    this.getVideos();
  }

}
