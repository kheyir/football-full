import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { DomSanitizer } from '@angular/platform-browser';
import {LoaderService} from "../../services/loader.service";
import {PagesService} from "../../services/pages.service";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.sass']
})
export class AboutComponent implements OnInit {

  about;

  bypassSecurityTrustHtml(large_text){
    return this.sanitizer.bypassSecurityTrustHtml(large_text);
  }

  constructor(
      private loaderService: LoaderService,
      private pagesService: PagesService,
      private router: Router,
      private sanitizer: DomSanitizer,
      private _seoService: SEOService
  ) { }

  ngOnInit() {
    this.getAbout();

    this._seoService.seoAbout()
  }

  getAbout(){
    this.about = null;

    this.loaderService.increaseLoaderCount();
    this.pagesService.getAbout()
        .subscribe((response: any) => {
              this.loaderService.decreaseLoaderCount()

              if(response.code == 0){

                this.loaderService.decreaseLoaderCount()

                this.about = response.pageItem;
              }
            },
            (errorResponse: any) => {
              this.loaderService.decreaseLoaderCount()
              this.router.navigate(["404"])
            })
  }

}
