import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentsGlobalComponent } from './tournaments-global.component';

describe('TournamentsGlobalComponent', () => {
  let component: TournamentsGlobalComponent;
  let fixture: ComponentFixture<TournamentsGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentsGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentsGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
