import { Component, OnInit, HostListener } from '@angular/core';
import $ from 'jquery';
import { TranslateService } from '@ngx-translate/core';
import {LoaderService} from "../../services/loader.service";
import {TournamentsService} from "../../services/tournaments.service";
import {MatchesService} from "../../services/matches.service";
import { SettingsService } from 'src/app/store/settings.service';
import {ActivatedRoute} from "@angular/router";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.sass']
})
export class ReviewComponent implements OnInit {

  langLocal = this.settingsService.lang$.value;

  tournaments = null;
  responseMatches = null;

  matchesPage = 1;

  tournamentId = null;
  statusNgSelected = null;
  dateNgSelected = null;
  searchWord = null;

  statusesNgSelect: any[] = [
    {label: 'HAMISI', value: 0},
    {label: 'Gələcək', value: 1},
    {label: 'Yekunlaşmış', value: 2},
  ];

  innerWidth = window.innerWidth;

  @HostListener('window:resize', ['$event']) onResize(event) {
      this.innerWidth = window.innerWidth;
  }

  constructor(
      private route: ActivatedRoute,
      private loaderService: LoaderService,
      private tournamentsService: TournamentsService,
      private matchesService: MatchesService,
      private settingsService: SettingsService,
      private translateService: TranslateService,
      private _seoService: SEOService
  ) {

      this.settingsService.lang$
          .subscribe(value => {
              this.langLocal = value;
          })
  }

  ngOnInit() {

    $(".tab-content").each(function(index, item){
      $(item).find(".tab-pane").first().css("display", "block")
    })
    $(".nav-tabs").each(function(index, item){
        $(item).find(".nav-tabs__item").first().addClass("active")
    })

      this.route.paramMap
          .subscribe(params => {

              this.translateService.get('all')
                  .subscribe((text: string) => {
                      this.statusesNgSelect[0].label = text;
                      this.statusesNgSelect = [...this.statusesNgSelect];
                  })

              this.translateService.get('future')
                  .subscribe((text: string) => {
                      this.statusesNgSelect[1].label = text;
                      this.statusesNgSelect = [...this.statusesNgSelect];
                  })

              this.translateService.get('finished')
                  .subscribe((text: string) => {
                      this.statusesNgSelect[2].label = text;
                      this.statusesNgSelect = [...this.statusesNgSelect];
                  })

              this.getTournamentsPriorityForCountry()
              this.getMatches()
          })

    this._seoService.seoReview()
  }

  getTournamentsPriorityForCountry(){
    this.loaderService.increaseLoaderCount()

    this.tournaments = null;
    this.tournamentsService.getTournamentsPriorityForCountry()
        .subscribe((response: any) => {

            this.loaderService.decreaseLoaderCount()
            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.tournaments = response.tournaments
            }
        },
        (errorResponse) => {

            this.loaderService.decreaseLoaderCount()
            this.loaderService.decreaseLoaderCount()
        })
  }

  getMatches(){

        this.responseMatches = null;

        let data = {
            page: this.matchesPage,
            tournamentId: this.tournamentId,
            status: this.statusNgSelected,
            date: this.dateNgSelected ? `${this.dateNgSelected.year}-${this.dateNgSelected.month}-${this.dateNgSelected.day}` : null,
            search: this.searchWord
        }
        this.matchesService.getMatches(data)
            .subscribe((response: any) => {

                    if(response.code == 0){
                        this.responseMatches = response.matches
                    }
                },
                (errorResponse) => {

                })
    }

    tournamentClick(tournamentId){
      this.tournamentId = tournamentId;

      this.getMatches()
    }

    pageChanged(page){
      this.matchesPage = page;

      this.getMatches()
    }

    filter(){

        this.getMatches()

        console.log(this.statusNgSelected, this.dateNgSelected, this.searchWord)
    }

}
