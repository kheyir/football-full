import { Component, OnInit, HostListener } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ScriptsService } from '../../scripts.service';
import $ from 'jquery'
import {NewsService} from "../../services/news.service";
import {TournamentsService} from "../../services/tournaments.service";
import {VideosService} from "../../services/videos.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SettingsService } from 'src/app/store/settings.service';
import {LoaderService} from "../../services/loader.service";
import {MatchesService} from "../../services/matches.service";
import {GroupsService} from "../../services/groups.service";
import { HomeService } from 'src/app/services/home.service';
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

    inHomeNews = null;
    topNews = null;
    latestNews = null;
    matchesTournaments = null;
    matchesTournamentId = null;
    matches = null;
    sidebarGroups = null;
    sidebarGroupsTournamentName = null;
    sidebarTournaments = null;
    videosTournaments = null;
    topVideos = null;
    selectedVideoSrc = ""

    langLocal = this.settingsService.lang$.value;

    optionsMainNews: OwlOptions = {
        items: 1,
        loop: true,
        autoplay: true,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        touchDrag: false,
    }

      optionsLigas: OwlOptions = {
        items: 4,
        loop: false,
        nav: false,
        dots: false,
        touchDrag: false,
        margin: 16,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 4
            }
        }
      }

      optionsSidebarSlider: OwlOptions = {
        items: 1,
        loop: true,
        nav: false,
        dots: false,
        autoplay: true,
        autoplayHoverPause: true,
        touchDrag: false,
      }

    innerWidth = window.innerWidth;

    @HostListener('window:resize', ['$event']) onResize(event) {
        this.innerWidth = window.innerWidth;
    }

  constructor(private scriptsService: ScriptsService,
              private newsService: NewsService,
              private tournamentsService: TournamentsService,
              private videosService: VideosService,
              private matchesService: MatchesService,
              private groupsService: GroupsService,
              private route: ActivatedRoute,
              private settingsService: SettingsService,
              private loaderService: LoaderService,
              private homeService: HomeService,
              private _seoService: SEOService
  ) {

    this.settingsService.lang$
      .subscribe(value => {
        this.langLocal = value;
      })
  }

    ngOnInit() {

        this.scriptsService.checkLangParam(this.route.snapshot.paramMap.get("lang"));

        $(".tab-content").each(function(index, item){
            $(item).find(".tab-pane").first().css("display", "block")
        })
        $(".nav-tabs").each(function(index, item){
            $(item).find(".nav-tabs__item").first().addClass("active")
        })

        this.scriptsService.ligasButtons()

        this.route.paramMap
            .subscribe(params => {
                this.getHomeItems()

                // this.getSlidersNews()
                // this.getTops()
                // this.getLatest()
                // this.getTournamentsPriorityForCountry()
                this.getMatches()
                // this.getTournamentsIsVideosCategory()
                this.getTopVideosByTournament(0)
                this.getSidebarTournamentsForHome()
            })

        // this.loaderService.decreaseLoaderCount()

        this._seoService.seoMain()
    }

    getHomeItems(){
        this.inHomeNews = null;
        this.topNews = null;
        this.latestNews = null;
        this.matchesTournaments = null;
        this.videosTournaments = null;

        this.loaderService.increaseLoaderCount()
        this.homeService.getHomeItems()
            .subscribe((response: any) => {
                this.loaderService.decreaseLoaderCount()
                this.loaderService.decreaseLoaderCount()

                if(response.code == 0){
                    this.inHomeNews = response.inHomeNews;
                    this.topNews = response.topNews;
                    this.latestNews = response.latestNews;
                    this.matchesTournaments = response.tournaments;
                    this.videosTournaments = response.videoCategoryTournaments;
                }
            },
            errorResponse => {
                this.loaderService.decreaseLoaderCount()
                this.loaderService.decreaseLoaderCount()
            })
    }

    getSlidersNews(){
        this.inHomeNews = null;

        this.newsService.getSlidersNews()
        .subscribe((response: any) => {
            if(response.code == 0){
                this.inHomeNews = response.inHomeNews
            }
        })
    }

    getTops(){
        this.topNews = null;

        this.newsService.getTops()
            .subscribe((response: any) => {
                if(response.code == 0){
                    this.topNews = response.topNews
                }
            })
    }

    getLatest(){
        this.newsService.getLatest()
            .subscribe((response: any) => {
                if(response.code == 0){
                    this.latestNews = response.latestNews
                }
            })
    }

    getTournamentsIsVideosCategory(){
        this.tournamentsService.getTournamentsIsVideosCategory()
            .subscribe((response: any) => {
                if(response.code == 0){
                    this.videosTournaments = response.tournaments
                }
            })
    }

    getTopVideosByTournament(id){
        let data = {
            id: id
        }

        this.topVideos = null;

        this.videosService.getTopVideosByTournament(data)
            .subscribe((response: any) => {
                if(response.code == 0){
                    this.topVideos = response.videos
                }
            })
    }

    showVideoModal(videoSrc){
        this.selectedVideoSrc = videoSrc

        this.settingsService.setVideoModalActivity(true)
    }

    getTodayMatches(tournamentId){
        this.matchesTournamentId = tournamentId;

        this.getMatches()
    }

    getTournamentsPriorityForCountry(){

        this.matchesTournaments = null;
        this.tournamentsService.getTournamentsPriorityForCountry()
            .subscribe((response: any) => {

                    if(response.code == 0){
                        this.matchesTournaments = response.tournaments
                    }
                })
    }

    getMatches(){
        let data = {
            tournamentId: this.matchesTournamentId,
            forHome: 1
        }

        this.matches = null;
        this.matchesService.getMatches(data)
            .subscribe((response: any) => {
                if(response.code == 0){

                    let tmpArray = [];
                    response.matches.data.map((item) => {
                        tmpArray = [...tmpArray, ...item.matches]
                    })

                    this.matches = []
                    this.matches.push(tmpArray.splice(0, 5))
                    this.matches.push(tmpArray.splice(0, 5))

                    console.log(this.matches)
                }
            })
    }

    getSidebarTournamentsForHome(){
        this.groupsService.getSidebarTournamentsForHome()
            .subscribe((response: any) => {
                if(response.code == 0){
                    this.sidebarTournaments = response.tournaments
                    // this.sidebarGroups = response.groups
                    // this.sidebarGroupsTournamentName = response.tournament_name
                }
            })
    }

}
