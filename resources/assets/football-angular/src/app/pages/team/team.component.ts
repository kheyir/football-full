import { Component, OnInit, HostListener } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import $ from 'jquery';
import { ScriptsService } from '../../scripts.service';
import {SettingsService} from "../../store/settings.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TeamsService} from "../../services/teams.service";
import {NewsService} from "../../services/news.service";
import {PlayersService} from "../../services/players.service";
import {MatchesService} from "../../services/matches.service";
import {VideosService} from "../../services/videos.service";
import {LoaderService} from "../../services/loader.service";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.sass']
})
export class TeamComponent implements OnInit {

  teamSlug = null;
  teamId = null;
  team = null
  news = null
  players = null;
  matches = null;
  withVideosMatches = null;
  videos = null

  newsPage = 1;
  matchesPage = 1;
  matchesWithVideosPage = 1;

  selectedVideoSrc = "";

  langLocal = this.settingsService.lang$.value;

  selectedVideoMatchIndex = null;

  optionsTeam: OwlOptions = {
    items: 5,
    loop: false,
    nav: false,
    dots: false,
    autoWidth: true,
    touchDrag: false,
    margin: 16,
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 2
        },
        768: {
            items: 3
        }
    }
  }

  innerWidth = window.innerWidth;

  @HostListener('window:resize', ['$event']) onResize(event) {
      this.innerWidth = window.innerWidth;
  }

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private scriptsService: ScriptsService,
      private settingsService: SettingsService,
      private teamService: TeamsService,
      private newsService: NewsService,
      private playersService: PlayersService,
      private matchesService: MatchesService,
      private videosService: VideosService,
      private loaderService: LoaderService,
      private _seoService: SEOService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value;
        })
  }

  ngOnInit() {

    $(".tab-content").each(function(index, item){
      $(item).find(".tab-pane").first().css("display", "block")
    })
    $(".nav-tabs").each(function(index, item){
        $(item).find(".nav-tabs__item").first().addClass("active")
    })

    setTimeout(() => {
        this.scriptsService.init()
        this.scriptsService.ligasButtons()
    }, 1000);

    this.route.paramMap
        .subscribe(params => {

          this.teamSlug = params.get("slug")

          let data = {
            slug: this.teamSlug
          }

          this.getTeamBySlug(data)
        })
  }

  getTeamBySlug(data){

    this.loaderService.increaseLoaderCount()

    this.teamService.getTeamBySlug(data)
      .subscribe((response: any) => {
        this.loaderService.decreaseLoaderCount()

        if(response.code == 0){
          this.team = response.team;

          this.teamId = this.team.id;

          this.loaderService.decreaseLoaderCount();


          let newsData = {
                id: this.teamId,
                page: this.newsPage
          }

          this.getNewsByTeam(newsData);

          this.getPlayersByTeam(newsData);

          let matchesData = {
                id: this.teamId,
                page: this.matchesPage
          }

          this.getMatchesByTeam(matchesData);

          let matchesWithVideosData = {
                id: this.teamId,
                page: this.matchesWithVideosPage
          }

          this.getMatchesByTeamWithVideos(matchesWithVideosData)

          let seoData = {
              teamName: this.team.team_name
          }
          this._seoService.seoTeam(seoData);
        }
        else{
            this.router.navigate(["404"])
        }
      },
      (errorResponse: any) => {
          this.loaderService.decreaseLoaderCount()
          this.router.navigate(["404"])
      })
  }

  getNewsByTeam(data){

    this.news = null;

    this.newsService.getNewsByTeam(data)
        .subscribe((response: any) => {
            if(response.code == 0){
                this.news = response.news
            }
            else{
                this.router.navigate(["404"])
            }
        })
  }

  getPlayersByTeam(data){

    this.players = null;

    this.playersService.getPlayersByTeam(data)
        .subscribe((response: any) => {
            if(response.code == 0){
                this.players = response.players
            }
            else{
                this.router.navigate(["404"])
            }
        })
  }

  getMatchesByTeam(data){

    this.matches = null;

    this.matchesService.getMatchesByTeam(data)
        .subscribe((response: any) => {
            if(response.code == 0){
                this.matches = response.matches
            }
        })
  }

  getMatchesByTeamWithVideos(data){

      this.withVideosMatches = null;

        this.matchesService.getMatchesByTeamWithVideos(data)
            .subscribe((response: any) => {
                if(response.code == 0){
                    this.withVideosMatches = response.matches
                }
            })
    }

  getVideosByTeamAndMatch(data){

      this.videos = null
      this.selectedVideoSrc = "";

      this.videosService.getVideosByTeamAndMatch(data)
        .subscribe((response: any) => {
            if(response.code == 0){
                this.videos = response.videos
            }
        })
  }


  newsClick(){
    let data = {
        page: this.newsPage,
        id: this.teamId
    }

    this.getNewsByTeam(data)
  }


  matchesClick(){
    let data = {
        id: this.teamId,
        page: this.matchesPage
    }

    this.getMatchesByTeam(data)
  }

    playersClick(){
        let data = {
            id: this.teamId
        }

        this.getPlayersByTeam(data)
    }


    videosClick(){
        let data = {
            id: this.teamId,
            page: this.matchesWithVideosPage
        }

        this.getMatchesByTeamWithVideos(data)
    }

    pageChanged(page){

        this.newsPage = page;

        let data = {
            page: this.newsPage,
            id: this.teamId
        }

        this.getNewsByTeam(data)
    }

    matchesPageChanged(page){

        this.matchesPage = page;

        let data = {
            id: this.teamId,
            page: this.matchesPage
        }

        this.getMatchesByTeam(data)
    }

    videoMatchesPageChanged(page){

        this.matchesWithVideosPage = page;

        let data = {
            id: this.teamId,
            page: this.matchesWithVideosPage
        }

        this.getMatchesByTeamWithVideos(data)
    }

    videoMatchClick(index){
        this.selectedVideoMatchIndex = index

        if(this.selectedVideoMatchIndex != null){
            let data = {
                teamId: this.teamId,
                matchId: this.withVideosMatches.data[index].id
            }

            this.getVideosByTeamAndMatch(data)
        }
    }

    showVideoModal(videoSrc){
        this.selectedVideoSrc = videoSrc

        this.settingsService.setVideoModalActivity(true)
    }

}
