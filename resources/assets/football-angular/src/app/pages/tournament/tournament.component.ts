import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { TranslateService } from '@ngx-translate/core';
import $ from 'jquery';
import { ScriptsService } from '../../scripts.service';
import { TournamentsService } from '../../services/tournaments.service';
import {ActivatedRoute, Router} from "@angular/router";
import {NewsService} from "../../services/news.service";
import {TeamsService} from "../../services/teams.service";
import {SettingsService} from "../../store/settings.service";
import {MatchesService} from "../../services/matches.service";
import {ElementsService} from "../../services/elements.service";
import {StagesService} from "../../services/stages.service";
import {GroupsService} from "../../services/groups.service";
import {LoaderService} from "../../services/loader.service";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.sass']
})
export class TournamentComponent implements OnInit {

  tournament = null;
  seasons = [];
  selectedSeasonId = null;
  selectedSeasonParticipantCount = null;

  listGroups = null;
  listMatches = null;

  sidebarGroups = null;

  news = null;
  teams = null;
  matches = null;
  elements = null;
  stages = null;

  newsPage = 1;

  langLocal = this.settingsService.lang$.value;

  optionsLigas: OwlOptions = {
    items: 5,
    loop: false,
    nav: false,
    dots: false,
    autoWidth: true,
    touchDrag: false,
    margin: 16,
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 2
        },
        768: {
            items: 3
        }
    }
}

  optionsSidebarSlider: OwlOptions = {
    items: 1,
    loop: true,
    nav: false,
    dots: false,
    autoplay: true,
    autoplayHoverPause: true,
    touchDrag: true,
  }


  elementsNgSelect = [{label: 'Hamısı', id: 0}];
  teamsNgSelect = [{name: 'Hamısı', id: 0}]
  matchesNgSelect = [
      {label: 'Hamısı', id: 0},
      {label: 'Gələcək', id: 1},
      {label: 'Yekunlaşmış', id: 2},
  ];

  stagesNgSelect = [];


  elementsNgSelectedId = 0;
  groupsNgSelectedId = 0;
  teamsNgSelectedId = 0;
  matchesNgSelectedId = 0;

  stagesNgSelectedId = null;


  innerWidth = window.innerWidth;

  @HostListener('window:resize', ['$event']) onResize(event) {
      this.innerWidth = window.innerWidth;
  }

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private scriptsService: ScriptsService,
      private tournamentsService: TournamentsService,
      private newsService: NewsService,
      private teamsService: TeamsService,
      private matchesService: MatchesService,
      private elementsService: ElementsService,
      private stagesService: StagesService,
      private groupsService: GroupsService,
      private settingsService: SettingsService,
      private loaderService: LoaderService,
    private translateService: TranslateService,
    private _seoService: SEOService
  ) {
      this.settingsService.lang$
          .subscribe(value => {
              this.langLocal = value;
          })
  }

  ngOnInit() {

    $(".tab-content").each(function(index, item){
      $(item).find(".tab-pane").first().css("display", "block")
    })
    $(".nav-tabs").each(function(index, item){
        $(item).find(".nav-tabs__item").first().addClass("active")
    })

    setTimeout(() => {
      this.scriptsService.ligasButtons()
    }, 1000);

    this.route.paramMap
        .subscribe(params => {

          this.translateService.get('all')
              .subscribe((text: string) => {

                this.teamsNgSelect[0].name = text;
                this.elementsNgSelect[0].label = text;

                this.matchesNgSelect[0].label = text;
                this.matchesNgSelect = [...this.matchesNgSelect];
              })

          this.translateService.get('future')
              .subscribe((text: string) => {
                this.matchesNgSelect[1].label = text;
                this.matchesNgSelect = [...this.matchesNgSelect];
              })

          this.translateService.get('finished')
              .subscribe((text: string) => {
                this.matchesNgSelect[2].label = text;
                this.matchesNgSelect = [...this.matchesNgSelect];
              })

          let data = {
            slug: params.get("slug")
          }

          this.getTournamentBySlug(data)
        })
  }

  getTournamentBySlug(data){
    this.tournament = null;
    this.seasons = null;
    this.selectedSeasonId = null;

    this.loaderService.increaseLoaderCount()

    this.tournamentsService.getTournamentBySlug(data)
      .subscribe((response: any) => {
        this.loaderService.decreaseLoaderCount()

        if(response.code == 0){
          this.tournament = response.tournament
          this.seasons = response.seasons.map(item => {
            item.label = item.start_year + (item.end_year != null ? ' - ' + item.end_year : '');
            return item
          });

          if(this.seasons.length > 0){
              this.selectedSeasonId = this.seasons[0].id;
              this.selectedSeasonParticipantCount = this.seasons[0].teams_count;
          }


          let newsData = {
            id: this.selectedSeasonId,
            page: this.newsPage
          }


          this.getNews(newsData);
          this.getTeamsBySeason();
          this.getMatchesBySeason();
          this.getElementsBySeason();
          this.getStagesBySeason();
          this.getTournamentSidebarGroups();

          this.loaderService.decreaseLoaderCount()

          let seoData = {
              tournamentName: `${this.tournament.tournament_name}`
          }

          if(this.seasons.length > 0){
            seoData.tournamentName += ` (${this.seasons[0].start_year + (this.seasons[0].end_year ? `-${this.seasons[0].end_year}` : "")})`
          }

          this._seoService.seoTournament(seoData)
        }
        else{
          this.router.navigate(["404"])
        }
      },
      (errorResponse: any) => {
          this.loaderService.decreaseLoaderCount()
          this.router.navigate(["404"])
      })
  }

  getNews(data){
      this.news = null;
      this.newsService.getNewsBySeason(data)
          .subscribe((response: any) => {
              if(response.code == 0){
                  this.news = response.news
              }
          })
  }

  getTeamsBySeason(){

      if(this.selectedSeasonId){

          let data = {
              id: this.selectedSeasonId
          }

          this.teams = null
          this.teamsService.getTeamsBySeason(data)
              .subscribe((response: any) => {
                  this.teams = response.teams;
                  this.teamsNgSelect = [this.teamsNgSelect[0], ...this.teams]
              })
      }
  }

  getMatchesBySeason(){

      if(this.selectedSeasonId){
          let data = {
              id: this.selectedSeasonId,
              element_id: this.elementsNgSelectedId,
              group_id: this.groupsNgSelectedId,
              team_id: this.teamsNgSelectedId,
              status: this.matchesNgSelectedId
          }

          this.matches = null;
          this.matchesService.getMatchesBySeason(data)
              .subscribe((response: any) => {
                  this.matches = response.matches
              })
      }
  }

  getMatchesBySeasonAndStage(){

      if(this.selectedSeasonId && this.stagesNgSelectedId != null){
          let data = {
              season_id: this.selectedSeasonId,
              stage_id: this.stagesNgSelectedId,
          }

          this.listGroups = null;
          this.listMatches = null;
          this.matchesService.getMatchesBySeasonAndStage(data)
              .subscribe((response: any) => {
                  if(response.code == 0){
                      if(response.has_groups){
                          this.listGroups = response.groups

                          this.listMatches = []
                      }
                      else{
                          this.listMatches = response.matches

                          this.listGroups = []
                      }
                  }
              })
      }
    }

  getElementsBySeason(){
      if(this.selectedSeasonId){

          let data = {
              id: this.selectedSeasonId
          }

          this.elements = null;
          this.elementsService.getElementsBySeason(data)
              .subscribe((response: any) => {
                  this.elements = response.elements
                  this.elementsNgSelect = [this.elementsNgSelect[0], ...this.elements.map(item => {
                      item.id = item.element_id
                      item.label = item.element_name + (item.has_groups ? ' ' + item.group_name : '')

                      return item
                  })];

              })
      }
  }

  getStagesBySeason(){
      if(this.selectedSeasonId){

          let data = {
              id: this.selectedSeasonId
          }

          this.stages = null;
          this.stagesNgSelectedId = null;

          this.stagesService.getStagesBySeason(data)
              .subscribe((response: any) => {
                  this.stages = response.stages

                  this.stagesNgSelect = this.stages
                  if(this.stages.length > 0){
                      this.stagesNgSelectedId = this.stages[this.stages.length - 1].stage_id;

                      this.getMatchesBySeasonAndStage()
                  }
              })
      }
  }

  getTournamentSidebarGroups(){

        if(this.selectedSeasonId){
            let data = {
                season_id: this.selectedSeasonId
            }

            this.sidebarGroups = null;
            this.groupsService.getTournamentSidebarGroups(data)
                .subscribe((response: any) => {
                    if(response.code == 0){
                        this.sidebarGroups = response.groups
                    }
                })
        }
    }

  newsClick(){

      if(this.selectedSeasonId){

          let data = {
              id: this.selectedSeasonId,
              page: this.newsPage
          }

          this.getNews(data)
          this.getTournamentSidebarGroups()
      }
  }

  teamsClick(){

    this.getTeamsBySeason()
  }

  listClick(){

    this.getStagesBySeason()
  }

  calendarClick(){

    this.getMatchesBySeason()
    this.getElementsBySeason()
    this.getTeamsBySeason()
  }

  onSeasonChange(seasonsNgSelect){
    this.selectedSeasonId = seasonsNgSelect.selectedValues[0].id
    this.selectedSeasonParticipantCount = seasonsNgSelect.selectedValues[0].teams_count;


    let data = {
      id: this.selectedSeasonId,
        page: this.newsPage
    }

    this.getNews(data)
    this.getTeamsBySeason()
    this.getMatchesBySeason()
    this.getElementsBySeason()
    this.getStagesBySeason()
    this.getTournamentSidebarGroups()

    let seoData = {
        tournamentName: `${this.tournament.tournament_name} (${seasonsNgSelect.selectedValues[0].start_year + (seasonsNgSelect.selectedValues[0].end_year ? `-${seasonsNgSelect.selectedValues[0].end_year}` : "")})`
    }

    this._seoService.seoTournament(seoData)
  }

  onCalendarNgSelectChange(){
      this.getMatchesBySeason()
  }

  onElementsNgSelectChange(ngSelect){

      let elementsNgSelected = ngSelect.selectedValues[0]

      this.elementsNgSelectedId = elementsNgSelected.element_id
      this.groupsNgSelectedId = elementsNgSelected.group_id
      this.onCalendarNgSelectChange()
  }

  onStageNgSelectChange(){
    this.getMatchesBySeasonAndStage()
  }

  pageChanged(page){

      this.newsPage = page;

      if(this.selectedSeasonId){

          let data = {
              id: this.selectedSeasonId,
              page: this.newsPage
          }

          this.getNews(data)
      }
  }

}
