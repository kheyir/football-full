import { Component, OnInit } from '@angular/core';
import {LoaderService} from "../../services/loader.service";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.sass']
})
export class PageNotFoundComponent implements OnInit {

  constructor(
      private loaderService: LoaderService,
      private _seoService: SEOService
  ) { }

  ngOnInit() {
    this.loaderService.decreaseLoaderCount()

    this._seoService.seoMain()
  }

}
