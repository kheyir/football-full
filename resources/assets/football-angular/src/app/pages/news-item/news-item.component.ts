import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import $ from 'jquery';
import {NewsService} from "../../services/news.service";
import {ActivatedRoute, Router} from "@angular/router";
import { SettingsService } from 'src/app/store/settings.service';
import {LoaderService} from "../../services/loader.service";
import {SEOService} from "../../services/seo.service";
import { FacebookService, InitParams } from 'ngx-facebook';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.sass']
})
export class NewsItemComponent implements OnInit {

  newsItem;
  otherNews = null;

  langLocal = this.settingsService.lang$.value;

  bypassSecurityTrustHtml(large_text){
      return this.sanitizer.bypassSecurityTrustHtml(large_text);
  }

  get locationHref(){
    return window.location.href;
  }

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private newsService: NewsService,
      private settingsService: SettingsService,
      private loaderService: LoaderService,
      private sanitizer: DomSanitizer,
      private _seoService: SEOService,
      private fb: FacebookService
  ) {
    this.settingsService.lang$
      .subscribe(value => {
        this.langLocal = value
      })

      const initParams: InitParams = {
        appId: '179935456205855',
        xfbml: true,
        version: 'v8.0',
        autoLogAppEvents: true
      };
   
      fb.init(initParams);
  }

  ngOnInit() {

    $(".tab-content").each(function(index, item){
      $(item).find(".tab-pane").first().css("display", "block")
    })
    $(".nav-tabs").each(function(index, item){
        $(item).find(".nav-tabs__item").first().addClass("active")
    })

    this.route.paramMap
        .subscribe(params => {
          let data = {
            slug: params.get("slug")
          }

          this.getNewsItem(data)

        })
  }

  getNewsItem(data){
    this.loaderService.increaseLoaderCount()

    this.otherNews = null;

    this.newsService.getNewsItem(data)
        .subscribe((response: any) => {
            this.loaderService.decreaseLoaderCount()

            if(response.code == 0){
                this.newsItem = response.newsItem;
                this.otherNews = response.otherNews;

                this.loaderService.decreaseLoaderCount()

                this._seoService.seoNewsItem({
                    title: this.newsItem.title,
                    description: this.newsItem.short_text,
                    url: window.location.href,
                    siteName: "Football.az",
                    ogType: "article",
                    image: `${window.location.origin}/assets/site/assets/images/${this.newsItem.image}`,
                    section: "Sport",
                    publishedTime: this.newsItem.creating_date,
                    twitterCard: "summary_large_image"
                })
            }
            else{
                this.router.navigate(["404"])
            }
          },
          (errorResponse: any) => {
            this.loaderService.decreaseLoaderCount()
            this.router.navigate(["404"])
          })
  }

}
