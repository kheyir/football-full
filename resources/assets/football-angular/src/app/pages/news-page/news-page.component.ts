import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import $ from 'jquery';
import {ActivatedRoute} from "@angular/router";
import { CategoriesService } from '../../services/categories.service';
import { SettingsService } from '../../store/settings.service';
import { NewsService } from '../../services/news.service';
import {LoaderService} from "../../services/loader.service";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.sass']
})
export class NewsPageComponent implements OnInit {

  fullTypes = null;

  inHomeNews = null;
  lastRightSlideNews = null;
  byAllFeaturedCategoriesNews = []
  allNewsObject = null;

  newsPage = 1

  langLocal = this.settingsService.lang$.value;

  optionsMainNews: OwlOptions = {
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    touchDrag: false,
  }

  optionsCategories: OwlOptions = {
    items: 3,
    loop: true,
    nav: false,
    dots: false,
    touchDrag: false,
    margin: 32,
    responsive: {
      0: {
        items: 1
      },
      480: {
        items: 2
      },
      768: {
        items: 3
      }
    }
  }

  constructor(
      private route: ActivatedRoute,
      private categoriesService: CategoriesService,
      private settingsService: SettingsService,
      private newsService: NewsService,
      private loaderService: LoaderService,
      private _seoService: SEOService
  ) {
    this.settingsService.lang$
      .subscribe(value => {
        this.langLocal = value
      })
  }

  ngOnInit() {

    $(".tab-content").each(function(index, item){
        $(item).find(".tab-pane").first().css("display", "block")
    })
    $(".nav-tabs").each(function(index, item){
        $(item).find(".nav-tabs__item").first().addClass("active")
    })

    $(".filter-open-button").trigger("click")

    this.route.paramMap
        .subscribe(params => {
          this.getFullTypes()

          this.getSlidersNews()
          this.getLastRightSlideNews()
          this.getNewsByAllFeaturedCategories()

          let data = {
            page: this.newsPage
          }

          this.getAll(data)
        })

      this.loaderService.decreaseLoaderCount()

      this._seoService.seoNews();
  }

  getFullTypes(){
    this.fullTypes = null;

    this.categoriesService.getFullTypes()
      .subscribe((response: any) => {
        if(response.code == 0){
          this.fullTypes = response.fullTypes

          if(this.fullTypes.length > 0){

              this.fullTypes = this.fullTypes.map(item => {
                  if(item.category_id){
                      item.url = `/${this.langLocal}/news/category/${item.slug}`
                  }
                  else{
                      if(item.tournament_id){
                          item.url = `/${this.langLocal}/news/tournament/${item.slug}`
                      }
                      else{
                          item.url = `/${this.langLocal}/news/country/${item.slug}`
                      }
                  }

                  return item
              })

            let blocksCount = 5

            let tmpArr = []
            for(let i = 0; i < blocksCount; i++){
              tmpArr.push([])
            }

            for(let i = 0; i < this.fullTypes.length; i++){
                tmpArr[i % 5].push(this.fullTypes[i])
            }

            this.fullTypes = tmpArr;
          }
        }
      })
  }


  getSlidersNews(){
    this.inHomeNews = null;

    this.newsService.getSlidersNews()
    .subscribe((response: any) => {
        if(response.code == 0){
            this.inHomeNews = response.inHomeNews
        }
    })
  }

  getLastRightSlideNews(){
      this.lastRightSlideNews = null;

      this.newsService.getLastRightSlideNews()
        .subscribe((response: any) => {
            if(response.code == 0){
                this.lastRightSlideNews = response.lastRightSlideNews
            }
        })
  }

  getAll(data){
    this.allNewsObject = null;

    this.newsService.getAll(data)
        .subscribe((response: any) => {
            if(response.code == 0){
                this.allNewsObject = response.news
            }
        })
  }

  getNewsByAllFeaturedCategories(){
      this.byAllFeaturedCategoriesNews = null;

      this.newsService.getNewsByAllFeaturedCategories()
    .subscribe((response: any) => {
        if(response.code == 0){
            this.byAllFeaturedCategoriesNews = response.byAllFeaturedCategoriesNews
        }
    })
  }

  pageChanged(event){
    this.newsPage = event;

    let data = {
      page: this.newsPage
    }

    this.getAll(data)
  }

}
