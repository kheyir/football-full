import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsService } from 'src/app/services/news.service';
import { SettingsService } from 'src/app/store/settings.service';
import {LoaderService} from "../../services/loader.service";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-type-news',
  templateUrl: './type-news.component.html',
  styleUrls: ['./type-news.component.sass']
})
export class TypeNewsComponent implements OnInit {

  newsType;
  newsTypeSlug;

  langLocal = this.settingsService.lang$.value;

  typeNewsObject;
  typeName;
  newsPage = 1;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private newsService: NewsService,
    private settingsService: SettingsService,
    private loaderService: LoaderService,
    private _seoService: SEOService
  ) {
    this.settingsService.lang$
      .subscribe(value => {
        this.langLocal = value
      })
  }

  ngOnInit() {

    this.route.paramMap
        .subscribe(params => {
          this.newsType = params.get("type")
          this.newsTypeSlug = params.get("typeSlug")
          let data = {
            slug: this.newsTypeSlug,
            page: this.newsPage
          }

          if(this.newsType == "country"){

            this.getNewsByCountry(data)
          }
          else if(this.newsType == "category"){

            this.getNewsByCategory(data)
          }
          else if(this.newsType == "tournament"){

            this.getNewsByTournament(data)
          }
          else{
            this.router.navigate(["404"])
          }
        })
  }

  getNewsByCountry(data){
    this.loaderService.increaseLoaderCount()

    this.newsService.getNewsByCountry(data)
      .subscribe((response: any) => {
        this.loaderService.decreaseLoaderCount()

        if(response.code == 0){
          this.typeNewsObject = response.news
          this.typeName = response.typeName

          this.loaderService.decreaseLoaderCount()

          let seoData = {
            typeName: this.typeName
          }
      
          this._seoService.seoTypeNews(seoData)
        }
        else if(response.code == 404){
          this.router.navigate(["404"]);
        }
      },
      (errorResponse: any) => {
          this.loaderService.decreaseLoaderCount()
          this.router.navigate(["404"])
      })
  }

  getNewsByCategory(data){
      this.loaderService.increaseLoaderCount()

      this.newsService.getNewsByCategory(data)
      .subscribe((response: any) => {
        this.loaderService.decreaseLoaderCount()

        if(response.code == 0){
          this.typeNewsObject = response.news
          this.typeName = response.typeName

          this.loaderService.decreaseLoaderCount()

          let seoData = {
            typeName: this.typeName
          }
      
          this._seoService.seoTypeNews(seoData)
        }
        else if(response.code == 404){
          this.router.navigate(["404"]);
        }
      },
      (errorResponse: any) => {
          this.loaderService.decreaseLoaderCount()
          this.router.navigate(["404"])
      })
  }

  getNewsByTournament(data){
      this.loaderService.increaseLoaderCount()

      this.newsService.getNewsByTournament(data)
      .subscribe((response: any) => {
        this.loaderService.decreaseLoaderCount()

        if(response.code == 0){
          this.typeNewsObject = response.news
          this.typeName = response.typeName

          this.loaderService.decreaseLoaderCount()

          let seoData = {
            typeName: this.typeName
          }
      
          this._seoService.seoTypeNews(seoData)
        }
        else if(response.code == 404){
          this.router.navigate(["404"]);
        }
      },
      (errorResponse: any) => {
          this.loaderService.decreaseLoaderCount()
          this.router.navigate(["404"])
      })
  }

  pageChanged(event){

    this.newsPage = event;

    let data = {
      slug: this.newsTypeSlug,
      page: this.newsPage
    }

    if(this.newsType == "country"){

      this.getNewsByCountry(data)
    }
    else if(this.newsType == "category"){

      this.getNewsByCategory(data)
    }
    else if(this.newsType == "tournament"){

      this.getNewsByTournament(data)
    }
  }

}
