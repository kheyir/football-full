import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsService } from 'src/app/services/news.service';
import { SettingsService } from 'src/app/store/settings.service';
import {LoaderService} from "../../services/loader.service";
import { SEOService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-search-news',
  templateUrl: './search-news.component.html',
  styleUrls: ['./search-news.component.sass']
})
export class SearchNewsComponent implements OnInit {


  langLocal = this.settingsService.lang$.value;

  searchWord = null;

  news = null;

  newsPage = 1;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private newsService: NewsService,
      private settingsService: SettingsService,
      private loaderService: LoaderService,
      private _seoService: SEOService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        });

    this.settingsService.searchWord$
        .subscribe((value: string) => {
            this.searchWord = value;
        })
  }

  ngOnInit() {

    this.route.paramMap
        .subscribe(params => {

          this.settingsService.setSearchWord(params.get("searchWord"))

          let data = {
            page: this.newsPage,
            searchWord: this.searchWord
          }

          this.getAll(data)
        })

    this._seoService.seoSearch();
  }

  getAll(data){
    this.loaderService.increaseLoaderCount()

    this.newsService.getAll(data)
        .subscribe((response: any) => {
              this.loaderService.decreaseLoaderCount()

              if(response.code == 0){
                this.news = response.news

                this.loaderService.decreaseLoaderCount()
              }
              else if(response.code == 404){
                this.router.navigate(["404"]);
              }
            },
            (errorResponse: any) => {
              this.loaderService.decreaseLoaderCount()
              this.router.navigate(["404"])
            })
  }

  pageChanged(event){

    this.newsPage = event;

    let data = {
      page: this.newsPage,
      searchWord: this.searchWord
    }

    this.getAll(data);
  }

}
