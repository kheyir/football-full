import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { CarouselModule } from 'ngx-owl-carousel-o';

import { NgSelectModule } from '@ng-select/ng-select';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http'

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { FacebookModule } from 'ngx-facebook';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { NewsPageComponent } from './pages/news-page/news-page.component';
import { NewsItemComponent } from './pages/news-item/news-item.component';

import { TagsComponent } from './containers/tags/tags.component';
import { HeaderComponent } from './containers/header/header.component';
import { HeadComponent } from './containers/head/head.component';
import { FooterComponent } from './containers/footer/footer.component';
import { TeamComponent } from './pages/team/team.component';
import { TournamentComponent } from './pages/tournament/tournament.component';
import { TypeNewsComponent } from './pages/type-news/type-news.component';
import { VideosPageComponent } from './pages/videos-page/videos-page.component';
import { ReviewComponent } from './pages/review/review.component';
import { VideoModalComponent } from './containers/video-modal/video-modal.component';
import { HeadCountriesGhostComponent } from './ghosts/head-countries-ghost/head-countries-ghost.component';
import { MatchDatePipe } from './pipes/match-date.pipe';
import { PlayerDatePipe } from './pipes/player-date.pipe';
import { BasicGhostComponent } from './ghosts/basic-ghost/basic-ghost.component';
import { TournamentsGlobalComponent } from './pages/tournaments-global/tournaments-global.component';
import { LoaderComponent } from './ghosts/loader/loader.component';
import { NewsSliderGhostComponent } from './ghosts/news-slider-ghost/news-slider-ghost.component';
import { RightSidebarNewsGhostComponent } from './ghosts/right-sidebar-news-ghost/right-sidebar-news-ghost.component';
import { NewsListGhostComponent } from './ghosts/news-list-ghost/news-list-ghost.component';
import { MatchDate2Pipe } from './pipes/match-date2.pipe';
import { SearchNewsComponent } from './pages/search-news/search-news.component';
import { AdvertisingComponent } from './pages/advertising/advertising.component';
import { AboutComponent } from './pages/about/about.component';
import { ShareSocialComponent } from './containers/share-social/share-social.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient): TranslateLoader {
  return new TranslateHttpLoader(http, './assets/site/assets/locale/', '.json?v=2.0');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    TagsComponent,
    HeaderComponent,
    HeadComponent,
    FooterComponent,
    NewsPageComponent,
    NewsItemComponent,
    TeamComponent,
    TournamentComponent,
    TypeNewsComponent,
    VideosPageComponent,
    ReviewComponent,
    VideoModalComponent,
    HeadCountriesGhostComponent,
    MatchDatePipe,
    PlayerDatePipe,
    BasicGhostComponent,
    TournamentsGlobalComponent,
    LoaderComponent,
    NewsSliderGhostComponent,
    RightSidebarNewsGhostComponent,
    NewsListGhostComponent,
    MatchDate2Pipe,
    SearchNewsComponent,
    AdvertisingComponent,
    AboutComponent,
    ShareSocialComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CarouselModule,
    NgSelectModule, FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    NgxPaginationModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      useDefaultLang: false,
    }),
    FacebookModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
