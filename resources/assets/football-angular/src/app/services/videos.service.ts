import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http'
import {SettingsService} from '../store/settings.service'
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  langLocal = this.settingsService.lang$.value

  constructor(
      private http: HttpClient,
      private settingsService: SettingsService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
  }

  getVideos(data: any = {}){

    let httpParams = new HttpParams();

    if(data.match_id) httpParams = httpParams.append("match_id", data.match_id)
    if(data.player_id) httpParams = httpParams.append("player_id", data.player_id)
    if(data.team_id) httpParams = httpParams.append("team_id", data.team_id)

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/videos/get-videos/${data.page||1}`, {params: httpParams})
  }

  getTopVideosByTournament(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/videos/get-top-by-tournament/${data.id}`)
  }

  getVideosByTeamAndMatch(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/videos/get-videos-by-team-and-match/${data.teamId}/${data.matchId}`)
  }
}
