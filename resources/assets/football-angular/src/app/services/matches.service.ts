import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {SettingsService} from "../store/settings.service";
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class MatchesService {

  langLocal = this.settingsService.lang$.value

  constructor(
      private http: HttpClient,
      private settingsService: SettingsService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
  }

  getMatchesBySeason(data){

    let httpParams = new HttpParams();

    if(data.element_id) httpParams = httpParams.append("element_id", data.element_id)
    if(data.group_id) httpParams = httpParams.append("group_id", data.group_id)
    if(data.team_id) httpParams = httpParams.append("team_id", data.team_id)
    if(data.status) httpParams = httpParams.append("status", data.status)

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/matches/get-matches-by-season/${data.id}`, {params: httpParams})
  }

  getMatchesBySeasonAndStage(data){

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/matches/get-matches-by-season-and-stage/${data.season_id}/${data.stage_id}`)
  }

  getMatchesByTeam(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/matches/get-matches-by-team/${data.id}/${data.page||1}`)
  }

  getMatchesByTeamWithVideos(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/matches/get-matches-by-team-with-videos/${data.id}/${data.page||1}`)
  }

  getMatches(data){

    let httpParams = new HttpParams();

    if(data.tournamentId) httpParams = httpParams.append("tournament_id", data.tournamentId)
    if(data.status) httpParams = httpParams.append("status", data.status)
    if(data.date) httpParams = httpParams.append("date", data.date)
    if(data.search) httpParams = httpParams.append("search", data.search)
    if(data.forHome) httpParams = httpParams.append("forHome", data.forHome)

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/matches/get-matches/${data.page||1}`, {params: httpParams})
  }

  getMatchesThatHaveVideo(data: any = {}){

    let httpParams = new HttpParams();

    if(data.category_id) httpParams = httpParams.append("category_id", data.category_id)
    if(data.tournament_id) httpParams = httpParams.append("tournament_id", data.tournament_id)
    if(data.season_id) httpParams = httpParams.append("season_id", data.season_id)
    if(data.stage_id) httpParams = httpParams.append("stage_id", data.stage_id)
    if(data.team_id) httpParams = httpParams.append("team_id", data.team_id)

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/matches/get-matches-that-have-videos/${data.page||1}`, {params: httpParams})
  }
}
