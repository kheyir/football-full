import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SettingsService} from "../store/settings.service";
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  langLocal = this.settingsService.lang$.value

  constructor(
      private http: HttpClient,
      private settingsService: SettingsService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
  }

  getTournamentSidebarGroups(data){

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/groups/get-tournament-sidebar-groups-by-season/${data.season_id}`)
  }

  getSidebarTournamentsForHome(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/groups/get-tournament-sidebar-for-home`)
  }
}
