import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {SettingsService} from "../store/settings.service";
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  langLocal = this.settingsService.lang$.value

  constructor(
      private http: HttpClient,
      private settingsService: SettingsService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
  }

  getTeams(data: any = {}){

    let httpParams = new HttpParams();

    if(data.team_name) httpParams = httpParams.append("team_name", data.team_name)

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/teams/get-teams-that-have-videos/${data.page||1}`, {params: httpParams})
  }

  getTeamsBySeason(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/teams/get-teams-by-season/${data.id}`)
  }

  getTeamBySlug(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/team/get-team-by-slug/${data.slug}`)
  }
}
