import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment'
import { SettingsService } from '../store/settings.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  langLocal = this.settingsService.lang$.value

  constructor(
    private http: HttpClient,
    private settingsService: SettingsService
  ) {
    this.settingsService.lang$
      .subscribe(value => {
        this.langLocal = value
      })
  }

  getHomeItems(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/home/get-home-items`)
  }
}
