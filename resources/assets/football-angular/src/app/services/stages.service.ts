import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {SettingsService} from "../store/settings.service";
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class StagesService {

  langLocal = this.settingsService.lang$.value;

  constructor(
    private http: HttpClient,
    private settingsService: SettingsService
  ) {
    this.settingsService.lang$
      .subscribe(value => {
        this.langLocal = value
      })
  }

  getAll(data: any = {}){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("season_id")) httpParams = httpParams.append("season_id", data.season_id);

    return this.http.get(environment.BASE_URL + '/stages/all', {params: httpParams})
  }

  getStagesBySeason(data) {
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/stages/get-stages-by-season/${data.id}`)
  }
}
