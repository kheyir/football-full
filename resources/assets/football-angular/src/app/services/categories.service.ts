import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SettingsService } from '../store/settings.service';
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  langLocal = this.settingsService.lang$.value

  constructor(
    private http: HttpClient,
    private settingsService: SettingsService
    ) { 
      this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
  }

  getFullTypes(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/categories/get-full-types`)
  }

  getCategories(data){

    let httpParams = new HttpParams();

    if(data.hasOwnProperty("is_for_video")) httpParams = httpParams.append("is_for_video", data.is_for_video);

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/categories/get`, {params: httpParams})
  }
}
