import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class SEOService {

  constructor(
      private title: Title,
      private meta: Meta,
      private translateService: TranslateService
  ) { }

  seoNewsItem(data: any){
    this.updateTitle(`Football.az - ${data.title}`)
    this.updateDescription(data.description)

    this.updateOgUrl(data.url)
    this.updateOgSiteName(data.siteName)
    this.updateOgTitle(data.title)
    this.updateOgType(data.ogType)
    this.updateOgImage(data.image)
    this.updateOgDescription(data.description)

    this.updateArticleSection(data.section)
    this.updateArticlePublishedTime(data.publishedTime)

    this.updateTwitterCard(data.twitterCard)
    this.updateTwitterTitle(data.title)
    this.updateTwitterDescription(data.description)
    this.updateTwitterImage(data.image)
  }

  seoMain(){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();

      this.updateTitle('Football.az')
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoAbout(){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();
      let aboutResult = await this.translateService.get("about").toPromise();

      this.updateTitle(`Football.az - ${aboutResult}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoAdvertising(){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();
      let advertisingResult = await this.translateService.get("advertising").toPromise();

      this.updateTitle(`Football.az - ${advertisingResult}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoNews(){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();
      let newsResult = await this.translateService.get("news").toPromise();

      this.updateTitle(`Football.az - ${newsResult}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoReview(){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();
      let reviewResult = await this.translateService.get("review").toPromise();

      this.updateTitle(`Football.az - ${reviewResult}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoSearch(){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();
      let searchResult = await this.translateService.get("search").toPromise();

      this.updateTitle(`Football.az - ${searchResult}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoTeam(data){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();

      this.updateTitle(`Football.az - ${data.teamName}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoTournament(data){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();

      this.updateTitle(`Football.az - ${data.tournamentName}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoTypeNews(data){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();

      this.updateTitle(`Football.az - ${data.typeName}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  seoVideo(){
    const metaExecute = async () => {
      let metaDescriptionResult = await this.translateService.get("metaDescription").toPromise();
      let videoResult = await this.translateService.get("video").toPromise();

      this.updateTitle(`Football.az - ${videoResult}`)
      this.updateDescription(metaDescriptionResult)

      this.updateOgUrl(window.location.href)
      this.updateOgSiteName("Football.az")
      this.updateOgTitle("Football.az")
      this.updateOgType("website")
      this.updateOgImage(window.location.href + "/assets/site/assets/images/football-simple-white_2.png")
      this.updateOgDescription(metaDescriptionResult)
    }

    metaExecute()
  }

  //main
  updateTitle(title: string) {
    this.title.setTitle(title);
  }

  updateDescription(description: string) {
    this.meta.updateTag({ name: 'description', content: description })
  }

  //og
  updateOgUrl(url: string) {
    this.meta.updateTag({ property: 'og:url', content: url })
  }

  updateOgSiteName(siteName: string) {
    this.meta.updateTag({ property: 'og:site_name', content: siteName })
  }

  updateOgTitle(title: string) {
    this.meta.updateTag({ property: 'og:title', content: title })
  }

  updateOgType(type: string) {
    this.meta.updateTag({ property: 'og:type', content: type })
  }

  updateOgImage(image: string) {
    this.meta.updateTag({ property: 'og:image', content: image })
  }

  updateOgDescription(description: string) {
    this.meta.updateTag({ property: 'og:description', content: description })
  }

  //article
  updateArticleSection(section: string) {
    this.meta.updateTag({ property: 'article:section', content: section })
  }

  updateArticlePublishedTime(publishedTime: string) {
    this.meta.updateTag({ property: 'article:published_time', content: publishedTime })
  }

  //twitter
  updateTwitterCard(card: string) {
    this.meta.updateTag({ property: 'twitter:card', content: card })
  }

  updateTwitterTitle(title: string) {
    this.meta.updateTag({ property: 'twitter:title', content: title })
  }

  updateTwitterDescription(description: string) {
    this.meta.updateTag({ property: 'twitter:description', content: description })
  }

  updateTwitterImage(image: string) {
    this.meta.updateTag({ property: 'twitter:image', content: image })
  }
}
