import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http'
import {SettingsService} from '../store/settings.service'
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  langLocal = this.settingsService.lang$.value

  constructor(
    private http: HttpClient,
    private settingsService: SettingsService
    ) { 
      this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
    }

  getAll(data){

    let httpParams = new HttpParams();

    if(data.searchWord) httpParams = httpParams.append("searchWord", data.searchWord)

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-all/${data.page || 1}`, {params: httpParams})
  }

  getSlidersNews(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-home`)
  }

  getTops(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-tops`)
  }

  getLatest(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-latest`)
  }

  getLastRightSlideNews(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-last-right-slide`)
  }

  getNewsItem(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news-item/${data.slug}`)
  }

  getNewsByCountry(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-news-by-country/${data.slug}/${data.page || 1}`)
  }

  getNewsByCategory(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-news-by-category/${data.slug}/${data.page || 1}`)
  }

  getNewsByTournament(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-news-by-tournament/${data.slug}/${data.page || 1}`)
  }

  getNewsBySeason(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-news-by-season/${data.id}/${data.page || 1}`)
  }

  getNewsByTeam(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-news-by-team/${data.id}/${data.page || 1}`)
  }

  getNewsByAllFeaturedCategories(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/news/get-by-featured-categories`)
  }
}
