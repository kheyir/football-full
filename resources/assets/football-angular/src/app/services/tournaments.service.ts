import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SettingsService} from "../store/settings.service";
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class TournamentsService {

  langLocal = this.settingsService.lang$.value

  constructor(
      private http: HttpClient,
      private settingsService: SettingsService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
  }

  getAll(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/tournaments/all`)
  }

  getTournamentsIsNewsCategory(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/tournaments/get-tournaments-is-news-category`)
  }

  getTournamentsIsVideosCategory(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/tournaments/get-tournaments-is-videos-category`)
  }

  getTournamentBySlug(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/tournament/get-tournament-by-slug/${data.slug}`)
  }

  getGlobalTournaments(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/tournaments/get-global-tournaments`)
  }

  getTournamentsPriorityForCountry(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/tournaments/get-tournaments-priority-for-country`)
  }
}
