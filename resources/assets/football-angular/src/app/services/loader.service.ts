import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  loadersCount$: BehaviorSubject<number> = new BehaviorSubject(1);

  constructor() { }

  decreaseLoaderCount(){
    this.loadersCount$.next(this.loadersCount$.value > 0 ? this.loadersCount$.value - 1 : 0)
  }

  increaseLoaderCount(){
    this.loadersCount$.next(this.loadersCount$.value + 1)
  }
}
