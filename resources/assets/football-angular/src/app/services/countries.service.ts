import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {SettingsService} from '../store/settings.service'
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  langLocal = this.settingsService.lang$.value

  constructor(
    private http: HttpClient,
    private settingsService: SettingsService
    ) { 
      this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
    }

  getAll(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/countries/get-all`)
  }

  getAllForHead(){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/countries/get-all-for-head`)
  }
}
