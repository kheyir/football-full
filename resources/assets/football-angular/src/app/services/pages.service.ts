import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {SettingsService} from '../store/settings.service'
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor(
      private http: HttpClient,
      private settingsService: SettingsService
  ) { }

  getAdvertising(){
    return this.http.get(environment.BASE_URL + `api/${this.settingsService.lang$.value}/page/advertising`)
  }

  getAbout(){
    return this.http.get(environment.BASE_URL + `api/${this.settingsService.lang$.value}/page/about`)
  }
}
