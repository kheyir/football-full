import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {SettingsService} from "../store/settings.service";
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  langLocal = this.settingsService.lang$.value

  constructor(
      private http: HttpClient,
      private settingsService: SettingsService
  ) {
    this.settingsService.lang$
        .subscribe(value => {
          this.langLocal = value
        })
  }

  getPlayers(data: any = {}){

    let httpParams = new HttpParams();

    if(data.player_name) httpParams = httpParams.append("player_name", data.player_name)

    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/players/get-players-that-have-videos/${data.page||1}`, {params: httpParams})
  }

  getPlayersByTeam(data){
    return this.http.get(environment.BASE_URL + `api/${this.langLocal}/players/get-players-by-team/${data.id}`)
  }
}
