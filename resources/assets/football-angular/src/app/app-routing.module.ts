import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { NewsPageComponent } from './pages/news-page/news-page.component';
import { NewsItemComponent } from './pages/news-item/news-item.component';
import { TeamComponent } from './pages/team/team.component';
import { TournamentComponent } from './pages/tournament/tournament.component';
import { TournamentsGlobalComponent } from './pages/tournaments-global/tournaments-global.component';
import { TypeNewsComponent } from './pages/type-news/type-news.component';
import { VideosPageComponent } from './pages/videos-page/videos-page.component';
import { ReviewComponent } from './pages/review/review.component';
import { SearchNewsComponent } from './pages/search-news/search-news.component';
import { AdvertisingComponent } from './pages/advertising/advertising.component';
import { AboutComponent } from './pages/about/about.component';


const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: "full" },
  { path: '404', component: PageNotFoundComponent, pathMatch: "full" },
  { path: ':lang', component: HomeComponent },
  // { path: ':lang', component: HomeComponent},
  { path: ':lang/news', component: NewsPageComponent },
  { path: ':lang/news/search/:searchWord', component: SearchNewsComponent },
  { path: ':lang/news/:type/:typeSlug', component: TypeNewsComponent },
  { path: ':lang/news-item/:slug', component: NewsItemComponent },
  { path: ':lang/tournament/:slug', component: TournamentComponent },
  { path: ':lang/tournaments', component: TournamentsGlobalComponent },
  { path: ':lang/team/:slug', component: TeamComponent },
  // { path: 'all-news', component: TypeNewsComponent },
  { path: ':lang/videos', component: VideosPageComponent },
  { path: ':lang/review', component: ReviewComponent },
  { path: ':lang/advertising', component: AdvertisingComponent },
  { path: ':lang/about', component: AboutComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }