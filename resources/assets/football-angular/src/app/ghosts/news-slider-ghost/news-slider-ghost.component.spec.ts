import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsSliderGhostComponent } from './news-slider-ghost.component';

describe('NewsSliderGhostComponent', () => {
  let component: NewsSliderGhostComponent;
  let fixture: ComponentFixture<NewsSliderGhostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsSliderGhostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsSliderGhostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
