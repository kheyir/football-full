import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-list-ghost',
  templateUrl: './news-list-ghost.component.html',
  styleUrls: ['./news-list-ghost.component.sass']
})
export class NewsListGhostComponent implements OnInit {

  @Input() newsCount;
  news = []

  constructor() { }

  ngOnInit() {
    this.news = Array(this.newsCount).fill(0)
  }

}
