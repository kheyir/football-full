import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsListGhostComponent } from './news-list-ghost.component';

describe('NewsListGhostComponent', () => {
  let component: NewsListGhostComponent;
  let fixture: ComponentFixture<NewsListGhostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsListGhostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsListGhostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
