import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-right-sidebar-news-ghost',
  templateUrl: './right-sidebar-news-ghost.component.html',
  styleUrls: ['./right-sidebar-news-ghost.component.sass']
})
export class RightSidebarNewsGhostComponent implements OnInit {

  @Input() newsCount;
  topNews = []

  constructor() { }

  ngOnInit() {
    this.topNews = Array(this.newsCount).fill(0)
  }

}
