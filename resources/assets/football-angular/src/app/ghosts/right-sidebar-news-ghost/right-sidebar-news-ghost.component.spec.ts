import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightSidebarNewsGhostComponent } from './right-sidebar-news-ghost.component';

describe('RightSidebarNewsGhostComponent', () => {
  let component: RightSidebarNewsGhostComponent;
  let fixture: ComponentFixture<RightSidebarNewsGhostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightSidebarNewsGhostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightSidebarNewsGhostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
