import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {TweenLite, Power2} from 'gsap'
import {LoaderService} from "../../services/loader.service";

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.sass']
})
export class LoaderComponent implements OnInit {
  isLoaderActive = true;

  get documentReadyState(){
    return document.readyState
  }

  constructor(
      private loaderService: LoaderService
  ) {
    this.loaderService.loadersCount$.subscribe((value: number) => {

      let loader = document.getElementById("loader")

      if(value == 1 && !this.isLoaderActive){
        this.isLoaderActive = true
        TweenLite.to(loader, 0.5, {opacity: "1", ease: Power2.easeInOut});
      }
      else if(value == 0 && this.isLoaderActive){
        TweenLite.to(loader, 0.5, {opacity: "0", ease: Power2.easeInOut, onComplete: () => {this.isLoaderActive = false}});
      }
    })
  }

  ngOnInit() {

  }

}
