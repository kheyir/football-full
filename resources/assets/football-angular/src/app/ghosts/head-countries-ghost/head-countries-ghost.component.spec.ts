import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadCountriesGhostComponent } from './head-countries-ghost.component';

describe('HeadCountriesGhostComponent', () => {
  let component: HeadCountriesGhostComponent;
  let fixture: ComponentFixture<HeadCountriesGhostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadCountriesGhostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadCountriesGhostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
