import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicGhostComponent } from './basic-ghost.component';

describe('BasicGhostComponent', () => {
  let component: BasicGhostComponent;
  let fixture: ComponentFixture<BasicGhostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicGhostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicGhostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
