import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  lang$: BehaviorSubject<string> = new BehaviorSubject<string>("az")

  videoModalIsActive$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

  searchWord$: BehaviorSubject<string> = new BehaviorSubject<string>("")

  constructor(
      @Inject(DOCUMENT) private _document: any
  ) { }

  setLang(newLang: any){
    this.lang$.next(newLang)
    this._document.documentElement.lang = newLang
  }

  setVideoModalActivity(newVideoActivity: boolean){
    this.videoModalIsActive$.next(newVideoActivity)
  }

  setSearchWord(value: string){
    this.searchWord$.next(value)
  }
}
