import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

import { registerLocaleData } from '@angular/common';
import localeEnglish from '@angular/common/locales/en';
import localeAzeri from '@angular/common/locales/az';
import localeRussian from '@angular/common/locales/ru';

@Pipe({
  name: 'matchDate2'
})
export class MatchDate2Pipe implements PipeTransform {

  constructor(private translateService: TranslateService) {}

  // transform(value: any, pattern: string = 'mediumDate'): any {
  //   const datePipe: DatePipe = new DatePipe(this.translateService.currentLang);
  //   return datePipe.transform(value, pattern);
  // }

  transform(value: any, ...args: any[]): any {
    // this.translateService.currentLang

    switch (this.translateService.currentLang) {
      case 'az': {
        registerLocaleData(localeAzeri);
        break;
      }
      case 'ru': {
        registerLocaleData(localeRussian);
        break;
      }
      default: {
        registerLocaleData(localeEnglish);
      }
    }
    const datePipe: DatePipe = new DatePipe(this.translateService.currentLang);
    return datePipe.transform(value, "h:mm a");
  }

}
