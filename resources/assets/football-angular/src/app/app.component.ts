import { Component, OnInit} from '@angular/core';
import $ from 'jquery'
import {Router, NavigationEnd } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import { ScriptsService } from './scripts.service'
import { SettingsService } from './store/settings.service'
import {environment} from '../environments/environment'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  constructor(private router: Router,
    private scripts: ScriptsService,
    private settingsService: SettingsService,
    private translateService: TranslateService
    ){

    let splittedPathname = window.location.pathname.split('/')

    if(environment.locales.indexOf(splittedPathname[1]) > -1){
      let localeFromPathname = splittedPathname[1];

      this.settingsService.setLang(localeFromPathname)
      this.translateService.use(localeFromPathname);
    }
    else{
      if(window.location.pathname == '/'){
        let localeFromPathname = 'az';

        this.settingsService.setLang(localeFromPathname)
        this.translateService.use(localeFromPathname);
      }
      else{
        let localeFromPathname = 'az';

        this.settingsService.setLang(localeFromPathname)
        this.translateService.use(localeFromPathname);

        this.router.navigate(["404"])
      }
    }

  }

  ngOnInit(){
    this.scripts.init();

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }

}
