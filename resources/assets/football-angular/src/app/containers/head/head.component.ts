import { Component, OnInit } from '@angular/core';
import {CountriesService} from "../../services/countries.service";
import { SettingsService } from '../../store/settings.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.sass']
})
export class HeadComponent implements OnInit {

  localSearchWord = "";

  langLocal = null;

  forHeadCountries = null

  langs = [
    "az",
    // "ru"
  ]

  constructor(
    private router: Router,
    private countriesService: CountriesService,
    private settingsService: SettingsService
    ) {

    this.settingsService.lang$
      .subscribe(value => {
        console.log(value)

        if(this.langLocal != value){
          this.langLocal = value;
          this.getAllCountriesForHead()
        }
      })

    this.settingsService.searchWord$
        .subscribe((value: string) => {
          this.localSearchWord = value
        })
    }

  ngOnInit() {

  }

  getAllCountriesForHead(){

    this.forHeadCountries = null

    this.countriesService.getAllForHead()
      .subscribe((response: any) => {
        if(response.code == 0){
          this.forHeadCountries = response.countries
        }
      })
  }

  searchMethod(){
    (<HTMLAnchorElement>document.querySelector(".nav-mobile-menu-button--close")).click()

    this.settingsService.setSearchWord(this.localSearchWord)

    this.router.navigate(['/' + this.langLocal + `/news/search/${this.localSearchWord}`])
  }

}
