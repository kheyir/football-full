import { Component, OnInit } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import { SettingsService } from '../../store/settings.service';
import $ from 'jquery'
import {environment} from '../../../environments/environment'
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  signInShow: boolean = false;
  signUpShow: boolean = false;

  localSearchWord = "";

  langLocal = this.settingsService.lang$.value;

  langs = [
    "az",
    // "ru"
  ]

  constructor(
    private router: Router,
    private settingsService: SettingsService,
    private translateService: TranslateService
  ) {

    this.settingsService.lang$
      .subscribe(value => {

          // if(value == 'ru'){
          //     window.location.href = 'http://azerifootball.com/ru/'
          // }

        if(this.langLocal != value){
            this.langLocal = value

            let localeFromPathname = environment.locales.indexOf(value) > -1 ? value : environment.defaultLocale

            this.langLocal = localeFromPathname

            this.translateService.use(localeFromPathname);

            let splittedPathname = window.location.pathname.split('/')

            if(splittedPathname[1] != localeFromPathname && (window.location.pathname == '/' && localeFromPathname != environment.defaultLocale) || window.location.pathname != '/'){

                splittedPathname[1] = localeFromPathname;
                this.router.navigate([`/${this.langLocal}`]);
                // this.router.navigate([splittedPathname.join('/')]);
            }
        }
      });

    this.settingsService.searchWord$
        .subscribe((value: string) => {
            this.localSearchWord = value
        })
  }

  ngOnInit() {
  }

  changeLang(newLang){
    this.settingsService.setLang(newLang)

    $(".langs-block__selected").trigger("click")
  }

  searchMethod(){
      this.settingsService.setSearchWord(this.localSearchWord)

      this.router.navigate(['/' + this.langLocal + `/news/search/${this.localSearchWord}`])
  }

  reloadByClickingLogo(event){
    event.preventDefault();

    if(this.router.url == '/' || this.router.url == '/ru') window.location.href = '/' + (this.langLocal != 'az' ? this.langLocal : '')
    else this.router.navigate(['/' + (this.langLocal != 'az' ? this.langLocal : '')])
  }

}
