import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/store/settings.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  langLocal = this.settingsService.lang$.value;

  constructor(
      private settingsService: SettingsService,
  ) {
    this.settingsService.lang$
      .subscribe(value => {
        this.langLocal = value
      })
  }

  ngOnInit() {
  }

}
