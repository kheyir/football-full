import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-share-social',
  templateUrl: './share-social.component.html',
  styleUrls: ['./share-social.component.sass']
})
export class ShareSocialComponent implements OnInit {

  @Input() url;
  constructor() { }

  ngOnInit() {
  }

  shareLinkClick(e, type){
    e.preventDefault();

    let socialUrl;

    switch(type){
      case 'facebook':
        socialUrl = "https://www.facebook.com/sharer/sharer.php?u=";
        break;
      case 'vk':
        socialUrl = "https://vk.com/share.php?url=";
        break;
      case 'twitter':
        socialUrl = "https://twitter.com/intent/tweet/?url=";
        break;
    }

    window.open(socialUrl + this.url, "_blank", "width=500,height=500")
  }

}
