export const environment = {
  production: true,
  BASE_URL: "/",
  locales: ['az', 'ru'],
  defaultLocale: 'az',
};
