var gulp = require('gulp')
	uglify = require('gulp-uglify'),
	minifyCSS = require('gulp-minify-css'),
	concat = require('gulp-concat'),
	del = require('del'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass');


gulp.task('scripts',function(){
	return gulp.src([
			'../../../public/assets/site/runtime-es2015*.js',
			'../../../public/assets/site/polyfills-es2015*.js',
			'../../../public/assets/site/main-es2015*.js',
			// '../../../public/assets/site/runtime-es5*.js',
			// '../../../public/assets/site/polyfills-es5*.js',
			'../../../public/assets/site/main-es5*.js',
		])
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('../../../public/assets/site/assets/js'))
});


gulp.task("sass", function() {
	return gulp.src("src/assets/css/style.sass")
	.pipe(sass().on('error', sass.logError))
    .pipe(rename('style.css'))
	.pipe(gulp.dest('src/assets/css'))
});

gulp.task("styles", function() {
	return gulp.src([
		"src/assets/css/bootstrap-grid.min.css",
		"src/assets/css/reset.css",
		"src/assets/css/san_francisco.css",
		"src/assets/css/style.css",
		"../../../public/assets/site/*.css"
	])
	.pipe(minifyCSS())
	.pipe(concat('site.css'))
	.pipe(gulp.dest('../../../public/assets/site/assets/css'))
});

gulp.task("minifyCss", function() {
  return gulp.src("css/site.css")
    .pipe(rename('site.css'))
    .pipe(gulp.dest('../../../public/assets/site/assets/css'));
});

gulp.task('clean', function() {
  return del(['../../../public/assets/site/assets/css/site.css']);
});

gulp.task('watch',function(){
    gulp.watch('css/*.sass',['sass', 'sass-resp', 'styles', 'minifyCss']);
});

gulp.task('default', ['sass', 'styles', 'minifyCss', 'clean']);