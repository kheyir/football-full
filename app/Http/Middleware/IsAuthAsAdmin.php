<?php

namespace App\Http\Middleware;

use Closure;

use DB;
use Session;
use Request;
use JWTAuth;
use JWTFactory;

class IsAuthAsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        DB::table("users");

//        $customClaims = [
//            'uid' => 'khojaly',
//            'exp' => '158776029000',
//            "iss" => "http://example.org"
//        ];
//
//        JWTFactory::customClaims($customClaims);
//
//        $payload = JWTFactory::make($customClaims);
//
//        $token = JWTAuth::encode($payload);
//
//        JWTAuth::setToken($token);
//        JWTAuth::setToken($token);

        $token = $request->cookie('token');

        try {
            JWTAuth::setToken($token); //<-- set token and check
            if (! $claim = JWTAuth::getPayload()) {
                return response()->json([
                    'code' => 401,
                    'message' => 'User not found'
                ], 401);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                'code' => 401,
                'message' => 'Token expired'
            ], 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json([
                'code' => 401,
                'message' => 'Token invalid'
            ], 401);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'code' => 401,
                'message' => 'Token absent'
            ], 401);
        }

        return $next($request);
    }
}
