<?php

namespace App\Http\Middleware;

use Closure;

use Session;

class IsNotAuthAsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = Request::input('token');

        try {
            JWTAuth::setToken($token); //<-- set token and check
            if (! $claim = JWTAuth::getPayload()) {
                return $next($request);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return $next($request);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return $next($request);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return $next($request);
        }

        return response()->json([
            'code' => 401,
            'message' => 'Token absent'
        ], 401);
    }
}
