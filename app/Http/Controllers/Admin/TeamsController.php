<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;

class TeamsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $teams = DB::table("teams")
            ->leftJoin("team_translations", "team_translations.team_id", "=", "teams.id")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "teams.country_id")
            ->leftJoin("stadium_translations", function($join){
                $join->on("stadium_translations.stadium_id", "=", "teams.stadium_id")
                    ->where("stadium_translations.locale", Lang::getLocale());
            })
            ->leftJoin("coach_translations", "coach_translations.coach_id", "=", "teams.coach_id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $teams = $teams->where([
                ['team_translations.name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        if(isset($input['country_id']) && !empty($input['country_id'])){
            $teams = $teams->where([
                'teams.country_id' => $input['country_id']
            ]);
        }

        $teams = $teams
            ->where([
                "team_translations.locale" => Lang::getLocale(),
                "country_translations.locale" => Lang::getLocale(),
                "coach_translations.locale" => Lang::getLocale()
            ])
            ->select("teams.*", 'team_translations.name',
                'country_translations.name as country_name', 'stadium_translations.name as stadium_name',
                'coach_translations.name as coach_name')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'teams' => $teams
        ]);
    }

    public function getTeams(){

        $teams = DB::table("teams")
            ->leftJoin("team_translations", "team_translations.team_id", "=", "teams.id");

        $input = Request::except('_token');

        if(isset($input['season_id']) && !empty($input['season_id'])){
            $teams = $teams->leftJoin("seasons__teams", "seasons__teams.team_id", "=", "teams.id")
                ->where([
                    'seasons__teams.season_id' => $input['season_id']
                ])
                ->groupBy("teams.id");
        }

        if(isset($input['season_id']) && !empty($input['season_id']) && isset($input['group_id']) && !empty($input['group_id'])){
            $teams = $teams->leftJoin("seasons__stages__groups__teams", "seasons__stages__groups__teams.team_id", "=", "teams.id")
                ->leftJoin("seasons__stages__groups", "seasons__stages__groups.id", "=", "seasons__stages__groups__teams.seasons__stages__group_id")
                ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                ->where([
                    'stages__groups.group_id' => $input['group_id']
                ]);
        }

        $teams = $teams
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('team_translations.name', 'ASC')
            ->select("teams.*", "team_translations.*")
            ->get();

        return response()->json([
            'code' => 0,
            'teams' => $teams
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['slug'] = "required|unique:teams,slug";
            $rules['country_id'] = "required|exists:countries,id";
            $rules['stadium_id'] = "exists:stadiums,id";
            $rules['coach_id'] = "required|exists:coaches,id";
            $rules['logo'] = "required|mimes:jpeg,png,jpg,bmp,gif,svg";
            $rules['year'] = "required";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                if(isset($input['logo'])){
                    $file = $input['logo'];
                    $input['logo'] = date("YmdHis") . $file->getClientOriginalName();

                    $invImage = InterventionImage::make($file);

                    $invImage->fit(100, 100)->save(public_path() . '/assets/site/assets/images/'.$input['logo']);
                }

                $teamId = DB::table("teams")->insertGetId([
                    'slug' => $input['slug'],
                    'country_id' => $input['country_id'],
                    'stadium_id' => isset($input['stadium_id']) && $input['stadium_id'] ? $input['stadium_id'] : null,
                    'coach_id' => $input['coach_id'],
                    'logo' => isset($input['logo']) ? $input['logo'] : null,
                    'year' => $input['year'],
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'team_id' => $teamId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("team_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $team = DB::table("teams")
            ->where("id", $id)
            ->first();

        if($team){

            $teamTranslations = DB::table("team_translations")
                ->where("team_id", $id)
                ->get();

            $teamTranslations = $teamTranslations->groupBy("locale");

            $team->translations = $teamTranslations;

            return response()->json([
                'code' => 0,
                'team' => $team
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $team = DB::table("teams")
            ->where("id", $id)
            ->first();

        if($team){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['slug'] = "required|unique:teams,slug,$id";
                $rules['country_id'] = "required|exists:countries,id";
                $rules['stadium_id'] = "exists:stadiums,id";
                $rules['coach_id'] = "required|exists:coaches,id";
                $rules['logo'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
                $rules['year'] = "required";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    $updateData = [
                        'slug' => $input['slug'],
                        'country_id' => $input['country_id'],
                        'stadium_id' => isset($input['stadium_id']) && $input['stadium_id'] ? $input['stadium_id'] : null,
                        'coach_id' => $input['coach_id'],
                        'year' => $input['year'],
                    ];

                    if(isset($input['logo'])){
                        $file = $input['logo'];
                        $input['logo'] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(100, 100)->save(public_path() . '/assets/site/assets/images/' . $input['logo']);

                        // if($team->logo) File::delete(public_path() . '/assets/site/assets/images/' . $team->logo);

                        $updateData['logo'] = $input['logo'];
                    }

                    DB::table("teams")
                        ->where('id', $id)
                        ->update($updateData);

                    DB::table("team_translations")
                        ->where('team_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'team_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("team_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $team = DB::table("teams")
            ->where([
                "id" => $id
            ])
            ->first();

        if($team){

            $match = DB::table("matches")
                ->where("home_team_id", $id)
                ->orWhere("guest_team_id", $id)
                ->first();

            $newsTeam = DB::table("news__teams")
                ->where([
                    "team_id" => $id
                ])
                ->first();

            $playersTeam = DB::table("players__teams")
                ->where([
                    "team_id" => $id
                ])
                ->first();

            $seasonsStagesGroupsTeam = DB::table("seasons__stages__groups__teams")
                ->where([
                    "team_id" => $id
                ])
                ->first();

            $seasonsTeam = DB::table("seasons__teams")
                ->where([
                    "team_id" => $id
                ])
                ->first();

            $video = DB::table("videos")
                ->where([
                    "team_id" => $id
                ])
                ->first();

            if(!$match && !$newsTeam && !$playersTeam && !$seasonsStagesGroupsTeam && !$seasonsTeam && !$video){
                DB::table("teams")->where("id", $id)->delete();
                DB::table("team_translations")->where("team_id", $id)->delete();

                // File::delete(public_path() . '/assets/site/assets/images/' . $team->logo);

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
