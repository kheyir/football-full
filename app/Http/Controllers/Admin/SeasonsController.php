<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;

class SeasonsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $seasons = DB::table("seasons")
            ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "seasons.tournament_id")
            ->leftJoin("tournaments_category_translations", "tournaments_category_translations.tournaments_category_id", "=", "tournaments.tournaments_category_id")
            ->leftJoin("country_translations", function($join){
                $join->on("country_translations.country_id", "=", "tournaments.country_id")
                    ->where('country_translations.locale', Lang::getLocale());
            });

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $seasons = $seasons->where([
                ['tournament_translations.name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        if(isset($input['tournament_id']) && !empty($input['tournament_id'])){
            $seasons = $seasons->where([
                'seasons.tournament_id' => $input['tournament_id']
            ]);
        }

        if(isset($input['tournaments_category_id']) && !empty($input['tournaments_category_id'])){
            $seasons = $seasons->where([
                'tournaments.tournaments_category_id' => $input['tournaments_category_id']
            ]);
        }

        if(isset($input['country_id']) && $input['country_id'] > -1){
            $seasons = $seasons->where([
                'tournaments.country_id' => $input['country_id']
            ]);
        }

        $whiteIsLeagueList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_league']) && in_array($input['is_league'], $whiteIsLeagueList)){
            $seasons = $seasons
                ->where([
                    "tournaments.is_league" => array_search($input['is_league'], $whiteIsLeagueList)
                ]);
        }

        if(isset($input['start_year']) && !empty($input['start_year'])){
            $seasons = $seasons->where([
                'seasons.start_year' => $input['start_year']
            ]);
        }

        if(isset($input['end_year']) && !empty($input['end_year'])){
            $seasons = $seasons->where([
                'seasons.end_year' => $input['end_year']
            ]);
        }

        $seasons = $seasons
            ->where([
                "tournament_translations.locale" => Lang::getLocale(),
                "tournaments_category_translations.locale" => Lang::getLocale()
            ])
            ->groupBy('seasons.id')
            ->select("seasons.*",
                'tournaments.is_league',
                'tournament_translations.name as tournament_name',
                'tournaments_category_translations.name as tournaments_category_name',
                'country_translations.name as country_name')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'seasons' => $seasons
        ]);
    }

    public function getSeasons(){

        $seasons = DB::table("seasons");

        $input = Request::except('_token');

        if(isset($input['tournament_id']) && !empty($input['tournament_id'])){
            $seasons = $seasons->where([
                'seasons.tournament_id' => $input['tournament_id']
            ]);
        }

        $seasons = $seasons
            ->orderBy('start_year', 'DESC')
            ->get();

        return response()->json([
            'code' => 0,
            'seasons' => $seasons
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['tournament_id'] = "required|exists:tournaments,id";
            $rules['start_year'] = "required|size:4";
            $rules['end_year'] = "size:4";
            $rules['is_in_home'] = "";

            $rules['teams_ids'] = "required";
            $rules['teams_ids.*.*'] = "required";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

//                if(isset($input['is_in_home'])){
//                    DB::table("seasons")->update([
//                        'is_in_home' => 0
//                    ]);
//                }

                $seasonId = DB::table("seasons")->insertGetId([
                    'tournament_id' => $input['tournament_id'],
                    'start_year' => $input['start_year'],
                    'end_year' => isset($input['end_year']) ? $input['end_year'] : null,
                    'is_in_home' => isset($input['is_in_home']) ? 1 : 0
                ]);


                $insertTeamsData = [];
                foreach (explode(',', $input['teams_ids']) as $key => $value){
                    $insertTeamsData[] = [
                        'season_id' => $seasonId,
                        'team_id' => $value
                    ];
                }
                DB::table("seasons__teams")->insert($insertTeamsData);


                if(isset($input['elements_ids'])){
                    $insertElementsData = [];
                    foreach ($input['elements_ids'] as $key => $valueElements){
                        foreach (explode(',', $valueElements) as $valueElement){
                            $insertElementsData[] = [
                                'season_id' => $seasonId,
                                'element_id' => $valueElement
                            ];
                        }
                    }
                    DB::table("seasons__elements")->insert($insertElementsData);
                }


                if(isset($input['has_groups'])){
                    foreach ($input['has_groups'] as $stageId1 => $hasGroupValue){
                        if($hasGroupValue){
                            foreach ($input['stages__group_id'][$stageId1] as $stagesGroupIdKey => $stagesGroupIdValue){
                                $stagesGroupId = DB::table("seasons__stages__groups")->insertGetId([
                                    'season_id' => $seasonId,
                                    'draw_points' => $input['draw_points'][$stageId1],
                                    'victory_points' => $input['victory_points'][$stageId1],
                                    'stages__group_id' => $stagesGroupIdValue,
                                ]);

                                foreach (explode(',', $input['group_teams_ids'][$stageId1][$stagesGroupIdKey]) as $groupTeamIdKey => $groupTeamIdValue){
                                    DB::table("seasons__stages__groups__teams")->insert([
                                        'seasons__stages__group_id' => $stagesGroupId,
                                        'team_id' => $groupTeamIdValue
                                    ]);
                                }
                            }
                        }
                    }
                }

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $season = DB::table("seasons")
            ->where("id", $id)
            ->first();

        if($season){

            $seasonsTeams = DB::table("seasons__teams")
                ->where("season_id", $id)
                ->get();

            $season->seasonsTeams = $seasonsTeams;


            $seasonsElementsByStageId = DB::table("seasons__elements")
                ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
                ->leftJoin("stages", "stages.id", "=", "elements.stage_id")
                ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "elements.stage_id")
                ->leftJoin("matches", "matches.seasons__element_id", "=", "seasons__elements.id")
                ->where([
                    "seasons__elements.season_id" => $id,
                    "locale" => Lang::getLocale()
                ])
                ->select("stages.*", "stage_translations.*",
                    "element_id", "matches.id as match_id")
                ->get()
                ->groupBy('stage_id')
                ->toArray();

            $seasonWithStages = [];
            $index = 0;
            foreach ($seasonsElementsByStageId as $key => $valueElements){

                $seasonWithStages[$index] = [
                  'stage' => [
                      'id' => $valueElements[0]->id,
                      'priority' => $valueElements[0]->priority,
                      'stage_id' => $valueElements[0]->stage_id,
                      'has_groups' => $valueElements[0]->has_groups,
                      'name' => $valueElements[0]->name,
                      'locale' => $valueElements[0]->locale,
                  ],
                  'has_groups' => $valueElements[0]->has_groups
                ];

                $seasonWithStages[$index]['elements_ids'] = [];
                $seasonWithStages[$index]['has_match'] = [];
                foreach ($valueElements as $valueElement){

                    $elementIndex = array_search($valueElement->element_id, $seasonWithStages[$index]['elements_ids']);

                    if($elementIndex === false){
                        $seasonWithStages[$index]['elements_ids'][] = $valueElement->element_id;
                        $seasonWithStages[$index]['has_match'][] = $valueElement->match_id != null ? true : false;
                    }
                    else{
                        if(!$seasonWithStages[$index]['has_match'][$elementIndex]){
                            $seasonWithStages[$index]['has_match'][$elementIndex] = $valueElement->match_id != null ? true : false;
                        }
                    }
                }

                if($valueElements[0]->has_groups){

                    $seasonsStagesGroups = DB::table("seasons__stages__groups")
                        ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                        ->leftJoin("groups", "groups.id", "=", "stages__groups.group_id")
                        ->leftJoin("group_translations", "group_translations.group_id", "=", "groups.id")
                        ->leftJoin("matches", "matches.seasons__stages__group_id", "=", "seasons__stages__groups.id")
                        ->where([
                            'seasons__stages__groups.season_id' => $id,
                            'stages__groups.stage_id' => $key,
                            'group_translations.locale' => Lang::getLocale()
                        ])
                        ->select("seasons__stages__groups.id as seasons__stages__group_id", "seasons__stages__groups.draw_points", "seasons__stages__groups.victory_points", "seasons__stages__groups.stages__group_id",
                            "groups.*", "group_translations.name", "matches.id as match_id")
                        ->groupBy("seasons__stages__groups.id")
                        ->get();

                    $seasonWithStages[$index]['draw_points'] = $seasonsStagesGroups[0]->draw_points;
                    $seasonWithStages[$index]['victory_points'] = $seasonsStagesGroups[0]->victory_points;
                    $seasonWithStages[$index]['group'] = [];
                    $seasonWithStages[$index]['group_teams_ids'] = [];

                    $jndex = 0;
                    foreach ($seasonsStagesGroups as $seasonsStagesGroup){
                        $seasonWithStages[$index]['group'][] = [
                            'id' => $seasonsStagesGroup->id,
                            'name' => $seasonsStagesGroup->name,
                            'priority' => $seasonsStagesGroup->priority,
                            'stages__group_id' => $seasonsStagesGroup->stages__group_id,
                            'has_match' => $seasonsStagesGroup->match_id != null ? true : false
                        ];

                        $seasonsStagesGroupsTeams = DB::table("seasons__stages__groups__teams")
                            ->where('seasons__stages__group_id', $seasonsStagesGroup->seasons__stages__group_id)
                            ->get();

                        foreach ($seasonsStagesGroupsTeams as $seasonsStagesGroupsTeam){
                            $seasonWithStages[$index]['group_teams_ids'][$jndex][] = $seasonsStagesGroupsTeam->team_id;
                        }
                        $jndex++;

                    }
                }

                $index++;
            }

            $season->fullStages = $seasonWithStages;


            return response()->json([
                'code' => 0,
                'season' => $season
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $season = DB::table("seasons")
            ->where("id", $id)
            ->first();

        if($season){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['tournament_id'] = "required|exists:tournaments,id";
                $rules['start_year'] = "required|size:4";
                $rules['end_year'] = "size:4";
                $rules['is_in_home'] = "";

                $rules['teams_ids'] = "required";
                $rules['teams_ids.*.*'] = "required";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

//                    if(isset($input['is_in_home'])){
//                        DB::table("seasons")->update([
//                            'is_in_home' => 0
//                        ]);
//                    }

                    DB::table("seasons")
                        ->where('id', $id)
                        ->update([
                            'tournament_id' => $input['tournament_id'],
                            'start_year' => $input['start_year'],
                            'end_year' => isset($input['end_year']) ? $input['end_year'] : null,
                            'is_in_home' => isset($input['is_in_home']) ? 1 : 0
                        ]);


                    DB::table("seasons__teams")->where('season_id', $id)->delete();
                    $insertTeamsData = [];
                    foreach (explode(',', $input['teams_ids']) as $key => $value){
                        $insertTeamsData[] = [
                            'season_id' => $id,
                            'team_id' => $value
                        ];
                    }
                    DB::table("seasons__teams")->insert($insertTeamsData);


                    $elementsIds = isset($input['elements_ids']) && is_array($input['elements_ids']) ? $input['elements_ids'] : [];

                    $fullElementsIds = [];
                    foreach ($elementsIds as $key => $elementsId){
                        $fullElementsIds = array_merge($fullElementsIds, explode(',', $elementsId));
                    }
                    $seasonsElements = DB::table("seasons__elements")->where('season_id', $id)->pluck('element_id')->toArray();

                    $forAddingElementsIds = array_diff($fullElementsIds, $seasonsElements);
                    $forDeletingElementsIds = array_diff($seasonsElements, $fullElementsIds);

                    $insertElementsData = [];
                    foreach ($forAddingElementsIds as $valueElement){
                        $insertElementsData[] = [
                            'season_id' => $id,
                            'element_id' => $valueElement
                        ];
                    }
                    DB::table("seasons__elements")->insert($insertElementsData);


                    foreach ($forDeletingElementsIds as $valueElement){

                        DB::table("seasons__elements")
                            ->where([
                                'season_id' => $id,
                                'element_id' => $valueElement
                            ])
                            ->delete();
                    }


                    $seasonsStagesGroups = DB::table("seasons__stages__groups")
                        ->where("season_id", $id)
                        ->get();

                    if($seasonsStagesGroups->count() > 0){

                        $seasonsStagesGroupIds = [];
                        foreach ($seasonsStagesGroups as $seasonsStagesGroup){
                            $seasonsStagesGroupIds[] = $seasonsStagesGroup->id;
                        }

                        DB::table("seasons__stages__groups__teams")->whereIn("seasons__stages__group_id", $seasonsStagesGroupIds)->delete();
                    }

                    if(isset($input['has_groups'])){
                        foreach ($input['has_groups'] as $stageId1 => $hasGroupValue){
                            if($hasGroupValue){
                                $stagesGroupIds = $input['stages__group_id'][$stageId1];

                                $seasonsStagesGroups = DB::table("seasons__stages__groups")->where([
                                    "season_id" => $id
                                ])->pluck('stages__group_id')->toArray();


                                $forAddingStagesGroupIds = array_diff($stagesGroupIds, $seasonsStagesGroups);
                                $forDeletingStagesGroupIds = array_diff($seasonsStagesGroups, $stagesGroupIds);
                                $forUpdatingStagesGroupIds = array_intersect($seasonsStagesGroups, $stagesGroupIds);

//                                dd($forAddingStagesGroupIds, $forDeletingStagesGroupIds, $forUpdatingStagesGroupIds, $input['group_teams_ids'], $stageId1);


                                if(count($forDeletingStagesGroupIds) > 0) DB::table("seasons__stages__groups")->whereIn("stages__group_id", $forDeletingStagesGroupIds)->delete();

                                foreach ($forUpdatingStagesGroupIds as $stagesGroupIdKey => $stagesGroupIdValue){

                                    $seasonsStagesGroup = DB::table("seasons__stages__groups")->where([
                                        "season_id" => $id,
                                        "stages__group_id" => $stagesGroupIdValue
                                    ])->first();

                                    DB::table("seasons__stages__groups")
                                        ->where("id", $seasonsStagesGroup->id)
                                        ->update([
                                            'draw_points' => $input['draw_points'][$stageId1],
                                            'victory_points' => $input['victory_points'][$stageId1]
                                        ]);

                                    $neededKey = array_search($stagesGroupIdValue, $input['stages__group_id'][$stageId1]);

                                    foreach (explode(',', $input['group_teams_ids'][$stageId1][$neededKey]) as $groupTeamIdKey => $groupTeamIdValue){
                                        DB::table("seasons__stages__groups__teams")->insert([
                                            'seasons__stages__group_id' => $seasonsStagesGroup->id,
                                            'team_id' => $groupTeamIdValue
                                        ]);
                                    }
                                }

                                foreach ($forAddingStagesGroupIds as $stagesGroupIdKey => $stagesGroupIdValue){

                                    $stagesGroupId = DB::table("seasons__stages__groups")->insertGetId([
                                        'season_id' => $id,
                                        'draw_points' => $input['draw_points'][$stageId1],
                                        'victory_points' => $input['victory_points'][$stageId1],
                                        'stages__group_id' => $stagesGroupIdValue,
                                    ]);

                                    $neededKey = array_search($stagesGroupIdValue, $input['stages__group_id'][$stageId1]);

                                    foreach (explode(',', $input['group_teams_ids'][$stageId1][$neededKey]) as $groupTeamIdKey => $groupTeamIdValue){
                                        DB::table("seasons__stages__groups__teams")->insert([
                                            'seasons__stages__group_id' => $stagesGroupId,
                                            'team_id' => $groupTeamIdValue
                                        ]);
                                    }
                                }
                            }
                        }
                    }

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $season = DB::table("seasons")
            ->where([
                "id" => $id
            ])
            ->first();

        if($season){

            $match = DB::table("matches")
                ->leftJoin("seasons__elements", "seasons__elements.id", "=", "matches.seasons__element_id")
                ->leftJoin("seasons__stages__groups", "seasons__stages__groups.id", "=", "matches.seasons__stages__group_id")
                ->where("matches.season_id", $id)
                ->orWhere("seasons__elements.season_id", $id)
                ->orWhere("seasons__stages__groups.season_id", $id)
                ->first();

            $newsItem = DB::table("news")
                ->where("season_id", $id)
                ->first();

            if(!$match && !$newsItem){
                DB::table("seasons")->where("id", $id)->delete();

                if($season->is_in_home){
                    DB::table("seasons")
                        ->orderBy('seasons.id', 'DESC')
                        ->limit(1)
                        ->update([
                            'is_in_home' => 1
                        ]);
                }

                DB::table("seasons__elements")->where("season_id", $id)->delete();
                DB::table("seasons__teams")->where("season_id", $id)->delete();

                $seasonsStagesGroups = DB::table("seasons__stages__groups")
                    ->where("season_id", $id)
                    ->get();

                if($seasonsStagesGroups->count() > 0){

                    $seasonsStagesGroupIds = [];
                    foreach ($seasonsStagesGroups as $seasonsStagesGroup){
                        $seasonsStagesGroupIds[] = $seasonsStagesGroup->id;
                    }

                    DB::table("seasons__stages__groups")->where("season_id", $id)->delete();

                    DB::table("seasons__stages__groups__teams")->whereIn("seasons__stages__group_id", $seasonsStagesGroupIds)->delete();
                }

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
