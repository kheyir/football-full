<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class StagesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $stages = DB::table("stages")
            ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "stages.id");


        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $stages = $stages->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $stages = $stages
            ->where([
                "stage_translations.locale" => Lang::getLocale(),
            ])
            ->orderBy('stages.priority', 'DESC')
            ->orderBy('stage_translations.name', 'ASC')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'stages' => $stages
        ]);
    }

    public function getStages(){

        $stages = DB::table("stages")
            ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "stages.id");

        $input = Request::except('_token');

        if(isset($input['season_id']) && !empty($input['season_id'])){
            $stages = $stages
                ->leftJoin("elements", "elements.stage_id", "=", "stages.id")
                ->leftJoin("seasons__elements", "seasons__elements.element_id", "=", "elements.id")
                ->where([
                    'seasons__elements.season_id' => $input['season_id']
                ])
                ->groupBy("stages.id");
        }

        $stages = $stages
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->select("stages.*", "stage_translations.name")
            ->get();

        return response()->json([
            'code' => 0,
            'stages' => $stages
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['priority'] = "required|numeric";
            $rules['has_groups'] = "";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $stageId = DB::table("stages")->insertGetId([
                    'priority' => $input['priority'],
                    'has_groups' => isset($input['has_groups']) ? 1 : 0
                ]);

                if(isset($input['has_groups'])){
                    $stagesGroupsData = [];

                    foreach (explode(',', $input['groups']) as $groupId){
                        $stagesGroupsData[] = [
                            'stage_id' => $stageId,
                            'group_id' => $groupId
                        ];
                    }

                    DB::table("stages__groups")->insert($stagesGroupsData);
                }

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'stage_id' => $stageId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("stage_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $stage = DB::table("stages")
            ->where("id", $id)
            ->first();

        if($stage){

            $stageTranslations = DB::table("stage_translations")
                ->where("stage_id", $id)
                ->get();

            $stage->translations = $stageTranslations->groupBy("locale");


            $stagesGroups = DB::table("stages__groups")
                ->leftJoin("groups", "groups.id", "=", "stages__groups.group_id")
                ->leftJoin("group_translations", "group_translations.group_id", "=", "stages__groups.group_id")
                ->leftJoin("seasons__stages__groups", "seasons__stages__groups.stages__group_id", "=", "stages__groups.id")
                ->where([
                    "stage_id" => $id,
                    "locale" => Lang::getLocale(),
                ])
                ->orderBy('groups.priority', 'ASC')
                ->select('stages__groups.id as stages__groups_id', 'stages__groups.group_id', 'group_translations.name', 'seasons__stages__groups.id as seasons__stages__group_id')
                ->groupBy('stages__groups.id')
                ->get();

            $stage->groups = $stagesGroups;

            return response()->json([
                'code' => 0,
                'stage' => $stage
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $stage = DB::table("stages")
            ->where("id", $id)
            ->first();

        if($stage){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['priority'] = "required|numeric";
                $rules['has_groups'] = "";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("stages")
                        ->where('id', $id)
                        ->update([
                            'priority' => $input['priority'],
                            'has_groups' => isset($input['has_groups']) ? 1 : 0
                        ]);

                    $stagesGroups = DB::table("stages__groups")
                        ->where("stage_id", $id)
                        ->get()
                        ->toArray();

                    $realStagesGroups = array_column($stagesGroups, 'group_id');
                    $inputStagesGroups = explode(',', $input['groups']);

                    $forAddingStagesGroups = array_diff($inputStagesGroups, $realStagesGroups);
                    $forDeletingStagesGroups = array_diff($realStagesGroups, $inputStagesGroups);

                    $insertElementsData = [];
                    foreach ($forAddingStagesGroups as $valueElement){
                        $insertElementsData[] = [
                            'stage_id' => $id,
                            'group_id' => $valueElement
                        ];
                    }
                    DB::table("stages__groups")->insert($insertElementsData);


                    foreach ($forDeletingStagesGroups as $valueElement){

                        DB::table("stages__groups")
                            ->where([
                                'stage_id' => $id,
                                'group_id' => $valueElement
                            ])
                            ->delete();
                    }


//                    DB::table("stages__groups")
//                        ->where('stage_id', $id)
//                        ->delete();

//                    if(isset($input['has_groups'])){
//                        $stagesGroupsData = [];
//
//                        foreach (explode(',', $input['groups']) as $groupId){
//                            $stagesGroupsData[] = [
//                                'stage_id' => $id,
//                                'group_id' => $groupId
//                            ];
//                        }
//
//                        DB::table("stages__groups")->insert($stagesGroupsData);
//                    }


                    DB::table("stage_translations")
                        ->where('stage_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'stage_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("stage_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $stage = DB::table("stages")
            ->where([
                "id" => $id
            ])
            ->first();

        if($stage){

            $element = DB::table("elements")
                ->where([
                    "stage_id" => $id
                ])
                ->first();

            $seasonsStagesGroup = DB::table("seasons__stages__groups")
                ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                ->where([
                    "stages__groups.stage_id" => $id
                ])
                ->first();

            if(!$element && !$seasonsStagesGroup){
                DB::table("stages")->where("id", $id)->delete();
                DB::table("stage_translations")->where("stage_id", $id)->delete();
                DB::table("stages__groups")->where("stage_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
