<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class ElementsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $elements = DB::table("elements")
            ->leftJoin("element_translations", "element_translations.element_id", "=", "elements.id")
            ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "elements.stage_id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $elements = $elements->where([
                ['element_translations.name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        if(isset($input['stage_id']) && !empty($input['stage_id'])){
            $elements = $elements->where([
                'elements.stage_id' => $input['stage_id']
            ]);
        }

        $elements = $elements
            ->where([
                "element_translations.locale" => Lang::getLocale(),
                "stage_translations.locale" => Lang::getLocale()
            ])
            ->select('elements.id', 'element_translations.name', 'stage_translations.name as stage_name')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'elements' => $elements
        ]);
    }

    public function getElements(){

        $elements = DB::table("elements")
            ->leftJoin("element_translations", "element_translations.element_id", "=", "elements.id");

        $input = Request::except('_token');

        if(isset($input['stage_id']) && !empty($input['stage_id'])){
            $elements = $elements->where([
                'elements.stage_id' => $input['stage_id']
            ]);
        }

        if(isset($input['season_id']) && !empty($input['season_id'])){
            $elements = $elements
                ->leftJoin("seasons__elements", "seasons__elements.element_id", "=", "elements.id")
                ->where([
                    'seasons__elements.season_id' => $input['season_id']
                ]);
        }

        $elements = $elements->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('elements.priority', 'ASC')
            ->select("elements.*", "element_translations.name")
            ->get();

        return response()->json([
            'code' => 0,
            'elements' => $elements
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['stage_id'] = "required|exists:stages,id";
            $rules['priority'] = "required|numeric";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $elementId = DB::table("elements")->insertGetId([
                    'stage_id' => $input['stage_id'],
                    'priority' => $input['priority']
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'element_id' => $elementId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("element_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $element = DB::table("elements")
            ->where("id", $id)
            ->first();

        if($element){

            $elementTranslations = DB::table("element_translations")
                ->where("element_id", $id)
                ->get();

            $elementTranslations = $elementTranslations->groupBy("locale");

            $element->translations = $elementTranslations;

            return response()->json([
                'code' => 0,
                'element' => $element
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $element = DB::table("elements")
            ->where("id", $id)
            ->first();

        if($element){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['stage_id'] = "required|exists:stages,id";
                $rules['priority'] = "required|numeric";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("elements")
                        ->where('id', $id)
                        ->update([
                            'stage_id' => $input['stage_id'],
                            'priority' => $input['priority']
                        ]);

                    DB::table("element_translations")
                        ->where('element_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'element_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("element_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $element = DB::table("elements")
            ->where([
                "id" => $id
            ])
            ->first();

        if($element){

            $seasonsElement = DB::table("seasons__elements")
                ->where([
                    "element_id" => $id
                ])
                ->first();

            if(!$seasonsElement){
                DB::table("elements")->where("id", $id)->delete();
                DB::table("element_translations")->where("element_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
