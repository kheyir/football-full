<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;
use JWTFactory;
use JWTAuth;

class DashboardController extends AdminController {

    function __construct(){

    }

    public function get(){

        $customClaims = [
            'uid' => 'khojaly',
            'exp' => '158776029000',
            "iss" => "http://example.org"
        ];

        JWTFactory::customClaims($customClaims);

        $payload = JWTFactory::make($customClaims);

        $token = JWTAuth::encode($payload);

        JWTAuth::setToken($token);

        return response()->json([
            'code' => 1,
        ]);
    }

}
