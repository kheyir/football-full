<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;

class TournamentsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->leftJoin("tournaments_category_translations", "tournaments_category_translations.tournaments_category_id", "=", "tournaments.tournaments_category_id")
            ->leftJoin("country_translations", function($join){
                $join->on("country_translations.country_id", "=", "tournaments.country_id")
                    ->where("country_translations.locale", Lang::getLocale());
            });

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $tournaments = $tournaments->where([
                ['tournament_translations.name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        if(isset($input['tournaments_category_id']) && !empty($input['tournaments_category_id'])){
            $tournaments = $tournaments->where([
                'tournaments.tournaments_category_id' => $input['tournaments_category_id']
            ]);
        }

        if(isset($input['country_id']) && $input['country_id'] > -1){
            $tournaments = $tournaments->where([
                'tournaments.country_id' => $input['country_id']
            ]);
        }

        $whiteIsNewsCategoryList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_news_category']) && in_array($input['is_news_category'], $whiteIsNewsCategoryList)){
            $tournaments = $tournaments
                ->where([
                    "tournaments.is_news_category" => array_search($input['is_news_category'], $whiteIsNewsCategoryList)
                ]);
        }

        $whiteIsVideosCategoryList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_videos_category']) && in_array($input['is_videos_category'], $whiteIsVideosCategoryList)){
            $tournaments = $tournaments
                ->where([
                    "tournaments.is_videos_category" => array_search($input['is_videos_category'], $whiteIsVideosCategoryList)
                ]);
        }

        $whitePriorityForCountryList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['priority_for_country']) && in_array($input['priority_for_country'], $whitePriorityForCountryList)){
            $tournaments = $tournaments
                ->where([
                    "tournaments.priority_for_country" => array_search($input['priority_for_country'], $whitePriorityForCountryList)
                ]);
        }

        $whiteIsLeagueList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_league']) && in_array($input['is_league'], $whiteIsLeagueList)){
            $tournaments = $tournaments
                ->where([
                    "tournaments.is_league" => array_search($input['is_league'], $whiteIsLeagueList)
                ]);
        }

        $tournaments = $tournaments
            ->where([
                "tournament_translations.locale" => Lang::getLocale(),
                "tournaments_category_translations.locale" => Lang::getLocale()
            ])
            ->select("tournaments.*", 'tournament_translations.name',
                'country_translations.name as country_name', 'tournaments_category_translations.name as tournaments_category_name')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'tournaments' => $tournaments
        ]);
    }

    public function getTournaments(){

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'tournaments' => $tournaments
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['slug'] = "required|unique:tournaments,slug";
            $rules['tournaments_category_id'] = "required|exists:tournaments_categories,id";
            $rules['country_id'] = "required";
            $rules['logo_mini'] = "required|mimes:jpeg,png,jpg,bmp,gif,svg";
            $rules['logo_big'] = "required|mimes:jpeg,png,jpg,bmp,gif,svg";
            if(isset($input['is_league'])){
                $rules['league_victory_points'] = "required|numeric";
                $rules['league_draw_points'] = "required|numeric";
            }

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                if(isset($input['priority_for_country'])){
                    DB::table("tournaments")
                        ->where('country_id', $input['country_id'])
                        ->update([
                            'priority_for_country' => 0
                        ]);
                }

                if(isset($input['logo_mini'])){
                    $file = $input['logo_mini'];
                    $input['logo_mini'] = date("YmdHis") . $file->getClientOriginalName();

                    $invImage = InterventionImage::make($file);

                    $invImage->fit(50, 50)->save(public_path() . '/assets/site/assets/images/'.$input['logo_mini']);
                }

                if(isset($input['logo_big'])){
                    $file = $input['logo_big'];
                    $input['logo_big'] = date("YmdHis") . $file->getClientOriginalName();

                    $invImage = InterventionImage::make($file);

                    $invImage->fit(600, 600)->save(public_path() . '/assets/site/assets/images/'.$input['logo_big']);
                }

                $tournamentId = DB::table("tournaments")->insertGetId([
                    'slug' => $input['slug'],
                    'tournaments_category_id' => $input['tournaments_category_id'],
                    'country_id' => $input['country_id'],
                    'logo_mini' => $input['logo_mini'],
                    'logo_big' => $input['logo_big'],
                    'is_news_category' => isset($input['is_news_category']) ? 1 : 0,
                    'is_videos_category' => isset($input['is_videos_category']) ? 1 : 0,
                    'priority_for_country' => isset($input['is_league']) && isset($input['priority_for_country']) ? 1 : 0,
                    'is_league' => isset($input['is_league']) ? 1 : 0,
                    'league_victory_points' => isset($input['is_league']) ? $input['league_victory_points'] : null,
                    'league_draw_points' => isset($input['is_league']) ? $input['league_draw_points'] : null
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'tournament_id' => $tournamentId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("tournament_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $tournament = DB::table("tournaments")
            ->where("id", $id)
            ->first();

        if($tournament){

            $tournamentTranslations = DB::table("tournament_translations")
                ->where("tournament_id", $id)
                ->get();

            $tournamentTranslations = $tournamentTranslations->groupBy("locale");

            $tournament->translations = $tournamentTranslations;

            return response()->json([
                'code' => 0,
                'tournament' => $tournament
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $tournament = DB::table("tournaments")
            ->where("id", $id)
            ->first();

        if($tournament){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['slug'] = "required|unique:tournaments,slug,$id";
                $rules['tournaments_category_id'] = "required|exists:tournaments_categories,id";
                $rules['country_id'] = "required";
                $rules['logo_mini'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
                $rules['logo_big'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
                if(isset($input['is_league'])){
                    $rules['league_victory_points'] = "required|numeric";
                    $rules['league_draw_points'] = "required|numeric";
                }

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    if(isset($input['priority_for_country'])){
                        DB::table("tournaments")
                            ->where('country_id', $input['country_id'])
                            ->update([
                                'priority_for_country' => 0
                            ]);
                    }

                    $updateData = [
                        'slug' => $input['slug'],
                        'tournaments_category_id' => $input['tournaments_category_id'],
                        'country_id' => $input['country_id'],
                        'is_news_category' => isset($input['is_news_category']) ? 1 : 0,
                        'is_videos_category' => isset($input['is_videos_category']) ? 1 : 0,
                        'priority_for_country' => isset($input['is_league']) && isset($input['priority_for_country']) ? 1 : 0,
                        'is_league' => isset($input['is_league']) ? 1 : 0,
                        'league_victory_points' => isset($input['is_league']) ? $input['league_victory_points'] : null,
                        'league_draw_points' => isset($input['is_league']) ? $input['league_draw_points'] : null
                    ];

                    if(isset($input['logo_mini'])){
                        $file = $input['logo_mini'];
                        $input['logo_mini'] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(50, 50)->save(public_path() . '/assets/site/assets/images/' . $input['logo_mini']);

                        // if($tournament->logo_mini) File::delete(public_path() . '/assets/site/assets/images/' . $tournament->logo_mini);

                        $updateData['logo_mini'] = $input['logo_mini'];
                    }

                    if(isset($input['logo_big'])){
                        $file = $input['logo_big'];
                        $input['logo_big'] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(600, 600)->save(public_path() . '/assets/site/assets/images/' . $input['logo_big']);

                        // if($tournament->logo_big) File::delete(public_path() . '/assets/site/assets/images/' . $tournament->logo_big);

                        $updateData['logo_big'] = $input['logo_big'];
                    }

                    DB::table("tournaments")
                        ->where('id', $id)
                        ->update($updateData);

                    DB::table("tournament_translations")
                        ->where('tournament_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'tournament_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("tournament_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $tournament = DB::table("tournaments")
            ->where([
                "id" => $id
            ])
            ->first();

        if($tournament){

            $newsItem = DB::table("news")
                ->where("tournament_id", $id)
                ->first();

            $season = DB::table("seasons")
                ->where([
                    "tournament_id" => $id
                ])
                ->first();

            if(!$newsItem && !$season){
                DB::table("tournaments")->where("id", $id)->delete();
                DB::table("tournament_translations")->where("tournament_id", $id)->delete();

                if($tournament->priority_for_country){
                    DB::table("tournaments")
                        ->where([
                            'country_id' => $tournament->country_id,
                            'is_league' => $tournament->is_league
                        ])
                        ->orderBy('tournaments.id', 'DESC')
                        ->limit(1)
                        ->update([
                            'priority_for_country' => 1
                        ]);
                }

                // File::delete(public_path() . '/assets/site/assets/images/' . $tournament->logo_mini);
                // File::delete(public_path() . '/assets/site/assets/images/' . $tournament->logo_big);

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
