<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class AuthController extends AdminController {

    function __construct(){

    }

    public function login(){

        $input = Request::except('_token');

        if(isset($input)) {
            $rules['username'] = "required";
            $rules['password'] = "required";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $user = DB::table("users")
                    ->where(["username" => $input["username"]])
                    ->first();

                if($user){
                    if(Hash::check($input["password"], $user->password)){

                        $customClaims = [
                            'uid' => $input["username"]
                        ];

                        JWTFactory::customClaims($customClaims);

                        $payload = JWTFactory::make($customClaims);

                        $token = JWTAuth::encode($payload)->get();

                        return response()->json([
                            'code' => 0,
                            'token' => $token,
                            'message' => 'Authorized'
                        ]);
                    }
                    else{
                        return response()->json([
                            'code' => 1,
                            'message' => 'Password is incorrect'
                        ]);
                    }
                }
            }

            return response()->json([
                'code' => 1,
                'message' => 'Username and Password are required'
            ]);
        }

        return response()->json([
            'code' => 1,
            'message' => 'Username or Password is absent'
        ]);
    }

    public function authCheck(){

        return response()->json([
            'code' => 0,
            'message' => 'Authorized'
        ]);
    }

    public function getAuth(){

        $username = JWTAuth::getPayload()->get("uid");

        if($username){

            $user = DB::table("users")
                ->where(["username" => $username])
                ->first();

            if($user){
                return response()->json([
                    'code' => 0,
                    'username' => $username
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'User not found'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 1,
                'message' => 'uid is absent'
            ]);
        }

    }

    public function logout(){

        return response()->json([
            'code' => 0,
            'message' => 'Successful unauthorized'
        ]);
    }

}
