<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;

class CountriesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $countries = DB::table("countries")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "countries.id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $countries = $countries->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $whiteInHeadList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['in_head']) && in_array($input['in_head'], $whiteInHeadList)){
            $countries = $countries
                ->where([
                    "countries.in_head" => array_search($input['in_head'], $whiteInHeadList)
                ]);
        }

        $whiteIsNewsCategoryList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_news_category']) && in_array($input['is_news_category'], $whiteIsNewsCategoryList)){
            $countries = $countries
                ->where([
                    "countries.is_news_category" => array_search($input['is_news_category'], $whiteIsNewsCategoryList)
                ]);
        }

        $countries = $countries
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'countries' => $countries
        ]);
    }

    public function getCountries(){

        $countries = DB::table("countries")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "countries.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'countries' => $countries
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['slug'] = "required|unique:countries,slug";
            $rules['in_head'] = "";
            $rules['is_news_category'] = "";
            $rules['flag'] = "mimes:jpeg,png,jpg,bmp,gif,svg";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                if(isset($input['flag'])){
                    $file = $input['flag'];
                    $input['flag'] = date("YmdHis") . $file->getClientOriginalName();

                    $invImage = InterventionImage::make($file);

                    $invImage->fit(60, 60)->save(public_path() . '/assets/site/assets/images/'.$input['flag']);
                }

                $countryId = DB::table("countries")->insertGetId([
                    'slug' => $input['slug'],
                    'in_head' => isset($input['in_head']) ? 1 : 0,
                    'is_news_category' => isset($input['is_news_category']) ? 1 : 0,
                    'flag' => isset($input['flag']) ? $input['flag'] : null,
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'country_id' => $countryId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("country_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $country = DB::table("countries")
            ->where("id", $id)
            ->first();

        if($country){

            $countryTranslations = DB::table("country_translations")
                ->where("country_id", $id)
                ->get();

            $countryTranslations = $countryTranslations->groupBy("locale");

            $country->translations = $countryTranslations;

            return response()->json([
                'code' => 0,
                'country' => $country
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $country = DB::table("countries")
            ->where("id", $id)
            ->first();

        if($country){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['slug'] = "required|unique:countries,slug,$id";
                $rules['in_head'] = "";
                $rules['is_news_category'] = "";
                $rules['flag'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
                $rules['old_flag'] = "required|integer";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    $updateData = [
                        'slug' => $input['slug'],
                        'in_head' => isset($input['in_head']) ? 1 : 0,
                        'is_news_category' => isset($input['is_news_category']) ? 1 : 0
                    ];

                    if(isset($input['flag'])){
                        $file = $input['flag'];
                        $input['flag'] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(60, 60)->save(public_path() . '/assets/site/assets/images/' . $input['flag']);

                        $updateData['flag'] = $input['flag'];
                    }

                    if((isset($input['flag']) || $input['old_flag'] == 2) && $country->flag) File::delete(public_path() . '/assets/site/assets/images/' . $country->flag);

                    if(!isset($input['flag']) && $input['old_flag'] == 2){
                        $updateData['flag'] = null;
                    }

                    DB::table("countries")
                        ->where('id', $id)
                        ->update($updateData);

                    DB::table("country_translations")
                        ->where('country_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'country_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("country_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $country = DB::table("countries")
            ->where([
                "id" => $id
            ])
            ->first();

        if($country){

            $city = DB::table("cities")
                ->where([
                    "country_id" => $id
                ])
                ->first();

            $newsCountry = DB::table("news__countries")
                ->where([
                    "country_id" => $id
                ])
                ->first();

            $player = DB::table("players")
                ->where([
                    "country_id" => $id
                ])
                ->first();

            $team = DB::table("teams")
                ->where([
                    "country_id" => $id
                ])
                ->first();

            $tournament = DB::table("tournaments")
                ->where([
                    "country_id" => $id
                ])
                ->first();

            if(!$city && !$newsCountry && !$player && !$team && !$tournament){
                DB::table("countries")->where("id", $id)->delete();
                DB::table("country_translations")->where("country_id", $id)->delete();

                if($country->flag) File::delete(public_path() . '/assets/site/assets/images/' . $country->flag);

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
