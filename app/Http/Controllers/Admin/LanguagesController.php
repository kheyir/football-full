<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class LanguagesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $languages = DB::table("languages")
            ->orderBy("locale", "ASC")
            ->get();

        return response()->json([
            'code' => 0,
            'languages' => $languages
        ]);
    }

}
