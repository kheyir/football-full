<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;
use Storage;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;
use \App\Libraries\Curl;
use \App\Libraries\HelpFunctions;

class NewsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $input = Request::except('_token');

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $news = DB::table("news_item_translations")
            ->leftJoin("news",  "news.id", "=", "news_item_translations.news_item_id")
            ->leftJoin("tournament_translations", function($join){
                $join->on("tournament_translations.tournament_id", "=", "news.tournament_id")
                    ->where("tournament_translations.locale", Lang::getLocale());
            })
            ->leftJoin("seasons", function($join){
                $join->on("seasons.id", "=", "news.season_id");
            })
            ->leftJoin("news__categories", "news__categories.news_item_id", "=", "news.id")
            ->leftJoin("category_translations", function($join){
                $join->on("category_translations.category_id", "=", "news__categories.category_id")
                    ->where("category_translations.locale", Lang::getLocale());
            })
            ->leftJoin("news__countries", "news__countries.news_item_id", "=", "news.id")
            ->leftJoin("country_translations", function($join){
                $join->on("country_translations.country_id", "=", "news__countries.country_id")
                    ->where("country_translations.locale", Lang::getLocale());
            })
            ->leftJoin("news__teams", "news__teams.news_item_id", "=", "news.id")
            ->leftJoin("team_translations", function($join){
                $join->on("team_translations.team_id", "=", "news__teams.team_id")
                    ->where("team_translations.locale", Lang::getLocale());
            });

        if(isset($input['title']) && !empty($input['title'])){
            $news = $news->where([
                ['news_item_translations.title', "LIKE", '%' . $input['title'] . '%']
            ]);
        }

        if(isset($input['short_text']) && !empty($input['short_text'])){
            $news = $news->where([
                ['news_item_translations.short_text', "LIKE", '%' . $input['short_text'] . '%']
            ]);
        }

        if(isset($input['category_id']) && !empty($input['category_id'])){
            $news = $news->where([
                'news__categories.category_id' => $input['category_id']
            ]);
        }

        if(isset($input['country_id']) && !empty($input['country_id'])){
            $news = $news->where([
                'news__countries.country_id' => $input['country_id']
            ]);
        }

        if(isset($input['team_id']) && !empty($input['team_id'])){
            $news = $news->where([
                'news__teams.team_id' => $input['team_id']
            ]);
        }

        if(isset($input['tournament_id']) && !empty($input['tournament_id'])){
            $news = $news->where([
                'news.tournament_id' => $input['tournament_id']
            ]);
        }

        if(isset($input['season_id']) && !empty($input['season_id'])){
            $news = $news->where([
                'news.season_id' => $input['season_id']
            ]);
        }

        if(isset($input['min_creating_date']) && !empty($input['min_creating_date'])){
            $news = $news->where([
                ['news.creating_date', ">=", $input['min_creating_date']]
            ]);
        }

        if(isset($input['max_creating_date']) && !empty($input['max_creating_date'])){
            $news = $news->where([
                ['news.creating_date', "<=", $input['max_creating_date']]
            ]);
        }

        $whiteIsInSliderList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_in_slider']) && in_array($input['is_in_slider'], $whiteIsInSliderList)){
            $news = $news
                ->where([
                    "news.is_in_slider" => array_search($input['is_in_slider'], $whiteIsInSliderList)
                ]);
        }

        $whiteIsInRightList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_in_right']) && in_array($input['is_in_right'], $whiteIsInRightList)){
            $news = $news
                ->where([
                    "news.is_in_right" => array_search($input['is_in_right'], $whiteIsInRightList)
                ]);
        }

        $whiteIsActiveList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_active']) && in_array($input['is_active'], $whiteIsActiveList)){
            $news = $news
                ->where([
                    "news.is_active" => array_search($input['is_active'], $whiteIsActiveList)
                ]);
        }

        $news = $news->groupBy("news.id")
            ->orderBy("news.id", "DESC")
            ->select("news.*", "news_item_translations.id as news_item_translation_id", "news_item_translations.title", "news_item_translations.short_text", "news_item_translations.locale",
                "tournament_translations.name as tournament_name", "seasons.start_year", "seasons.end_year",
                DB::raw("GROUP_CONCAT(DISTINCT news_item_translations.title SEPARATOR ', ') as title"),
                DB::raw("GROUP_CONCAT(DISTINCT category_translations.name SEPARATOR ', ') as categories_names"),
                DB::raw("GROUP_CONCAT(DISTINCT country_translations.name SEPARATOR ', ') as countries_names"),
                DB::raw("GROUP_CONCAT(DISTINCT team_translations.name SEPARATOR ', ') as teams_names"))
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'news' => $news
        ]);
    }

    public function getNews(){

        $news = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.team_id", "=", "news.id");

        $input = Request::except('_token');

        if(isset($input['season_id']) && !empty($input['season_id'])){
            $news = $news->leftJoin("seasons__news", "seasons__news.team_id", "=", "news.id")
                ->where([
                    'seasons__news.season_id' => $input['season_id']
                ])
                ->groupBy("news.id");
        }

        if(isset($input['season_id']) && !empty($input['season_id']) && isset($input['group_id']) && !empty($input['group_id'])){
            $news = $news->leftJoin("seasons__stages__groups__news", "seasons__stages__groups__news.team_id", "=", "news.id")
                ->leftJoin("seasons__stages__groups", "seasons__stages__groups.id", "=", "seasons__stages__groups__news.seasons__stages__group_id")
                ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                ->where([
                    'stages__groups.group_id' => $input['group_id']
                ]);
        }

        $news = $news
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('news_item_translations.name', 'ASC')
            ->select("news.*", "news_item_translations.*")
            ->get();

        return response()->json([
            'code' => 0,
            'news' => $news
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $languages = DB::table("languages")->get();

            $rules['locale'] = "required";
            $rules['title'] = "required|max:200";
            $rules['short_text'] = "required|max:200";
            $rules['large_text'] = "required";
            $rules['views_count'] = "required|numeric";
            $rules['image'] = "required|mimes:jpeg,png,jpg,bmp,gif,svg";
            $rules['creating_date'] = "required";
            $rules['tournament_id'] = "exists:tournaments,id";
            $rules['season_id'] = "exists:seasons,id";
            $rules['categories_ids'] = "";
            $rules['countries_ids'] = "";
            $rules['teams_ids'] = "";
            $rules['priority'] = "required|numeric";
            $rules['is_in_slider'] = "";
            $rules['is_in_right'] = "";
            $rules['is_active'] = "";
           $rules['to_publish'] = "";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $file = $input['image'];
                $input['image'] = date("YmdHis") . $file->getClientOriginalName();

                $invImage = InterventionImage::make($file);

                $invImage->fit(800, 600)->save(public_path() . '/assets/site/assets/images/'.$input['image']);


                $newsItemId = DB::table("news")->insertGetId([
                    'tournament_id' => isset($input['tournament_id']) ? $input['tournament_id'] : null,
                    'season_id' => isset($input['season_id']) ? $input['season_id'] : null,
                    'image' => $input['image'],
                    'creating_date' => $input['creating_date'],
                    'priority' => $input['priority'],
                    'is_in_slider' => isset($input['is_in_slider']) ? 1 : 0,
                    'is_in_right' => isset($input['is_in_right']) ? 1 : 0,
                    'is_active' => isset($input['is_active']) ? 1 : 0
                ]);

                if(isset($input['categories_ids'])){
                    $insertAddData = [];
                    foreach (explode(',', $input['categories_ids']) as $categoryId){
                        $insertAddData[] = [
                            'news_item_id' => $newsItemId,
                            'category_id' => $categoryId
                        ];
                    }
                    DB::table("news__categories")->insert($insertAddData);
                }

                if(isset($input['countries_ids'])){
                    $insertAddData = [];
                    foreach (explode(',', $input['countries_ids']) as $countryId){
                        $insertAddData[] = [
                            'news_item_id' => $newsItemId,
                            'country_id' => $countryId
                        ];
                    }
                    DB::table("news__countries")->insert($insertAddData);
                }

                if(isset($input['teams_ids'])){
                    $insertAddData = [];
                    foreach (explode(',', $input['teams_ids']) as $teamId){
                        $insertAddData[] = [
                            'news_item_id' => $newsItemId,
                            'team_id' => $teamId
                        ];
                    }
                    DB::table("news__teams")->insert($insertAddData);
                }

                //news_item_translations
                do{
                    $slug = str_slug($input['title']);

                    $slugsNews = DB::table('news_item_translations')
                        ->where([
                            'slug' => $slug
                        ])
                        ->get();

                    if($slugsNews->count() > 0) $input['title'] .= '-' . $slugsNews->count();

                }while($slugsNews->count() > 0);

                $insertData = [
                    'news_item_id' => $newsItemId,
                    'title' => $input['title'],
                    'short_text' => $input['short_text'],
                    'large_text' => $input['large_text'],
                    'views_count' => $input['views_count'],
                    'slug' => $slug,
                    'locale' => $input['locale'],
                ];

                DB::table("news_item_translations")->insert($insertData);

                if(isset($input['to_publish']) && $input['locale'] == 'az'){
                    $helpFunctions = new HelpFunctions();
                    $helpFunctions->pushLink("https://football.az/" . $input['locale'] ."/news-item/$slug");
                }

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $newsItem = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where("news_item_translations.id", $id)
            ->select("news.*", "news_item_translations.title", "news_item_translations.short_text", "news_item_translations.large_text",
            "news_item_translations.slug", "news_item_translations.views_count", "news_item_translations.locale")
            ->first();

        if($newsItem){

            $newsItem->categories_ids = DB::table("news__categories")
                ->where("news_item_id", $newsItem->id)
                ->get();


            $newsItem->countries_ids = DB::table("news__countries")
                ->where("news_item_id", $newsItem->id)
                ->get();


            $newsItem->teams_ids = DB::table("news__teams")
                ->where("news_item_id", $newsItem->id)
                ->get();


            return response()->json([
                'code' => 0,
                'newsItem' => $newsItem
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $newsItem = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where("news_item_translations.id", $id)
            ->select("news.*")
            ->first();

        if($newsItem){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $languages = DB::table("languages")->get();

                $rules['locale'] = "required";
                $rules['title'] = "required|max:200";
                $rules['short_text'] = "required|max:200";
                $rules['large_text'] = "required";
                $rules['views_count'] = "required|numeric";
                $rules['image'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
                $rules['creating_date'] = "required";
                $rules['tournament_id'] = "exists:tournaments,id";
                $rules['season_id'] = "exists:seasons,id";
                $rules['categories_ids'] = "";
                $rules['countries_ids'] = "";
                $rules['teams_ids'] = "";
                $rules['priority'] = "required|numeric";
                $rules['is_in_slider'] = "";
                $rules['is_in_right'] = "";
                $rules['is_active'] = "";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    $updateData = [
                        'tournament_id' => isset($input['tournament_id']) ? $input['tournament_id'] : null,
                        'season_id' => isset($input['season_id']) ? $input['season_id'] : null,
                        'creating_date' => $input['creating_date'],
                        'priority' => $input['priority'],
                        'is_in_slider' => isset($input['is_in_slider']) ? 1 : 0,
                        'is_in_right' => isset($input['is_in_right']) ? 1 : 0,
                        'is_active' => isset($input['is_active']) ? 1 : 0
                    ];

                    if(isset($input['image'])){

                        $file = $input['image'];
                        $input['image'] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(800, 600)->save(public_path() . '/assets/site/assets/images/'.$input['image']);

                        if($newsItem->image) File::delete(public_path() . '/assets/site/assets/images/' . $newsItem->image);

                        $updateData['image'] = $input['image'];
                    }

                    DB::table("news")
                        ->where('id', $newsItem->id)
                        ->update($updateData);


                    DB::table("news__categories")->where('news_item_id', $newsItem->id)->delete();

                    if(isset($input['categories_ids'])){
                        $insertAddData = [];
                        foreach (explode(',', $input['categories_ids']) as $categoryId){
                            $insertAddData[] = [
                                'news_item_id' => $newsItem->id,
                                'category_id' => $categoryId
                            ];
                        }
                        DB::table("news__categories")->insert($insertAddData);
                    }

                    DB::table("news__countries")->where('news_item_id', $newsItem->id)->delete();

                    if(isset($input['countries_ids'])){
                        $insertAddData = [];
                        foreach (explode(',', $input['countries_ids']) as $countryId){
                            $insertAddData[] = [
                                'news_item_id' => $newsItem->id,
                                'country_id' => $countryId
                            ];
                        }
                        DB::table("news__countries")->insert($insertAddData);
                    }

                    DB::table("news__teams")->where('news_item_id', $newsItem->id)->delete();

                    if(isset($input['teams_ids'])){
                        $insertAddData = [];
                        foreach (explode(',', $input['teams_ids']) as $teamId){
                            $insertAddData[] = [
                                'news_item_id' => $newsItem->id,
                                'team_id' => $teamId
                            ];
                        }
                        DB::table("news__teams")->insert($insertAddData);
                    }

                    //news_item_translations
                    $slug = $input['title'];
                    do{
                        $slug = str_slug($slug);

                        $slugsNews = DB::table('news_item_translations')
                            ->where([
                                'slug' => $slug,
                                ["news_item_translations.id", "!=", $id]
                            ])
                            ->get();

                        if($slugsNews->count() > 0){
                            $slug = $input['title'];
                            $slug .= '-' . $slugsNews->count();
                        }

                    }while($slugsNews->count() > 0);

                    $updateData = [
                        'news_item_id' => $newsItem->id,
                        'title' => $input['title'],
                        'short_text' => $input['short_text'],
                        'large_text' => $input['large_text'],
                        'views_count' => $input['views_count'],
                        'slug' => $slug,
                        'locale' => $input['locale'],
                    ];

                    DB::table("news_item_translations")
                        ->where("id", $id)
                        ->update($updateData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $newsItem = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where([
                "news_item_translations.id" => $id
            ])
            ->select("news.*")
            ->first();

        if($newsItem){

            DB::table("news")->where("id", $newsItem->id)->delete();
            DB::table("news_item_translations")->where("news_item_id", $newsItem->id)->delete();
            DB::table("news__categories")->where("news_item_id", $newsItem->id)->delete();
            DB::table("news__countries")->where("news_item_id", $newsItem->id)->delete();
            DB::table("news__teams")->where("news_item_id", $newsItem->id)->delete();
            DB::table("news_item_views")->where("news_item_id", $newsItem->id)->delete();

            File::delete(public_path() . '/assets/site/assets/images/' . $newsItem->image);

            return response()->json([
                'code' => 0,
                'message' => 'Element silindi'
            ]);
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

    public function imageUpload(){

        $input = Request::except('_token');
                
        $rules['file'] = "required";

        $validator = Validator::make($input, $rules);

        if(!$validator->fails()) {

            $file = $input['file'];
            $input['file'] = date("YmdHis_") . $file->getClientOriginalName();

            // dd($input, $file);

            Storage::disk('public')->putFileAs('assets/site/assets/images', $file, $input['file']);

            return response()->json([
                "url" => '/assets/site/assets/images/' . $input['file']
            ]);
            
        }
        else{
            return response()->json([
                "status" => false,
                "msg" => "Şəkil yüklənmədi"
            ]);

        }
    }

}
