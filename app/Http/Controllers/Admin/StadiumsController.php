<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class StadiumsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $stadiums = DB::table("stadiums")
            ->leftJoin("stadium_translations", "stadium_translations.stadium_id", "=", "stadiums.id")
            ->leftJoin("city_translations", "city_translations.city_id", "=", "stadiums.city_id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $stadiums = $stadiums->where([
                ['stadium_translations.name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        if(isset($input['city_id']) && !empty($input['city_id'])){
            $stadiums = $stadiums->where([
                'stadiums.city_id' => $input['city_id']
            ]);
        }

        $stadiums = $stadiums
            ->where([
                "stadium_translations.locale" => Lang::getLocale(),
                "city_translations.locale" => Lang::getLocale()
            ])
            ->select('stadiums.id', 'stadium_translations.name', 'city_translations.name as city_name')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'stadiums' => $stadiums
        ]);
    }

    public function getStadiums(){

        $stadiums = DB::table("stadiums")
            ->leftJoin("stadium_translations", "stadium_translations.stadium_id", "=", "stadiums.id")
            ->leftJoin("cities", "cities.id", "=", "stadiums.city_id");


        $input = Request::except('_token');

        if(isset($input['country_id']) && !empty($input['country_id'])){
            $stadiums = $stadiums->where([
                'cities.country_id' => $input['country_id']
            ]);
        }

        $stadiums = $stadiums
            ->where([
                "stadium_translations.locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->select("stadiums.*", "stadium_translations.name")
            ->get();

        return response()->json([
            'code' => 0,
            'stadiums' => $stadiums
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['city_id'] = "required|exists:cities,id";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $stadiumId = DB::table("stadiums")->insertGetId([
                    'city_id' => $input['city_id']
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'stadium_id' => $stadiumId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("stadium_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $stadium = DB::table("stadiums")
            ->leftJoin("cities", "cities.id", "=", "stadiums.city_id")
            ->where("stadiums.id", $id)
            ->select("stadiums.id", "stadiums.city_id", "stadiums.size", "stadiums.games_count",
                "cities.country_id")
            ->first();

        if($stadium){

            $stadiumTranslations = DB::table("stadium_translations")
                ->where("stadium_id", $id)
                ->get();

            $stadiumTranslations = $stadiumTranslations->groupBy("locale");

            $stadium->translations = $stadiumTranslations;

            return response()->json([
                'code' => 0,
                'stadium' => $stadium
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $stadium = DB::table("stadiums")
            ->where("id", $id)
            ->first();

        if($stadium){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['city_id'] = "required|exists:cities,id";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("stadiums")
                        ->where('id', $id)
                        ->update([
                            'city_id' => $input['city_id']
                        ]);

                    DB::table("stadium_translations")
                        ->where('stadium_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'stadium_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("stadium_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $stadium = DB::table("stadiums")
            ->where([
                "id" => $id
            ])
            ->first();

        if($stadium){

            $match = DB::table("matches")
                ->where([
                    "stadium_id" => $id
                ])
                ->first();

            $team = DB::table("teams")
                ->where([
                    "stadium_id" => $id
                ])
                ->first();

            if(!$match && !$team){
                DB::table("stadiums")->where("id", $id)->delete();
                DB::table("stadium_translations")->where("stadium_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
