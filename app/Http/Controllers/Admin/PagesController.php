<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;

class PagesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $pages = DB::table("add_pages")->get();

        return response()->json([
            'code' => 0,
            'pages' => $pages
        ]);
    }

    public function getById(){

        $id = Request::segment(4);

        $pageItem = DB::table("add_pages")
            ->where("id", $id)
            ->first();

        if($pageItem){

            return response()->json([
                'code' => 0,
                'pageItem' => $pageItem
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $pageItem = DB::table("add_pages")
            ->where("id", $id)
            ->first();

        if($pageItem){

            $input = Request::except('_token');

            $rules['text'] = "required";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $updateData = [
                    'text' => $input['text']
                ];

                DB::table("add_pages")
                    ->where('id', $id)
                    ->update($updateData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

}
