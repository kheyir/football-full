<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class PositionsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $positions = DB::table("positions")
            ->leftJoin("position_translations", "position_translations.position_id", "=", "positions.id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $positions = $positions->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $positions = $positions
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'positions' => $positions
        ]);
    }

    public function getPositions(){

        $positions = DB::table("positions")
            ->leftJoin("position_translations", "position_translations.position_id", "=", "positions.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'positions' => $positions
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $positionId = DB::table("positions")->insertGetId([]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'position_id' => $positionId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("position_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $position = DB::table("positions")
            ->where("id", $id)
            ->first();

        if($position){

            $positionTranslations = DB::table("position_translations")
                ->where("position_id", $id)
                ->get();

            $positionTranslations = $positionTranslations->groupBy("locale");

            $position->translations = $positionTranslations;

            return response()->json([
                'code' => 0,
                'position' => $position
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $position = DB::table("positions")
            ->where("id", $id)
            ->first();

        if($position){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("position_translations")
                        ->where('position_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'position_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("position_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $position = DB::table("positions")
            ->where([
                "id" => $id
            ])
            ->first();

        if($position){

            $team = DB::table("players")
                ->where([
                    "position_id" => $id
                ])
                ->first();

            if(!$team){
                DB::table("positions")->where("id", $id)->delete();
                DB::table("position_translations")->where("position_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
