<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;

class CategoriesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $categories = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $categories = $categories->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $whiteInHeadList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_featured']) && in_array($input['is_featured'], $whiteInHeadList)){
            $categories = $categories
                ->where([
                    "categories.is_featured" => array_search($input['is_featured'], $whiteInHeadList)
                ]);
        }

        $whiteIsNewsCategoryList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_news_category']) && in_array($input['is_news_category'], $whiteIsNewsCategoryList)){
            $categories = $categories
                ->where([
                    "categories.is_news_category" => array_search($input['is_news_category'], $whiteIsNewsCategoryList)
                ]);
        }

        $whiteIsForVideoList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_for_video']) && in_array($input['is_for_video'], $whiteIsForVideoList)){
            $categories = $categories
                ->where([
                    "categories.is_for_video" => array_search($input['is_for_video'], $whiteIsForVideoList)
                ]);
        }

        $categories = $categories
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'categories' => $categories
        ]);
    }

    public function getCategories(){

        $categories = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id");

        $input = Request::except('_token');

        if(isset($input['is_for_video']) && !empty($input['is_for_video'])){
            $categories = $categories
                ->where([
                    'is_for_video' => 1
                ]);
        }

        $categories = $categories
            ->where([
                "category_translations.locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'categories' => $categories
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['slug'] = "required|unique:categories,slug";
            $rules['is_news_category'] = "";
            $rules['is_featured'] = "";
            $rules['is_for_video'] = "";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $categoryId = DB::table("categories")->insertGetId([
                    'slug' => $input['slug'],
                    'is_news_category' => isset($input['is_news_category']) ? 1 : 0,
                    'is_featured' => isset($input['is_news_category']) && isset($input['is_featured']) ? 1 : 0,
                    'is_for_video' => isset($input['is_for_video']) ? 1 : 0
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'category_id' => $categoryId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("category_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $category = DB::table("categories")
            ->where("id", $id)
            ->first();

        if($category){

            $categoryTranslations = DB::table("category_translations")
                ->where("category_id", $id)
                ->get();

            $categoryTranslations = $categoryTranslations->groupBy("locale");

            $category->translations = $categoryTranslations;

            return response()->json([
                'code' => 0,
                'category' => $category
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $category = DB::table("categories")
            ->where("id", $id)
            ->first();

        if($category){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['slug'] = "required|unique:categories,slug,$id";
                $rules['is_news_category'] = "";
                $rules['is_featured'] = "";
                $rules['is_for_video'] = "";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("categories")
                        ->where('id', $id)
                        ->update([
                            'slug' => $input['slug'],
                            'is_news_category' => isset($input['is_news_category']) ? 1 : 0,
                            'is_featured' => isset($input['is_news_category']) && isset($input['is_featured']) ? 1 : 0,
                            'is_for_video' => isset($input['is_for_video']) ? 1 : 0
                        ]);

                    DB::table("category_translations")
                        ->where('category_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'category_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("category_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $category = DB::table("categories")
            ->where([
                "id" => $id
            ])
            ->first();

        if($category){

            $newsCategory = DB::table("news__categories")
                ->where([
                    "category_id" => $id
                ])
                ->first();

            $videosCategories = DB::table("videos__categories")
                ->where([
                    "category_id" => $id
                ])
                ->first();

            if(!$newsCategory && !$videosCategories){
                DB::table("categories")->where("id", $id)->delete();
                DB::table("category_translations")->where("category_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
