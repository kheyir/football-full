<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class CitiesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $cities = DB::table("cities")
            ->leftJoin("city_translations", "city_translations.city_id", "=", "cities.id")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "cities.country_id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $cities = $cities->where([
                ['city_translations.name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        if(isset($input['country_id']) && !empty($input['country_id'])){
            $cities = $cities->where([
                'cities.country_id' => $input['country_id']
            ]);
        }

        $cities = $cities
            ->where([
                "city_translations.locale" => Lang::getLocale(),
                "country_translations.locale" => Lang::getLocale()
            ])
            ->select('cities.id', 'city_translations.name', 'country_translations.name as country_name')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'cities' => $cities
        ]);
    }

    public function getCities(){

        $cities = DB::table("cities")
            ->leftJoin("city_translations", "city_translations.city_id", "=", "cities.id");

        $input = Request::except('_token');

        if(isset($input['country_id']) && !empty($input['country_id'])){
            $cities = $cities->where([
                'cities.country_id' => $input['country_id']
            ]);
        }

        $cities = $cities->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'cities' => $cities
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['country_id'] = "required|exists:countries,id";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $cityId = DB::table("cities")->insertGetId([
                    'country_id' => $input['country_id']
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'city_id' => $cityId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("city_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $city = DB::table("cities")
            ->where("id", $id)
            ->first();

        if($city){

            $cityTranslations = DB::table("city_translations")
                ->where("city_id", $id)
                ->get();

            $cityTranslations = $cityTranslations->groupBy("locale");

            $city->translations = $cityTranslations;

            return response()->json([
                'code' => 0,
                'city' => $city
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $city = DB::table("cities")
            ->where("id", $id)
            ->first();

        if($city){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['country_id'] = "required|exists:countries,id";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("cities")
                        ->where('id', $id)
                        ->update([
                            'country_id' => $input['country_id']
                        ]);

                    DB::table("city_translations")
                        ->where('city_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'city_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("city_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $city = DB::table("cities")
            ->where([
                "id" => $id
            ])
            ->first();

        if($city){

            $stadium = DB::table("stadiums")
                ->where([
                    "city_id" => $id
                ])
                ->first();

            if(!$stadium){
                DB::table("cities")->where("id", $id)->delete();
                DB::table("city_translations")->where("city_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
