<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class GroupsController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $groups = DB::table("groups")
            ->leftJoin("group_translations", "group_translations.group_id", "=", "groups.id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $groups = $groups->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $groups = $groups
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy("priority", "DESC")
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'groups' => $groups
        ]);
    }

    public function getGroups(){

        $groups = DB::table("groups")
            ->leftJoin("group_translations", "group_translations.group_id", "=", "groups.id");

        $input = Request::except('_token');

        if(isset($input['stage_id']) && !empty($input['stage_id'])){
            $groups = $groups->leftJoin("stages__groups", "stages__groups.group_id", "=", "groups.id")
                ->where([
                    'stages__groups.stage_id' => $input['stage_id']
                ])
                ->select("groups.*", "group_translations.name",
                    "stages__groups.id as stages__group_id");
        }

        $groups = $groups->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('groups.priority', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'groups' => $groups
        ]);
    }

    public function getGroupsByStage(){

        $groups = DB::table("groups")
            ->leftJoin("group_translations", "group_translations.group_id", "=", "groups.id")
            ->leftJoin("stages__groups", "stages__groups.group_id", "=", "groups.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'groups' => $groups
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['priority'] = "required|numeric";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $groupId = DB::table("groups")->insertGetId([
                    'priority' => $input['priority']
                ]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'group_id' => $groupId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("group_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $group = DB::table("groups")
            ->where("id", $id)
            ->first();

        if($group){

            $groupTranslations = DB::table("group_translations")
                ->where("group_id", $id)
                ->get();

            $groupTranslations = $groupTranslations->groupBy("locale");

            $group->translations = $groupTranslations;

            return response()->json([
                'code' => 0,
                'group' => $group
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $group = DB::table("groups")
            ->where("id", $id)
            ->first();

        if($group){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['priority'] = "required|numeric";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("groups")
                        ->where('id', $id)
                        ->update([
                            'priority' => $input['priority']
                        ]);

                    DB::table("group_translations")
                        ->where('group_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'group_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("group_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $group = DB::table("groups")
            ->where([
                "id" => $id
            ])
            ->first();

        if($group){

            $stagesGroup = DB::table("stages__groups")
                ->where([
                    "group_id" => $id
                ])
                ->first();

            if(!$stagesGroup){
                DB::table("groups")->where("id", $id)->delete();
                DB::table("group_translations")->where("group_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
