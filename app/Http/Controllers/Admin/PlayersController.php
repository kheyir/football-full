<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;

class PlayersController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $players = DB::table("players")
            ->leftJoin("player_translations", "player_translations.player_id", "=", "players.id")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "players.country_id")
            ->leftJoin("position_translations", "position_translations.position_id", "=", "players.position_id")
            ->leftJoin("players__teams", "players__teams.player_id", "=", "players.id")
            ->leftJoin("team_translations", function($join){
                $join->on("team_translations.team_id", "=", "players__teams.team_id")
                    ->where("team_translations.locale", Lang::getLocale());
            });

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $players = $players->where([
                ['player_translations.name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        if(isset($input['country_id']) && !empty($input['country_id'])){
            $players = $players->where([
                'players.country_id' => $input['country_id']
            ]);
        }

        if(isset($input['position_id']) && !empty($input['position_id'])){
            $players = $players->where([
                'players.position_id' => $input['position_id']
            ]);
        }

        if(isset($input['team_id']) && !empty($input['team_id'])){
            $players = $players->where([
                'players__teams.team_id' => $input['team_id']
            ]);
        }

        $players = $players
            ->where([
                "player_translations.locale" => Lang::getLocale(),
                "country_translations.locale" => Lang::getLocale(),
                "position_translations.locale" => Lang::getLocale()
            ])
            ->groupBy('players.id')
            ->select("players.*", 'player_translations.name',
                'country_translations.name as country_name',
                'position_translations.name as position_name',
                DB::raw('GROUP_CONCAT(team_translations.name) as teams_names'),
                DB::raw('GROUP_CONCAT(team_translations.team_id) as teams_ids'))
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'players' => $players
        ]);
    }

    public function getPlayers(){

        $players = DB::table("players")
            ->leftJoin("player_translations", "player_translations.player_id", "=", "players.id");

        $input = Request::except('_token');

        if(isset($input['team_id']) && !empty($input['team_id'])){
            $players = $players
                ->leftJoin("players__teams", "players__teams.player_id", "=", "players.id")
                ->leftJoin("team_translations", "team_translations.team_id", "=", "players__teams.team_id")
                ->where([
                    'players__teams.team_id' => $input['team_id'],
                    "team_translations.locale" => Lang::getLocale()
                ]);
        }

        $players = $players
            ->where([
                "player_translations.locale" => Lang::getLocale()
            ])
            ->orderBy('player_translations.name', 'ASC')
            ->select("players.*", "player_translations.name")
            ->get();

        return response()->json([
            'code' => 0,
            'players' => $players
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";
            $rules['country_id'] = "required|exists:countries,id";
            $rules['position_id'] = "required|exists:positions,id";
            $rules['photo'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
            $rules['date_of_birthday'] = "required";
            $rules['height'] = "";
            $rules['weight'] = "";
            $rules['team_id.*'] = "required|exists:teams,id";
            $rules['number.*'] = "required|numeric";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                if(isset($input['photo'])){
                    $file = $input['photo'];
                    $input['photo'] = date("YmdHis") . $file->getClientOriginalName();

                    $invImage = InterventionImage::make($file);

                    $invImage->fit(100, 100)->save(public_path() . '/assets/site/assets/images/'.$input['photo']);
                }

                $playerId = DB::table("players")->insertGetId([
                    'country_id' => $input['country_id'],
                    'position_id' => $input['position_id'],
                    'photo' => isset($input['photo']) ? $input['photo'] : null,
                    'date_of_birthday' => $input['date_of_birthday'],
                    'height' => isset($input['height']) ? $input['height'] : null,
                    'weight' => isset($input['weight']) ? $input['weight'] : null,
                ]);


                if(isset($input['team_id'])){
                    $insertTeamsData = [];
                    foreach ($input['team_id'] as $key => $value){
                        $insertTeamsData[] = [
                            'player_id' => $playerId,
                            'team_id' => $input['team_id'][$key],
                            'number' => $input['number'][$key],
                        ];
                    }

                    DB::table("players__teams")->insert($insertTeamsData);
                }

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'player_id' => $playerId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("player_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $player = DB::table("players")
            ->where("id", $id)
            ->first();

        if($player){

            $playerTranslations = DB::table("player_translations")
                ->where("player_id", $id)
                ->get();

            $player->translations = $playerTranslations->groupBy("locale");


            $playersTeams = DB::table("players__teams")
                ->where("player_id", $id)
                ->get();

            $player->playersTeams = $playersTeams;


            return response()->json([
                'code' => 0,
                'player' => $player
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $player = DB::table("players")
            ->where("id", $id)
            ->first();

        if($player){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";
                $rules['country_id'] = "required|exists:countries,id";
                $rules['position_id'] = "required|exists:positions,id";
                $rules['photo'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
                $rules['old_photo'] = "required|integer";
                $rules['date_of_birthday'] = "required";
                $rules['height'] = "";
                $rules['weight'] = "";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    $updateData = [
                        'country_id' => $input['country_id'],
                        'position_id' => $input['position_id'],
                        'date_of_birthday' => $input['date_of_birthday'],
                        'height' => isset($input['height']) ? $input['height'] : null,
                        'weight' => isset($input['weight']) ? $input['weight'] : null,
                    ];

                    if(isset($input['photo'])){
                        $file = $input['photo'];
                        $input['photo'] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(60, 60)->save(public_path() . '/assets/site/assets/images/' . $input['photo']);

                        $updateData['photo'] = $input['photo'];
                    }

                    if((isset($input['photo']) || $input['old_photo'] == 2) && $player->photo){
                        File::delete(public_path() . '/assets/site/assets/images/' . $player->photo);
                    }

                    if(!isset($input['photo']) && $input['old_photo'] == 2){
                        $updateData['photo'] = null;
                    }

                    DB::table("players")
                        ->where('id', $id)
                        ->update($updateData);



                    if(isset($input['team_id'])){
                        DB::table("players__teams")->where('player_id', $id)->delete();

                        $insertTeamsData = [];
                        foreach ($input['team_id'] as $key => $value){
                            $insertTeamsData[] = [
                                'player_id' => $id,
                                'team_id' => $input['team_id'][$key],
                                'number' => $input['number'][$key],
                            ];
                        }

                        DB::table("players__teams")->insert($insertTeamsData);
                    }


                    DB::table("player_translations")
                        ->where('player_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'player_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("player_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $player = DB::table("players")
            ->where([
                "id" => $id
            ])
            ->first();

        if($player){

            $video = DB::table("videos")
                ->where("player_id", $id)
                ->first();

            if(!$video){
                DB::table("players")->where("id", $id)->delete();
                DB::table("player_translations")->where("player_id", $id)->delete();

                DB::table("players__teams")->where("player_id", $id)->delete();

                File::delete(public_path() . '/assets/site/assets/images/' . $player->photo);

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
