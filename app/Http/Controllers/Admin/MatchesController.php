<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Config;

use File;
use JWTFactory;
use JWTAuth;
use InterventionImage;
use Storage;

class MatchesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        DB::connection()->enableQueryLog();

        $matches = DB::table("matches")
            ->leftJoin("stadiums", "stadiums.id", "=", "matches.stadium_id")
            ->leftJoin("stadium_translations", "stadium_translations.stadium_id", "=", "matches.stadium_id")
            ->leftJoin("cities", "cities.id", "=", "stadiums.city_id")
            ->leftJoin("city_translations", "city_translations.city_id", "=", "stadiums.city_id")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "cities.country_id")
            ->leftJoin("team_translations as home_team_translations", "home_team_translations.team_id", "=", "matches.home_team_id")
            ->leftJoin("team_translations as guest_team_translations", "guest_team_translations.team_id", "=", "matches.guest_team_id")
            ->leftJoin("seasons", "seasons.id", "=", "matches.season_id")
            ->leftJoin("seasons__elements", "seasons__elements.id", "=", "matches.seasons__element_id")
            ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
            ->leftJoin("stage_translations", function($join){
                $join->on("stage_translations.stage_id", "=", "elements.stage_id")
                    ->where("stage_translations.locale", Lang::getLocale());
            })
            ->leftJoin("seasons__stages__groups", "seasons__stages__groups.id", "=", "matches.seasons__stages__group_id")
            ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
            ->leftJoin("tournament_translations", function($join){
                $join->on("tournament_translations.tournament_id", "=", "seasons.tournament_id")
                    ->where("tournament_translations.locale", Lang::getLocale());
            })
            ->leftJoin("element_translations", function($join){
                $join->on("element_translations.element_id", "=", "seasons__elements.element_id")
                    ->where("element_translations.locale", Lang::getLocale());
            })
            ->leftJoin("group_translations", function($join){
                $join->on("group_translations.group_id", "=", "stages__groups.group_id")
                    ->where("group_translations.locale", Lang::getLocale());
            });

        $input = Request::except('_token');

        if(isset($input['country_id']) && !empty($input['country_id'])){
            $matches = $matches->where([
                'cities.country_id' => $input['country_id']
            ]);
        }

        if(isset($input['stadium_id']) && !empty($input['stadium_id'])){
            $matches = $matches->where([
                'matches.stadium_id' => $input['stadium_id']
            ]);
        }

        if(isset($input['min_start_date']) && !empty($input['min_start_date'])){
            $matches = $matches->where([
                ['matches.start_time', '>=', $input['min_start_date']]
            ]);
        }

        if(isset($input['max_start_date']) && !empty($input['max_start_date'])){
            $matches = $matches->where([
                ['matches.start_time', '<=', $input['max_start_date']]
            ]);
        }

        if(isset($input['home_team_id']) && !empty($input['home_team_id'])){
            $matches = $matches->where([
                'matches.home_team_id' => $input['home_team_id']
            ]);
        }

        if(isset($input['guest_team_id']) && !empty($input['guest_team_id'])){
            $matches = $matches->where([
                'matches.guest_team_id' => $input['guest_team_id']
            ]);
        }

        $whiteStatusList = [
            0 => 1,
            1 => 2,
            2 => 3,
            3 => 4,
            4 => 5
        ];
        if(isset($input['status']) && in_array($input['status'], $whiteStatusList)){
            $matches = $matches
                ->where([
                    "matches.status" => array_search($input['status'], $whiteStatusList)
                ]);
        }

        $whiteIsActiveList = [
            1 => 1,
            0 => 2
        ];
        if(isset($input['is_active']) && in_array($input['is_active'], $whiteIsActiveList)){
            $matches = $matches
                ->where([
                    "matches.is_active" => array_search($input['is_active'], $whiteIsActiveList)
                ]);
        }

        if(isset($input['is_active']) && !empty($input['is_active'])){
            $matches = $matches->where([
                'matches.is_active' => $input['is_active']
            ]);
        }

        if(isset($input['tournament_id']) && !empty($input['tournament_id'])){
            $matches = $matches->where([
                'seasons.tournament_id' => $input['tournament_id']
            ]);
        }

        if(isset($input['seasons_id']) && !empty($input['seasons_id'])){
            $matches = $matches->where([
                'matches.season_id' => $input['seasons_id']
            ]);
        }

        if(isset($input['element_id']) && !empty($input['element_id'])) {
            $matches = $matches->where([
                'seasons__elements.element_id' => $input['element_id']
            ]);
        }

        if(isset($input['stage_id']) && !empty($input['stage_id'])){
            $matches = $matches->where([
                'stage_translations.stage_id' => $input['stage_id']
            ]);
        }

        if(isset($input['group_id']) && !empty($input['group_id'])){
            $matches = $matches->where([
                'stages__groups.group_id' => $input['group_id']
            ]);
        }

        $matches = $matches
            ->where([
                "stadium_translations.locale" => Lang::getLocale(),
                "city_translations.locale" => Lang::getLocale(),
                "country_translations.locale" => Lang::getLocale(),
                "home_team_translations.locale" => Lang::getLocale(),
                "guest_team_translations.locale" => Lang::getLocale()
            ])
            ->select("matches.*",
                'stadium_translations.name as stadium_name',
                'city_translations.name as city_name',
                'country_translations.name as country_name',
                'home_team_translations.name as home_team_name',
                'guest_team_translations.name as guest_team_name',
                'tournament_translations.name as tournament_name',
                'seasons.start_year', 'seasons.end_year',
                'stage_translations.name as stage_name',
                'element_translations.name as element_name',
                'group_translations.name as group_name');

        $queries = DB::getQueryLog();
        $queries1 = $matches->toSql();

            $matches = $matches
            ->paginate(20, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'matches' => $matches
        ]);
    }

    public function getMatches(){

        $matches = DB::table("matches")
            ->leftJoin("match_translations", "match_translations.match_id", "=", "matches.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'matches' => $matches
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['stadium_id'] = "required|exists:stadiums,id";
            if(!isset($input['friendly_match'])){
                $rules['season_id'] = "required|exists:seasons,id";
                $rules['stage_id'] = "required|exists:stages,id";
                $rules['element_id'] = "required|exists:elements,id";

                if(isset($input['has_groups'])){
                    $rules['group_id'] = "required|exists:groups,id";
                }
            }
            $rules['start_time'] = "required";
            $rules['home_team_id'] = "required|exists:teams,id";
            $rules['guest_team_id'] = "required|exists:teams,id";
            $rules['home_team_goals_count'] = "required";
            $rules['guest_team_goals_count'] = "required";
            $rules['status'] = "required";
            $rules['is_active'] = "";
            $rules['preview_image'] = "mimes:jpeg,png,jpg,bmp,gif,svg";

            $rules['file.*'] = "required";
            $rules['categories.*'] = "required";
            $rules['video_preview_image.*'] = "required|mimes:jpeg,png,jpg,bmp,gif,svg";
            $rules['categories.*'] = "required";
            $rules['team_id.*'] = "required|exists:teams,id";
            $rules['player_id.*'] = "required|exists:players,id";
            $rules['minute.*'] = "required";
            $rules['is_top_goal.*'] = "required";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                if(isset($input['preview_image'])){
                    //preview start
                    $file = $input['preview_image'];
                    $input['preview_image'] = date("YmdHis") . $file->getClientOriginalName();

                    $invImage = InterventionImage::make($file);

                    $invImage->fit(368, 256)->save(public_path() . '/assets/site/assets/images/'.$input['preview_image']);
                    //preview end
                }

                if(!isset($input['friendly_match'])){
                    $seasonsElementId = DB::table("seasons__elements")
                        ->where([
                            'season_id' => $input['season_id'],
                            'element_id' => $input['element_id']
                        ])
                        ->select("seasons__elements.id")
                        ->first();

                    $input['seasons__element_id'] = $seasonsElementId ? $seasonsElementId->id : null;

                    if(isset($input['has_groups'])){
                        $seasonsStagesGroupId = DB::table("seasons__stages__groups")
                            ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                            ->where([
                                'seasons__stages__groups.season_id' => $input['season_id'],
                                'stages__groups.stage_id' => $input['stage_id'],
                                'stages__groups.group_id' => $input['group_id']
                            ])
                            ->select("seasons__stages__groups.id")
                            ->first();

                        $input['seasons__stages__group_id'] = $seasonsStagesGroupId ? $seasonsStagesGroupId->id : null;
                    }
                }

                $matchId = DB::table("matches")->insertGetId([
                    'stadium_id' => $input['stadium_id'],
                    'season_id' => !isset($input['friendly_match']) ? $input['season_id'] : null,
                    'seasons__element_id' => !isset($input['friendly_match']) ? $input['seasons__element_id'] : null,
                    'seasons__stages__group_id' => !isset($input['friendly_match']) && isset($input['has_groups']) ? $input['seasons__stages__group_id'] : null,
                    'start_time' => $input['start_time'],
                    'home_team_id' => $input['home_team_id'],
                    'guest_team_id' => $input['guest_team_id'],
                    'home_team_goals_count' => $input['home_team_goals_count'],
                    'guest_team_goals_count' => $input['guest_team_goals_count'],
                    'preview_image' => isset($input['preview_image']) ? $input['preview_image'] : null,
                    'status' => $input['status'],
                    'is_active' => isset($input['is_active']) ? 1 : 0
                ]);


                if(isset($input['file'])){

                    $input['video_name'] = [];

                    foreach ($input['file'] as $key => $value){

                        $filename = Storage::disk('public')->put('assets/site/assets/videos/', $input['file'][$key]);

                        $filenameExploded = explode('/', $filename);

                        $input['video_name'][$key] = $filenameExploded[count($filenameExploded) - 1];

                        //preview start
                        $file = $input['video_preview_image'][$key];
                        $input['video_preview_image'][$key] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(368, 256)->save(public_path() . '/assets/site/assets/images/'.$input['video_preview_image'][$key]);
                        //preview end

                        $insertVideosData = [
                            'match_id' => $matchId,
                            'player_id' => $input['player_id'][$key],
                            'team_id' => $input['team_id'][$key],
                            'minute' => $input['minute'][$key],
                            'name' => $input['video_name'][$key],
                            'preview_image' => $input['video_preview_image'][$key],
                            'is_top_goal' => $input['is_top_goal'][$key],
                        ];

                        $videoId = DB::table("videos")->insertGetId($insertVideosData);

                        foreach (explode(',', $input['categories'][$key]) as $category){
                            DB::table("videos__categories")->insert([
                                'video_id' => $videoId,
                                'category_id' => $category
                            ]);
                        }
                    }
                }

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $match = DB::table("matches")
            ->leftJoin("stadiums", "stadiums.id", "=", "matches.stadium_id")
            ->leftJoin("cities", "cities.id", "=", "stadiums.city_id")
            ->leftJoin("seasons", "seasons.id", "=", "matches.season_id")
            ->leftJoin("seasons__elements", "seasons__elements.id", "=", "matches.seasons__element_id")
            ->leftJoin("seasons__stages__groups", "seasons__stages__groups.id", "=", "matches.seasons__stages__group_id")
            ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
            ->where("matches.id", $id)
            ->select("matches.*", "seasons.tournament_id", "cities.country_id", "seasons__elements.element_id", "stages__groups.group_id")
            ->first();

        if($match){

            $match->friendly_match = $match->season_id ? 0 : 1;

            $stage = DB::table("stages")
                ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "stages.id")
                ->leftJoin("elements", "elements.stage_id", "=", "stages.id")
                ->leftJoin("seasons__elements", "seasons__elements.element_id", "=", "elements.id")
                ->where("seasons__elements.id", $match->seasons__element_id)
                ->select("stages.*", "stage_translations.*")
                ->first();

            $match->stage = $stage;


            $videos = DB::table("videos")
                ->where("match_id", $id)
                ->get();

            foreach ($videos as $video){
                $videoCategories = DB::table("videos__categories")
                    ->where("video_id", $video->id)
                    ->select("category_id")
                    ->get();

                $videoCategoriesArray = [];
                foreach ($videoCategories as $videoCategory) $videoCategoriesArray[] = $videoCategory->category_id;

                $video->categories = $videoCategoriesArray;
            }

            $match->videos = $videos;


            return response()->json([
                'code' => 0,
                'match' => $match
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $match = DB::table("matches")
            ->where("id", $id)
            ->first();

        if($match){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['stadium_id'] = "required|exists:stadiums,id";
                if(!isset($input['friendly_match'])){
                    $rules['season_id'] = "required|exists:seasons,id";
                    $rules['stage_id'] = "required|exists:stages,id";
                    $rules['element_id'] = "required|exists:elements,id";

                    if(isset($input['has_groups'])){
                        $rules['group_id'] = "required|exists:groups,id";
                    }
                }
                $rules['start_time'] = "required";
                $rules['home_team_id'] = "required|exists:teams,id";
                $rules['guest_team_id'] = "required|exists:teams,id";
                $rules['home_team_goals_count'] = "required";
                $rules['guest_team_goals_count'] = "required";
                $rules['status'] = "required";
                $rules['is_active'] = "";
                $rules['preview_image'] = "mimes:jpeg,png,jpg,bmp,gif,svg";

                if(isset($input['team_id']) && isset($input['video_id'])){
                    foreach ($input['team_id'] as $key => $teamId)
                        if($input['video_id'][$key]){
                            $rules["video_id.$key"] = "required|exists:videos,id";
                        }
                        else{
                            $rules["file.$key"] = "required";
                            $rules["video_preview_image.$key"] = "required";
                        }
                }
                $rules['categories.*'] = "required";
                $rules['preview_image.*'] = "mimes:jpeg,png,jpg,bmp,gif,svg";
                $rules['categories.*'] = "required";
                $rules['team_id.*'] = "required|exists:teams,id";
                $rules['player_id.*'] = "required|exists:players,id";
                $rules['minute.*'] = "required";
                $rules['is_top_goal.*'] = "required";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    $updateData = [
                        'stadium_id' => $input['stadium_id'],
                        'start_time' => $input['start_time'],
                        'home_team_id' => $input['home_team_id'],
                        'guest_team_id' => $input['guest_team_id'],
                        'home_team_goals_count' => $input['home_team_goals_count'],
                        'guest_team_goals_count' => $input['guest_team_goals_count'],
                        'preview_image' => null,
                        'status' => $input['status'],
                        'is_active' => isset($input['is_active']) ? 1 : 0
                    ];

                    if(isset($input['preview_image'])){
                        //preview start
                        $file = $input['preview_image'];
                        $input['preview_image'] = date("YmdHis") . $file->getClientOriginalName();

                        $invImage = InterventionImage::make($file);

                        $invImage->fit(368, 256)->save(public_path() . '/assets/site/assets/images/'.$input['preview_image']);

                        $updateData['preview_image'] = $input['preview_image'];

                        File::delete(public_path() . '/assets/site/assets/images/' . $match->preview_image);
                        //preview end
                    }

                    if(!isset($input['friendly_match'])){

                        $updateData['season_id'] = $input['season_id'];

                        $seasonsElementId = DB::table("seasons__elements")
                            ->where([
                                'season_id' => $input['season_id'],
                                'element_id' => $input['element_id']
                            ])
                            ->select("seasons__elements.id")
                            ->first();

                        $updateData['seasons__element_id'] = $seasonsElementId ? $seasonsElementId->id : null;

                        if(isset($input['has_groups'])){
                            $seasonsStagesGroupId = DB::table("seasons__stages__groups")
                                ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                                ->where([
                                    'seasons__stages__groups.season_id' => $input['season_id'],
                                    'stages__groups.stage_id' => $input['stage_id'],
                                    'stages__groups.group_id' => $input['group_id']
                                ])
                                ->select("seasons__stages__groups.id")
                                ->first();

                            $updateData['seasons__stages__group_id'] = $seasonsStagesGroupId ? $seasonsStagesGroupId->id : null;
                        }
                    }
                    else{
                        $updateData['season_id'] = null;
                        $updateData['seasons__element_id'] = null;
                        $updateData['seasons__stages__group_id'] = null;
                    }

                    DB::table("matches")->where("id", $id)->update($updateData);



                    //videos deleting start
                    $allVideos = DB::table("videos")->where("match_id", $id)->select("id")->get();
                    $allVideoIds = [];
                    foreach ($allVideos as $allVideo) $allVideoIds[] = $allVideo->id;

                    $realVideoIds = [];
                    if(isset($input['video_id']) && is_array($input['video_id'])){
                        $realVideoIds = array_filter($input['video_id'], function($videoId){
                            return $videoId != '0';
                        });
                    }

                    DB::table("videos")->whereIn("id", array_diff($allVideoIds, $realVideoIds))->delete();
                    DB::table("videos__categories")->whereIn("video_id", array_diff($allVideoIds, $realVideoIds))->delete();

                    //videos deleting end

                    if(isset($input['file'])){

                        foreach ($input['team_id'] as $key => $value){

                            $data = [
                                'player_id' => $input['player_id'][$key],
                                'team_id' => $input['team_id'][$key],
                                'minute' => $input['minute'][$key],
                                'is_top_goal' => $input['is_top_goal'][$key],
                            ];

                            if($input['file'][$key]){

                                $filename = Storage::disk('public')->put('assets/site/assets/videos/', $input['file'][$key]);

                                $filenameExploded = explode('/', $filename);

                                $filename = $filenameExploded[count($filenameExploded) - 1];

                                $data['name'] = $filename;

                                if($input['video_id'][$key]){
                                    $video = DB::table("videos")->where("id", $input['video_id'][$key])->first();

                                    File::delete(public_path() . '/assets/site/assets/videos/' . $video->name);
                                }
                            }

                            if($input['video_preview_image'][$key]){
                                //preview start
                                $file = $input['video_preview_image'][$key];
                                $input['video_preview_image'][$key] = date("YmdHis") . $file->getClientOriginalName();

                                $invImage = InterventionImage::make($file);

                                $invImage->fit(368, 256)->save(public_path() . '/assets/site/assets/images/'.$input['video_preview_image'][$key]);

                                $data['preview_image'] = $input['video_preview_image'][$key];

                                if($input['video_id'][$key]){
                                    $video = DB::table("videos")->where("id", $input['video_id'][$key])->first();

                                    File::delete(public_path() . '/assets/site/assets/images/' . $video->preview_image);
                                }

                                //preview end
                            }

                            $videoId = null;
                            if($input['video_id'][$key]){
                                DB::table("videos")->where("id", $input['video_id'][$key])->update($data);
                            }
                            else{
                                $data['match_id'] = $id;
                                $videoId = DB::table("videos")->insertGetId($data);
                            }


                            if($input['video_id'][$key]) DB::table("videos__categories")->where("video_id", $input['video_id'][$key])->delete();

                            foreach (explode(',', $input['categories'][$key]) as $category){

                                DB::table("videos__categories")->insert([
                                    'video_id' => $videoId ? $videoId : $input['video_id'][$key],
                                    'category_id' => $category
                                ]);
                            }
                        }
                    }

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $match = DB::table("matches")
            ->where([
                "id" => $id
            ])
            ->first();

        if($match){

            DB::table("matches")->where("id", $id)->delete();

            File::delete(public_path() . '/assets/site/assets/images/' . $match->preview_image);

            $videos = DB::table("videos")
                ->where("match_id", $id)
                ->get();

            if($videos->count() > 0){

                $videosIds = [];
                foreach ($videos as $video){
                    $videosIds[] = $video->id;
                }

                DB::table("videos")->where("match_id", $id)->delete();

                DB::table("videos__categories")->whereIn("video_id", $videosIds)->delete();
            }

            return response()->json([
                'code' => 0,
                'message' => 'Element silindi'
            ]);
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
