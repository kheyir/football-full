<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class TournamentsCategoriesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $tournamentsCategories = DB::table("tournaments_categories")
            ->leftJoin("tournaments_category_translations", "tournaments_category_translations.tournaments_category_id", "=", "tournaments_categories.id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $tournamentsCategories = $tournamentsCategories->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $tournamentsCategories = $tournamentsCategories
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'tournamentsCategories' => $tournamentsCategories
        ]);
    }

    public function getTournamentsCategories(){

        $tournamentsCategories = DB::table("tournaments_categories")
            ->leftJoin("tournaments_category_translations", "tournaments_category_translations.tournaments_category_id", "=", "tournaments_categories.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'tournamentsCategories' => $tournamentsCategories
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $tournamentsCategoryId = DB::table("tournaments_categories")->insertGetId([]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'tournaments_category_id' => $tournamentsCategoryId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("tournaments_category_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $tournamentsCategory = DB::table("tournaments_categories")
            ->where("id", $id)
            ->first();

        if($tournamentsCategory){

            $tournamentsCategoryTranslations = DB::table("tournaments_category_translations")
                ->where("tournaments_category_id", $id)
                ->get();

            $tournamentsCategoryTranslations = $tournamentsCategoryTranslations->groupBy("locale");

            $tournamentsCategory->translations = $tournamentsCategoryTranslations;

            return response()->json([
                'code' => 0,
                'tournamentsCategory' => $tournamentsCategory
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $tournamentsCategory = DB::table("tournaments_categories")
            ->where("id", $id)
            ->first();

        if($tournamentsCategory){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("tournaments_category_translations")
                        ->where('tournaments_category_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'tournaments_category_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("tournaments_category_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $tournamentsCategory = DB::table("tournaments_categories")
            ->where([
                "id" => $id
            ])
            ->first();

        if($tournamentsCategory){

            $team = DB::table("tournaments")
                ->where([
                    "tournaments_category_id" => $id
                ])
                ->first();

            if(!$team){
                DB::table("tournaments_categories")->where("id", $id)->delete();
                DB::table("tournaments_category_translations")->where("tournaments_category_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
