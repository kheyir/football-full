<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Lang;
use Hash;
use DB;
use Session;
use Mail;
use Config;

use JWTFactory;
use JWTAuth;

class CoachesController extends AdminController {

    function __construct(){

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $coaches = DB::table("coaches")
            ->leftJoin("coach_translations", "coach_translations.coach_id", "=", "coaches.id");

        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $coaches = $coaches->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $coaches = $coaches
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'coaches' => $coaches
        ]);
    }

    public function getCoaches(){

        $coaches = DB::table("coaches")
            ->leftJoin("coach_translations", "coach_translations.coach_id", "=", "coaches.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'coaches' => $coaches
        ]);
    }

    public function add(){

        $input = Request::except('_token');

        if(isset($input) && !empty($input)) {

            $rules['name.*'] = "required|max:200";

            $validator = Validator::make($input, $rules);

            if (!$validator->fails()) {

                $coachId = DB::table("coaches")->insertGetId([]);

                $languages = DB::table("languages")->get();

                $insertData = [];
                foreach ($languages as $language){
                    $insertData[] = [
                        'coach_id' => $coachId,
                        'name' => $input['name'][$language->locale],
                        'locale' => $language->locale
                    ];
                }

                DB::table("coach_translations")->insert($insertData);

                return response()->json([
                    'code' => 0
                ]);
            }
            else{

                $message = '';
                foreach ($validator->failed() as $field => $failedRules){
                    foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                        $message .= $field .' - ' . $failedRuleName . ';';
                    }
                }

                return response()->json([
                    'code' => 1,
                    'message' => $message
                ]);
            }
        }

    }

    public function getById(){

        $id = Request::segment(4);

        $coach = DB::table("coaches")
            ->where("id", $id)
            ->first();

        if($coach){

            $coachTranslations = DB::table("coach_translations")
                ->where("coach_id", $id)
                ->get();

            $coachTranslations = $coachTranslations->groupBy("locale");

            $coach->translations = $coachTranslations;

            return response()->json([
                'code' => 0,
                'coach' => $coach
            ]);
        }
        else{

            return response()->json([
                'code' => 404
            ]);
        }
    }

    public function edit(){

        $id = Request::segment(5);

        $coach = DB::table("coaches")
            ->where("id", $id)
            ->first();

        if($coach){

            $input = Request::except('_token');

            if(isset($input) && !empty($input)) {

                $rules['name.*'] = "required|max:200";

                $validator = Validator::make($input, $rules);

                if (!$validator->fails()) {

                    DB::table("coach_translations")
                        ->where('coach_id', $id)
                        ->delete();

                    $languages = DB::table("languages")->get();

                    $insertData = [];
                    foreach ($languages as $language){
                        $insertData[] = [
                            'coach_id' => $id,
                            'name' => $input['name'][$language->locale],
                            'locale' => $language->locale
                        ];
                    }

                    DB::table("coach_translations")->insert($insertData);

                    return response()->json([
                        'code' => 0
                    ]);
                }
                else{

                    $message = '';
                    foreach ($validator->failed() as $field => $failedRules){
                        foreach ($failedRules as $failedRuleName => $failedRuleConfigs){
                            $message .= $field .' - ' . $failedRuleName . ';';
                        }
                    }

                    return response()->json([
                        'code' => 1,
                        'message' => $message
                    ]);
                }
            }
        }

    }

    public function delete(){

        $id = Request::segment(5) != null ? Request::segment(5) : 1;

        $coach = DB::table("coaches")
            ->where([
                "id" => $id
            ])
            ->first();

        if($coach){

            $team = DB::table("teams")
                ->where([
                    "coach_id" => $id
                ])
                ->first();

            if(!$team){
                DB::table("coaches")->where("id", $id)->delete();
                DB::table("coach_translations")->where("coach_id", $id)->delete();

                return response()->json([
                    'code' => 0,
                    'message' => 'Element silindi'
                ]);
            }
            else{
                return response()->json([
                    'code' => 1,
                    'message' => 'Element silinmədi: ona bağlı digər cədvəllər var'
                ]);
            }
        }
        else{
            return response()->json([
                'code' => 2,
                'message' => 'Element mövcud deyil'
            ]);
        }
    }

}
