<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class GroupsController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getSidebarGroupsBySeason(){

        $seasonId = Request::segment(5);

        $season = DB::table("seasons")
            ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "tournament_translations.locale" => Lang::getLocale()
            ]);

        $input = Request::except('_token');

        if(isset($input['forHome']) && !empty($input['forHome'])){
            $season->where('seasons.is_in_home', 1);
        }
        else{
            $season->where('seasons.id', $seasonId);
        }

        $season = $season
            ->select("seasons.id", "tournaments.id as tournament_id", "tournaments.is_league", "tournament_translations.name as tournament_name")
            ->first();

        $seasonId = $season->id;

        if($season){

            if($season->is_league){

                $stage = DB::table("stages")
                    ->leftJoin("elements", "elements.stage_id", "=", "stages.id")
                    ->leftJoin("seasons__elements", "seasons__elements.element_id", "=", "elements.id")
                    ->where([
                        "seasons__elements.season_id" => $seasonId
                    ])
                    ->select("stages.id", "stages.has_groups")
                    ->first();

                if($stage){
                    $groups = DB::table("seasons__teams")
                        ->leftJoin("seasons", "seasons.id", "=", "seasons__teams.season_id")
                        ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
                        ->leftJoin("teams", "teams.id", "=", "seasons__teams.team_id")
                        ->leftJoin("team_translations", "team_translations.team_id", "=", "seasons__teams.team_id")
                        ->leftJoin("seasons__elements", "seasons__elements.season_id", "=", "seasons__teams.season_id")
                        ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
                        ->leftJoin("stages", "stages.id", "=", "elements.stage_id")
                        ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "elements.stage_id")
                        ->where([
                            'team_translations.locale' => Lang::getLocale(),
                            'seasons__teams.season_id' => $seasonId,
                            'stage_translations.stage_id' => $stage->id
                        ])
                        ->orderBy("stages.priority", "ASC")
                        ->orderBy("elements.priority", "ASC")
                        ->groupBy("stages.id", "teams.id")
                        ->select("team_translations.team_id", "team_translations.name as team_name", "teams.logo",
                            "stage_translations.stage_id", "stage_translations.name as group_name",
                            "tournaments.league_victory_points", "tournaments.league_draw_points")
                        ->get();

                    $groups = $groups->groupBy("group_name")->toArray();

                    $responseGroups = [];
                    foreach ($groups as $groupName => $group){
                        $matches = DB::table("matches")
                            ->where([
                                'matches.season_id' => $seasonId,
                                'matches.is_active' => 1,
                                'matches.status' => 1
                            ])
                            ->get();

                        foreach ($group as $key => $teamAndGroup){
                            $groups[$groupName][$key]->gamesCount = 0;
                            $groups[$groupName][$key]->victoryGamesCount = 0;
                            $groups[$groupName][$key]->drawGamesCount = 0;
                            $groups[$groupName][$key]->defeatGamesCount = 0;
                            $groups[$groupName][$key]->goalsScoredCount = 0;
                            $groups[$groupName][$key]->goalsConcededCount = 0;
                            $groups[$groupName][$key]->goalsDiffCount = 0;
                            $groups[$groupName][$key]->pointsCount = 0;

                            $groups[$groupName][$key]->goalsScoredCountAsGuest = 0;

                            $groups[$groupName][$key]->victoryCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount = 0;
                        }

                        foreach ($matches as $match){

                            $homeIndex = null;
                            $guestIndex = null;
                            foreach ($group as $key => $teamAndGroup){
                                if($match->home_team_id == $teamAndGroup->team_id) $homeIndex = $key;
                                if($match->guest_team_id == $teamAndGroup->team_id) $guestIndex = $key;
                            }

                            //games count
                            $groups[$groupName][$homeIndex]->gamesCount++;
                            $groups[$groupName][$guestIndex]->gamesCount++;

                            //games results
                            if($match->home_team_goals_count > $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->victoryGamesCount++;
                                $groups[$groupName][$guestIndex]->defeatGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_victory_points;
                            }
                            elseif($match->home_team_goals_count < $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->defeatGamesCount++;
                                $groups[$groupName][$guestIndex]->victoryGamesCount++;

                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_victory_points;
                            }
                            else{
                                $groups[$groupName][$homeIndex]->drawGamesCount++;
                                $groups[$groupName][$guestIndex]->drawGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_draw_points;
                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_draw_points;
                            }

                            //goals
                            $groups[$groupName][$homeIndex]->goalsScoredCount += $match->home_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsConcededCount += $match->guest_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsDiffCount = $groups[$groupName][$homeIndex]->goalsScoredCount - $groups[$groupName][$homeIndex]->goalsConcededCount;
                            
                            $groups[$groupName][$guestIndex]->goalsScoredCount += $match->guest_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsConcededCount += $match->home_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsDiffCount = $groups[$groupName][$guestIndex]->goalsScoredCount - $groups[$groupName][$guestIndex]->goalsConcededCount;

                            $groups[$groupName][$guestIndex]->goalsScoredCountAsGuest += $match->guest_team_goals_count;

                        }

                        //group by pointsCount
                        $grouppedByPointsCount = [];
                        foreach ($groups[$groupName] as $key => $item) {
                            if(!isset($grouppedByPointsCount[$item->pointsCount])) $grouppedByPointsCount[$item->pointsCount] = [];

                            $grouppedByPointsCount[$item->pointsCount][] = $item;
                        }

                        foreach ($grouppedByPointsCount as $pointCount => $teams) {
                            for($i = 0; $i < count($teams); $i++){
                                for($j = $i + 1; $j < count($teams); $j++){
                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($teams, $i, $j) {
                                        return ($match->home_team_id == $teams[$i]->team_id && $match->guest_team_id == $teams[$j]->team_id) ||
                                                ($match->guest_team_id == $teams[$i]->team_id && $match->home_team_id == $teams[$j]->team_id);
                                    });

                                    foreach($suitableMatches as $key => $suitableMatch){
                                        //goalsScoredCountWithSamePointsCount, goalsConcededCountWithSamePointsCount, goalsDiffCountWithSamePointsCount, goalsScoredCountAsGuestWithSamePointsCount
                                        foreach($groups[$groupName] as $key => $team){
                                            if($suitableMatch->home_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;
                                            }
                                            elseif($suitableMatch->guest_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;

                                                $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                            }
                                        }

                                        //victoryCountWithSamePointsCount
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count){
                                            foreach($groups[$groupName] as $key => $team) if($suitableMatch->home_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                        }
                                        else{
                                            if($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count){
                                                foreach($groups[$groupName] as $key => $team) if($suitableMatch->guest_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //

                        if($season->tournament_id == 1){

                            array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                                $groups[$groupName]);

                            for($i=0; $i < count($groups[$groupName]) - 1; $i++){
                                if($groups[$groupName][$i]->pointsCount == $groups[$groupName][$i+1]->pointsCount){

                                    $firstTeamVictoryCount = 0;
                                    $secondTeamVictoryCount = 0;

                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($groups, $groupName, $i) {
                                        return ($match->home_team_id == $groups[$groupName][$i]->team_id && $match->guest_team_id == $groups[$groupName][$i+1]->team_id);
                                    });
                                    foreach($suitableMatches as $key => $suitableMatch){
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count) $firstTeamVictoryCount++;
                                        elseif($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count) $secondTeamVictoryCount++;
                                    }
                                    
                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($groups, $groupName, $i) {
                                        return ($match->guest_team_id == $groups[$groupName][$i]->team_id && $match->home_team_id == $groups[$groupName][$i+1]->team_id);
                                    });
                                    foreach($suitableMatches as $key => $suitableMatch){
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count) $secondTeamVictoryCount++;
                                        elseif($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count) $firstTeamVictoryCount++;
                                    }

                                    if($secondTeamVictoryCount > $firstTeamVictoryCount){
                                        $temp = $groups[$groupName][$i];
                                        $groups[$groupName][$i] = $groups[$groupName][$i+1];
                                        $groups[$groupName][$i+1] = $temp;
                                    }
                                }
                            }
                        }
                        else{
                            array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                                array_column($groups[$groupName], 'victoryCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCountAsGuestWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCount'), SORT_DESC,
                                array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCountAsGuest'), SORT_DESC,
                                $groups[$groupName]);
                        }

                        $responseGroups[] = [
                            "group_name" => $groupName,
                            "teams" => $groups[$groupName]
                        ];
                    }

                    $data = [
                        'code' => 0,
                        'groups' => $responseGroups,
                        'tournament_name' => $season->tournament_name
                    ];
                }
                else{
                    $data = [
                        'code' => 0,
                        'groups' => []
                    ];
                }

            }
            else{

                $stage = DB::table("stages")
                    ->leftJoin("elements", "elements.stage_id", "=", "stages.id")
                    ->leftJoin("seasons__elements", "seasons__elements.element_id", "=", "elements.id")
                    ->where([
                        "stages.has_groups" => 1,
                        "seasons__elements.season_id" => $seasonId
                    ])
                    ->select("stages.id", "stages.has_groups")
                    ->first();

                if($stage && $stage->has_groups == 1){

                    $groups = DB::table("seasons__stages__groups__teams")
                        ->leftJoin("seasons__stages__groups", "seasons__stages__groups.id", "=", "seasons__stages__groups__teams.seasons__stages__group_id")
                        ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                        ->leftJoin("stages", "stages.id", "=", "stages__groups.stage_id")
                        ->leftJoin("groups", "groups.id", "=", "stages__groups.group_id")
                        ->leftJoin("group_translations", "group_translations.group_id", "=", "stages__groups.group_id")
                        ->leftJoin("teams", "teams.id", "=", "seasons__stages__groups__teams.team_id")
                        ->leftJoin("team_translations", "team_translations.team_id", "=", "seasons__stages__groups__teams.team_id")
                        ->where([
                            'group_translations.locale' => Lang::getLocale(),
                            'team_translations.locale' => Lang::getLocale(),
                            'seasons__stages__groups.season_id' => $seasonId,
                            'stages__groups.stage_id' => $stage->id,
                        ])
                        ->orderBy("stages.priority", "ASC")
                        ->orderBy("groups.priority", "ASC")
                        ->select("team_translations.team_id", "team_translations.name as team_name", "teams.logo",
                            "group_translations.group_id", "group_translations.name as group_name",
                            "seasons__stages__groups.id as seasons__stages__group_id",
                            "seasons__stages__groups.draw_points", "seasons__stages__groups.victory_points")
                        ->get();

                    $groups = $groups->groupBy("group_name")->toArray();

                    $responseGroups = [];
                    foreach ($groups as $groupName => $group){
                        $matches = DB::table("matches")
                            ->where([
                                'matches.season_id' => $seasonId,
                                'matches.seasons__stages__group_id' => $group[0]->seasons__stages__group_id,
                                'matches.is_active' => 1,
                                'matches.status' => 1
                            ])
                            ->get();

                        foreach ($group as $key => $teamAndGroup){
                            $groups[$groupName][$key]->gamesCount = 0;
                            $groups[$groupName][$key]->victoryGamesCount = 0;
                            $groups[$groupName][$key]->drawGamesCount = 0;
                            $groups[$groupName][$key]->defeatGamesCount = 0;
                            $groups[$groupName][$key]->goalsScoredCount = 0;
                            $groups[$groupName][$key]->goalsConcededCount = 0;
                            $groups[$groupName][$key]->goalsDiffCount = 0;
                            $groups[$groupName][$key]->pointsCount = 0;

                            $groups[$groupName][$key]->goalsScoredCountAsGuest = 0;

                            $groups[$groupName][$key]->victoryCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount = 0;
                        }

                        foreach ($matches as $match){

                            $homeIndex = null;
                            $guestIndex = null;
                            foreach ($group as $key => $teamAndGroup){
                                if($match->home_team_id == $teamAndGroup->team_id) $homeIndex = $key;
                                if($match->guest_team_id == $teamAndGroup->team_id) $guestIndex = $key;
                            }

                            //games count
                            $groups[$groupName][$homeIndex]->gamesCount++;
                            $groups[$groupName][$guestIndex]->gamesCount++;

                            //games results
                            if($match->home_team_goals_count > $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->victoryGamesCount++;
                                $groups[$groupName][$guestIndex]->defeatGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->victory_points;
                            }
                            elseif($match->home_team_goals_count < $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->defeatGamesCount++;
                                $groups[$groupName][$guestIndex]->victoryGamesCount++;

                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->victory_points;
                            }
                            else{
                                $groups[$groupName][$homeIndex]->drawGamesCount++;
                                $groups[$groupName][$guestIndex]->drawGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->draw_points;
                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->draw_points;
                            }

                            //goals
                            $groups[$groupName][$homeIndex]->goalsScoredCount += $match->home_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsConcededCount += $match->guest_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsDiffCount = $groups[$groupName][$homeIndex]->goalsScoredCount - $groups[$groupName][$homeIndex]->goalsConcededCount;

                            $groups[$groupName][$guestIndex]->goalsScoredCount += $match->guest_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsConcededCount += $match->home_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsDiffCount = $groups[$groupName][$guestIndex]->goalsScoredCount - $groups[$groupName][$guestIndex]->goalsConcededCount;

                            $groups[$groupName][$guestIndex]->goalsScoredCountAsGuest += $match->guest_team_goals_count;

                        }

                        //group by pointsCount
                        $grouppedByPointsCount = [];
                        foreach ($groups[$groupName] as $key => $item) {
                            if(!isset($grouppedByPointsCount[$item->pointsCount])) $grouppedByPointsCount[$item->pointsCount] = [];

                            $grouppedByPointsCount[$item->pointsCount][] = $item;
                        }

                        foreach ($grouppedByPointsCount as $pointCount => $teams) {
                            for($i = 0; $i < count($teams); $i++){
                                for($j = $i + 1; $j < count($teams); $j++){
                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($teams, $i, $j) {
                                        return ($match->home_team_id == $teams[$i]->team_id && $match->guest_team_id == $teams[$j]->team_id) ||
                                                ($match->guest_team_id == $teams[$i]->team_id && $match->home_team_id == $teams[$j]->team_id);
                                    });

                                    foreach($suitableMatches as $key => $suitableMatch){
                                        //goalsScoredCountWithSamePointsCount, goalsConcededCountWithSamePointsCount, goalsDiffCountWithSamePointsCount, goalsScoredCountAsGuestWithSamePointsCount
                                        foreach($groups[$groupName] as $key => $team){
                                            if($suitableMatch->home_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;
                                            }
                                            elseif($suitableMatch->guest_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;

                                                $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                            }
                                        }

                                        //victoryCountWithSamePointsCount
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count){
                                            foreach($groups[$groupName] as $key => $team) if($suitableMatch->home_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                        }
                                        else{
                                            if($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count){
                                                foreach($groups[$groupName] as $key => $team) if($suitableMatch->guest_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //



                        array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                            array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCountAsGuestWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                            array_column($groups[$groupName], 'victoryCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                            $groups[$groupName]);

                        $responseGroups[] = [
                            "group_name" => $groupName,
                            "teams" => $groups[$groupName]
                        ];
                    }

                    $data = [
                        'code' => 0,
                        'groups' => $responseGroups,
                        'tournament_name' => $season->tournament_name
                    ];
                }
                else{
                    $data = [
                        'code' => 0,
                        'groups' => []
                    ];
                }
            }
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getSidebarTournamentsForHome(){

        $seasons = DB::table("seasons")
            ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "tournament_translations.locale" => Lang::getLocale(),
                "seasons.is_in_home" => 1,
                "tournaments.is_league" => 1
            ]);

        $seasons = $seasons
            ->select("seasons.id", "tournaments.id as tournament_id", "tournaments.is_league", "tournament_translations.name as tournament_name")
            ->get();

        $data = [
            'code' => 0,
            'tournaments' => []
        ];

        if($seasons->count() > 0){

            foreach ($seasons as $season){

                $stage = DB::table("stages")
                    ->leftJoin("elements", "elements.stage_id", "=", "stages.id")
                    ->leftJoin("seasons__elements", "seasons__elements.element_id", "=", "elements.id")
                    ->where([
                        "seasons__elements.season_id" => $season->id
                    ])
                    ->select("stages.id", "stages.has_groups")
                    ->first();

                if($stage){
                    $groups = DB::table("seasons__teams")
                        ->leftJoin("seasons", "seasons.id", "=", "seasons__teams.season_id")
                        ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
                        ->leftJoin("teams", "teams.id", "=", "seasons__teams.team_id")
                        ->leftJoin("team_translations", "team_translations.team_id", "=", "seasons__teams.team_id")
                        ->leftJoin("seasons__elements", "seasons__elements.season_id", "=", "seasons__teams.season_id")
                        ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
                        ->leftJoin("stages", "stages.id", "=", "elements.stage_id")
                        ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "elements.stage_id")
                        ->where([
                            'team_translations.locale' => Lang::getLocale(),
                            'seasons__teams.season_id' => $season->id,
                            'stage_translations.stage_id' => $stage->id
                        ])
                        ->orderBy("stages.priority", "ASC")
                        ->orderBy("elements.priority", "ASC")
                        ->groupBy("stages.id", "teams.id")
                        ->select("team_translations.team_id", "team_translations.name as team_name", "teams.logo",
                            "stage_translations.stage_id", "stage_translations.name as group_name",
                            "tournaments.league_victory_points", "tournaments.league_draw_points")
                        ->get();

                    $groups = $groups->groupBy("group_name")->toArray();

                    foreach ($groups as $groupName => $group){
                        $matches = DB::table("matches")
                            ->where([
                                'matches.season_id' => $season->id,
                                'matches.is_active' => 1,
                                'matches.status' => 1
                            ])
                            ->get();

                        foreach ($group as $key => $teamAndGroup){
                            $groups[$groupName][$key]->gamesCount = 0;
                            $groups[$groupName][$key]->victoryGamesCount = 0;
                            $groups[$groupName][$key]->drawGamesCount = 0;
                            $groups[$groupName][$key]->defeatGamesCount = 0;
                            $groups[$groupName][$key]->goalsScoredCount = 0;
                            $groups[$groupName][$key]->goalsConcededCount = 0;
                            $groups[$groupName][$key]->goalsDiffCount = 0;
                            $groups[$groupName][$key]->pointsCount = 0;

                            $groups[$groupName][$key]->goalsScoredCountAsGuest = 0;

                            $groups[$groupName][$key]->victoryCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount = 0;
                        }

                        foreach ($matches as $match){

                            $homeIndex = null;
                            $guestIndex = null;
                            foreach ($group as $key => $teamAndGroup){
                                if($match->home_team_id == $teamAndGroup->team_id) $homeIndex = $key;
                                if($match->guest_team_id == $teamAndGroup->team_id) $guestIndex = $key;
                            }

                            //games count
                            $groups[$groupName][$homeIndex]->gamesCount++;
                            $groups[$groupName][$guestIndex]->gamesCount++;

                            //games results
                            if($match->home_team_goals_count > $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->victoryGamesCount++;
                                $groups[$groupName][$guestIndex]->defeatGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_victory_points;
                            }
                            elseif($match->home_team_goals_count < $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->defeatGamesCount++;
                                $groups[$groupName][$guestIndex]->victoryGamesCount++;

                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_victory_points;
                            }
                            else{
                                $groups[$groupName][$homeIndex]->drawGamesCount++;
                                $groups[$groupName][$guestIndex]->drawGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_draw_points;
                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_draw_points;
                            }

                            //goals
                            $groups[$groupName][$homeIndex]->goalsScoredCount += $match->home_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsConcededCount += $match->guest_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsDiffCount = $groups[$groupName][$homeIndex]->goalsScoredCount - $groups[$groupName][$homeIndex]->goalsConcededCount;

                            $groups[$groupName][$guestIndex]->goalsScoredCount += $match->guest_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsConcededCount += $match->home_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsDiffCount = $groups[$groupName][$guestIndex]->goalsScoredCount - $groups[$groupName][$guestIndex]->goalsConcededCount;

                            $groups[$groupName][$guestIndex]->goalsScoredCountAsGuest += $match->guest_team_goals_count;

                        }

                        //group by pointsCount
                        $grouppedByPointsCount = [];
                        foreach ($groups[$groupName] as $key => $item) {
                            if(!isset($grouppedByPointsCount[$item->pointsCount])) $grouppedByPointsCount[$item->pointsCount] = [];

                            $grouppedByPointsCount[$item->pointsCount][] = $item;
                        }

                        foreach ($grouppedByPointsCount as $pointCount => $teams) {
                            for($i = 0; $i < count($teams); $i++){
                                for($j = $i + 1; $j < count($teams); $j++){
                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($teams, $i, $j) {
                                        return ($match->home_team_id == $teams[$i]->team_id && $match->guest_team_id == $teams[$j]->team_id) ||
                                                ($match->guest_team_id == $teams[$i]->team_id && $match->home_team_id == $teams[$j]->team_id);
                                    });

                                    foreach($suitableMatches as $key => $suitableMatch){
                                        //goalsScoredCountWithSamePointsCount, goalsConcededCountWithSamePointsCount, goalsDiffCountWithSamePointsCount, goalsScoredCountAsGuestWithSamePointsCount
                                        foreach($groups[$groupName] as $key => $team){
                                            if($suitableMatch->home_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;
                                            }
                                            elseif($suitableMatch->guest_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;

                                                $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                            }
                                        }

                                        //victoryCountWithSamePointsCount
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count){
                                            foreach($groups[$groupName] as $key => $team) if($suitableMatch->home_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                        }
                                        else{
                                            if($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count){
                                                foreach($groups[$groupName] as $key => $team) if($suitableMatch->guest_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //

                        if($season->tournament_id == 1){

                            array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                                $groups[$groupName]);

                            for($i=0; $i < count($groups[$groupName]) - 1; $i++){
                                if($groups[$groupName][$i]->pointsCount == $groups[$groupName][$i+1]->pointsCount){

                                    $firstTeamVictoryCount = 0;
                                    $secondTeamVictoryCount = 0;

                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($groups, $groupName, $i) {
                                        return ($match->home_team_id == $groups[$groupName][$i]->team_id && $match->guest_team_id == $groups[$groupName][$i+1]->team_id);
                                    });
                                    foreach($suitableMatches as $key => $suitableMatch){
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count) $firstTeamVictoryCount++;
                                        elseif($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count) $secondTeamVictoryCount++;
                                    }
                                    
                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($groups, $groupName, $i) {
                                        return ($match->guest_team_id == $groups[$groupName][$i]->team_id && $match->home_team_id == $groups[$groupName][$i+1]->team_id);
                                    });
                                    foreach($suitableMatches as $key => $suitableMatch){
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count) $secondTeamVictoryCount++;
                                        elseif($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count) $firstTeamVictoryCount++;
                                    }

                                    if($secondTeamVictoryCount > $firstTeamVictoryCount){
                                        $temp = $groups[$groupName][$i];
                                        $groups[$groupName][$i] = $groups[$groupName][$i+1];
                                        $groups[$groupName][$i+1] = $temp;
                                    }
                                }
                            }
                        }
                        else{
                            array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                                array_column($groups[$groupName], 'victoryCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCountWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCountAsGuestWithSamePointsCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsDiffCount'), SORT_DESC,
                                array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                                array_column($groups[$groupName], 'goalsScoredCountAsGuest'), SORT_DESC,
                                $groups[$groupName]);
                        }

                        $responseGroups = [
                            'tournament_name' => $season->tournament_name,
                            "group_name" => $groupName,
                            "teams" => array_slice($groups[$groupName], 0, 10)
                        ];
                    }

                    $data['tournaments'][] = $responseGroups;
                }
                else{
//                    $data['tournaments'][] = [];
                }
            }

        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }
}
