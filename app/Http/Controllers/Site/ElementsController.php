<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class ElementsController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getElementsBySeason(){

        $id = Request::segment(5);

        $season = DB::table("seasons")
            ->where([
                "seasons.id" => $id
            ])
            ->first();

        if($season){
            $elements = DB::table("seasons__elements")
                ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
                ->leftJoin("element_translations", "element_translations.element_id", "=", "seasons__elements.element_id")
                ->leftJoin("stages", "stages.id", "=", "elements.stage_id")
                ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "elements.stage_id")


                ->leftJoin("stages__groups", "stages__groups.stage_id", "=", "elements.stage_id")

                ->leftJoin("groups", "groups.id", "=", "stages__groups.group_id")

                ->leftJoin("group_translations", function($join) use($id){
                    $join->on("group_translations.group_id", '=', "stages__groups.group_id")
                        ->where("group_translations.locale", Lang::getLocale());
                })

                ->leftJoin("seasons__stages__groups", function($join) use($id){
                    $join->on("seasons__stages__groups.stages__group_id", '=', "stages__groups.id")
                        ->where("seasons__stages__groups.season_id", $id);
                })
                ->where([
                    "seasons__elements.season_id" => $id,
                    "element_translations.locale" => Lang::getLocale(),
                    "stage_translations.locale" => Lang::getLocale()
                ])
                ->orderBy("stages.priority", "ASC")
                ->orderBy("elements.priority", "ASC")
                ->orderBy("groups.priority", "ASC")
                ->select("element_translations.element_id as element_id", "element_translations.name as element_name",
                    "stage_translations.stage_id", "stage_translations.name as stage_name", "stages.has_groups",
                    "group_translations.group_id as group_id", "group_translations.name as group_name",
                    "groups.priority")
                ->get();

            $data = [
                'code' => 0,
                'elements' => $elements
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }
}
