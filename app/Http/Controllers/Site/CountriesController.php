<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class CountriesController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getAllForHead(){

        $countries = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->leftJoin("countries", "countries.id", "=", "tournaments.country_id")
            ->leftJoin("country_translations", function($join){
                $join->on("country_translations.country_id", "=", "tournaments.country_id")
                    ->where("country_translations.locale", Lang::getLocale());
            })
            ->where([
                "countries.in_head" => 1,
                "tournament_translations.locale" => Lang::getLocale()
            ])
            ->select("tournament_translations.tournament_id", "tournaments.slug as tournament_slug", "country_translations.name as country_name")
            ->get();

        $data = [
            'code' => 0,
            'countries' => $countries
        ];

        return json_encode($data);
    }

    public function getAll(){

        $countries = DB::table("countries")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "countries.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->get();

        $data = [
            'code' => 0,
            'countries' => $countries
        ];

        return json_encode($data);
    }
}
