<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class SeasonsController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getAll(){

        $seasons = DB::table("seasons")
            ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "seasons.tournament_id")
            ->leftJoin("tournaments_category_translations", "tournaments_category_translations.tournaments_category_id", "=", "tournaments.tournaments_category_id")
            ->leftJoin("country_translations", function($join){
                $join->on("country_translations.country_id", "=", "tournaments.country_id")
                    ->where('country_translations.locale', Lang::getLocale());
            });

        $input = Request::except('_token');

        if(isset($input['tournament_id']) && !empty($input['tournament_id'])){
            $seasons = $seasons->where([
                'seasons.tournament_id' => $input['tournament_id']
            ]);
        }

        $seasons = $seasons
            ->where([
                "tournament_translations.locale" => Lang::getLocale(),
                "tournaments_category_translations.locale" => Lang::getLocale()
            ])
            ->groupBy('seasons.id')
            ->select("seasons.*",
                'tournaments.is_league',
                'tournament_translations.name as tournament_name',
                'tournaments_category_translations.name as tournaments_category_name',
                'country_translations.name as country_name')
            ->get();

        return response()->json([
            'code' => 0,
            'seasons' => $seasons
        ]);
    }
}
