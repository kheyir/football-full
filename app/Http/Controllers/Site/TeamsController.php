<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class TeamsController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getTeamsBySeason(){

        $id = Request::segment(5);

        $season = DB::table("seasons")
            ->where([
                "seasons.id" => $id
            ])
            ->first();

        if($season){
            $teams = DB::table("seasons__teams")
                ->leftJoin("teams", "teams.id", "=", "seasons__teams.team_id")
                ->leftJoin("team_translations", "team_translations.team_id", "=", "seasons__teams.team_id")
                ->where([
                    "seasons__teams.season_id" => $id,
                    "team_translations.locale" => Lang::getLocale(),
                ])
                ->select("teams.id", "teams.slug", "team_translations.name", "teams.logo")
                ->get();

            $data = [
                'code' => 0,
                'teams' => $teams
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getTeamBySlug(){

        $slug = Request::segment(5);

        $team = DB::table("teams")
            ->leftJoin("team_translations", "team_translations.team_id", "=", "teams.id")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "teams.country_id")
            ->leftJoin("stadium_translations", function($join){
                $join->on("stadium_translations.stadium_id", "=", "teams.stadium_id")
                    ->where("stadium_translations.locale", Lang::getLocale());
            })
            ->leftJoin("coach_translations", "coach_translations.coach_id", "=", "teams.coach_id")
            ->where([
                "teams.slug" => $slug,
                "team_translations.locale" => Lang::getLocale(),
                "country_translations.locale" => Lang::getLocale(),
                "coach_translations.locale" => Lang::getLocale()
            ])
            ->select("teams.id", "teams.logo", "team_translations.name as team_name", "country_translations.name as country_name", "stadium_translations.name as stadium_name", "coach_translations.name as coach_name")
            ->first();

        if($team){

            $data = [
                'code' => 0,
                'team' => $team
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getTeams(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $teams = DB::table("videos")
            ->leftJoin("videos__categories", "videos__categories.video_id", "=", "videos.id")
            ->leftJoin("teams", "teams.id", "=", "videos.team_id")
            ->leftJoin("team_translations", "team_translations.team_id", "=", "teams.id");

        $input = Request::except('_token');

        if(isset($input['team_name']) && !empty($input['team_name'])){
            $teams = $teams->where([
                ['team_translations.name', 'LIKE', '%' . $input['team_name'] . '%']
            ]);
        }

        $teams = $teams->groupBy("teams.id")
            ->where([
                "team_translations.locale" => Lang::getLocale()
            ])
            ->select("teams.*", "team_translations.name")
            ->paginate(10, ['*'], null, $page);

        $data = [
            'code' => 0,
            'teams' => $teams
        ];

        return json_encode($data);
    }
}
