<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class VideosController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getAll(){

        $categories = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id")
            ->where(["locale" => Lang::getLocale()])
            ->get();

        $data = [
            'code' => 0,
            'categories' => $categories
        ];

        return json_encode($data);
    }

    public function getTopVideosByTournament(){

        $id = Request::segment(5);

        if(ctype_digit($id)){

            $whereArray = [
                "team_translations_home.locale" => Lang::getLocale(),
                "team_translations_guest.locale" => Lang::getLocale(),
                "player_translations.locale" => Lang::getLocale(),
                "is_top_goal" => 1
            ];

            if($id != 0) $whereArray["seasons.tournament_id"] = $id;

            $videos = DB::table("videos")
                ->leftJoin("matches", "matches.id", "=", "videos.match_id")
                ->leftJoin("seasons", "seasons.id", "=", "matches.season_id")
                ->leftJoin("team_translations as team_translations_home", "team_translations_home.team_id", "=", "matches.home_team_id")
                ->leftJoin("team_translations as team_translations_guest", "team_translations_guest.team_id", "=", "matches.guest_team_id")
                ->leftJoin("player_translations", "player_translations.player_id", "=", "videos.player_id")
                ->where($whereArray)
                ->limit(6)
                ->orderBy("matches.start_time", "DESC")
                ->select("videos.id",
                    "team_translations_home.name as home_team_name", "team_translations_guest.name as guest_team_name",
                    "home_team_goals_count", "guest_team_goals_count",
                    "start_time", "player_translations.name as player_name", "minute", "videos.preview_image", "videos.name as video_name")
                ->get();

            $data = [
                'code' => 0,
                'videos' => $videos
            ];
        }
        else{

            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getVideosByTeamAndMatch(){

        $teamId = Request::segment(5);
        $matchId = Request::segment(6);

        $videos = DB::table("videos")
            ->leftJoin("player_translations", "player_translations.player_id", "=", "videos.player_id")
            ->leftJoin("matches", "matches.id", "=", "videos.match_id")
            ->leftJoin("team_translations as home_team", "home_team.team_id", "=", "matches.home_team_id")
            ->leftJoin("team_translations as guest_team", "guest_team.team_id", "=", "matches.guest_team_id")
            ->where([
                "player_translations.locale" => Lang::getLocale(),
                "home_team.locale" => Lang::getLocale(),
                "guest_team.locale" => Lang::getLocale(),
                "videos.team_id" => $teamId,
                "videos.match_id" => $matchId,
            ])
            ->orderBy("minute", "ASC")
            ->select("videos.id", "videos.preview_image", "videos.name as video_name",
                "home_team.name as home_team_name", "home_team_goals_count",
                "guest_team.name as guest_team_name", "guest_team_goals_count",
                "matches.start_time", "player_translations.name as player_name", "minute")
            ->get();

        $data = [
            'code' => 0,
            'videos' => $videos
        ];

        return json_encode($data);
    }

    public function getVideos(){
        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $videos = DB::table("videos")
            ->leftJoin("player_translations", "player_translations.player_id", "=", "videos.player_id")
            ->leftJoin("matches", "matches.id", "=", "videos.match_id")
            ->leftJoin("seasons", "seasons.id", "=", "matches.season_id")
            ->leftJoin("tournament_translations", function($join){
                $join->on("tournament_translations.tournament_id", "=", "seasons.tournament_id")
                    ->where('tournament_translations.locale', Lang::getLocale());
            })
            ->leftJoin("team_translations as home_team", "home_team.team_id", "=", "matches.home_team_id")
            ->leftJoin("team_translations as guest_team", "guest_team.team_id", "=", "matches.guest_team_id");

        $input = Request::except('_token');

        if(isset($input['match_id']) && !empty($input['match_id'])){
            $videos = $videos->where(['videos.match_id' => $input['match_id']]);
        }

        if(isset($input['player_id']) && !empty($input['player_id'])){
            $videos = $videos->where(['videos.player_id' => $input['player_id']]);
        }

        if(isset($input['team_id']) && !empty($input['team_id'])){
            $videos = $videos->where(['videos.team_id' => $input['team_id']]);
        }

        $videos = $videos->where([
                "player_translations.locale" => Lang::getLocale(),
                "home_team.locale" => Lang::getLocale(),
                "guest_team.locale" => Lang::getLocale(),
            ])
            ->orderBy("matches.start_time", "DESC")
            ->orderBy("minute", "ASC")
            ->select("videos.id", "videos.preview_image", "videos.name as video_name",
                "home_team.name as home_team_name", "home_team_goals_count",
                "guest_team.name as guest_team_name", "guest_team_goals_count",
                "matches.start_time", "player_translations.name as player_name", "minute",
                "tournament_translations.name as tournament_name")
            ->paginate(10, ['*'], null, $page);

        $data = [
            'code' => 0,
            'videos' => $videos
        ];

        return json_encode($data);
    }
}
