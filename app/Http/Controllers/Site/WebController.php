<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;

class WebController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function home(){

        $data = [
            'title' => 'Football.az',
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az',
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_2.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function page404(){

        $data = [
            'title' => 'Football.az',
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az',
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function news(){

        $data = [
            'title' => 'Football.az' . ' - ' . Lang::get('site.news'),
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az' . ' - ' . Lang::get('site.news'),
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function newsSearch(){

        $data = [
            'title' => 'Football.az' . ' - ' . Lang::get('site.search'),
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az' . ' - ' . Lang::get('site.search'),
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function videos(){

        $data = [
            'title' => 'Football.az' . ' - ' . Lang::get('site.video'),
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az' . ' - ' . Lang::get('site.video'),
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function review(){

        $data = [
            'title' => 'Football.az' . ' - ' . Lang::get('site.review'),
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az' . ' - ' . Lang::get('site.review'),
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function advertising(){

        $data = [
            'title' => 'Football.az' . ' - ' . Lang::get('site.advertising'),
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az' . ' - ' . Lang::get('site.advertising'),
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function about(){

        $data = [
            'title' => 'Football.az' . ' - ' . Lang::get('site.about'),
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az' . ' - ' . Lang::get('site.about'),
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function tournaments(){

        $data = [
            'title' => 'Football.az' . ' - ' . Lang::get('site.tournaments'),
            'description' => Lang::get('site.metaDescription'),
            'ogUrl' => Request::fullUrl(),
            'ogSiteName' => 'Football.az',
            'ogTitle' => 'Football.az' . ' - ' . Lang::get('site.tournaments'),
            'ogType' => 'website',
            'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
            'ogDescription' => Lang::get('site.metaDescription'),
        ];

        return view('angular_client', $data);
    }

    public function tournament($lang, $slug){

        $tournament = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "tournaments.slug" => $slug,
                "tournament_translations.locale" => Lang::getLocale()
            ])
            ->select("tournament_translations.name as tournament_name")
            ->first();

        if($tournament){

            $data = [
                'title' => 'Football.az' . ' - ' . $tournament->tournament_name,
                'description' => Lang::get('site.metaDescription'),
                'ogUrl' => Request::fullUrl(),
                'ogSiteName' => 'Football.az',
                'ogTitle' => 'Football.az' . ' - ' . $tournament->tournament_name,
                'ogType' => 'website',
                'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
                'ogDescription' => Lang::get('site.metaDescription'),
            ];
    
            return view('angular_client', $data);
        }
        else{
            return $this->page404();
        }
    }

    public function team($lang, $slug){

        $team = DB::table("teams")
            ->leftJoin("team_translations", "team_translations.team_id", "=", "teams.id")
            ->where([
                "teams.slug" => $slug,
                "team_translations.locale" => Lang::getLocale()
            ])
            ->select("team_translations.name as team_name")
            ->first();

        if($team){

            $data = [
                'title' => 'Football.az' . ' - ' . $team->team_name,
                'description' => Lang::get('site.metaDescription'),
                'ogUrl' => Request::fullUrl(),
                'ogSiteName' => 'Football.az',
                'ogTitle' => 'Football.az' . ' - ' . $team->team_name,
                'ogType' => 'website',
                'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
                'ogDescription' => Lang::get('site.metaDescription'),
            ];
    
            return view('angular_client', $data);
        }
        else{
            return $this->page404();
        }
    }

    public function newsItem($lang, $slug){

        $newsItem = DB::table("news_item_translations")
            ->leftJoin("news", "news.id", "=", "news_item_translations.news_item_id")
            ->where([
                "news_item_translations.slug" => $slug,
                "news_item_translations.locale" => Lang::getLocale()
            ])
            ->select("news_item_translations.title", "news_item_translations.short_text", "news.image", "news.creating_date")
            ->first();

        if($newsItem){

            $data = [
                'title' => 'Football.az' . ' - ' . $newsItem->title,
                'description' => $newsItem->short_text,
                'ogUrl' => Request::fullUrl(),
                'ogSiteName' => 'Football.az',
                'ogTitle' => 'Football.az' . ' - ' . $newsItem->title,
                'ogType' => 'article',
                'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/' . $newsItem->image,
                'ogDescription' => $newsItem->short_text,
                'section' => "Sport",
                'publishedTime' => $newsItem->creating_date,
                'twitterCard' => "summary_large_image",
                'twitterTitle' => 'Football.az' . ' - ' . $newsItem->title,
                'twitterDescription' => $newsItem->short_text,
            ];
    
            return view('angular_client', $data);
        }
        else{
            return $this->page404();
        }
    }

    public function newsCountry($lang, $slug){

        $country = DB::table("countries")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "countries.id")
            ->where([
                "country_translations.locale" => Lang::getLocale(),
                "slug" => $slug
            ])
            ->select("country_translations.name")
            ->first();

        if($country){

            $data = [
                'title' => 'Football.az' . ' - ' . Lang::get("site.news") . ' - ' . $country->name,
                'description' => Lang::get('site.metaDescription'),
                'ogUrl' => Request::fullUrl(),
                'ogSiteName' => 'Football.az',
                'ogTitle' => 'Football.az' . ' - ' . Lang::get("site.news") . ' - ' . $country->name,
                'ogType' => 'website',
                'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
                'ogDescription' => Lang::get('site.metaDescription'),
            ];
    
            return view('angular_client', $data);
        }
        else{
            return $this->page404();
        }
    }

    public function newsCategory($lang, $slug){

        $category = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id")
            ->where([
                "category_translations.locale" => Lang::getLocale(),
                "slug" => $slug
            ])
            ->first();

        if($category){

            $data = [
                'title' => 'Football.az' . ' - ' . Lang::get("site.news") . ' - ' . $category->name,
                'description' => Lang::get('site.metaDescription'),
                'ogUrl' => Request::fullUrl(),
                'ogSiteName' => 'Football.az',
                'ogTitle' => 'Football.az' . ' - ' . Lang::get("site.news") . ' - ' . $category->name,
                'ogType' => 'website',
                'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
                'ogDescription' => Lang::get('site.metaDescription'),
            ];
    
            return view('angular_client', $data);
        }
        else{
            return $this->page404();
        }
    }

    public function newsTournament($lang, $slug){

        $tournament = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "tournament_translations.locale" => Lang::getLocale(),
                "slug" => $slug
            ])
            ->first();

        if($tournament){

            $data = [
                'title' => 'Football.az' . ' - ' . Lang::get("site.news") . ' - ' . $tournament->name,
                'description' => Lang::get('site.metaDescription'),
                'ogUrl' => Request::fullUrl(),
                'ogSiteName' => 'Football.az',
                'ogTitle' => 'Football.az' . ' - ' . Lang::get("site.news") . ' - ' . $tournament->name,
                'ogType' => 'website',
                'ogImage' => Request::getSchemeAndHttpHost() . '/assets/site/assets/images/football-simple-white_3.png',
                'ogDescription' => Lang::get('site.metaDescription'),
            ];
    
            return view('angular_client', $data);
        }
        else{
            return $this->page404();
        }
    }
}
