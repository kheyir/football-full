<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class CategoriesController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getAll(){

        $categories = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id")
            ->where(["locale" => Lang::getLocale()])
            ->get();

        $data = [
            'code' => 0,
            'categories' => $categories
        ];

        return json_encode($data);
    }

    public function getFullTypes(){

        $categories = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_news_category" => 1
            ])
            ->get();

        $countries = DB::table("countries")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "countries.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_news_category" => 1
            ])
            ->get();

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_news_category" => 1
            ])
            ->get();

        $fullTypes = $categories->merge($tournaments)->merge($countries);

        $data = [
            'code' => 0,
            'fullTypes' => $fullTypes
        ];

        return json_encode($data);
    }

    public function getCategories(){

        $categories = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id");

        $input = Request::except('_token');

        if(isset($input['is_for_video']) && !empty($input['is_for_video'])){
            $categories = $categories
                ->where([
                    'is_for_video' => 1
                ]);
        }

        $categories = $categories
            ->where([
                "category_translations.locale" => Lang::getLocale()
            ])
            ->orderBy('name', 'ASC')
            ->get();

        return response()->json([
            'code' => 0,
            'categories' => $categories
        ]);
    }
}
