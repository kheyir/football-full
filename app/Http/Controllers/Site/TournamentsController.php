<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class TournamentsController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getAll(){

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where(["locale" => Lang::getLocale()])
            ->get();

        $data = [
            'code' => 0,
            'tournaments' => $tournaments
        ];

        return json_encode($data);
    }

    public function getTournamentsIsNewsCategory(){

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_news_category" => 1
            ])
            ->get();

        $data = [
            'code' => 0,
            'tournaments' => $tournaments
        ];

        return json_encode($data);
    }

    public function getTournamentsIsVideosCategory(){

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_videos_category" => 1
            ])
            ->get();

        $data = [
            'code' => 0,
            'tournaments' => $tournaments
        ];

        return json_encode($data);
    }

    public function getTournamentBySlug(){

        $slug = Request::segment(5);

        $tournament = DB::table("tournaments")
            ->leftJoin("tournaments_category_translations", "tournaments_category_translations.tournaments_category_id", "=", "tournaments.tournaments_category_id")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->leftJoin("country_translations", function($join){
                $join->on("country_translations.country_id", '=', "tournaments.country_id")
                    ->where("country_translations.locale", Lang::getLocale());
            })
            ->where([
                "tournaments.slug" => $slug,
                "tournament_translations.locale" => Lang::getLocale(),
                "tournaments_category_translations.locale" => Lang::getLocale()
            ])
            ->select("tournaments.*", "tournament_translations.name as tournament_name", "country_translations.name as country_name", "tournaments_category_translations.name as tournaments_category_name")
            ->first();

        if($tournament){
            $seasons = DB::table("seasons")
                ->leftJoin("seasons__teams", "seasons__teams.season_id", "=", "seasons.id")
                ->where([
                    "tournament_id" => $tournament->id,
                ])
                ->orderBy("start_year", "DESC")
                ->groupBy("seasons.id")
                ->select("seasons.id", "start_year", "end_year", DB::raw("count(seasons__teams.team_id) as teams_count"))
                ->get();

            $data = [
                'code' => 0,
                'tournament' => $tournament,
                'seasons' => $seasons
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getGlobalTournaments(){

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "locale" => Lang::getLocale()
            ])
            ->whereRaw('tournaments.country_id IN (0,1) AND tournaments.id != 1')
            ->get();

        $data = [
            'code' => 0,
            'tournaments' => $tournaments
        ];

        return json_encode($data);
    }

    public function getTournamentsPriorityForCountry(){

        $tournaments = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->leftJoin("country_translations", function($join){
                $join->on("country_translations.country_id", "=", "tournaments.country_id")
                    ->where("country_translations.locale", Lang::getLocale());
            })
            ->where([
                "tournament_translations.locale" => Lang::getLocale()
            ])
            ->where(function($query){
                $query->where("tournaments.country_id", 0)
                    ->orWhere("priority_for_country", 1);
            })
            ->select("tournament_translations.tournament_id", "tournament_translations.name as tournament_name",
                "tournaments.logo_mini",
                "country_translations.country_id", "country_translations.name as country_name")
            ->get();

        $data = [
            'code' => 0,
            'tournaments' => $tournaments
        ];

        return json_encode($data);
    }
}
