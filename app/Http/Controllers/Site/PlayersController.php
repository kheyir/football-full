<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class PlayersController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getPlayersByTeam(){

        $id = Request::segment(5);

        $team = DB::table("teams")
            ->where([
                "teams.id" => $id
            ])
            ->first();

        if($team){
            $players = DB::table("players")
                ->leftJoin("player_translations", "player_translations.player_id", "=", "players.id")
                ->leftJoin("countries", "countries.id", "=", "players.country_id")
                ->leftJoin("country_translations", "country_translations.country_id", "=", "players.country_id")
                ->leftJoin("position_translations", "position_translations.position_id", "=", "players.position_id")
                ->leftJoin("players__teams", "players__teams.player_id", '=', "players.id")
                ->where([
                    "players__teams.team_id" => $id,
                    "player_translations.locale" => Lang::getLocale(),
                    "country_translations.locale" => Lang::getLocale(),
                    "position_translations.locale" => Lang::getLocale()
                ])
                ->select("players.id", "players__teams.number", "country_translations.name as country_name", "countries.flag",
                    "players.photo", "player_translations.name as player_name", "position_translations.name as position_name",
                    "date_of_birthday", "height", "weight")
                ->get();

            $data = [
                'code' => 0,
                'players' => $players
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getPlayersThatHaveVideo(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $players = DB::table("videos")
            ->leftJoin("videos__categories", "videos__categories.video_id", "=", "videos.id")
            ->leftJoin("players", "players.id", "=", "videos.player_id")
            ->leftJoin("player_translations", "player_translations.player_id", "=", "players.id")
            ->leftJoin("countries", "countries.id", "=", "players.country_id")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "players.country_id")
            ->leftJoin("position_translations", "position_translations.position_id", "=", "players.position_id");

        $input = Request::except('_token');

        if(isset($input['player_name']) && !empty($input['player_name'])){
            $players = $players->where([
                ['player_translations.name', 'LIKE', '%' . $input['player_name'] . '%']
            ]);
        }

        $players = $players
            ->where([
                "player_translations.locale" => Lang::getLocale(),
                "country_translations.locale" => Lang::getLocale(),
                "position_translations.locale" => Lang::getLocale()
            ])
            ->groupBy("videos.player_id")
            ->select("players.id", "country_translations.name as country_name", "countries.flag",
                "players.photo", "player_translations.name as player_name", "position_translations.name as position_name",
                "date_of_birthday", "height", "weight")
            ->paginate(10, ['*'], null, $page);

        $data = [
            'code' => 0,
            'players' => $players
        ];

        return json_encode($data);
    }
}
