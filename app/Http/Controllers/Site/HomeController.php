<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class HomeController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getHomeItems(){

        $data = [
            'code' => 0,
        ];

        //slider news
        $inHomeNewsData = json_decode(app('App\Http\Controllers\Site\NewsController')->getSlidersNews());

        if($inHomeNewsData->code == 0){
            $data['inHomeNews'] = $inHomeNewsData->inHomeNews;
        }
        else{
            $data['inHomeNews'] = [];
        }

        //top news
        $topNewsData = json_decode(app('App\Http\Controllers\Site\NewsController')->getTops());

        if($topNewsData->code == 0){
            $data['topNews'] = $topNewsData->topNews;
        }
        else{
            $data['topNews'] = [];
        }

        //latest news
        $latestNewsData = json_decode(app('App\Http\Controllers\Site\NewsController')->getLatest());

        if($latestNewsData->code == 0){
            $data['latestNews'] = $latestNewsData->latestNews;
        }
        else{
            $data['latestNews'] = [];
        }

        //tournaments
        $tournamentsData = json_decode(app('App\Http\Controllers\Site\TournamentsController')->getTournamentsPriorityForCountry());

        if($tournamentsData->code == 0){
            $data['tournaments'] = $tournamentsData->tournaments;
        }
        else{
            $data['tournaments'] = [];
        }

        //tournaments
        $tournamentsData = json_decode(app('App\Http\Controllers\Site\TournamentsController')->getTournamentsIsVideosCategory());

        if($tournamentsData->code == 0){
            $data['videoCategoryTournaments'] = $tournamentsData->tournaments;
        }
        else{
            $data['videoCategoryTournaments'] = [];
        }

        return json_encode($data);
    }
}
