<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class PagesController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getPage(){

        $slug = Request::segment(4);

        $pageItem = DB::table("add_pages")
            ->where([
                "locale" => Lang::getLocale(),
                "slug" => $slug
            ])
            ->first();

        if($pageItem){

            $data = [
                'code' => 0,
                'pageItem' => $pageItem
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }
}
