<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;

use Request;
use Lang;
use View;
use DB;
use Cookie;
use Hash;
use Config;

class SiteController extends Controller
{

    protected $contact;

    function __construct(){

       $this->setLocale();
//
//        $this->headerData();
    }

    private function setLocale(){
        $newLang = Request::segment(1) == 'api' ? Request::segment(2) : Request::segment(1);

        $languages = DB::table("languages")->get();

        if($newLang && $languages->where("locale", $newLang)->count() > 0){
            Cookie::queue("lang", $newLang);
            Lang::setLocale($newLang);
        }
        else {
            $lang = Cookie::get("lang");

            if ($languages->where("locale", $lang)->count() > 0) {
                Lang::setLocale($lang);
            }
        }
    }

    private function headerData(){

        $languages = DB::table("languages")->get();

        $socialLinks = DB::table("social_links")
            ->get();

        $vacanciesCount = DB::table("vacancies")
            ->where("is_published", "=", 1)
            ->where("duration_from", ">=", date("Y-m-d"))
            ->where("duration_from", "<=", date("Y-m-d", strtotime("+1 day")))
            ->where("duration_to", ">=", date("Y-m-d H:i:s"))
            ->count();

        $resumesCount = DB::table("resumes")
            ->where("is_published", "=", 1)
            ->where("duration_from", ">=", date("Y-m-d"))
            ->where("duration_from", "<=", date("Y-m-d", strtotime("+1 day")))
            ->count();

        View::share("languages", $languages);
        View::share("socialLinks", $socialLinks);
        View::share("vacanciesCount", $vacanciesCount);
        View::share("resumesCount", $resumesCount);

    }
}
