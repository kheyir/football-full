<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class MatchesController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getMatchesBySeason(){

        $id = Request::segment(5);

        $season = DB::table("seasons")
            ->where([
                "seasons.id" => $id
            ])
            ->first();

        if($season){

            DB::enableQueryLog();
            $matches = DB::table("matches")
                ->leftJoin("teams as home_teams_original", "home_teams_original.id", "=", "matches.home_team_id")
                ->leftJoin("team_translations as home_teams", "home_teams.team_id", "=", "matches.home_team_id")
                ->leftJoin("teams as guest_teams_original", "guest_teams_original.id", "=", "matches.guest_team_id")
                ->leftJoin("team_translations as guest_teams", "guest_teams.team_id", "=", "matches.guest_team_id")
                ->leftJoin("seasons__elements", "seasons__elements.id", "=", "matches.seasons__element_id");

            $whereArray = [
                "seasons__elements.season_id" => $id,
                "home_teams.locale" => Lang::getLocale(),
                "guest_teams.locale" => Lang::getLocale(),
                "matches.is_active" => 1
                // ["matches.start_time", ">=", date('Y-m-d', time() - (60 * 60 * 24 * 10) )]
            ];

            $input = Request::except('_token');

            if(isset($input['element_id']) && !empty($input['element_id'])){
                $whereArray['seasons__elements.element_id'] = $input['element_id'];
            }

            if(isset($input['group_id']) && !empty($input['group_id'])){

                $matches = $matches->leftJoin("seasons__stages__groups", function($join) use($id){
                    $join->on("seasons__stages__groups.id", "=", "matches.seasons__stages__group_id")
                        ->where("seasons__stages__groups.season_id", $id);
                });
                $matches = $matches->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id");

                $whereArray['stages__groups.group_id'] = $input['group_id'];
            }

            if(isset($input['team_id']) && !empty($input['team_id'])){
                $matches = $matches->where((function ($query) use ($input) {
                    $query->where("matches.home_team_id", $input['team_id']);
                    $query->orWhere("matches.guest_team_id", $input['team_id']);
                }));
            }

            $whiteStatusList = [
              0 => 1,
              1 => 2
            ];
            if(isset($input['status']) && in_array($input['status'], $whiteStatusList)){
                $whereArray['matches.status'] = array_search($input['status'], $whiteStatusList);
            }

            $matches = $matches->where($whereArray)
                ->select("matches.id", "home_team_id", "home_teams_original.slug as home_team_slug", "home_teams.name as home_team_name", "home_team_goals_count",
                    "guest_team_id", "guest_teams_original.slug as guest_team_slug", "guest_teams.name as guest_team_name", "guest_team_goals_count",
                    "matches.start_time", "matches.status")
                ->orderBy("matches.start_time", "DESC")
                ->limit(20)
                ->get();

        //    dd(DB::getQueryLog());

            $data = [
                'code' => 0,
                'matches' => $matches
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getMatchesBySeasonAndStage(){

        $seasonId = Request::segment(5);
        $stageId = Request::segment(6);

        $season = DB::table("seasons")
            ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
            ->where([
                "seasons.id" => $seasonId
            ])
            ->first();

        $stage = DB::table("stages")
            ->where([
                "stages.id" => $stageId
            ])
            ->first();

        if($season && $stage){

            if($season->is_league){

                $groups = DB::table("seasons__teams")
                    ->leftJoin("seasons", "seasons.id", "=", "seasons__teams.season_id")
                    ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
                    ->leftJoin("teams", "teams.id", "=", "seasons__teams.team_id")
                    ->leftJoin("team_translations", "team_translations.team_id", "=", "seasons__teams.team_id")
                    ->leftJoin("seasons__elements", "seasons__elements.season_id", "=", "seasons__teams.season_id")
                    ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
                    ->leftJoin("stages", "stages.id", "=", "elements.stage_id")
                    ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "elements.stage_id")
                    ->where([
                        'team_translations.locale' => Lang::getLocale(),
                        'seasons__teams.season_id' => $seasonId,
                        'stage_translations.stage_id' => $stageId
                    ])
                    ->orderBy("stages.priority", "ASC")
                    ->orderBy("elements.priority", "ASC")
                    ->groupBy("stages.id", "teams.id")
                    ->select("team_translations.team_id", "team_translations.name as team_name", "teams.logo", "teams.slug",
                        "stage_translations.stage_id", "stage_translations.name as group_name",
                        "tournaments.league_victory_points", "tournaments.league_draw_points")
                    ->get();

                $groups = $groups->groupBy("group_name")->toArray();

                $responseGroups = [];
                foreach ($groups as $groupName => $group){
                    $matches = DB::table("matches")
                        ->where([
                            'matches.season_id' => $seasonId,
                            'matches.is_active' => 1,
                            'matches.status' => 1
                        ])
                        ->get();

                    foreach ($group as $key => $teamAndGroup){
                        $groups[$groupName][$key]->gamesCount = 0;
                        $groups[$groupName][$key]->victoryGamesCount = 0;
                        $groups[$groupName][$key]->drawGamesCount = 0;
                        $groups[$groupName][$key]->defeatGamesCount = 0;
                        $groups[$groupName][$key]->goalsScoredCount = 0;
                        $groups[$groupName][$key]->goalsConcededCount = 0;
                        $groups[$groupName][$key]->goalsDiffCount = 0;
                        $groups[$groupName][$key]->pointsCount = 0;

                        $groups[$groupName][$key]->goalsScoredCountAsGuest = 0;

                        $groups[$groupName][$key]->victoryCountWithSamePointsCount = 0;
                        $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount = 0;
                        $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount = 0;
                        $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = 0;
                        $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount = 0;
                    }

                    foreach ($matches as $match){

                        $homeIndex = null;
                        $guestIndex = null;
                        foreach ($group as $key => $teamAndGroup){
                            if($match->home_team_id == $teamAndGroup->team_id) $homeIndex = $key;
                            if($match->guest_team_id == $teamAndGroup->team_id) $guestIndex = $key;
                        }

                        //games count
                        $groups[$groupName][$homeIndex]->gamesCount++;
                        $groups[$groupName][$guestIndex]->gamesCount++;

                        //games results
                        if($match->home_team_goals_count > $match->guest_team_goals_count){
                            $groups[$groupName][$homeIndex]->victoryGamesCount++;
                            $groups[$groupName][$guestIndex]->defeatGamesCount++;

                            $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_victory_points;
                        }
                        elseif($match->home_team_goals_count < $match->guest_team_goals_count){
                            $groups[$groupName][$homeIndex]->defeatGamesCount++;
                            $groups[$groupName][$guestIndex]->victoryGamesCount++;

                            $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_victory_points;
                        }
                        else{
                            $groups[$groupName][$homeIndex]->drawGamesCount++;
                            $groups[$groupName][$guestIndex]->drawGamesCount++;

                            $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_draw_points;
                            $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->league_draw_points;
                        }

                        //goals
                        $groups[$groupName][$homeIndex]->goalsScoredCount += $match->home_team_goals_count;
                        $groups[$groupName][$homeIndex]->goalsConcededCount += $match->guest_team_goals_count;
                        $groups[$groupName][$homeIndex]->goalsDiffCount = $groups[$groupName][$homeIndex]->goalsScoredCount - $groups[$groupName][$homeIndex]->goalsConcededCount;

                        $groups[$groupName][$guestIndex]->goalsScoredCount += $match->guest_team_goals_count;
                        $groups[$groupName][$guestIndex]->goalsConcededCount += $match->home_team_goals_count;
                        $groups[$groupName][$guestIndex]->goalsDiffCount = $groups[$groupName][$guestIndex]->goalsScoredCount - $groups[$groupName][$guestIndex]->goalsConcededCount;

                        $groups[$groupName][$guestIndex]->goalsScoredCountAsGuest += $match->guest_team_goals_count;

                    }

                    //group by pointsCount
                    $grouppedByPointsCount = [];
                    foreach ($groups[$groupName] as $key => $item) {
                        if(!isset($grouppedByPointsCount[$item->pointsCount])) $grouppedByPointsCount[$item->pointsCount] = [];

                        $grouppedByPointsCount[$item->pointsCount][] = $item;
                    }

                    foreach ($grouppedByPointsCount as $pointCount => $teams) {
                        for($i = 0; $i < count($teams); $i++){
                            for($j = $i + 1; $j < count($teams); $j++){
                                list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($teams, $i, $j) {
                                    return ($match->home_team_id == $teams[$i]->team_id && $match->guest_team_id == $teams[$j]->team_id) ||
                                            ($match->guest_team_id == $teams[$i]->team_id && $match->home_team_id == $teams[$j]->team_id);
                                });

                                foreach($suitableMatches as $key => $suitableMatch){
                                    //goalsScoredCountWithSamePointsCount, goalsConcededCountWithSamePointsCount, goalsDiffCountWithSamePointsCount, goalsScoredCountAsGuestWithSamePointsCount
                                    foreach($groups[$groupName] as $key => $team){
                                        if($suitableMatch->home_team_id == $team->team_id){
                                            $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                            $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                            $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;
                                        }
                                        elseif($suitableMatch->guest_team_id == $team->team_id){
                                            $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                            $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                            $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;

                                            $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                        }
                                    }

                                    //victoryCountWithSamePointsCount
                                    if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count){
                                        foreach($groups[$groupName] as $key => $team) if($suitableMatch->home_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                    }
                                    else{
                                        if($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count){
                                            foreach($groups[$groupName] as $key => $team) if($suitableMatch->guest_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //

                    if($season->tournament_id == 1){

                        array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                            array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsDiffCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                            $groups[$groupName]);

                        for($i=0; $i < count($groups[$groupName]) - 1; $i++){
                            if($groups[$groupName][$i]->pointsCount == $groups[$groupName][$i+1]->pointsCount){

                                $firstTeamVictoryCount = 0;
                                $secondTeamVictoryCount = 0;

                                list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($groups, $groupName, $i) {
                                    return ($match->home_team_id == $groups[$groupName][$i]->team_id && $match->guest_team_id == $groups[$groupName][$i+1]->team_id);
                                });
                                foreach($suitableMatches as $key => $suitableMatch){
                                    if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count) $firstTeamVictoryCount++;
                                    elseif($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count) $secondTeamVictoryCount++;
                                }
                                
                                list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($groups, $groupName, $i) {
                                    return ($match->guest_team_id == $groups[$groupName][$i]->team_id && $match->home_team_id == $groups[$groupName][$i+1]->team_id);
                                });
                                foreach($suitableMatches as $key => $suitableMatch){
                                    if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count) $secondTeamVictoryCount++;
                                    elseif($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count) $firstTeamVictoryCount++;
                                }

                                if($secondTeamVictoryCount > $firstTeamVictoryCount){
                                    $temp = $groups[$groupName][$i];
                                    $groups[$groupName][$i] = $groups[$groupName][$i+1];
                                    $groups[$groupName][$i+1] = $temp;
                                }
                            }
                        }
                    }
                    else{
                        array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                            array_column($groups[$groupName], 'victoryCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCountAsGuestWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsDiffCount'), SORT_DESC,
                            array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCountAsGuest'), SORT_DESC,
                            $groups[$groupName]);
                    }

                    $responseGroups[] = [
                        "group_name" => $groupName,
                        "teams" => $groups[$groupName]
                    ];
                }

                $data = [
                    'code' => 0,
                    'groups' => $responseGroups,
                    'has_groups' => 1
                ];
            }
            else{
                if($stage->has_groups == 0){
                    DB::enableQueryLog();
                    $matches = DB::table("matches")
                        ->leftJoin("teams as home_teams_original", "home_teams_original.id", "=", "matches.home_team_id")
                        ->leftJoin("team_translations as home_teams", "home_teams.team_id", "=", "matches.home_team_id")
                        ->leftJoin("teams as guest_teams_original", "guest_teams_original.id", "=", "matches.guest_team_id")
                        ->leftJoin("team_translations as guest_teams", "guest_teams.team_id", "=", "matches.guest_team_id")
                        ->leftJoin("seasons__elements", "seasons__elements.id", "=", "matches.seasons__element_id")
                        ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
                        ->leftJoin("element_translations", "element_translations.element_id", "=", "seasons__elements.element_id")
                        ->leftJoin("stages", "stages.id", "=", "elements.stage_id")
                        ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "stages.id")
                        ->where([
                            'home_teams.locale' => Lang::getLocale(),
                            'guest_teams.locale' => Lang::getLocale(),
                            'stage_translations.locale' => Lang::getLocale(),
                            'element_translations.locale' => Lang::getLocale(),
                            'matches.season_id' => $seasonId,
                            'seasons__elements.season_id' => $seasonId,
                            'stages.id' => $stageId,
                            'matches.is_active' => 1
                        ])
                        ->orderBy("stages.priority", "ASC")
                        ->orderBy("elements.priority", "ASC")
                        ->select("matches.id", "matches.start_time", "matches.status", "seasons__element_id",
                            "stage_translations.name as stage_name",
                            "element_translations.name as element_name",
                            "home_teams_original.slug as home_team_slug", "guest_teams_original.slug as guest_team_slug",
                            "home_teams.team_id as home_team_id", "home_teams.name as home_team_name", "home_team_goals_count",
                            "guest_teams.team_id as guest_team_id", "guest_teams.name as guest_team_name", "guest_team_goals_count")
                        ->get();

                    $matches = $matches->groupBy("element_name");

                    $responseMatches = [];
                    foreach ($matches as $elementName => $match){
                        $responseMatches[] = [
                            "element_name" => $elementName,
                            "matches" => $match,
                        ];
                    }

                    $data = [
                        'code' => 0,
                        'matches' => $responseMatches,
                        'has_groups' => 0
                    ];
                }
                else{

                    $groups = DB::table("seasons__stages__groups__teams")
                        ->leftJoin("seasons__stages__groups", "seasons__stages__groups.id", "=", "seasons__stages__groups__teams.seasons__stages__group_id")
                        ->leftJoin("stages__groups", "stages__groups.id", "=", "seasons__stages__groups.stages__group_id")
                        ->leftJoin("stages", "stages.id", "=", "stages__groups.stage_id")
                        ->leftJoin("groups", "groups.id", "=", "stages__groups.group_id")
                        ->leftJoin("group_translations", "group_translations.group_id", "=", "stages__groups.group_id")
                        ->leftJoin("teams", "teams.id", "=", "seasons__stages__groups__teams.team_id")
                        ->leftJoin("team_translations", "team_translations.team_id", "=", "seasons__stages__groups__teams.team_id")
                        ->where([
                            'group_translations.locale' => Lang::getLocale(),
                            'team_translations.locale' => Lang::getLocale(),
                            'seasons__stages__groups.season_id' => $seasonId,
                            'stages__groups.stage_id' => $stageId,
                        ])
                        ->orderBy("stages.priority", "ASC")
                        ->orderBy("groups.priority", "ASC")
                        ->select("team_translations.team_id", "team_translations.name as team_name", "teams.logo",
                            "group_translations.group_id", "group_translations.name as group_name",
                            "seasons__stages__groups.id as seasons__stages__group_id",
                            "seasons__stages__groups.draw_points", "seasons__stages__groups.victory_points")
                        ->get();

                    $groups = $groups->groupBy("group_name")->toArray();

                    $responseGroups = [];
                    foreach ($groups as $groupName => $group){
                        $matches = DB::table("matches")
                            ->where([
                                'matches.season_id' => $seasonId,
                                'matches.seasons__stages__group_id' => $group[0]->seasons__stages__group_id,
                                'matches.is_active' => 1
                            ])
                            ->get();

                        foreach ($group as $key => $teamAndGroup){
                            $groups[$groupName][$key]->gamesCount = 0;
                            $groups[$groupName][$key]->victoryGamesCount = 0;
                            $groups[$groupName][$key]->drawGamesCount = 0;
                            $groups[$groupName][$key]->defeatGamesCount = 0;
                            $groups[$groupName][$key]->goalsScoredCount = 0;
                            $groups[$groupName][$key]->goalsConcededCount = 0;
                            $groups[$groupName][$key]->goalsDiffCount = 0;
                            $groups[$groupName][$key]->pointsCount = 0;

                            $groups[$groupName][$key]->goalsScoredCountAsGuest = 0;

                            $groups[$groupName][$key]->victoryCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = 0;
                            $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount = 0;
                        }

                        foreach ($matches as $match){

                            $homeIndex = null;
                            $guestIndex = null;
                            foreach ($group as $key => $teamAndGroup){
                                if($match->home_team_id == $teamAndGroup->team_id) $homeIndex = $key;
                                if($match->guest_team_id == $teamAndGroup->team_id) $guestIndex = $key;
                            }

                            //games count
                            $groups[$groupName][$homeIndex]->gamesCount++;
                            $groups[$groupName][$guestIndex]->gamesCount++;

                            //games results
                            if($match->home_team_goals_count > $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->victoryGamesCount++;
                                $groups[$groupName][$guestIndex]->defeatGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->victory_points;
                            }
                            elseif($match->home_team_goals_count < $match->guest_team_goals_count){
                                $groups[$groupName][$homeIndex]->defeatGamesCount++;
                                $groups[$groupName][$guestIndex]->victoryGamesCount++;

                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->victory_points;
                            }
                            else{
                                $groups[$groupName][$homeIndex]->drawGamesCount++;
                                $groups[$groupName][$guestIndex]->drawGamesCount++;

                                $groups[$groupName][$homeIndex]->pointsCount += $groups[$groupName][$homeIndex]->draw_points;
                                $groups[$groupName][$guestIndex]->pointsCount += $groups[$groupName][$homeIndex]->draw_points;
                            }

                            //goals
                            $groups[$groupName][$homeIndex]->goalsScoredCount += $match->home_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsConcededCount += $match->guest_team_goals_count;
                            $groups[$groupName][$homeIndex]->goalsDiffCount = $groups[$groupName][$homeIndex]->goalsScoredCount - $groups[$groupName][$homeIndex]->goalsConcededCount;

                            $groups[$groupName][$guestIndex]->goalsScoredCount += $match->guest_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsConcededCount += $match->home_team_goals_count;
                            $groups[$groupName][$guestIndex]->goalsDiffCount = $groups[$groupName][$guestIndex]->goalsScoredCount - $groups[$groupName][$guestIndex]->goalsConcededCount;

                            $groups[$groupName][$guestIndex]->goalsScoredCountAsGuest += $match->guest_team_goals_count;

                        }

                        //group by pointsCount
                        $grouppedByPointsCount = [];
                        foreach ($groups[$groupName] as $key => $item) {
                            if(!isset($grouppedByPointsCount[$item->pointsCount])) $grouppedByPointsCount[$item->pointsCount] = [];

                            $grouppedByPointsCount[$item->pointsCount][] = $item;
                        }

                        foreach ($grouppedByPointsCount as $pointCount => $teams) {
                            for($i = 0; $i < count($teams); $i++){
                                for($j = $i + 1; $j < count($teams); $j++){
                                    list($suitableMatches, $unSuitableMatches) = $matches->partition(function ($match) use($teams, $i, $j) {
                                        return ($match->home_team_id == $teams[$i]->team_id && $match->guest_team_id == $teams[$j]->team_id) ||
                                                ($match->guest_team_id == $teams[$i]->team_id && $match->home_team_id == $teams[$j]->team_id);
                                    });

                                    foreach($suitableMatches as $key => $suitableMatch){
                                        //goalsScoredCountWithSamePointsCount, goalsConcededCountWithSamePointsCount, goalsDiffCountWithSamePointsCount, goalsScoredCountAsGuestWithSamePointsCount
                                        foreach($groups[$groupName] as $key => $team){
                                            if($suitableMatch->home_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;
                                            }
                                            elseif($suitableMatch->guest_team_id == $team->team_id){
                                                $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                                $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount += $suitableMatch->home_team_goals_count;
                                                $groups[$groupName][$key]->goalsDiffCountWithSamePointsCount = $groups[$groupName][$key]->goalsScoredCountWithSamePointsCount - $groups[$groupName][$key]->goalsConcededCountWithSamePointsCount;

                                                $groups[$groupName][$key]->goalsScoredCountAsGuestWithSamePointsCount += $suitableMatch->guest_team_goals_count;
                                            }
                                        }

                                        //victoryCountWithSamePointsCount
                                        if($suitableMatch->home_team_goals_count > $suitableMatch->guest_team_goals_count){
                                            foreach($groups[$groupName] as $key => $team) if($suitableMatch->home_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                        }
                                        else{
                                            if($suitableMatch->guest_team_goals_count > $suitableMatch->home_team_goals_count){
                                                foreach($groups[$groupName] as $key => $team) if($suitableMatch->guest_team_id == $team->team_id) $groups[$groupName][$key]->victoryCountWithSamePointsCount++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //



                        array_multisort(array_column($groups[$groupName], 'pointsCount'),  SORT_DESC,
                            array_column($groups[$groupName], 'goalsDiffCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCountAsGuestWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'goalsScoredCount'), SORT_DESC,
                            array_column($groups[$groupName], 'victoryCountWithSamePointsCount'), SORT_DESC,
                            array_column($groups[$groupName], 'victoryGamesCount'), SORT_DESC,
                            $groups[$groupName]);

                        $responseGroups[] = [
                            "group_name" => $groupName,
                            "teams" => $groups[$groupName]
                        ];
                    }

                    $data = [
                        'code' => 0,
                        'groups' => $responseGroups,
                        'has_groups' => 1
                    ];
                }
            }

        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getMatchesByTeam(){

        $id = Request::segment(5);
        $page = Request::segment(6) != null ? Request::segment(6) : 1;

        $team = DB::table("teams")
            ->where([
                "teams.id" => $id
            ])
            ->first();

        if($team){

            $matches = DB::table("matches")
                ->leftJoin("teams as home_teams_original", "home_teams_original.id", "=", "matches.home_team_id")
                ->leftJoin("team_translations as home_teams", "home_teams.team_id", "=", "matches.home_team_id")
                ->leftJoin("teams as guest_teams_original", "guest_teams_original.id", "=", "matches.guest_team_id")
                ->leftJoin("team_translations as guest_teams", "guest_teams.team_id", "=", "matches.guest_team_id");

            $whereArray = [
                "home_teams.locale" => Lang::getLocale(),
                "guest_teams.locale" => Lang::getLocale(),
                "matches.is_active" => 1
            ];

            $input = Request::except('_token');

            $whiteStatusList = [
                0 => 1,
                1 => 2
            ];
            if(isset($input['status']) && in_array($input['status'], $whiteStatusList)){
                $whereArray['matches.status'] = array_search($input['status'], $whiteStatusList);
            }

            $matches = $matches->where($whereArray)
                ->where(function($query) use($id){
                    $query->where("matches.home_team_id", $id)
                        ->orWhere("matches.guest_team_id", $id);
                })
                ->orderBy("start_time", "DESC")
                ->select("matches.id", "home_teams_original.slug as home_team_slug", "guest_teams_original.slug as guest_team_slug", "home_team_id", "home_teams.name as home_team_name", "home_team_goals_count",
                    "guest_team_id", "guest_teams.name as guest_team_name", "guest_team_goals_count",
                    "matches.start_time", "matches.status")
                ->paginate(10, ['*'], null, $page);

            $data = [
                'code' => 0,
                'matches' => $matches
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getMatchesByTeamWithVideos(){

        $id = Request::segment(5);
        $page = Request::segment(6) != null ? Request::segment(6) : 1;

        $team = DB::table("teams")
            ->where([
                "teams.id" => $id
            ])
            ->first();

        if($team){

            $matches = DB::table("videos")
                ->leftJoin("matches", "matches.id", "=", "videos.match_id")
                ->leftJoin("team_translations as home_teams", "home_teams.team_id", "=", "matches.home_team_id")
                ->leftJoin("team_translations as guest_teams", "guest_teams.team_id", "=", "matches.guest_team_id");

            $matches = $matches
                ->where([
                    "home_teams.locale" => Lang::getLocale(),
                    "guest_teams.locale" => Lang::getLocale(),
                    "videos.team_id" => $id,
                    "matches.is_active" => 1
                ])
                ->orderBy("start_time", "DESC")
                ->groupBy("matches.id")
                ->select("matches.id", "home_team_id", "home_teams.name as home_team_name", "home_team_goals_count",
                    "guest_team_id", "guest_teams.name as guest_team_name", "guest_team_goals_count",
                    "matches.start_time", "matches.status")
                ->paginate(10, ['*'], null, $page);

            $data = [
                'code' => 0,
                'matches' => $matches
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getMatches(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        DB::enableQueryLog();
        $matches = DB::table("matches")
            ->leftJoin("teams as home_teams_original", "home_teams_original.id", "=", "matches.home_team_id")
            ->leftJoin("team_translations as home_teams", "home_teams.team_id", "=", "matches.home_team_id")
            ->leftJoin("teams as guest_teams_original", "guest_teams_original.id", "=", "matches.guest_team_id")
            ->leftJoin("team_translations as guest_teams", "guest_teams.team_id", "=", "matches.guest_team_id")
            ->leftJoin("seasons__elements", "seasons__elements.id", "=", "matches.seasons__element_id")
            ->leftJoin("seasons", "seasons.id", "=", "seasons__elements.season_id")
            ->leftJoin("tournaments", "tournaments.id", "=", "seasons.tournament_id")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id");

        $whereArray = [
            "home_teams.locale" => Lang::getLocale(),
            "guest_teams.locale" => Lang::getLocale(),
            "tournament_translations.locale" => Lang::getLocale(),
            "matches.is_active" => 1
        ];

        $input = Request::except('_token');

        if(isset($input['tournament_id']) && !empty($input['tournament_id'])){
            $whereArray['tournament_translations.tournament_id'] = $input['tournament_id'];
        }

        $whiteStatusList = [
            0 => 1,
            1 => 2
        ];
        if(isset($input['status']) && in_array($input['status'], $whiteStatusList)){
            $whereArray['matches.status'] = array_search($input['status'], $whiteStatusList);
        }

        if(isset($input['date']) && !empty($input['date'])){
            $matches->where([
                ["matches.start_time", ">=", date("Y-m-d 00:00:00", strtotime($input['date']))],
                ["matches.start_time", "<=", date("Y-m-d 23:59:59", strtotime($input['date']))]
            ]);
        }

        if(isset($input['search']) && !empty($input['search'])){
            $matches->where(function ($query) use($input){
               $query->where("home_teams.name", "LIKE", '%'.$input["search"].'%')
                   ->orWhere("guest_teams.name", "LIKE", '%'.$input["search"].'%')
                   ->orWhere("tournament_translations.name", "LIKE", '%'.$input["search"].'%');
            });
        }

        if(isset($input['forHome']) && !empty($input['forHome'])){
            $matches->where([
                ["matches.start_time", ">=", date("Y-m-d 00:00:00")],
                ["matches.start_time", "<=", date("Y-m-d 23:59:59")]
            ]);

            $matches->limit(10);
        }

        $matches = $matches->where($whereArray)
            ->orderBy("start_time", "DESC")
            ->select("matches.id", "home_team_id", "home_teams_original.slug as home_team_slug", "home_teams.name as home_team_name", "home_team_goals_count",
                "guest_team_id", "guest_teams_original.slug as guest_team_slug", "guest_teams.name as guest_team_name", "guest_team_goals_count",
                "matches.start_time", "matches.status",
                "tournament_translations.tournament_id", "tournament_translations.name as tournament_name", "tournaments.logo_mini")
            ->paginate(10, ['*'], null, $page);

//        dd(DB::getQueryLog());

        $matchesByTournaments = $matches->groupBy("tournament_id");

        $responseMatches = [];
        foreach ($matchesByTournaments as $tournamentName => $matchesByTournament){
            $responseMatches[] = [
                "tournament" => [
                    "tournament_id" => $matchesByTournament[0]->tournament_id,
                    "tournament_name" => $matchesByTournament[0]->tournament_name,
                    "logo_mini" => $matchesByTournament[0]->logo_mini
                ],
                "matches" => $matchesByTournament
            ];
        }

//        dd(DB::getQueryLog());

        $data = [
            'code' => 0,
            'matches' => [
                'data' => $responseMatches,
                "per_page" => $matches->perPage(),
                "current_page" => $matches->currentPage(),
                "total" => $matches->total()

            ]
        ];

        return json_encode($data);
    }

    public function getMatchesThatHaveVideo(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $matches = DB::table("videos")
            ->leftJoin("videos__categories", "videos__categories.video_id", "=", "videos.id")
            ->leftJoin("matches", "matches.id", "=", "videos.match_id")
            ->leftJoin("team_translations as home_teams", "home_teams.team_id", "=", "matches.home_team_id")
            ->leftJoin("team_translations as guest_teams", "guest_teams.team_id", "=", "matches.guest_team_id")
            ->leftJoin("seasons__elements", "seasons__elements.id", "=", "matches.seasons__element_id")
            ->leftJoin("seasons", "seasons.id", "=", "seasons__elements.season_id")
            ->leftJoin("tournament_translations", function($join){
                $join->on("tournament_translations.tournament_id", "=", "seasons.tournament_id")
                    ->where('tournament_translations.locale', Lang::getLocale());
            })
            ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id");

        $input = Request::except('_token');

        if(isset($input['category_id']) && !empty($input['category_id'])){
            $matches = $matches->where(['videos__categories.category_id' => $input['category_id']]);
        }

        if(isset($input['tournament_id']) && !empty($input['tournament_id'])){
            $matches = $matches->where(['seasons.tournament_id' => $input['tournament_id']]);
        }

        if(isset($input['season_id']) && !empty($input['season_id'])){
            $matches = $matches->where(['seasons.id' => $input['season_id']]);
        }

        if(isset($input['stage_id']) && !empty($input['stage_id'])){
            $matches = $matches->where(['elements.stage_id' => $input['stage_id']]);
        }

        if(isset($input['team_id']) && !empty($input['team_id'])){
            $matches = $matches->where(['videos.team_id' => $input['team_id']]);
        }

        $matches = $matches->groupBy("matches.id")
            ->where([
                "home_teams.locale" => Lang::getLocale(),
                "guest_teams.locale" => Lang::getLocale()
            ])
            ->orderBy("start_time", "DESC")
            ->select("matches.*",
                "home_teams.name as home_team_name",
                "guest_teams.name as guest_team_name",
                "tournament_translations.name as tournament_name")
            ->paginate(10, ['*'], null, $page);

        $data = [
            'code' => 0,
            'matches' => $matches
        ];

        return json_encode($data);

    }
}
