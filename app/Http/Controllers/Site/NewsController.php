<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class NewsController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $news = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_active" => 1
            ]);

        $input = Request::except('_token');

        if(isset($input['searchWord']) && !empty($input['searchWord'])){
            $news = $news->where(function($query) use($input){
                $query->where("title", "LIKE", '%'.$input['searchWord'].'%')
                    ->orWhere("short_text", "LIKE", '%'.$input['searchWord'].'%')
                    ->orWhere("large_text", "LIKE", '%'.$input['searchWord'].'%');
            });
        }

        $news = $news->orderBy("creating_date", "DESC")
            ->orderBy("priority", "DESC")
            ->paginate(10, ['*'], null, $page);

        $data = [
            'code' => 0,
            'news' => $news
        ];

        return json_encode($data);
    }

    public function getSlidersNews(){

        $inHomeNews = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_in_slider" => 1,
                "is_active" => 1
            ])
            ->orderBy("creating_date", "DESC")
            ->orderBy("priority", "DESC")
            ->limit(5)
            ->get();

        $data = [
            'code' => 0,
            'inHomeNews' => $inHomeNews
        ];

        return json_encode($data);
    }

    //latest 5
    public function getLatest(){

        $latestNews = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_active" => 1
            ])
            ->orderBy("creating_date", "DESC")
            ->orderBy("priority", "DESC")
            ->limit(10)
            ->get();

        $data = [
            'code' => 0,
            'latestNews' => $latestNews
        ];

        return json_encode($data);
    }

    public function getTops(){

        $topNews = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_in_right" => 1,
                "is_active" => 1
            ])
            ->orderBy("creating_date", "DESC")
            ->orderBy("priority", "DESC")
            ->limit(5)
            ->get();

        $data = [
            'code' => 0,
            'topNews' => $topNews
        ];

        return json_encode($data);
    }

    public function getNewsItem(){

        $slug = Request::segment(4);

        $newsItem = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where([
                "locale" => Lang::getLocale(),
                "news_item_translations.slug" => $slug,
                "is_active" => 1
            ])
            ->select("news.*",
                "news_item_translations.id as news_item_translation_id", "news_item_translations.title", "news_item_translations.short_text",
                "news_item_translations.large_text", "news_item_translations.slug", "news_item_translations.views_count")
            ->first();

        if($newsItem){

            $newsCountriesArray = DB::table("news__countries")
                ->where([
                        "news__countries.news_item_id" => $newsItem->id
                ])
                ->get()
                ->pluck("country_id")
                ->toArray();

            $newsCategoriesArray = DB::table("news__categories")
                ->where([
                    "news__categories.news_item_id" => $newsItem->id
                ])
                ->get()
                ->pluck("category_id")
                ->toArray();

            $newsTeamsArray = DB::table("news__teams")
                ->where([
                    "news__teams.news_item_id" => $newsItem->id
                ])
                ->get()
                ->pluck("team_id")
                ->toArray();


            //query
            $otherNews = DB::table("news")
                ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
                ->leftJoin("news__countries", function($join) use($newsCountriesArray){
                    $join->on("news__countries.news_item_id", '=', "news.id")
                        ->whereIn("news__countries.country_id", $newsCountriesArray);
                })
                ->leftJoin("news__teams", function($join) use($newsTeamsArray){
                    $join->on("news__teams.news_item_id", '=', "news.id")
                        ->whereIn("news__teams.team_id", $newsTeamsArray);
                })
                ->leftJoin("news__categories", function($join) use($newsCategoriesArray){
                    $join->on("news__categories.news_item_id", '=', "news.id")
                        ->whereIn("news__categories.category_id", $newsCategoriesArray);
                })
                ->where((function ($query) use ($newsItem) {
                    $query->where([
                        "locale" => Lang::getLocale(),
                        ["news.id", "!=", $newsItem->id],
                        "is_active" => 1
                    ]);
                }))
                ->whereOr("news.tournament_id", $newsItem->tournament_id)
                ->orderBy("creating_date", "DESC")
                ->orderBy("priority", "DESC")
                ->limit(10)
                ->select("news.*",
                    "news_item_translations.news_item_id", "news_item_translations.title",
                    "news_item_translations.short_text", "news_item_translations.slug",
                    "news_item_translations.views_count")
                ->groupBy("news.id")
                ->get();

//            $newsItemIsViewed = DB::table("news_item_views")
//                ->where([
//                    "news_item_id" => $newsItem->news_item_translation_id,
//                    "ip_address" => Request::ip()
//                ])
//                ->first();
//
//            if(!$newsItemIsViewed){
//                DB::table("news_item_views")->insert([
//                    "news_item_id" => $newsItem->news_item_translation_id,
//                    "ip_address" => Request::ip(),
//                    "created_at" => date("Y-m-d H:i:s"),
//                ]);
//            }

            $result = DB::table("news_item_translations")
                ->where("id", $newsItem->news_item_translation_id)
                ->update([
                    "views_count" => DB::raw("`views_count` + 1")
                ]);

            $newsItem->views_count++;

            $data = [
                'code' => 0,
                'newsItem' => $newsItem,
                'otherNews' => $otherNews
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getNewsByCountry(){

        $slug = Request::segment(5);
        $page = Request::segment(6) != null ? Request::segment(6) : 1;

        if($slug){

            $typeName = DB::table("countries")
            ->leftJoin("country_translations", "country_translations.country_id", "=", "countries.id")
            ->where([
                "country_translations.locale" => Lang::getLocale(),
                "slug" => $slug
            ])
            ->first();

            if($typeName){

                $news = DB::table("news")
                    ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
                    ->leftJoin("news__countries", "news__countries.news_item_id", "=", "news.id")
                    ->leftJoin("tournaments", function($join) use($typeName){
                        $join->on("tournaments.id", '=', "news.tournament_id");
                        $join->on("tournaments.country_id", '=', DB::raw($typeName->id));
                    })
                    ->where([
                        "news_item_translations.locale" => Lang::getLocale(),
                        "news__countries.country_id" => $typeName->id,
                        "is_active" => 1
                    ])
                    ->orderBy("creating_date", "DESC")
                    ->orderBy("priority", "DESC")
                    ->select("news_item_translations.news_item_id", "news_item_translations.views_count", "title", "short_text", "news_item_translations.slug", "news.*")
                    ->paginate(10, ['*'], null, $page);
        
                $data = [
                    'code' => 0,
                    'news' => $news,
                    'typeName' => $typeName->name
                ];
            }
            else{
    
                $data = [
                    'code' => 404
                ];
            }
        }
        else{
    
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getNewsByCategory(){

        $slug = Request::segment(5);
        $page = Request::segment(6) != null ? Request::segment(6) : 1;

        if($slug){

            $typeName = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id")
            ->where([
                "category_translations.locale" => Lang::getLocale(),
                "slug" => $slug
            ])
            ->first();

            if($typeName){

                $news = DB::table("news")
                    ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
                    ->leftJoin("news__categories", "news__categories.news_item_id", "=", "news.id")
                    ->leftJoin("category_translations", "category_translations.category_id", "=", "news__categories.category_id")
                    ->where([
                        "news_item_translations.locale" => Lang::getLocale(),
                        "news__categories.category_id" => $typeName->id,
                        "category_translations.locale" => Lang::getLocale(),
                        "is_active" => 1
                    ])
                    ->orderBy("creating_date", "DESC")
                    ->orderBy("priority", "DESC")
                    ->select("news_item_translations.news_item_id", "news_item_translations.views_count", "title", "short_text", "news_item_translations.slug", "news.*")
                    ->paginate(10, ['*'], null, $page);
        
                $data = [
                    'code' => 0,
                    'news' => $news,
                    'typeName' => $typeName->name
                ];
            }
            else{
    
                $data = [
                    'code' => 404
                ];
            }
        }
        else{
    
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getNewsByTournament(){

        $slug = Request::segment(5);
        $page = Request::segment(6) != null ? Request::segment(6) : 1;

        if($slug){

            $typeName = DB::table("tournaments")
            ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "tournaments.id")
            ->where([
                "tournament_translations.locale" => Lang::getLocale(),
                "slug" => $slug
            ])
            ->first();

            if($typeName){

                $news = DB::table("news")
                    ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
                    ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "news.tournament_id")
                    ->where([
                        "news_item_translations.locale" => Lang::getLocale(),
                        "tournament_translations.locale" => Lang::getLocale(),
                        "news.tournament_id" => $typeName->id,
                        "is_active" => 1
                    ])
                    ->orderBy("creating_date", "DESC")
                    ->orderBy("priority", "DESC")
                    ->select("news_item_translations.news_item_id", "news_item_translations.views_count", "title", "short_text", "news_item_translations.slug", "news.*")
                    ->paginate(10, ['*'], null, $page);
        
                $data = [
                    'code' => 0,
                    'news' => $news,
                    'typeName' => $typeName->name
                ];
            }
            else{
    
                $data = [
                    'code' => 404
                ];
            }
        }
        else{
    
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getNewsBySeason(){

        $id = Request::segment(5);
        $page = Request::segment(6) != null ? Request::segment(6) : 1;

        if($id){

            $season = DB::table("seasons")
                ->where([
                    "id" => $id
                ])
                ->first();

            if($season){

                $news = DB::table("news")
                    ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
                    ->leftJoin("tournament_translations", "tournament_translations.tournament_id", "=", "news.tournament_id")
                    ->where([
                        "news_item_translations.locale" => Lang::getLocale(),
                        "tournament_translations.locale" => Lang::getLocale(),
                        "news.season_id" => $id,
                        "is_active" => 1
                    ])
                    ->orderBy("creating_date", "DESC")
                    ->orderBy("priority", "DESC")
                    ->select("news_item_translations.news_item_id", "news_item_translations.views_count", "title", "short_text", "news_item_translations.slug", "news.*")
                    ->paginate(10, ['*'], null, $page);

                $data = [
                    'code' => 0,
                    'news' => $news
                ];
            }
            else{

                $data = [
                    'code' => 404
                ];
            }
        }
        else{

            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getNewsByTeam(){

        $id = Request::segment(5);
        $page = Request::segment(6) != null ? Request::segment(6) : 1;

        if($id){

            $team = DB::table("teams")
                ->where([
                    "id" => $id
                ])
                ->first();

            if($team){

                $news = DB::table("news")
                    ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
                    ->leftJoin("news__teams", "news__teams.news_item_id", "=", "news.id")
                    ->where([
                        "news_item_translations.locale" => Lang::getLocale(),
                        "news__teams.team_id" => $id,
                        "is_active" => 1
                    ])
                    ->orderBy("creating_date", "DESC")
                    ->orderBy("priority", "DESC")
                    ->select("news_item_translations.news_item_id", "news_item_translations.views_count", "title", "short_text", "news_item_translations.slug", "news.*")
                    ->paginate(10, ['*'], null, $page);

                $data = [
                    'code' => 0,
                    'news' => $news
                ];
            }
            else{

                $data = [
                    'code' => 404
                ];
            }
        }
        else{

            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }

    public function getLastRightSlide(){

        $lastRightSlideNews = DB::table("news")
            ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_active" => 1
            ])
            ->orderBy("creating_date", "DESC")
            ->orderBy("priority", "DESC")
            ->limit(50)
            ->get();

        $data = [
            'code' => 0,
            'lastRightSlideNews' => $lastRightSlideNews
        ];

        return json_encode($data);
    }

    public function getNewsByAllFeaturedCategories(){

        $featuredCategories = DB::table("categories")
            ->leftJoin("category_translations", "category_translations.category_id", "=", "categories.id")
            ->where([
                "locale" => Lang::getLocale(),
                "is_featured" => 1
            ])
            ->get();

        if($featuredCategories->count() > 0){
            $data = [
                "code" => 0
            ];

            $tmpArr = [];
            foreach($featuredCategories as $featuredCategory){
                $news = DB::table("news")
                ->leftJoin("news_item_translations", "news_item_translations.news_item_id", "=", "news.id")
                    ->leftJoin("news__categories", "news__categories.news_item_id", "=", "news.id")
                ->where([
                    "locale" => Lang::getLocale(),
                    "is_active" => 1,
                    "category_id" => $featuredCategory->id
                ])
                ->orderBy("creating_date", "DESC")
                ->orderBy("priority", "DESC")
                ->limit(5)
                ->get();

                if($news->count() > 0){
                    $tmpArr[] = [
                        "category" => $featuredCategory,
                        "news" => $news
                    ];
                }
            }

            $data["byAllFeaturedCategoriesNews"] = $tmpArr;
        }
        else{
            $data = [
                'code' => 0,
                'byAllFeaturedCategoriesNews' => []
            ];
        }

        return json_encode($data);
    }
}
