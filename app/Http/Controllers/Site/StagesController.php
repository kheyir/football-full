<?php

namespace App\Http\Controllers\Site;

use Lang;
use DB;
use Request;
use Validator;

class StagesController extends SiteController{

    function __construct(){

        parent::__construct();

    }

    public function getAll(){

        $page = Request::segment(5) != null ? Request::segment(5) : 1;

        $stages = DB::table("stages")
            ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "stages.id");


        $input = Request::except('_token');

        if(isset($input['name']) && !empty($input['name'])){
            $stages = $stages->where([
                ['name', "LIKE", '%' . $input['name'] . '%']
            ]);
        }

        $stages = $stages
            ->where([
                "stage_translations.locale" => Lang::getLocale(),
            ])
            ->orderBy('stages.priority', 'DESC')
            ->orderBy('stage_translations.name', 'ASC')
            ->paginate(10, ['*'], null, $page);

        return response()->json([
            'code' => 0,
            'stages' => $stages
        ]);
    }

    public function getStagesBySeason(){

        $id = Request::segment(5);

        $season = DB::table("seasons")
            ->where([
                "seasons.id" => $id
            ])
            ->first();

        if($season){

            $stages = DB::table("seasons__elements")
                ->leftJoin("elements", "elements.id", "=", "seasons__elements.element_id")
                ->leftJoin("stages", "stages.id", "=", "elements.stage_id")
                ->leftJoin("stage_translations", "stage_translations.stage_id", "=", "elements.stage_id")
                ->where([
                    "seasons__elements.season_id" => $id,
                    "stage_translations.locale" => Lang::getLocale()
                ])
                ->orderBy("stages.priority", "ASC")
                ->distinct("stage_translations.stage_id", "stage_translations.name as stage_name", "stages.has_groups")
                ->select("stage_translations.stage_id", "stage_translations.name as stage_name", "stages.has_groups")
                ->get();

            $data = [
                'code' => 0,
                'stages' => $stages
            ];
        }
        else{
            $data = [
                'code' => 404
            ];
        }

        return json_encode($data);
    }
}
