<?php

namespace App\Libraries;

use DB;
use Google\Client;
use Revolution\Google\Sheets\Sheets;
use Google_Service_Sheets;

class HelpFunctions{

    public function getRandom($randomLength, $whichCaracters = 0){
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '0123456789';

        switch ($whichCaracters){
            case 1:
                $characters = $letters;
                break;
            case 2:
                $characters = $digits;
                break;
            default:
                $characters = $letters.$digits;
                break;
        }

        $randomText = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < $randomLength; $i++) {
            $randomText .= $characters[mt_rand(0, $max)];
        }

        return $randomText;
    }

    public function sendData($user, $data){
        $localsocket = 'tcp://127.0.0.1:1234';

        // соединяемся с локальным tcp-сервером
        $instance = stream_socket_client($localsocket);
        // отправляем сообщение
        fwrite($instance, json_encode(['user' => $user, 'data' => $data])  . "\n");
    }

    public function createAndSendNotification($userId, $notifType, $notifParameters, $messageData){
        $notificationData = [
            "user_client_id" => $userId,
            "type" => $notifType,
            "parameters" => json_encode($notifParameters),
            "created_at" => date("Y-m-d H:i:s")
        ];

        DB::table("notifications")->insert($notificationData);

        $this->sendData(DB::table("users_clients")->where("id", $userId)->first()->socket_key, $messageData);
    }

    public function pushLink($link){

        $client = new Client();
        $client->setApplicationName("google-sheets-for-footbal-az");
        $client->setDeveloperKey("AIzaSyC-GUzdJWCsYN1Vny88VEg7exVBuBd1lW8");
        $client->setScopes([Google_Service_Sheets::DRIVE, Google_Service_Sheets::SPREADSHEETS]);
        $client->setAuthConfig(public_path() . '/assets/admin/assets/files/cred.json');
        // setup Google Client

        $service = new Google_Service_Sheets($client);

        $sheets = new Sheets();
        $sheets->setService($service);

        $a = $sheets->spreadsheet('1mR3gUfL0y_sAfBT8ui9xF233VMYNPscGC4s_JSj-HiY')->sheet('mainList')->range('A1')->update([[$link]]);
    }
}