-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 01, 2020 at 11:27 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `football`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `is_featured` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `is_featured`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_translations`
--

CREATE TABLE `category_translations` (
  `category_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_translations`
--

INSERT INTO `category_translations` (`category_id`, `name`, `locale`) VALUES
(1, 'Millimiz', 'az'),
(1, 'Национальная сборная', 'ru'),
(1, 'National', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`) VALUES
(1, 3),
(2, 3),
(3, 6),
(4, 8),
(5, 7),
(6, 1),
(7, 2),
(8, 4),
(9, 5);

-- --------------------------------------------------------

--
-- Table structure for table `city_translations`
--

CREATE TABLE `city_translations` (
  `city_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city_translations`
--

INSERT INTO `city_translations` (`city_id`, `name`, `locale`) VALUES
(1, 'Barselona', 'az'),
(1, 'Барселона', 'ru'),
(1, 'Barcelona', 'en'),
(2, 'Madrid', 'az'),
(2, 'Мадрид', 'ru'),
(2, 'Madrid', 'en'),
(3, 'Turin', 'az'),
(3, 'Турин', 'ru'),
(3, 'Turin', 'en'),
(4, 'Münhen', 'az'),
(4, 'Мюнхен', 'ru'),
(4, 'Munich', 'en'),
(5, 'Paris', 'az'),
(5, 'Париж', 'ru'),
(5, 'Paris', 'en'),
(6, 'Bakı', 'az'),
(6, 'Баку', 'ru'),
(6, 'Baku', 'en'),
(7, 'İstanbul', 'az'),
(7, 'Истанбул', 'ru'),
(7, 'Istanbul', 'en'),
(8, 'Liverpul', 'az'),
(8, 'Ливерпуль', 'ru'),
(8, 'Liverpool', 'en'),
(9, 'Sankt-Peterburq', 'az'),
(9, 'Санкт-Петербург', 'ru'),
(9, 'Saint Petersburg', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `in_head` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `in_head`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `country_translations`
--

CREATE TABLE `country_translations` (
  `country_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country_translations`
--

INSERT INTO `country_translations` (`country_id`, `name`, `locale`) VALUES
(1, 'Azərbaycan', 'az'),
(1, 'Азербайджан', 'ru'),
(1, 'Azerbaijan', 'en'),
(2, 'Türkiyə', 'az'),
(2, 'Турция', 'ru'),
(2, 'Turkey', 'en'),
(3, 'İspaniya', 'az'),
(3, 'Испания', 'ru'),
(3, 'Spain', 'en'),
(4, 'İngiltərə', 'az'),
(4, 'Англия', 'ru'),
(4, 'England', 'en'),
(5, 'Rusiya', 'az'),
(5, 'Россия', 'ru'),
(5, 'Russia', 'en'),
(6, 'İtaliya', 'az'),
(6, 'Италия', 'ru'),
(6, 'Italy', 'en'),
(7, 'Fransa', 'az'),
(7, 'Франция', 'ru'),
(7, 'France', 'en'),
(8, 'Almaniya', 'az'),
(8, 'Германия', 'ru'),
(8, 'Germany', 'en'),
(9, 'Arqentina', 'az'),
(9, 'Аргентина', 'ru'),
(9, 'Argentina', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `locale`) VALUES
(1, 'az'),
(2, 'ru'),
(3, 'en');

-- --------------------------------------------------------

--
-- Table structure for table `tournaments`
--

CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL,
  `logo_mini` varchar(200) NOT NULL,
  `logo_big` varchar(200) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `is_in_list` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tournaments`
--

INSERT INTO `tournaments` (`id`, `logo_mini`, `logo_big`, `country_id`, `is_in_list`) VALUES
(1, 'liga_topaz.png', 'liga_topaz_big.png', 1, 1),
(2, 'Turkcell_Super_Tournament_logo_mini.png', 'Turkcell_Super_Tournament_logo_big.png', 2, 1),
(3, 'liga_spain.png', 'la_tournament.png', 3, 1),
(4, 'liga_england.png', 'England_Premier_Tournament_Logo.svg.png', 4, 1),
(5, 'russian_premiere_tournament_mini.png', 'russian_premiere_tournament_big.png', 5, 1),
(6, 'seria_a_mini.png', 'seria_a_big.jpg', 6, 1),
(7, 'france_tournament_mini.png', 'france_tournament_big.png', 7, 1),
(8, 'germany_liga_mini.png', 'germany_liga_big.png', 8, 1),
(9, 'liga_champions.png', '800px-UEFA_Champions_Tournament_logo.svg_big.png', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tournament_translations`
--

CREATE TABLE `tournament_translations` (
  `tournament_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tournament_translations`
--

INSERT INTO `tournament_translations` (`tournament_id`, `name`, `locale`) VALUES
(1, 'Topaz Premyer Liqası', 'az'),
(1, 'Премьер Лига Топаз', 'ru'),
(1, 'Premier Tournament Topaz', 'en'),
(2, 'Türkiye Süper Lig', 'az'),
(2, 'Турецкая Супер Лига', 'ru'),
(2, 'Turkey Super Tournament', 'en'),
(3, 'La liga', 'az'),
(3, 'Ла Лига', 'ru'),
(3, 'La Liga', 'en'),
(4, 'İngiltərə Premyer Liqası', 'az'),
(4, 'Английская Премьер Лига', 'ru'),
(4, 'English Premiere Tournament', 'en'),
(5, 'Rusiya Premyer Liqası', 'az'),
(5, 'Российская Пресьер-Лига', 'ru'),
(5, 'Russian Premiere Tournament', 'en'),
(6, 'A Seriyası', 'az'),
(6, 'Серия А', 'ru'),
(6, 'Serie A', 'en'),
(7, 'Liqa 1', 'az'),
(7, 'Лига 1', 'ru'),
(7, 'Ligue 1', 'en'),
(8, 'Bundesliqa', 'az'),
(8, 'Бундеслига', 'ru'),
(8, 'Bundesliga', 'en'),
(9, 'UEFA Çempionlar liqası', 'az'),
(9, 'Лига чемпионов УЕФА ', 'ru'),
(9, 'UEFA Champions Tournament', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `stadium_id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `tour` int(3) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `home_team_id` int(11) NOT NULL,
  `guest_team_id` int(11) NOT NULL,
  `home_team_goals_count` int(3) NOT NULL,
  `guest_team_goals_count` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '{''not_started'': 0, ''ended'': 1, ''goes'': 2 }'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`id`, `stadium_id`, `season_id`, `tour`, `start_time`, `end_time`, `home_team_id`, `guest_team_id`, `home_team_goals_count`, `guest_team_goals_count`, `status`) VALUES
(1, 1, 3, 1, '2020-03-16 21:00:00', '2020-03-16 22:50:00', 1, 2, 2, 1, 1),
(2, 3, 9, 1, '2020-03-16 21:00:00', '2020-03-16 22:50:00', 3, 4, 1, 1, 1),
(3, 5, 9, 1, '2020-03-16 21:00:00', '2020-03-16 22:50:00', 5, 6, 1, 0, 1),
(4, 7, 9, 1, '2020-03-16 21:00:00', '2020-03-16 22:50:00', 7, 8, 0, 3, 1),
(5, 9, 9, 1, '2020-03-21 21:00:00', '2020-03-21 22:50:00', 8, 9, 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `creating_date` datetime NOT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `is_in_home` tinyint(1) NOT NULL DEFAULT '0',
  `is_in_top` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `image`, `creating_date`, `tournament_id`, `category_id`, `is_in_home`, `is_in_top`, `is_active`) VALUES
(1, 'additional_news_1.png', '2020-03-22 02:14:28', NULL, 1, 1, 0, 1),
(2, 'additional_news_1.png', '2020-03-12 02:14:28', 1, NULL, 0, 1, 1),
(3, 'additional_news_2.png', '2020-03-22 02:14:28', 2, NULL, 0, 0, 1),
(4, 'additional_news_3.png', '2020-03-15 02:14:28', 3, NULL, 1, 0, 1),
(5, 'additional_news_1.png', '2020-03-22 02:14:28', 4, NULL, 0, 1, 1),
(6, 'additional_news_2.png', '2020-03-15 02:14:28', 7, NULL, 1, 0, 1),
(7, 'additional_news_2.png', '2020-03-12 02:14:28', 5, NULL, 0, 1, 1),
(8, 'additional_news_3.png', '2020-03-19 02:14:28', 6, NULL, 0, 1, 1),
(9, 'additional_news_3.png', '2020-03-22 02:14:28', 8, NULL, 0, 0, 1),
(10, 'additional_news_2.png', '2020-03-15 02:14:28', NULL, 1, 0, 0, 1),
(11, 'additional_news_2.png', '2020-03-10 05:11:00', 1, NULL, 0, 0, 1),
(12, 'additional_news_3.png', '2020-03-17 02:15:00', 1, NULL, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_item_translations`
--

CREATE TABLE `news_item_translations` (
  `id` int(11) NOT NULL,
  `news_item_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `short_text` text NOT NULL,
  `large_text` text NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_item_translations`
--

INSERT INTO `news_item_translations` (`id`, `news_item_id`, `title`, `short_text`, `large_text`, `locale`) VALUES
(1, 1, 'Bu gün Topaz Premyer Liqasında 20-ci tura start veriləcək', 'ilk oyun g&uuml;n&uuml;ndə &quot;Keşlə&quot; &ouml;z meydanında &quot;Qəbələ&quot;ni qəbul edəcək.', '<p>ilk oyun g&uuml;n&uuml;ndə &quot;Keşlə&quot; &ouml;z meydanında &quot;Qəbələ&quot;ni qəbul edəcək. \r\nRoman Qriqor&ccedil;ukun komandası 2-ci pillədəki m&ouml;vqeyini m&ouml;hkəmləndirmək &uuml;&ccedil;&uuml;n səfərdən qələbə ilə qayıtmaq niyyətindədir. Paytaxt təmsil&ccedil;isi isə qalib gəldiyi halda bir pillə y&uuml;ksələ bilər.\r\nBu komandalar arasında baş tutacaq oyun saat 15:30-da başlayacaq.\r\n\r\nTurun daha &uuml;&ccedil; oyunu sabah ke&ccedil;iriləcək.</p>', 'az'),
(2, 1, 'ru Bu gün Topaz Premyer Liqasında 20-ci tura start veriləcək', 'ru ilk oyun g&uuml;n&uuml;ndə &quot;Keşlə&quot; &ouml;z meydanında &quot;Qəbələ&quot;ni qəbul edəcək.', '<p>ru ilk oyun g&uuml;n&uuml;ndə &quot;Keşlə&quot; &ouml;z meydanında &quot;Qəbələ&quot;ni qəbul edəcək. \r\nRoman Qriqor&ccedil;ukun komandası 2-ci pillədəki m&ouml;vqeyini m&ouml;hkəmləndirmək &uuml;&ccedil;&uuml;n səfərdən qələbə ilə qayıtmaq niyyətindədir. Paytaxt təmsil&ccedil;isi isə qalib gəldiyi halda bir pillə y&uuml;ksələ bilər.\r\nBu komandalar arasında baş tutacaq oyun saat 15:30-da başlayacaq.\r\n\r\nTurun daha &uuml;&ccedil; oyunu sabah ke&ccedil;iriləcək.</p>', 'ru'),
(3, 1, 'en Bu gün Topaz Premyer Liqasında 20-ci tura start veriləcək', 'en ilk oyun g&uuml;n&uuml;ndə &quot;Keşlə&quot; &ouml;z meydanında &quot;Qəbələ&quot;ni qəbul edəcək.', '<p>en ilk oyun g&uuml;n&uuml;ndə &quot;Keşlə&quot; &ouml;z meydanında &quot;Qəbələ&quot;ni qəbul edəcək. \r\nRoman Qriqor&ccedil;ukun komandası 2-ci pillədəki m&ouml;vqeyini m&ouml;hkəmləndirmək &uuml;&ccedil;&uuml;n səfərdən qələbə ilə qayıtmaq niyyətindədir. Paytaxt təmsil&ccedil;isi isə qalib gəldiyi halda bir pillə y&uuml;ksələ bilər.\r\nBu komandalar arasında baş tutacaq oyun saat 15:30-da başlayacaq.\r\n\r\nTurun daha &uuml;&ccedil; oyunu sabah ke&ccedil;iriləcək.</p>', 'en'),
(4, 2, 'Topaz Premyer Liqasında 20-ci turun təyinatları dərc edilib', 'Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək.', '<p>Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək. Turun oyunları 13-14 mart tarixlərində keçiriləcək.\r\n\r\nTəyinatlar belədir :\r\n\r\n13 mart \r\n\r\n“Keşlə” – “Qəbələ”\r\nHakimlər: Ömər Paşayev, Namiq Hüseynov, Mübariz Haşımov, Elvin Əsgərov \r\n“İnter Arena”, 15:30 \r\n\r\n14 mart \r\n\r\n“Sumqayıt” – “Kəpəz”\r\nHakimlər: Əliyar Ağayev, Rza Məmmədov, Elşən Acalov, Ramil Diniyev \r\n“Kapital Bank Arena”, 20:00 \r\n\r\n“Neftçi” – “Qarabağ”\r\nHakimlər: İnqilab Məmmədov, Vaqif Musayev, Akif Əmirəli, Rəhim Həsənov \r\n“Bakcell Arena”, 18:00 \r\n\r\n“Zirə” – “Səbail”\r\nHakimlər: Orxan Məmmədov, Vüqar Bayramov, Rahil Ramazanov, Rauf Cabbarov \r\nZirə İdman Kompleksinin stadionu, 17:00</p>', 'az'),
(5, 2, 'ru Topaz Premyer Liqasında 20-ci turun təyinatları dərc edilib', 'Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək.', '<p>ru Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək. Turun oyunları 13-14 mart tarixlərində keçiriləcək.\r\n\r\nTəyinatlar belədir :\r\n\r\n13 mart \r\n\r\n“Keşlə” – “Qəbələ”\r\nHakimlər: Ömər Paşayev, Namiq Hüseynov, Mübariz Haşımov, Elvin Əsgərov \r\n“İnter Arena”, 15:30 \r\n\r\n14 mart \r\n\r\n“Sumqayıt” – “Kəpəz”\r\nHakimlər: Əliyar Ağayev, Rza Məmmədov, Elşən Acalov, Ramil Diniyev \r\n“Kapital Bank Arena”, 20:00 \r\n\r\n“Neftçi” – “Qarabağ”\r\nHakimlər: İnqilab Məmmədov, Vaqif Musayev, Akif Əmirəli, Rəhim Həsənov \r\n“Bakcell Arena”, 18:00 \r\n\r\n“Zirə” – “Səbail”\r\nHakimlər: Orxan Məmmədov, Vüqar Bayramov, Rahil Ramazanov, Rauf Cabbarov \r\nZirə İdman Kompleksinin stadionu, 17:00</p>', 'ru'),
(6, 2, 'en Topaz Premyer Liqasında 20-ci turun təyinatları dərc edilib', 'Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək.', '<p>en Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək. Turun oyunları 13-14 mart tarixlərində keçiriləcək.\r\n\r\nTəyinatlar belədir :\r\n\r\n13 mart \r\n\r\n“Keşlə” – “Qəbələ”\r\nHakimlər: Ömər Paşayev, Namiq Hüseynov, Mübariz Haşımov, Elvin Əsgərov \r\n“İnter Arena”, 15:30 \r\n\r\n14 mart \r\n\r\n“Sumqayıt” – “Kəpəz”\r\nHakimlər: Əliyar Ağayev, Rza Məmmədov, Elşən Acalov, Ramil Diniyev \r\n“Kapital Bank Arena”, 20:00 \r\n\r\n“Neftçi” – “Qarabağ”\r\nHakimlər: İnqilab Məmmədov, Vaqif Musayev, Akif Əmirəli, Rəhim Həsənov \r\n“Bakcell Arena”, 18:00 \r\n\r\n“Zirə” – “Səbail”\r\nHakimlər: Orxan Məmmədov, Vüqar Bayramov, Rahil Ramazanov, Rauf Cabbarov \r\nZirə İdman Kompleksinin stadionu, 17:00</p>', 'en'),
(7, 3, 'Super Liq az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(8, 3, 'Super Liq ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(9, 3, 'Super Liq en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en'),
(10, 4, 'La Liq az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(11, 4, 'La Liq ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(12, 4, 'La Liq en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en'),
(13, 5, 'England az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(14, 5, 'England ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(15, 5, 'England en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en'),
(16, 6, 'Russia az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(17, 6, 'Russia ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(18, 6, 'Russia en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en');
INSERT INTO `news_item_translations` (`id`, `news_item_id`, `title`, `short_text`, `large_text`, `locale`) VALUES
(19, 7, 'Seria A az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(20, 7, 'Seria A ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(21, 7, 'Seria A en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en'),
(22, 8, 'France az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(23, 8, 'France ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(24, 8, 'France en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en'),
(25, 9, 'Bundesliga az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(26, 9, 'Bundesliga ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(27, 9, 'Bundesliga en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en'),
(28, 10, 'Millimiz 2 az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'az'),
(29, 10, 'Millimiz 2 ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'ru'),
(30, 10, 'Millimiz 2 en Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'en Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> en İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'en'),
(31, 11, 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'az'),
(32, 11, 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'ru'),
(33, 11, 'TOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'en'),
(34, 12, 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'az'),
(35, 12, 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'ru'),
(36, 12, 'TOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ EN AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `country_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `date_of_birthday` date NOT NULL,
  `height` int(3) DEFAULT NULL,
  `weight` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `photo`, `country_id`, `team_id`, `position_id`, `date_of_birthday`, `height`, `weight`) VALUES
(1, 'Lionel_Messi_20180626.jpg', 9, 1, 10, '1987-06-24', 170, 72),
(2, 'James_Rodriguez.jpg', 3, 2, 10, '1991-06-19', 180, 78),
(3, 'Federico_Bernardeschi_2_(cropped).jpg', 6, 3, 12, '1994-02-16', 185, 77),
(4, 'Muller.jpg', 8, 4, 6, '1989-09-13', 186, 75),
(5, 'Mbappe.jpg', 7, 5, 10, '1998-12-20', 178, 73),
(6, 'haciagaaa.png', 1, 6, 6, '1998-09-16', 170, 72),
(7, 'Adem_buyuk.jpg', 2, 7, 10, '1987-08-30', 177, 76),
(8, 'p15157.png', 4, 8, 7, '1986-01-04', 176, 70),
(9, 'Zenit-Slavia_Pr_(7).jpg', 5, 9, 10, '1988-08-22', 194, 88);

-- --------------------------------------------------------

--
-- Table structure for table `player_translations`
--

CREATE TABLE `player_translations` (
  `player_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player_translations`
--

INSERT INTO `player_translations` (`player_id`, `name`, `locale`) VALUES
(1, 'Lionel Messi', 'az'),
(1, 'Лионель Месси', 'ru'),
(1, 'Lionel Messi', 'en'),
(2, 'Xames Rodriqes', 'az'),
(2, 'Хамес Родригес', 'ru'),
(2, 'James David Rodríguez Rubio', 'en'),
(3, 'Federiko Bernardesçi', 'az'),
(3, 'Федерико Бернардески', 'ru'),
(3, 'Federico Bernardeschi', 'en'),
(4, 'Tomas Müller', 'az'),
(4, 'Томас Мюллер', 'ru'),
(4, 'Thomas Müller', 'en'),
(5, 'Kilian Mbappe', 'az'),
(5, 'Килиан Мбаппе', 'ru'),
(5, 'Kylian Mbappe', 'en'),
(6, 'Hacıağa Hacılı', 'az'),
(6, 'Гаджиага Гаджилы', 'ru'),
(6, 'Hajiaga Hajili', 'en'),
(7, 'Adem Büyük', 'az'),
(7, 'Адем Бююк', 'ru'),
(7, 'Adem Buyuk', 'en'),
(8, 'Jeyms Filip Milner', 'az'),
(8, 'Джеймс Филип Милнер', 'ru'),
(8, 'James Philip Milner', 'en'),
(9, 'Artyom Dzyuba', 'az'),
(9, 'Артем Дзюба', 'ru'),
(9, 'Artem Dzyuba', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12);

-- --------------------------------------------------------

--
-- Table structure for table `position_translations`
--

CREATE TABLE `position_translations` (
  `position_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `position_translations`
--

INSERT INTO `position_translations` (`position_id`, `name`, `locale`) VALUES
(1, 'Qapıçı', 'az'),
(1, 'Вратарь', 'ru'),
(1, 'Goalkeeper', 'en'),
(2, 'Mərkəzi müdafiəçi', 'az'),
(2, 'Центральный защитник', 'ru'),
(2, 'Center Back', 'en'),
(3, 'Cinah müdafiəçisi', 'az'),
(3, 'Фланговый защитник', 'ru'),
(3, 'Fullback', 'en'),
(4, 'Kənar müdafiəçi', 'az'),
(4, 'Крайний защитник', 'ru'),
(4, 'Wingback', 'en'),
(5, 'Azad müdafiəçi (libero)', 'az'),
(5, 'Свободный защитник (либеро)', 'ru'),
(5, 'Sweeper', 'en'),
(6, 'Mərkəzi yarımmüdafiəçi', 'az'),
(6, 'Центральный полузащитник', 'ru'),
(6, 'Centre midfield', 'en'),
(7, 'Mərkəzi müdafiə yarımmüdafiəçisi', 'az'),
(7, 'Центральный опорный полузащитник', 'ru'),
(7, 'Defensive midfield', 'en'),
(8, 'Hücum edən yarımmüdafiəçi', 'az'),
(8, 'Атакующий полузащитник', 'ru'),
(8, 'Attacking midfield', 'en'),
(9, 'Cinah yarımmüdafiəçi', 'az'),
(9, 'Фланговый полузащитник', 'ru'),
(9, 'Wide midfield', 'en'),
(10, 'Mərkəz hücümçusu', 'az'),
(10, 'Центральный нападающий', 'ru'),
(10, 'Centre forward', 'en'),
(11, 'İkinci hücümçu', 'az'),
(11, 'Второй нападающий', 'ru'),
(11, 'Second striker', 'en'),
(12, 'Vinger', 'az'),
(12, 'Вингер', 'ru'),
(12, 'Winger', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `seasons`
--

CREATE TABLE `seasons` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `start_year` int(4) NOT NULL,
  `end_year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seasons`
--

INSERT INTO `seasons` (`id`, `tournament_id`, `start_year`, `end_year`) VALUES
(1, 1, 2019, 2020),
(2, 2, 2019, 2020),
(3, 3, 2019, 2020),
(4, 4, 2019, 2020),
(5, 5, 2019, 2020),
(6, 6, 2019, 2020),
(7, 7, 2019, 2020),
(8, 8, 2019, 2020),
(9, 9, 2019, 2020);

-- --------------------------------------------------------

--
-- Table structure for table `stadiums`
--

CREATE TABLE `stadiums` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `size` int(10) DEFAULT NULL,
  `games_count` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stadiums`
--

INSERT INTO `stadiums` (`id`, `city_id`, `size`, `games_count`) VALUES
(1, 1, 99354, 0),
(2, 2, 81044, 0),
(3, 3, 41507, 0),
(4, 4, 75000, 0),
(5, 5, 47929, 0),
(6, 6, 5800, 0),
(7, 7, 52223, 0),
(8, 8, 54074, 0),
(9, 9, 67800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stadium_translations`
--

CREATE TABLE `stadium_translations` (
  `stadium_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stadium_translations`
--

INSERT INTO `stadium_translations` (`stadium_id`, `name`, `locale`) VALUES
(1, 'Kemp Nou', 'az'),
(1, 'Кемп Ноу', 'ru'),
(1, 'Camp Nou', 'en'),
(2, 'Santyaqo Bernabeu', 'az'),
(2, 'Сантьяго Бернабеу', 'ru'),
(2, 'Santiago Bernabeu', 'en'),
(3, 'Yuventus Stadionu', 'az'),
(3, 'Альянц Стадиум', 'ru'),
(3, 'Juventus Stadium', 'en'),
(4, 'Allianz Arena', 'az'),
(4, 'Альянц Арена', 'ru'),
(4, 'Allianz Arena', 'en'),
(5, 'Prensler Parkı', 'az'),
(5, 'Парк де Пренс', 'ru'),
(5, 'Parc des Princes', 'en'),
(6, 'Azərsun Arena', 'az'),
(6, 'Азерсун Арена', 'ru'),
(6, 'Azersun Arena', 'en'),
(7, 'Türk Telekom Stadionu', 'az'),
(7, 'Тюрк Телеком Арена', 'ru'),
(7, 'Türk Telekom Stadium', 'en'),
(8, 'Enfild', 'az'),
(8, 'Anfield', 'en'),
(8, 'Энфилд', 'ru'),
(9, 'Qazprom Arena', 'az'),
(9, 'Газпром Арена', 'ru'),
(9, 'Gazprom Arena', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `country_id` int(11) NOT NULL,
  `stadion_id` int(11) DEFAULT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `logo`, `country_id`, `stadion_id`, `year`) VALUES
(1, 'FC_Barcelona.svg', 3, 1, 1899),
(2, 'Real_Madrid_CF.svg', 3, 2, 1947),
(3, 'Juventus_FC_2017_logo.svg', 6, 3, 1897),
(4, 'FC_Bayern_München_logo_(2017).svg', 8, 4, 1900),
(5, 'Paris_Saint-Germain_F.C..svg', 7, 5, 1970),
(6, 'Qarabag_FK_logo.svg', 1, 6, 1951),
(7, 'Galatasaray_Star_Logo.svg', 2, 7, 1905),
(8, 'Liverpool_FC.svg', 4, 8, 1892),
(9, 'FC_Zenit_1_star_2015_logo.png', 5, 9, 1925);

-- --------------------------------------------------------

--
-- Table structure for table `team_translations`
--

CREATE TABLE `team_translations` (
  `team_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_translations`
--

INSERT INTO `team_translations` (`team_id`, `name`, `locale`) VALUES
(1, 'FK Barselona', 'az'),
(1, 'ФК Барселона', 'ru'),
(1, 'FC Barcelona', 'en'),
(2, 'Real Madrid FK', 'az'),
(2, 'Реал Мадрид ФК', 'ru'),
(2, 'Real Madrid CF', 'en'),
(3, 'Yuventus FK', 'az'),
(3, 'ФК Ювентус', 'ru'),
(3, 'Juventus F.C.', 'en'),
(4, 'Bayern Münhen FK', 'az'),
(4, 'ФК Бавария', 'ru'),
(4, 'FC Bayern Munich', 'en'),
(5, 'Paris Sen-Jermen FK', 'az'),
(5, 'Пари Сен-Жермен', 'ru'),
(5, 'Paris Saint-Germain F.C.', 'en'),
(6, 'Qarabağ FK', 'az'),
(6, 'ФК Карабах', 'ru'),
(6, 'Qarabagh FC', 'en'),
(7, 'Galatasaray S.K.', 'az'),
(7, 'С.К. Галатасарай ', 'ru'),
(7, 'Galatasaray S.C.', 'en'),
(8, 'Liverpul F.K.', 'az'),
(8, 'Ф.К. Ливерпуль', 'ru'),
(8, 'Liverpool F.C.', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `minute` int(3) NOT NULL,
  `name` varchar(200) NOT NULL,
  `preview_image` varchar(200) NOT NULL,
  `is_top_goal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `match_id`, `player_id`, `team_id`, `minute`, `name`, `preview_image`, `is_top_goal`) VALUES
(1, 1, 1, 1, 36, 'd2ccb44b4b8807919df130467b09e00a.mp4', 'video_1.png', 1),
(2, 1, 2, 2, 45, '0a857f3afe33d719eda34c721be4e45f.mp4', 'video_2.png', 1),
(3, 2, 3, 3, 43, 'a1b1eaf60b0173bc566cb84996832b3f.mp4', 'video_3.png', 1),
(4, 2, 4, 4, 12, 'ceeb0536759923c53bb3fb9ecfe23dbe.mp4', 'video_4.png', 1),
(5, 3, 5, 5, 90, 'a5ab3d85f7020698a21430eaf61e984b.mp4', 'video_5.png', 1),
(6, 3, 6, 6, 25, 'b6abc74bb04d9f5c7d3f8e1d397d88f2.mp4', 'video_6.png', 1),
(7, 4, 7, 7, 78, '668772fea891fd4da2f15e443789ac36.mp4', 'video_1.png', 1),
(8, 4, 8, 8, 67, '378dee2429fba641c42c3e0c20a3d416.mp4', 'video_2.png', 1),
(9, 5, 8, 8, 65, 'e45eb925c8acbe49b2b066d58d98b325.mp4', 'video_3.png', 1),
(10, 5, 9, 9, 28, '93e5c79f045494d7ca14e8c15dc0c1eb.mp4', 'video_4.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_item_translations`
--
ALTER TABLE `news_item_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stadiums`
--
ALTER TABLE `stadiums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `news_item_translations`
--
ALTER TABLE `news_item_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `seasons`
--
ALTER TABLE `seasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `stadiums`
--
ALTER TABLE `stadiums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
