-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 13 2020 г., 22:49
-- Версия сервера: 5.6.47
-- Версия PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `football_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `is_news_category` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL,
  `is_for_video` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `slug`, `is_news_category`, `is_featured`, `is_for_video`) VALUES
(1, 'millimiz', 1, 1, 1),
(2, 'avrokuboklar', 1, 0, 1),
(3, 'olkedaxili-turnirler', 1, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `category_translations`
--

CREATE TABLE `category_translations` (
  `category_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category_translations`
--

INSERT INTO `category_translations` (`category_id`, `name`, `locale`) VALUES
(1, 'Millimiz', 'az'),
(1, 'Национальная сборная', 'ru'),
(2, 'Avrokuboklar', 'az'),
(2, 'Еврокубки', 'ru'),
(3, 'Ölkədaxili turnirlər', 'az'),
(3, 'Национальные турниры', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `country_id`) VALUES
(1, 3),
(2, 3),
(3, 6),
(4, 8),
(5, 7),
(6, 1),
(7, 2),
(8, 4),
(9, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `city_translations`
--

CREATE TABLE `city_translations` (
  `city_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `city_translations`
--

INSERT INTO `city_translations` (`city_id`, `name`, `locale`) VALUES
(1, 'Barselona', 'az'),
(1, 'Барселона', 'ru'),
(1, 'Barcelona', 'en'),
(2, 'Madrid', 'az'),
(2, 'Мадрид', 'ru'),
(2, 'Madrid', 'en'),
(3, 'Turin', 'az'),
(3, 'Турин', 'ru'),
(3, 'Turin', 'en'),
(4, 'Münhen', 'az'),
(4, 'Мюнхен', 'ru'),
(4, 'Munich', 'en'),
(5, 'Paris', 'az'),
(5, 'Париж', 'ru'),
(5, 'Paris', 'en'),
(6, 'Bakı', 'az'),
(6, 'Баку', 'ru'),
(6, 'Baku', 'en'),
(7, 'İstanbul', 'az'),
(7, 'Истанбул', 'ru'),
(7, 'Istanbul', 'en'),
(8, 'Liverpul', 'az'),
(8, 'Ливерпуль', 'ru'),
(8, 'Liverpool', 'en'),
(9, 'Sankt-Peterburq', 'az'),
(9, 'Санкт-Петербург', 'ru'),
(9, 'Saint Petersburg', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `coaches`
--

CREATE TABLE `coaches` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `coaches`
--

INSERT INTO `coaches` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9);

-- --------------------------------------------------------

--
-- Структура таблицы `coach_translations`
--

CREATE TABLE `coach_translations` (
  `coach_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `coach_translations`
--

INSERT INTO `coach_translations` (`coach_id`, `name`, `locale`) VALUES
(1, 'Kike Setyen', 'az'),
(1, 'Кике Сетьен', 'ru'),
(1, 'Quique Setien', 'en'),
(2, 'Zinəddin Zidan', 'az'),
(2, 'Зинедин Зидан', 'ru'),
(2, 'Zinedine Zidane', 'en'),
(3, 'Mauritsio Sarri', 'az'),
(3, 'Маурицио Сарри', 'ru'),
(3, 'Maurizio Sarri', 'en'),
(4, 'Hans-Diter Flik', 'az'),
(4, 'Ханс-Дитер Флик', 'ru'),
(4, 'Hans-Dieter Flick', 'en'),
(5, 'Tomas Tuxel', 'az'),
(5, 'Томас Тухель', 'ru'),
(5, 'Thomas Tuchel', 'en'),
(6, 'Qurban Qurbanov', 'az'),
(6, 'Гурбан Гурбанов', 'ru'),
(6, 'Gurban Gurbanov', 'en'),
(7, 'Fatih Terim', 'az'),
(7, 'Фатих Терим', 'ru'),
(7, 'Fatih Terim', 'en'),
(8, 'Yurgen Klopp', 'az'),
(8, 'Юрген Клопп', 'ru'),
(8, 'Jürgen Klopp', 'en'),
(9, 'Sergey Semak', 'az'),
(9, 'Сергей Семак', 'ru'),
(9, 'Sergei Semak', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `flag` varchar(200) DEFAULT NULL,
  `is_news_category` tinyint(1) NOT NULL,
  `in_head` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `countries`
--

INSERT INTO `countries` (`id`, `slug`, `flag`, `is_news_category`, `in_head`) VALUES
(1, 'azerbaycan', NULL, 1, 0),
(2, 'turkiye', NULL, 1, 1),
(3, 'ispaniya', NULL, 1, 1),
(4, 'ingiltere', NULL, 1, 1),
(5, 'rusiya', NULL, 1, 1),
(6, 'italiya', NULL, 1, 1),
(7, 'fransa', NULL, 1, 1),
(8, 'almaniya', NULL, 1, 1),
(9, 'arqentina', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `country_translations`
--

CREATE TABLE `country_translations` (
  `country_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `country_translations`
--

INSERT INTO `country_translations` (`country_id`, `name`, `locale`) VALUES
(1, 'Azərbaycan', 'az'),
(1, 'Азербайджан', 'ru'),
(2, 'Türkiyə', 'az'),
(2, 'Турция', 'ru'),
(3, 'İspaniya', 'az'),
(3, 'Испания', 'ru'),
(4, 'İngiltərə', 'az'),
(4, 'Англия', 'ru'),
(5, 'Rusiya', 'az'),
(5, 'Россия', 'ru'),
(6, 'İtaliya', 'az'),
(6, 'Италия', 'ru'),
(7, 'Fransa', 'az'),
(7, 'Франция', 'ru'),
(8, 'Almaniya', 'az'),
(8, 'Германия', 'ru'),
(9, 'Arqentina', 'az'),
(9, 'Аргентина', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `elements`
--

CREATE TABLE `elements` (
  `id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `elements`
--

INSERT INTO `elements` (`id`, `stage_id`, `priority`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 2, 4),
(10, 2, 5),
(11, 2, 6),
(12, 3, 1),
(13, 3, 2),
(14, 3, 9),
(15, 3, 10),
(16, 4, 1),
(17, 4, 2),
(18, 4, 3),
(19, 4, 4),
(20, 4, 5),
(21, 4, 6),
(22, 4, 7),
(23, 4, 8),
(24, 4, 9),
(25, 4, 10),
(26, 4, 11),
(27, 4, 12),
(28, 4, 13),
(29, 4, 14),
(30, 4, 15),
(31, 4, 16),
(32, 4, 17),
(33, 4, 18),
(34, 4, 19),
(35, 4, 20),
(36, 4, 21),
(37, 4, 22),
(38, 4, 23),
(39, 4, 24),
(40, 4, 25),
(41, 4, 26),
(42, 4, 27),
(43, 4, 28),
(44, 4, 29),
(45, 4, 30),
(46, 4, 31),
(47, 4, 32),
(48, 4, 33),
(49, 4, 34),
(50, 4, 35),
(51, 4, 36),
(52, 4, 37),
(53, 4, 38);

-- --------------------------------------------------------

--
-- Структура таблицы `element_translations`
--

CREATE TABLE `element_translations` (
  `element_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `element_translations`
--

INSERT INTO `element_translations` (`element_id`, `name`, `locale`) VALUES
(1, 'İlkin mərhələ', 'az'),
(1, 'Предварительный раунд', 'ru'),
(1, 'Preliminary round', 'en'),
(2, 'Birinci təsnifat mərhələsi', 'az'),
(2, 'Первый раунд', 'ru'),
(2, 'First qualifying round', 'en'),
(3, 'İkinci təsnifat mərhələsi', 'az'),
(3, 'Второй раунд', 'ru'),
(3, 'Second qualifying round', 'en'),
(4, 'Üçüncü təsnifat mərhələsi', 'az'),
(4, 'Третий раунд', 'ru'),
(4, 'Third qualifying round', 'en'),
(5, 'Play-off turu', 'az'),
(5, 'Раунд плей-офф', 'ru'),
(5, 'Play-off round', 'en'),
(6, '1-ci tur', 'az'),
(6, 'Тур 1', 'ru'),
(6, 'Matchday 1', 'en'),
(7, '2-ci tur', 'az'),
(7, 'Тур 2', 'ru'),
(7, 'Matchday 2', 'en'),
(8, '3-cü tur', 'az'),
(8, 'Тур 3', 'ru'),
(8, 'Matchday 3', 'en'),
(9, '4-cü tur', 'az'),
(9, 'Тур 4', 'ru'),
(9, 'Matchday 4', 'en'),
(10, '5-ci tur', 'az'),
(10, 'Тур 5', 'ru'),
(10, 'Matchday 5', 'en'),
(11, '6-cı tur', 'az'),
(11, 'Тур 6', 'ru'),
(11, 'Matchday 6', 'en'),
(12, '1/8 Final', 'az'),
(12, '1/8 финала', 'ru'),
(12, 'Round of 16', 'en'),
(13, '1/4 Final', 'az'),
(13, 'Четвертьфинал', 'ru'),
(13, 'Quarter-final', 'en'),
(14, 'Yarımfinal', 'az'),
(14, 'Полуфиналы', 'ru'),
(14, 'Semi-final', 'en'),
(15, 'Final', 'az'),
(15, 'Финал', 'ru'),
(15, 'Final', 'en'),
(16, 'Tur 1', 'az'),
(16, 'Тур 1', 'ru'),
(16, 'Tour 1', 'en'),
(17, 'Tur 2', 'az'),
(17, 'Тур 2', 'ru'),
(17, 'Tour 2', 'en'),
(18, 'Tur 3', 'az'),
(18, 'Тур 3', 'ru'),
(18, 'Tour 3', 'en'),
(19, 'Tur 4', 'az'),
(19, 'Тур 4', 'ru'),
(19, 'Tour 4', 'en'),
(20, 'Tur 5', 'az'),
(20, 'Тур 5', 'ru'),
(20, 'Tour 5', 'en'),
(21, 'Tur 6', 'az'),
(21, 'Тур 6', 'ru'),
(21, 'Tour 6', 'en'),
(22, 'Tur 7', 'az'),
(22, 'Тур 7', 'ru'),
(22, 'Tour 7', 'en'),
(23, 'Tur 8', 'az'),
(23, 'Тур 8', 'ru'),
(23, 'Tour 8', 'en'),
(24, 'Tur 9', 'az'),
(24, 'Тур 9', 'ru'),
(24, 'Tour 9', 'en'),
(25, 'Tur 10', 'az'),
(25, 'Тур 10', 'ru'),
(25, 'Tour 10', 'en'),
(26, 'Tur 11', 'az'),
(26, 'Тур 11', 'ru'),
(26, 'Tour 11', 'en'),
(27, 'Tur 12', 'az'),
(27, 'Тур 12', 'ru'),
(27, 'Tour 12', 'en'),
(28, 'Tur 13', 'az'),
(28, 'Тур 13', 'ru'),
(28, 'Tour 13', 'en'),
(29, 'Tur 14', 'az'),
(29, 'Тур 14', 'ru'),
(29, 'Tour 14', 'en'),
(30, 'Tur 15', 'az'),
(30, 'Тур 15', 'ru'),
(30, 'Tour 15', 'en'),
(31, 'Tur 16', 'az'),
(31, 'Тур 16', 'ru'),
(31, 'Tour 16', 'en'),
(32, 'Tur 17', 'az'),
(32, 'Тур 17', 'ru'),
(32, 'Tour 17', 'en'),
(33, 'Tur 18', 'az'),
(33, 'Тур 18', 'ru'),
(33, 'Tour 18', 'en'),
(34, 'Tur 19', 'az'),
(34, 'Тур 19', 'ru'),
(34, 'Tour 19', 'en'),
(35, 'Tur 20', 'az'),
(35, 'Тур 20', 'ru'),
(35, 'Tour 20', 'en'),
(36, 'Tur 21', 'az'),
(36, 'Тур 21', 'ru'),
(36, 'Tour 21', 'en'),
(37, 'Tur 22', 'az'),
(37, 'Тур 22', 'ru'),
(37, 'Tour 22', 'en'),
(38, 'Tur 23', 'az'),
(38, 'Тур 23', 'ru'),
(38, 'Tour 23', 'en'),
(39, 'Tur 24', 'az'),
(39, 'Тур 24', 'ru'),
(39, 'Tour 24', 'en'),
(40, 'Tur 25', 'az'),
(40, 'Тур 25', 'ru'),
(40, 'Tour 25', 'en'),
(41, 'Tur 26', 'az'),
(41, 'Тур 26', 'ru'),
(41, 'Tour 26', 'en'),
(42, 'Tur 27', 'az'),
(42, 'Тур 27', 'ru'),
(42, 'Tour 27', 'en'),
(43, 'Tur 28', 'az'),
(43, 'Тур 28', 'ru'),
(43, 'Tour 28', 'en'),
(44, 'Tur 29', 'az'),
(44, 'Тур 29', 'ru'),
(44, 'Tour 29', 'en'),
(45, 'Tur 30', 'az'),
(45, 'Тур 30', 'ru'),
(45, 'Tour 30', 'en'),
(46, 'Tur 31', 'az'),
(46, 'Тур 31', 'ru'),
(46, 'Tour 31', 'en'),
(47, 'Tur 32', 'az'),
(47, 'Тур 32', 'ru'),
(47, 'Tour 32', 'en'),
(48, 'Tur 33', 'az'),
(48, 'Тур 33', 'ru'),
(48, 'Tour 33', 'en'),
(49, 'Tur 34', 'az'),
(49, 'Тур 34', 'ru'),
(49, 'Tour 34', 'en'),
(50, 'Tur 35', 'az'),
(50, 'Тур 35', 'ru'),
(50, 'Tour 35', 'en'),
(51, 'Tur 36', 'az'),
(51, 'Тур 36', 'ru'),
(51, 'Tour 36', 'en'),
(52, 'Tur 37', 'az'),
(52, 'Тур 37', 'ru'),
(52, 'Tour 37', 'en'),
(53, 'Tur 38', 'az'),
(53, 'Tour 38', 'en'),
(53, 'Тур 38', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `priority`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(10, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `group_translations`
--

CREATE TABLE `group_translations` (
  `group_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `group_translations`
--

INSERT INTO `group_translations` (`group_id`, `name`, `locale`) VALUES
(1, 'Qrup A', 'az'),
(1, 'Группа A', 'ru'),
(1, 'Group A', 'en'),
(2, 'Qrup B', 'az'),
(2, 'Группа B', 'ru'),
(2, 'Group B', 'en'),
(4, 'Qrup D', 'az'),
(4, 'Группа D', 'ru'),
(4, 'Group D', 'en'),
(5, 'Qrup E', 'az'),
(5, 'Группа E', 'ru'),
(5, 'Group E', 'en'),
(6, 'Qrup F', 'az'),
(6, 'Группа F', 'ru'),
(6, 'Group F', 'en'),
(7, 'Qrup G', 'az'),
(7, 'Группа G', 'ru'),
(7, 'Group G', 'en'),
(8, 'Qrup H', 'az'),
(8, 'Группа H', 'ru'),
(8, 'Group H', 'en'),
(3, 'Qrup C', 'az'),
(3, 'Группа C', 'ru'),
(3, 'Group C', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `locale` varchar(2) NOT NULL,
  `image_admin` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `languages`
--

INSERT INTO `languages` (`id`, `locale`, `image_admin`) VALUES
(1, 'az', 'az.png'),
(2, 'ru', 'ru.png');

-- --------------------------------------------------------

--
-- Структура таблицы `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `stadium_id` int(11) NOT NULL,
  `season_id` int(11) DEFAULT NULL,
  `seasons__element_id` int(11) DEFAULT NULL,
  `seasons__stages__group_id` int(11) DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `home_team_id` int(11) NOT NULL,
  `guest_team_id` int(11) NOT NULL,
  `home_team_goals_count` int(3) NOT NULL,
  `guest_team_goals_count` int(3) NOT NULL,
  `preview_image` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '{''not_started'': 0, ''ended'': 1, ''goes'': 2, ''canceled'': 3, ''postponed'': 4 }',
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `matches`
--

INSERT INTO `matches` (`id`, `stadium_id`, `season_id`, `seasons__element_id`, `seasons__stages__group_id`, `start_time`, `home_team_id`, `guest_team_id`, `home_team_goals_count`, `guest_team_goals_count`, `preview_image`, `status`, `is_active`) VALUES
(1, 1, 3, 31, NULL, '2020-05-29 23:00:00', 1, 2, 2, 1, 'video_1.png', 1, 1),
(2, 3, 9, 6, 1, '2020-04-20 21:00:00', 3, 4, 1, 1, 'video_2.png', 0, 1),
(3, 3, 9, 6, 1, '2020-04-20 21:00:00', 3, 4, 1, 1, 'video_2.png', 0, 1),
(4, 3, 9, 6, 1, '2020-04-20 21:00:00', 3, 4, 1, 1, 'video_2.png', 0, 1),
(5, 9, 9, 6, 1, '2020-04-20 21:00:00', 5, 6, 2, 2, 'video_5.png', 1, 1),
(6, 2, NULL, NULL, NULL, '2020-04-20 21:00:00', 4, 2, 2, 0, 'video_6.png', 1, 1),
(7, 3, 9, 1, NULL, '2020-04-20 21:00:00', 4, 5, 1, 1, 'video_2.png', 1, 1),
(8, 5, NULL, NULL, NULL, '2020-04-20 21:00:00', 1, 4, 1, 0, 'video_3.png', 1, 1),
(9, 7, 9, 7, 1, '2020-04-20 21:00:00', 3, 5, 0, 3, 'video_4.png', 2, 1),
(10, 5, 9, 1, NULL, '2020-04-20 21:00:00', 4, 5, 1, 0, 'video_3.png', 1, 1),
(11, 3, 9, 3, NULL, '2020-04-20 21:00:00', 5, 9, 1, 3, 'video_2.png', 1, 1),
(12, 3, 9, 2, NULL, '2020-04-20 21:00:00', 3, 8, 2, 1, 'video_2.png', 1, 1),
(16, 5, 9, 10, 1, '2020-05-04 23:33:57', 6, 4, 1, 2, 'video_5.png', 1, 1),
(18, 6, 9, 3, NULL, '2020-05-03 16:04:58', 4, 8, 1, 1, 'video_6.png', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `season_id` int(11) DEFAULT NULL,
  `image` varchar(200) NOT NULL,
  `creating_date` datetime NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `is_in_slider` tinyint(1) NOT NULL DEFAULT '0',
  `is_in_right` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `tournament_id`, `season_id`, `image`, `creating_date`, `priority`, `is_in_slider`, `is_in_right`, `is_active`) VALUES
(1, 1, 1, 'additional_news_1.png', '2020-03-22 02:14:28', 0, 1, 0, 1),
(2, 2, 2, 'additional_news_1.png', '2020-03-12 02:14:28', 0, 0, 1, 1),
(3, 3, 3, 'additional_news_2.png', '2020-03-22 02:14:28', 0, 0, 0, 1),
(4, 4, 4, 'additional_news_3.png', '2020-03-15 02:14:28', 0, 1, 0, 1),
(5, 6, 5, 'additional_news_1.png', '2020-03-22 02:14:28', 0, 0, 1, 1),
(6, 5, NULL, 'additional_news_2.png', '2020-03-15 02:14:28', 0, 1, 0, 1),
(7, 7, NULL, 'additional_news_2.png', '2020-03-12 02:14:28', 0, 0, 1, 1),
(8, 9, 9, 'additional_news_3.png', '2020-03-19 02:14:28', 0, 0, 1, 1),
(9, 9, 9, 'additional_news_3.png', '2020-03-22 02:14:28', 0, 0, 0, 1),
(10, NULL, NULL, 'additional_news_2.png', '2020-03-15 02:14:28', 0, 0, 0, 1),
(11, NULL, NULL, 'additional_news_2.png', '2020-03-10 05:11:00', 0, 0, 0, 1),
(12, NULL, NULL, 'additional_news_3.png', '2020-03-17 02:15:00', 0, 0, 0, 1),
(13, NULL, NULL, 'additional_news_1.png', '2020-05-26 15:09:29', 0, 0, 0, 1),
(16, NULL, NULL, '20200607174009img119.jpg', '2020-06-16 17:40:06', 0, 0, 0, 1),
(18, NULL, NULL, '20200613211412facebook.png', '2020-06-24 00:58:55', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `news_item_translations`
--

CREATE TABLE `news_item_translations` (
  `id` int(11) NOT NULL,
  `news_item_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `short_text` text NOT NULL,
  `large_text` text NOT NULL,
  `slug` varchar(100) NOT NULL,
  `views_count` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news_item_translations`
--

INSERT INTO `news_item_translations` (`id`, `news_item_id`, `title`, `short_text`, `large_text`, `slug`, `views_count`, `locale`) VALUES
(1, 2, 'Topaz Premyer Liqasında 20-ci turun təyinatları dərc edilib', 'Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək.', '<p>Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək. Turun oyunları 13-14 mart tarixlərində keçiriləcək.\r\n\r\nTəyinatlar belədir :\r\n\r\n13 mart \r\n\r\n“Keşlə” – “Qəbələ”\r\nHakimlər: Ömər Paşayev, Namiq Hüseynov, Mübariz Haşımov, Elvin Əsgərov \r\n“İnter Arena”, 15:30 \r\n\r\n14 mart \r\n\r\n“Sumqayıt” – “Kəpəz”\r\nHakimlər: Əliyar Ağayev, Rza Məmmədov, Elşən Acalov, Ramil Diniyev \r\n“Kapital Bank Arena”, 20:00 \r\n\r\n“Neftçi” – “Qarabağ”\r\nHakimlər: İnqilab Məmmədov, Vaqif Musayev, Akif Əmirəli, Rəhim Həsənov \r\n“Bakcell Arena”, 18:00 \r\n\r\n“Zirə” – “Səbail”\r\nHakimlər: Orxan Məmmədov, Vüqar Bayramov, Rahil Ramazanov, Rauf Cabbarov \r\nZirə İdman Kompleksinin stadionu, 17:00</p>', 'bu-gun-topaz-1', 1, 'az'),
(2, 2, 'ru Topaz Premyer Liqasında 20-ci turun təyinatları dərc edilib', 'Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək.', '<p>ru Turun mərkəzi görüşü olan “Neftçi” – “Qarabağ” oyununu İnqilab Məmmədov idarə edəcək. Turun oyunları 13-14 mart tarixlərində keçiriləcək.\r\n\r\nTəyinatlar belədir :\r\n\r\n13 mart \r\n\r\n“Keşlə” – “Qəbələ”\r\nHakimlər: Ömər Paşayev, Namiq Hüseynov, Mübariz Haşımov, Elvin Əsgərov \r\n“İnter Arena”, 15:30 \r\n\r\n14 mart \r\n\r\n“Sumqayıt” – “Kəpəz”\r\nHakimlər: Əliyar Ağayev, Rza Məmmədov, Elşən Acalov, Ramil Diniyev \r\n“Kapital Bank Arena”, 20:00 \r\n\r\n“Neftçi” – “Qarabağ”\r\nHakimlər: İnqilab Məmmədov, Vaqif Musayev, Akif Əmirəli, Rəhim Həsənov \r\n“Bakcell Arena”, 18:00 \r\n\r\n“Zirə” – “Səbail”\r\nHakimlər: Orxan Məmmədov, Vüqar Bayramov, Rahil Ramazanov, Rauf Cabbarov \r\nZirə İdman Kompleksinin stadionu, 17:00</p>', 'bu-gun-topaz-2', 0, 'ru'),
(3, 3, 'Super Liq az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-3', 1, 'az'),
(4, 3, 'Super Liq ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-4', 0, 'ru'),
(5, 4, 'La Liq az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-5', 0, 'az'),
(6, 4, 'La Liq ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-6', 0, 'ru'),
(7, 5, 'England az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-7', 1, 'az'),
(8, 5, 'England ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-8', 0, 'ru'),
(9, 6, 'Russia az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-9', 0, 'az'),
(10, 6, 'Russia ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-10', 0, 'ru'),
(11, 7, 'Seria A az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-11', 1, 'az'),
(12, 7, 'Seria A ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-12', 0, 'ru'),
(13, 8, 'France az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-13', 0, 'az'),
(14, 8, 'France ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-14', 0, 'ru'),
(15, 9, 'Bundesliga az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-15', 1, 'az');
INSERT INTO `news_item_translations` (`id`, `news_item_id`, `title`, `short_text`, `large_text`, `slug`, `views_count`, `locale`) VALUES
(16, 9, 'Bundesliga ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-16', 0, 'ru'),
(17, 10, 'Millimiz 2 az Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-17', 0, 'az'),
(18, 10, 'Millimiz 2 ru Avropa Lİqasında QARABAĞ FUTBOL KLUBUNUN rəqİblərİ məlumdur', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-18', 0, 'ru'),
(19, 11, 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', '<p>TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR</p>', 'bu-gun-topaz-19', 0, 'az'),
(20, 11, 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', '<p>TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR</p>', 'bu-gun-topaz-20', 0, 'ru'),
(21, 12, 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', '<p>TOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ AZ AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR</p>', 'bu-gun-topaz-21', 0, 'az'),
(22, 12, 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', 'TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR', '<p>TOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDURTOPAZ RU AVROPA LİQASINDA QARABAĞ FUTBOL KLUBUNUN RƏQİBLƏRİ MƏLUMDUR</p>', 'bu-gun-topaz-22', 0, 'ru'),
(23, 1, 'Bu gün Topaz Premyer Liqasında 20-ci tura start veriləcək', 'az Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> az İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-23', 0, 'az'),
(24, 1, 'ru Bu gün Topaz Premyer Liqasında 20-ci tura start veriləcək', 'ru Bu gün İsveçrənin Nyon şəhərində, UEFA-nın mənzil-qərargahında UEFA Gənclər Liqası turnirinin 2018/2019-cu il mövsümünün püşkatması keçirilib.', '<p> ru İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>\r\n                                        <p>İtaliya \"İnter\"inin və Xorvatiya millisinin futbolçusu İvan Perişiç Almaniya təmsilçisi \"Bavariya\"ya keçməyə çox yaxındır.<br/>\r\n                                        \"Report\" \"Football Italia\"ya istinadən xəbər verir ki, Münhem klubu 30 yaşlı yarımmüdafiəçini icarəyə götürmək üçün klubu ilə razılıq əldə edib.</p>\r\n\r\n                                        <p>Almaniya çempionu mövsümün sonunda Perişiçi satın almaq hüququna da malikdir. \"Bavariya\"nın icarə haqqı üçün \"İnter\"ə 5 milyon avro ödəyəcəyi bildirilib. Münhenlilər mövsümün sonunda xorvatiyalı oyunçunu 20 milyon avroya heyətinə qata bilər.</p>\r\n\r\n                                        <p>Qeyd edək ki, İvan Perişiç 2015-ci ildən \"İnter\"in formasını geyinir. Xorvatiya Azərbaycan millisinin AVRO-2020-nin seçmə mərhələsindəki rəqiblərindən biridir. Zaqrebdəki “Maksimir” stadionunda baş tutan ilk turdakı oyun rəqibin 2:1 hesablı qələbəsi ilə yekunlaşıb. Seçmə mərhələnin VI turunda keçiriləcək Azərbaycan - Xorvatiya matçı sentyabrın 9-da saat 20:00-da \"Bakcell Arena\"da başlayacaq.&nbsp;</p>', 'bu-gun-topaz-24', 0, 'ru'),
(25, 13, 'Azərbaycanın Yunanıstandakı səfiri: “Hər bir vətəndaşımızla əlaqədəyik” - MÜSAHİBƏ', 'Koronavirus praktik olaraq bütün dünya dövlətlərinə təsir edib. Hər bir dövlət öz üsuluna uyğun olaraq pandemiya ilə mübarizə aparır. Hardasa kütləvi şəkildə əhalini testən keçiriblər, bəzi ölkələr ta', '<p>Koronavirus praktik olaraq bütün dünya dövlətlərinə təsir edib. Hər bir dövlət öz üsuluna uyğun olaraq pandemiya ilə mübarizə aparır. Hardasa kütləvi şəkildə əhalini testən keçiriblər, bəzi ölkələr tam karantin metodunu seçməyə qərar veriblər. Ümumiyyətlə, aydın məsələdir ki, uzun müddət təcrid rejimində qalmaq çətindir, çünki bu, iqtisadiyyat üçün mənfi nəticələrə səbəb olur.</p><figure class=\"image\"><img></figure><p><a href=\"https://media.az/politics/1067777703/my-zabotimsya-o-sootechestvennikah-mediaaz-beseduet-s-poslom-azerbaydzhana-v-grecii-i-albanii-anarom-guseynovym/?fbclid=IwAR1j2ius4XePEv8kjulJaWbL_ATTpLO0gmA4o2vQrc_te-lTIUlvQ6Tb4EM\">Media.Az</a> Azərbaycanın dünya dövlətlərində olan səfirləri ilə müsahibələrə davam edir. Bugünkü müsahib Azərbaycanın Yunanıstan və Albaniyadakı fövqəladə və səlahiyyətli səfiri Anar Hüseynovdur:</p><p>op[p[</p><p><strong>- Dünyada davam edən koronovirus pandemiyası fonunda Yunanıstanda və Albaniyada vəziyyət necədir və hökumət tərəfindən hansı tədbirlər həyata keçirilir?</strong></p>', 'bu-gun-topaz-25', 1, 'az'),
(26, 13, 'sdfds', 'dsfsdf', '<p>dsfsdf</p>', 'bu-gun-topaz-26', 1, 'ru'),
(34, 16, 'Azərbaycan idmanına itki üz verib', 'asdasd asd', '<p>as das da das das da sdasas d</p>', 'azerbaycan-idmanina-itki-uz-verib', 0, 'ru'),
(51, 18, 'asdasd', 'asdasd', '<h2>aasdasdasd</h2><h3>aasdasdasd</h3><h4>aasdasdasd</h4><p>asdasdas <strong>aasdasd </strong><i><strong>adasd</strong></i></p><p><a href=\"https://youtube.com\">https://youtube.com</a></p><p>&nbsp;</p>', 'asdasd', 16, 'az'),
(52, 18, 'asdasd 2222', 'asdasd 222', '<p>asdasd asd asdasd 222</p>', 'asdasd-2222', 2, 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `news_item_views`
--

CREATE TABLE `news_item_views` (
  `id` int(11) NOT NULL,
  `news_item_id` int(11) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news_item_views`
--

INSERT INTO `news_item_views` (`id`, `news_item_id`, `ip_address`, `created_at`) VALUES
(30, 25, '127.0.0.1', '2020-06-13 22:43:26'),
(31, 3, '127.0.0.1', '2020-06-13 22:44:35'),
(32, 49, '127.0.0.1', '2020-06-13 22:44:42'),
(33, 26, '127.0.0.1', '2020-06-13 22:44:52'),
(34, 51, '127.0.0.1', '2020-06-13 22:48:56'),
(35, 7, '127.0.0.1', '2020-06-13 23:27:46'),
(36, 11, '127.0.0.1', '2020-06-13 23:27:54'),
(37, 1, '127.0.0.1', '2020-06-13 23:37:31'),
(38, 15, '127.0.0.1', '2020-06-13 23:37:47');

-- --------------------------------------------------------

--
-- Структура таблицы `news__categories`
--

CREATE TABLE `news__categories` (
  `news_item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news__categories`
--

INSERT INTO `news__categories` (`news_item_id`, `category_id`) VALUES
(2, 2),
(3, 3),
(2, 1),
(1, 1),
(13, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `news__countries`
--

CREATE TABLE `news__countries` (
  `news_item_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news__countries`
--

INSERT INTO `news__countries` (`news_item_id`, `country_id`) VALUES
(2, 2),
(3, 3),
(4, 4),
(5, 6),
(6, 5),
(7, 7),
(8, 8),
(9, 2),
(10, 1),
(11, 3),
(12, 4),
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `news__teams`
--

CREATE TABLE `news__teams` (
  `news_item_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news__teams`
--

INSERT INTO `news__teams` (`news_item_id`, `team_id`) VALUES
(1, 1),
(13, 6),
(13, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `date_of_birthday` date NOT NULL,
  `height` int(3) DEFAULT NULL,
  `weight` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `players`
--

INSERT INTO `players` (`id`, `photo`, `country_id`, `position_id`, `date_of_birthday`, `height`, `weight`) VALUES
(1, 'Lionel_Messi_20180626.jpg', 9, 10, '1987-06-24', 170, 72),
(2, 'Lionel_Messi_20180626.jpg', 3, 10, '1991-06-19', 180, 78),
(3, 'Lionel_Messi_20180626.jpg', 6, 12, '1994-02-16', 185, 77),
(4, 'Lionel_Messi_20180626.jpg', 8, 6, '1989-09-13', 186, 75),
(5, 'Lionel_Messi_20180626.jpg', 7, 10, '1998-12-20', 178, 73),
(6, 'Lionel_Messi_20180626.jpg', 1, 6, '1998-09-16', 170, 72),
(7, 'Lionel_Messi_20180626.jpg', 2, 10, '1987-08-30', 177, 76),
(8, 'Lionel_Messi_20180626.jpg', 4, 7, '1986-01-04', 176, 70),
(9, 'Lionel_Messi_20180626.jpg', 5, 10, '1988-08-22', 194, 88);

-- --------------------------------------------------------

--
-- Структура таблицы `players__teams`
--

CREATE TABLE `players__teams` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `number` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `players__teams`
--

INSERT INTO `players__teams` (`player_id`, `team_id`, `number`) VALUES
(1, 1, 58),
(2, 2, 69),
(3, 3, 25),
(4, 4, 36),
(5, 5, 14),
(6, 6, 86),
(7, 7, 32),
(8, 8, 14),
(9, 9, 52);

-- --------------------------------------------------------

--
-- Структура таблицы `player_translations`
--

CREATE TABLE `player_translations` (
  `player_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `player_translations`
--

INSERT INTO `player_translations` (`player_id`, `name`, `locale`) VALUES
(1, 'Lionel Messi', 'az'),
(1, 'Лионель Месси', 'ru'),
(1, 'Lionel Messi', 'en'),
(2, 'Xames Rodriqes', 'az'),
(2, 'Хамес Родригес', 'ru'),
(2, 'James David Rodríguez Rubio', 'en'),
(3, 'Federiko Bernardesçi', 'az'),
(3, 'Федерико Бернардески', 'ru'),
(3, 'Federico Bernardeschi', 'en'),
(4, 'Tomas Müller', 'az'),
(4, 'Томас Мюллер', 'ru'),
(4, 'Thomas Müller', 'en'),
(5, 'Kilian Mbappe', 'az'),
(5, 'Килиан Мбаппе', 'ru'),
(5, 'Kylian Mbappe', 'en'),
(6, 'Hacıağa Hacılı', 'az'),
(6, 'Гаджиага Гаджилы', 'ru'),
(6, 'Hajiaga Hajili', 'en'),
(7, 'Adem Büyük', 'az'),
(7, 'Адем Бююк', 'ru'),
(7, 'Adem Buyuk', 'en'),
(8, 'Jeyms Filip Milner', 'az'),
(8, 'Джеймс Филип Милнер', 'ru'),
(8, 'James Philip Milner', 'en'),
(9, 'Artyom Dzyuba', 'az'),
(9, 'Артем Дзюба', 'ru'),
(9, 'Artem Dzyuba', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `positions`
--

INSERT INTO `positions` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12);

-- --------------------------------------------------------

--
-- Структура таблицы `position_translations`
--

CREATE TABLE `position_translations` (
  `position_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_translations`
--

INSERT INTO `position_translations` (`position_id`, `name`, `locale`) VALUES
(1, 'Qapıçı', 'az'),
(1, 'Вратарь', 'ru'),
(1, 'Goalkeeper', 'en'),
(2, 'Mərkəzi müdafiəçi', 'az'),
(2, 'Центральный защитник', 'ru'),
(2, 'Center Back', 'en'),
(3, 'Cinah müdafiəçisi', 'az'),
(3, 'Фланговый защитник', 'ru'),
(3, 'Fullback', 'en'),
(4, 'Kənar müdafiəçi', 'az'),
(4, 'Крайний защитник', 'ru'),
(4, 'Wingback', 'en'),
(5, 'Azad müdafiəçi (libero)', 'az'),
(5, 'Свободный защитник (либеро)', 'ru'),
(5, 'Sweeper', 'en'),
(6, 'Mərkəzi yarımmüdafiəçi', 'az'),
(6, 'Центральный полузащитник', 'ru'),
(6, 'Centre midfield', 'en'),
(7, 'Mərkəzi müdafiə yarımmüdafiəçisi', 'az'),
(7, 'Центральный опорный полузащитник', 'ru'),
(7, 'Defensive midfield', 'en'),
(8, 'Hücum edən yarımmüdafiəçi', 'az'),
(8, 'Атакующий полузащитник', 'ru'),
(8, 'Attacking midfield', 'en'),
(9, 'Cinah yarımmüdafiəçi', 'az'),
(9, 'Фланговый полузащитник', 'ru'),
(9, 'Wide midfield', 'en'),
(10, 'Mərkəz hücümçusu', 'az'),
(10, 'Центральный нападающий', 'ru'),
(10, 'Centre forward', 'en'),
(11, 'İkinci hücümçu', 'az'),
(11, 'Второй нападающий', 'ru'),
(11, 'Second striker', 'en'),
(12, 'Vinger', 'az'),
(12, 'Вингер', 'ru'),
(12, 'Winger', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `seasons`
--

CREATE TABLE `seasons` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `start_year` int(4) NOT NULL,
  `end_year` int(4) DEFAULT NULL,
  `is_in_home` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seasons`
--

INSERT INTO `seasons` (`id`, `tournament_id`, `start_year`, `end_year`, `is_in_home`) VALUES
(1, 1, 2019, 2020, 0),
(2, 2, 2019, 2020, NULL),
(3, 3, 2019, 2020, NULL),
(4, 4, 2019, 2020, NULL),
(5, 5, 2019, 2020, NULL),
(6, 6, 2019, 2020, NULL),
(7, 7, 2019, 2020, NULL),
(8, 8, 2019, 2020, NULL),
(9, 9, 2018, 2019, 1),
(10, 9, 2019, 2020, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `seasons__elements`
--

CREATE TABLE `seasons__elements` (
  `id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seasons__elements`
--

INSERT INTO `seasons__elements` (`id`, `season_id`, `element_id`) VALUES
(1, 9, 1),
(2, 9, 2),
(3, 9, 3),
(4, 9, 4),
(5, 9, 5),
(6, 9, 6),
(7, 9, 7),
(8, 9, 8),
(9, 9, 9),
(10, 9, 10),
(11, 9, 11),
(12, 9, 12),
(13, 9, 13),
(14, 9, 14),
(15, 9, 15),
(16, 10, 1),
(17, 10, 2),
(18, 10, 3),
(19, 10, 4),
(20, 10, 5),
(21, 10, 6),
(22, 10, 7),
(23, 10, 8),
(24, 10, 9),
(25, 10, 10),
(26, 10, 11),
(27, 10, 12),
(28, 10, 13),
(29, 10, 14),
(30, 10, 15),
(31, 3, 16),
(32, 3, 17);

-- --------------------------------------------------------

--
-- Структура таблицы `seasons__stages__groups`
--

CREATE TABLE `seasons__stages__groups` (
  `id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `stages__group_id` int(11) NOT NULL,
  `draw_points` double(10,1) NOT NULL DEFAULT '1.0',
  `victory_points` double(10,1) NOT NULL DEFAULT '3.0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seasons__stages__groups`
--

INSERT INTO `seasons__stages__groups` (`id`, `season_id`, `stages__group_id`, `draw_points`, `victory_points`) VALUES
(1, 9, 1, 1.0, 3.0),
(2, 9, 2, 1.0, 3.0);

-- --------------------------------------------------------

--
-- Структура таблицы `seasons__stages__groups__teams`
--

CREATE TABLE `seasons__stages__groups__teams` (
  `seasons__stages__group_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seasons__stages__groups__teams`
--

INSERT INTO `seasons__stages__groups__teams` (`seasons__stages__group_id`, `team_id`) VALUES
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(2, 7),
(2, 8),
(2, 9);

-- --------------------------------------------------------

--
-- Структура таблицы `seasons__teams`
--

CREATE TABLE `seasons__teams` (
  `season_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seasons__teams`
--

INSERT INTO `seasons__teams` (`season_id`, `team_id`) VALUES
(2, 7),
(3, 1),
(3, 2),
(4, 8),
(5, 9),
(6, 3),
(7, 5),
(8, 4),
(9, 1),
(9, 3),
(9, 4),
(9, 5),
(9, 6),
(9, 7),
(9, 8),
(9, 9),
(1, 6),
(1, 5),
(1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `stadiums`
--

CREATE TABLE `stadiums` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `size` int(10) DEFAULT NULL,
  `games_count` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stadiums`
--

INSERT INTO `stadiums` (`id`, `city_id`, `size`, `games_count`) VALUES
(1, 1, 99354, 0),
(2, 2, 81044, 0),
(3, 3, 41507, 0),
(4, 4, 75000, 0),
(5, 5, 47929, 0),
(6, 6, 5800, 0),
(7, 7, 52223, 0),
(8, 8, 54074, 0),
(9, 9, 67800, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `stadium_translations`
--

CREATE TABLE `stadium_translations` (
  `stadium_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stadium_translations`
--

INSERT INTO `stadium_translations` (`stadium_id`, `name`, `locale`) VALUES
(1, 'Kemp Nou', 'az'),
(1, 'Кемп Ноу', 'ru'),
(1, 'Camp Nou', 'en'),
(2, 'Santyaqo Bernabeu', 'az'),
(2, 'Сантьяго Бернабеу', 'ru'),
(2, 'Santiago Bernabeu', 'en'),
(3, 'Yuventus Stadionu', 'az'),
(3, 'Альянц Стадиум', 'ru'),
(3, 'Juventus Stadium', 'en'),
(4, 'Allianz Arena', 'az'),
(4, 'Альянц Арена', 'ru'),
(4, 'Allianz Arena', 'en'),
(5, 'Prensler Parkı', 'az'),
(5, 'Парк де Пренс', 'ru'),
(5, 'Parc des Princes', 'en'),
(6, 'Azərsun Arena', 'az'),
(6, 'Азерсун Арена', 'ru'),
(6, 'Azersun Arena', 'en'),
(7, 'Türk Telekom Stadionu', 'az'),
(7, 'Тюрк Телеком Арена', 'ru'),
(7, 'Türk Telekom Stadium', 'en'),
(8, 'Enfild', 'az'),
(8, 'Anfield', 'en'),
(8, 'Энфилд', 'ru'),
(9, 'Qazprom Arena', 'az'),
(9, 'Газпром Арена', 'ru'),
(9, 'Gazprom Arena', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `stages`
--

CREATE TABLE `stages` (
  `id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `has_groups` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stages`
--

INSERT INTO `stages` (`id`, `priority`, `has_groups`) VALUES
(1, 0, 0),
(2, 1, 1),
(3, 2, 0),
(4, 0, 0),
(5, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `stages__groups`
--

CREATE TABLE `stages__groups` (
  `id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stages__groups`
--

INSERT INTO `stages__groups` (`id`, `stage_id`, `group_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 2, 4),
(5, 2, 5),
(6, 2, 6),
(7, 2, 7),
(8, 2, 8),
(9, 5, 1),
(10, 5, 2),
(11, 5, 3),
(12, 5, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `stage_translations`
--

CREATE TABLE `stage_translations` (
  `stage_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stage_translations`
--

INSERT INTO `stage_translations` (`stage_id`, `name`, `locale`) VALUES
(1, 'Təsnifat mərhələsi', 'az'),
(1, 'Квалификация', 'ru'),
(1, 'Qualifying', 'en'),
(2, 'Qrup mərhələsi', 'az'),
(2, 'Групповой этап', 'ru'),
(2, 'Group stage', 'en'),
(3, 'Play-off', 'az'),
(3, 'Плей-офф', 'ru'),
(3, 'Play-off', 'en'),
(4, 'Liqa', 'az'),
(4, 'Лига', 'ru'),
(4, 'League', 'en'),
(5, 'Qrup 2', 'az'),
(5, 'Qrup 2', 'ru'),
(5, 'Qrup 2', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `country_id` int(11) NOT NULL,
  `stadium_id` int(11) DEFAULT NULL,
  `coach_id` int(11) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `teams`
--

INSERT INTO `teams` (`id`, `slug`, `logo`, `country_id`, `stadium_id`, `coach_id`, `year`) VALUES
(1, 'barselona', 'FC_Barcelona.svg.png', 3, 1, 1, 1899),
(2, 'real-madrid', 'Real_Madrid_CF.svg.png', 3, 2, 2, 1947),
(3, 'yuventus', '302px-Juventus_FC_2017_logo.svg.png', 6, 3, 3, 1897),
(4, 'bayern-munchen', 'bayern-munchen-logo-68D0CB94C3-seeklogo.com.png', 8, 4, 4, 1900),
(5, 'paris-sen-jermen', 'e4dee7f6592fd16e05da224202626aec.png', 7, 5, 5, 1970),
(6, 'qarabag', 'fk-qarabag-azersun-agdam-logo-E33E2A743E-seeklogo.com.png', 1, 6, 6, 1951),
(7, 'galatasaray', 'Galatasaray_SK.svg', 2, 7, 7, 1905),
(8, 'liverpul', '1200px-FC_Liverpool.svg.png', 4, 8, 8, 1892),
(9, 'zenit', 'FC_Zenit_1_star_2015_logo.png', 5, 9, 9, 1925);

-- --------------------------------------------------------

--
-- Структура таблицы `team_translations`
--

CREATE TABLE `team_translations` (
  `team_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `team_translations`
--

INSERT INTO `team_translations` (`team_id`, `name`, `locale`) VALUES
(2, 'Real Madrid FK', 'az'),
(2, 'Реал Мадрид ФК', 'ru'),
(2, 'Real Madrid CF', 'en'),
(3, 'Yuventus FK', 'az'),
(3, 'ФК Ювентус', 'ru'),
(3, 'Juventus F.C.', 'en'),
(4, 'Bayern Münhen FK', 'az'),
(4, 'ФК Бавария', 'ru'),
(4, 'FC Bayern Munich', 'en'),
(5, 'Paris Sen-Jermen FK', 'az'),
(5, 'Пари Сен-Жермен', 'ru'),
(5, 'Paris Saint-Germain F.C.', 'en'),
(6, 'Qarabağ FK', 'az'),
(6, 'ФК Карабах', 'ru'),
(6, 'Qarabagh FC', 'en'),
(7, 'Galatasaray S.K.', 'az'),
(7, 'С.К. Галатасарай ', 'ru'),
(7, 'Galatasaray S.C.', 'en'),
(8, 'Liverpul F.K.', 'az'),
(8, 'Ф.К. Ливерпуль', 'ru'),
(8, 'Liverpool F.C.', 'en'),
(9, 'Zenit F.K. ', 'az'),
(9, 'Ф.К. Зенит ', 'ru'),
(9, 'Zenit F.C. ', 'en'),
(1, 'FK Barselona', 'az'),
(1, 'ФК Барселона', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `tournaments`
--

CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `tournaments_category_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `logo_mini` varchar(200) NOT NULL,
  `logo_big` varchar(200) NOT NULL,
  `is_news_category` tinyint(1) NOT NULL,
  `is_videos_category` tinyint(1) NOT NULL DEFAULT '0',
  `priority_for_country` int(11) NOT NULL DEFAULT '0',
  `is_league` tinyint(1) NOT NULL DEFAULT '0',
  `league_victory_points` double(10,1) DEFAULT NULL,
  `league_draw_points` double(10,1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tournaments`
--

INSERT INTO `tournaments` (`id`, `slug`, `tournaments_category_id`, `country_id`, `logo_mini`, `logo_big`, `is_news_category`, `is_videos_category`, `priority_for_country`, `is_league`, `league_victory_points`, `league_draw_points`) VALUES
(1, 'premyer-liqa', 1, 1, 'liga_topaz.png', 'liga_topaz_big.png', 0, 1, 1, 1, 3.0, 1.0),
(2, 'turkiye-super-liq', 1, 2, 'Turkcell_Super_League_logo_mini.png', 'Turkcell_Super_League_logo_big.png', 0, 1, 1, 1, 3.0, 1.0),
(3, 'la-liqa', 1, 3, 'liga_spain.png', 'la_league.png', 0, 1, 1, 1, 3.0, 1.0),
(4, 'ingiltere-premyer-liqasi', 1, 4, 'liga_england.png', 'England_Premier_League_Logo.svg.png', 0, 1, 1, 1, 3.0, 1.0),
(5, 'rusiya-premyer-liqasi', 1, 5, 'russian_premiere_league_mini.png', 'russian_premiere_league_big.png', 0, 1, 1, 1, 3.0, 1.0),
(6, 'a-seriyasi', 1, 6, 'seria_a_mini.png', 'seria_a_big.jpg', 0, 1, 1, 1, 3.0, 1.0),
(7, 'liqa-1', 1, 7, 'france_league_mini.png', 'france_league_big.png', 0, 1, 1, 1, 3.0, 1.0),
(8, 'bundesliqa', 1, 8, 'germany_liga_mini.png', 'germany_liga_big.png', 0, 1, 1, 1, 3.0, 1.0),
(9, 'chempionlar-liqasi', 2, 0, 'liga_champions.png', '800px-UEFA_Champions_League_logo.svg_big.png', 1, 1, 0, 1, 1.0, 1.0),
(10, 'avropa-liqasi', 2, 0, 'UEFA-Europa-League.png', 'uefa-league-vector-logo-400x400.png', 1, 1, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tournaments_categories`
--

CREATE TABLE `tournaments_categories` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tournaments_categories`
--

INSERT INTO `tournaments_categories` (`id`) VALUES
(1),
(2),
(3);

-- --------------------------------------------------------

--
-- Структура таблицы `tournaments_category_translations`
--

CREATE TABLE `tournaments_category_translations` (
  `tournaments_category_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tournaments_category_translations`
--

INSERT INTO `tournaments_category_translations` (`tournaments_category_id`, `name`, `locale`) VALUES
(1, 'Klub milli', 'az'),
(1, 'Клубные национальные', 'ru'),
(1, 'Club national', 'en'),
(2, 'Klub beynəlxalq', 'az'),
(2, 'Клубные международные', 'ru'),
(2, 'Club international', 'en'),
(3, 'Yığmalar', 'az'),
(3, 'Сборные', 'ru'),
(3, 'National', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `tournament_translations`
--

CREATE TABLE `tournament_translations` (
  `tournament_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `locale` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tournament_translations`
--

INSERT INTO `tournament_translations` (`tournament_id`, `name`, `locale`) VALUES
(1, 'Premyer Liqa', 'az'),
(1, 'Премьер Лига', 'ru'),
(2, 'Türkiye Süper Lig', 'az'),
(2, 'Турецкая Супер Лига', 'ru'),
(3, 'La liga', 'az'),
(3, 'Ла Лига', 'ru'),
(4, 'İngiltərə Premyer Liqası', 'az'),
(4, 'Английская Премьер Лига', 'ru'),
(5, 'Rusiya Premyer Liqası', 'az'),
(5, 'Российская Пресьер-Лига', 'ru'),
(6, 'A Seriyası', 'az'),
(6, 'Серия А', 'ru'),
(7, 'Liqa 1', 'az'),
(7, 'Лига 1', 'ru'),
(8, 'Bundesliqa', 'az'),
(8, 'Бундеслига', 'ru'),
(10, 'Avropa liqası', 'az'),
(10, 'Лига Европы', 'ru'),
(9, 'Çempionlar liqası', 'az'),
(9, 'Лига чемпионов', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `is_verified`) VALUES
(1, 'soccer-foot', '$2y$10$RlJbF8sGK9MDEeZKRR.kPuU/PMYyx5eH5rQ.vktrZPBdJ3Zf/txWK', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_verifications`
--

CREATE TABLE `user_verifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `minute` int(3) NOT NULL,
  `name` varchar(200) NOT NULL,
  `preview_image` varchar(200) NOT NULL,
  `is_top_goal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `videos`
--

INSERT INTO `videos` (`id`, `match_id`, `player_id`, `team_id`, `minute`, `name`, `preview_image`, `is_top_goal`) VALUES
(1, 1, 1, 1, 36, 'd2ccb44b4b8807919df130467b09e00a.mp4', 'video_1.png', 1),
(2, 1, 2, 2, 45, '0a857f3afe33d719eda34c721be4e45f.mp4', 'video_2.png', 1),
(3, 2, 3, 3, 43, 'a1b1eaf60b0173bc566cb84996832b3f.mp4', 'video_3.png', 1),
(4, 2, 4, 4, 12, 'ceeb0536759923c53bb3fb9ecfe23dbe.mp4', 'video_4.png', 1),
(5, 3, 5, 5, 90, 'a5ab3d85f7020698a21430eaf61e984b.mp4', 'video_5.png', 1),
(6, 3, 6, 6, 25, 'b6abc74bb04d9f5c7d3f8e1d397d88f2.mp4', 'video_6.png', 1),
(7, 4, 7, 7, 78, '668772fea891fd4da2f15e443789ac36.mp4', 'video_1.png', 1),
(8, 4, 8, 8, 67, '378dee2429fba641c42c3e0c20a3d416.mp4', 'video_2.png', 1),
(9, 5, 8, 8, 65, 'e45eb925c8acbe49b2b066d58d98b325.mp4', 'video_3.png', 1),
(24, 18, 8, 8, 3, 'ceeb0536759923c53bb3fb9ecfe23dbe.mp4', 'video_4.png', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `videos__categories`
--

CREATE TABLE `videos__categories` (
  `video_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `videos__categories`
--

INSERT INTO `videos__categories` (`video_id`, `category_id`) VALUES
(24, 2),
(1, 2),
(2, 2),
(3, 2),
(4, 1),
(5, 1),
(6, 1),
(7, 3),
(8, 3),
(9, 3);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `coaches`
--
ALTER TABLE `coaches`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news_item_translations`
--
ALTER TABLE `news_item_translations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news_item_views`
--
ALTER TABLE `news_item_views`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `seasons__elements`
--
ALTER TABLE `seasons__elements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `seasons__stages__groups`
--
ALTER TABLE `seasons__stages__groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stadiums`
--
ALTER TABLE `stadiums`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stages__groups`
--
ALTER TABLE `stages__groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tournaments_categories`
--
ALTER TABLE `tournaments_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_verifications`
--
ALTER TABLE `user_verifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `coaches`
--
ALTER TABLE `coaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `elements`
--
ALTER TABLE `elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `news_item_translations`
--
ALTER TABLE `news_item_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT для таблицы `news_item_views`
--
ALTER TABLE `news_item_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `seasons`
--
ALTER TABLE `seasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `seasons__elements`
--
ALTER TABLE `seasons__elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `seasons__stages__groups`
--
ALTER TABLE `seasons__stages__groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `stadiums`
--
ALTER TABLE `stadiums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `stages`
--
ALTER TABLE `stages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `stages__groups`
--
ALTER TABLE `stages__groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `tournaments_categories`
--
ALTER TABLE `tournaments_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `user_verifications`
--
ALTER TABLE `user_verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
