<?php

return [

    "currencies" => [
      "0" => "AZN",
      "1" => "USD",
      "2" => "RUR",
      "3" => "EUR",
      "4" => "TRY",
    ],

    "people_research_count" => "10 000 +",
    "main_email" => "info@position.az",

    "call_phone_number" => "994558144191",
    "view_phone_number" => "055 814 41 91",

    "reCaptcha" => [
        "key" => "6LfSWqQUAAAAAMLtyzGewzyAfSILq6O_Z9Gftj2J",
        "secret_key" => "6LfSWqQUAAAAADtidOTZc0UYIZly6rHM6LaahHVN",

        "check_link" => "https://www.google.com/recaptcha/api/siteverify"
    ]

];
